<?php

namespace Aura\Controllers;

use adLDAP;
use Aura\Core\Controllers;
use Aura\Core\Core;
use Aura\Core\Database;
use Aura\Models\ActionModel;
use Aura\Models\CoreModel;
use Aura\Models\MonitorModel;
use Aura\Models\RequestModel;
use Aura\Models\TimetableModel;
use Aura\Models\TopicsModel;
use DateTime;

class Ajax extends Controllers
{

    public $layout = 'main';

    public function modalWindow()
    {

        $data = $_GET;

        $problems_list = TopicsModel::getProblemsList('FIFY3');
        $number = $data['number'];
        $info = $data['info'];
        if ($data['name'] == 'new_request') {
            $realSites = Database::select('FIFY3', "SELECT NUMBER, MAGAZ FROM MAGAZ ORDER BY NUMBER");
        } elseif ($data['name'] == 'comment' || $data['name'] == 'close_ticket' || $data['name'] == 'to_work') {
            $request = RequestModel::getFullTicketInfo('FIFY3', $data['number']);
        } elseif ($data['name'] == 'complain') {
            $action = ActionModel::getAction('FIFY3', $data['number'], $data['action_number'])[0];
        }

        require AURA_VIEWS_PATH . 'modal-window/' . $data['name'] . '.php';
    }

    public function checkTicket()
    {
        $request = RequestModel::getCurrentTicket('FIFY3', $_POST['name'])[0];
        if (!empty($request['NUMBER'])) {
            echo $request['NUMBER'];
        } else {
            echo 0;
        }
    }

    public function takeJobLV3()
    {
        return MonitorModel::takeJobLV3($_POST['ticket'], Core::getLogin());
    }

    public function takeFon()
    {
        Database::connect('FIFY_NEXT');
        $tsql_callSP = "{call TakeFon(?,?,?)}";
        $params = [];
        $answer = null;
        $params[] = [$_POST['ticket'], SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [&$answer, SQLSRV_PARAM_INOUT, SQLSRV_PHPTYPE_INT];
        $stmt = sqlsrv_query(DataBase::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt === false) {
            die(file_put_contents(
                AURA_DATA_PATH . 'logs/' . $this->name . '.error.log',
                json_encode(sqlsrv_errors()), FILE_APPEND)
            );
        }
        sqlsrv_free_stmt($stmt);
        print_r(json_encode($answer));
    }

    public function ADSearch()
    {
        $data = $_POST;
        $adldap = new adLDAP();
        $filter = [];
        $value = [];
        if (!empty($data['name'])) {
            $filter[] = 'samaccountname';
            $value[] = $data['name'];
        }
        if (!empty($data['mail'])) {
            $filter[] = 'mail';
            $value[] = $data['mail'];
        }
        if (!empty($data['displayname'])) {
            $filter[] = 'displayname';
            $value[] = $data['displayname'];
        }

        if (!empty($data['department'])) {
            $filter[] = 'department';
            $value[] = $data['department'];
        }
        if (!empty($data['dn'])) {
            $filter[] = 'dn';
            $value[] = $data['dn'];
        }
        $result = $adldap->user()->find(true, $filter, $value);
        foreach ($result as $key => $value) {
            if (empty($value[3])) {
                $result[$key][3] = '';
            }
            if (empty($value[2])) {
                $result[$key][2] = '';
            }
            if (empty($value[1])) {
                $result[$key][1] = '';
            }
        }
        print_r(json_encode($result));
    }

    public function emlInfo()
    {
        $ticket = $_POST['ticket'];
        $eml = RequestModel::getEmlInfo($ticket);
        $attachments = '';
        if (!empty($eml['attachments'])) {
            foreach ($eml['attachments'] as $key => $value) {
                if ($key != 0) {
                    $attachments .= ', ';
                }
                if (strpos($value->filename, '%?koi8-r?Q?') != false) {
                    $filename = explode('%?koi8-r?Q?', $value->filename)[1];
                    $filename = mb_convert_encoding($filename, 'UTF-8', 'KOI8-R');
                } else {
                    $filename = $value->filename;
                }
                $attachments .= '<a href="/manage/downloadMailAttachment?indent='
                    . $ticket . '&img=' . $key . '">' . $filename . '</a>';
            }
        }
        $eml['attachments'] = $attachments;

        print_r(json_encode($eml, JSON_UNESCAPED_UNICODE));
    }

    public function getFullInfo()
    {
        $ticket = $_POST['ticket'];
        $data = RequestModel::getFullTicketInfo('FIFY3', $ticket);
        print_r(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    public function ticketEvents()
    {
        $ticket = $_POST['ticket'];
        $mode = 0;
        @$mode = @$_POST['mode'];
        $event = ActionModel::getTicketsHistory('FIFY3', $ticket, $mode);
        print_r(json_encode($event, JSON_UNESCAPED_UNICODE));
    }

    public function search()
    {
        @$param = $_POST['search'];
        $search = RequestModel::searchTickets('FIFY3', trim($param));
        print_r(json_encode($search, JSON_UNESCAPED_UNICODE));
    }

    public function timetableUpdate()
    {
        TimetableModel::updateTimetable('FIFY3', $_POST);
    }

    public function news()
    {
        $news = Database::select(
            'FIFY3',
            "SELECT * 
             FROM NEWS 
             WHERE TITLE = 'Активные' 
             ORDER BY ID DESC"
        );
        print_r(json_encode($news));
    }

    public function history()
    {
        $history = Database::select(
            'FIFY_NEXT',
            "SELECT * 
             FROM HistoryToday(
                    (SELECT FIO 
                     FROM FIFY3.dbo.MONITOR2 
                     WHERE LOGIN='" . Core::getLogin() . "'))
               ORDER BY DATETIME DESC"
        );

        print_r(json_encode($history));
    }

    public function oldNews()
    {
        $news = Database::select(
            'FIFY3',
            "SELECT TOP(50) * 
              FROM NEWS 
              WHERE TITLE = 'Закрытые' 
              ORDER BY ID DESC"
        );
        print_r(json_encode($news));
    }

    public function freshNews()
    {
        $date = new DateTime($_POST['date']);
        $news = Database::select(
            'FIFY3',
            "SELECT COUNT(*) AS count 
             FROM NEWS 
             WHERE TITLE = 'Активные'
             AND DATETIME >= '" . $date->format('Ymd H:i:s') . "'"
        );
        print_r($news[0]['count']);
    }

    public function manageTicket()
    {
        $variables = $GLOBALS['variables'];
        if (!is_numeric($_POST['LV'])) {
            $realsite = $_POST['LV'];
            $_POST['LV'] = $variables['LV']['Мобильная группа']['value'];
        }
        if (empty($_POST['PHYSICAL'])) {
            $_POST['PHYSICAL'] = 0;
        }
        $query = 'UPDATE ACTIVE_TICKET SET CIM = \'' . $_POST['CIM'] . '\',PHYSICAL = \'' . $_POST['PHYSICAL'] . '\',
                  KERNEL_JOB_TIME_LIMIT = \'' . $_POST['KERNEL_JOB_TIME_LIMIT'] . '\', LV = \'' . $_POST['LV'] . '\'';
        if (!empty($realsite)) {
            $query .= ', REAL_SITE = \'' . $realsite . '\', SEE_NEW=0, GROUPE = \'' . $_POST['GROUPE'] . '\'';
        } elseif ($_POST['LV'] == $variables['LV']['Контактный центр']['value']) {
            $query .= ', GROUPE = \'Контактный Центр\'';
        } elseif ($_POST['LV'] == $variables['LV']['Центр компетенций']['value']) {
            $query .= ', GROUPE = \'Центр Компетенций\'';
        } elseif ($_POST['LV'] == $variables['LV']['Атак']['value']) {
            $query .= ', GROUPE = \'АТАК\'';
        }

        $query .= ' WHERE NUMBER = \'' . $_POST['ticket'] . '\'';

        Database::query('FIFY3', $query);
    }

    public function seeNew()
    {
        $query = "UPDATE ACTIVE_TICKET SET SEE_NEW = 1 WHERE REAL_SITE IN
                  (SELECT MAGAZ FROM MAGAZ WHERE GROUPE=
                    (SELECT GROUPE FROM MONITOR2 WHERE LOGIN='" . Core::getLogin() . '\'))
                  AND STATUS_JOB IN (1,3) AND LV=3';
        Database::query('FIFY3', $query);
    }

    public function newRequest()
    {
        $data = json_decode($_POST['data'], true);
        $result = RequestModel::newRequest('FIFY_NEXT', $data);
        print_r($result);
    }

    public function dinnerBreak()
    {
        CoreModel::dinnerBreak('FIFY3', $_POST['mode']);
    }

    public function sendCuration()
    {
        Database::connect('FIFY_NEXT');
        $tsql_callSP = '{call CurationNew(?)}'; //подготавливаем вызов процедуры
        $params = [];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            die(
            file_put_contents(
                AURA_BASE_PATH . 'daemons/logs/' . $this->name . '.error.log',
                json_encode(sqlsrv_errors()),
                FILE_APPEND
            )
            );
        }
        sqlsrv_free_stmt($stmt);
    }

    public function sendCurationSpool()
    {
        $variables = $GLOBALS['variables'];
        $data = json_decode($_POST['data'], 1);
        foreach ($data as $key => $value) {
            if (!is_numeric($value[0]['LV'])) {
                $REAL_SITE = $value[0]['LV'];
                $value[0]['LV'] = $variables['LV']['Мобильная группа']['value'] * 1;
            } else {
                $REAL_SITE = null;
            }
            if (empty($value[0]['PHYSICAL'])) {
                $value[0]['PHYSICAL'] = 0;
            }
            if (empty($value[1]['PHYSICAL'])) {
                $value[1]['PHYSICAL'] = 0;
            }
            if ($value[0]['LV'] == $variables['LV']['Центр компетенций']['value'] * 1) {
                $value[0]['GROUPE'] = 'Центр компетенций';
            } elseif ($value[0]['LV'] == $variables['LV']['Атак']['value'] * 1) {
                $value[0]['GROUPE'] = 'АТАК';
            } elseif ($value[0]['LV'] == $variables['LV']['Контактный центр']['value'] * 1) {
                $value[0]['GROUPE'] = 'Контактный центр';
            }
            Database::connect('FIFY_NEXT');
            $tsql_callSP = '{call CurationOld(?, ?, ?, ?, ?, ?, ?, ?)}'; //подготавливаем вызов процедуры
            $CIM = ($value[0]['CIM'] != $value[1]['CIM'] ? $value[0]['CIM'] : null);
            $LV = ($value[0]['LV'] * 1 != $value[1]['LV'] * 1 ? $value[0]['LV'] * 1 : null);
            $GROUPE = ($value[0]['GROUPE'] != $value[1]['GROUPE'] ? $value[0]['GROUPE'] : null);
            $KERNEL_JOB_TIME_LIMIT = ($value[0]['KERNEL_JOB_TIME_LIMIT'] != $value[1]['KERNEL_JOB_TIME_LIMIT'] ?
                $value[0]['KERNEL_JOB_TIME_LIMIT'] : null);
            $PHYSICAL = ($value[0]['PHYSICAL'] != $value[1]['PHYSICAL'] ? $value[0]['PHYSICAL'] : null);
            $params = [];
            $params[] = [$value[0]['NUMBER'], SQLSRV_PARAM_IN];
            $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
            $params[] = [$CIM, SQLSRV_PARAM_IN];
            $params[] = [$LV, SQLSRV_PARAM_IN];
            $params[] = [$KERNEL_JOB_TIME_LIMIT, SQLSRV_PARAM_IN];
            $params[] = [$GROUPE, SQLSRV_PARAM_IN];
            $params[] = [$PHYSICAL, SQLSRV_PARAM_IN];
            $params[] = [$REAL_SITE, SQLSRV_PARAM_IN];

            $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
            if ($stmt == false) {
                die(
                file_put_contents(
                    AURA_BASE_PATH . 'daemons/logs/curation.error.log',
                    json_encode(sqlsrv_errors()),
                    FILE_APPEND
                )
                );
            }
            sqlsrv_free_stmt($stmt);
        }
    }

    public function complaint()
    {
        Database::connect('FIFY_NEXT');
        $data = json_decode($_POST['data'], true);
        $tsql_callSP = '{call complaint(?, ?, ?, ?)}'; //подготавливаем вызов процедуры

        $params = [];
        $params[] = [$data[1]['value'], SQLSRV_PARAM_IN];
        $params[] = [$data[2]['value'], SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$data[0]['value'], SQLSRV_PARAM_IN];

        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            die(
            file_put_contents(
                AURA_BASE_PATH . 'daemons/logs/curation.error.log',
                json_encode(sqlsrv_errors()),
                FILE_APPEND
            )
            );
        }
        sqlsrv_free_stmt($stmt);
    }

    public function returnToWork()
    {
        Database::connect('FIFY_NEXT');
        $data = json_decode($_POST['data'], true);
        $tsql_callSP = '{call ReturnToWork(?, ?, ?)}'; //подготавливаем вызов процедуры

        $params = [];
        $params[] = [$data[1]['value'], SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$data[0]['value'], SQLSRV_PARAM_IN];

        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            die(
            file_put_contents(
                AURA_BASE_PATH . 'daemons/logs/curation.error.log',
                json_encode(sqlsrv_errors()),
                FILE_APPEND
            )
            );
        }
        sqlsrv_free_stmt($stmt);
    }

    public function comment()
    {
        Database::connect('FIFY_NEXT');
        $data = json_decode($_POST['data'], true);
        $tsql_callSP = '{call Comment(?, ?, ?)}'; //подготавливаем вызов процедуры

        $params = [];
        $params[] = [$data[1]['value'], SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$data[0]['value'], SQLSRV_PARAM_IN];

        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            exit(
            file_put_contents(
                AURA_BASE_PATH . 'daemons/logs/curation.error.log',
                json_encode(sqlsrv_errors()),
                FILE_APPEND
            )
            );
        }
        sqlsrv_free_stmt($stmt);
    }

    public function setReady()
    {
        MonitorModel::setReady($_POST['ready']);
    }

    public function userComment()
    {
        @$data = json_decode($_POST['data'], true);
        if (!empty($data[0]['value'])) {
            $_POST['number'] = $data[1]['value'];
            $_POST['comment'] = $data[0]['value'];
            $_POST['star1'] = $data[2]['value'];
            $_POST['star2'] = $data[3]['value'];
            $_POST['star3'] = $data[4]['value'];
            $_POST['star4'] = $data[5]['value'];
            $_POST['action'] = 4;

        }
        if (!empty($data[5]['value'])) {
            echo 'Ваше мнение очень важно для нас!';
        }
        RequestModel::userComment(
            $_POST['number'],
            Core::getLogin(),
            $_POST['comment'],
            $_POST['action'],
            $_POST['mark']
        );
    }

    public function redirectToWorker()
    {
        return RequestModel::redirectToWorker($_POST['number'], $_POST['login']);
    }

    public function changeTopic()
    {
        return RequestModel::changeTopic($_POST['number'], $_POST['problem_id'], $_POST['problem_name']);
    }
}
