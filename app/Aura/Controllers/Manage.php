<?php

namespace Aura\Controllers;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\AssetManager;
use Assetic\AssetWriter;
use Aura\Core\Controllers;
use Aura\Core\Core;
use Aura\Core\ProcessData;
use Aura\Core\Utils;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class Manage extends Controllers
{
    public function renderMailAttachment()
    {
        $data = $_GET;
        if (empty($data['action'])) {
            $data['action'] = '';
        }
        Core::renderMailAttachment($data['indent'], $data['img'], $data['action']);
    }

    public function downloadMailAttachment()
    {
        $data = $_GET;
        Core::downloadMailAttachment($data['indent'], $data['img']);
    }

    public function QKO()
    {
        $tab_number = Core::checkLogin();
        $this->layout = 'main';
        $this->title = 'QKO';
        $qko = [];
        if (file_exists(AURA_DATA_PATH . 'files/requests/qko.txt') &&
            (filectime(AURA_DATA_PATH . 'files/requests/qko.txt')
                > filectime(AURA_DATA_PATH . 'files/requests/QKO_Helpdesl.xls'))
        ) {
            $qko = json_decode(file_get_contents(AURA_DATA_PATH . 'files/requests/qko.txt'), 1);
        } else {
            $files = ['QKO_Helpdesl.xls', 'actions_helpdesk.xls'];
            $headers = [
                'Number', 'Group', 'Comments (Action)', "[Groupe de l'action Origine (test JD)]",
                'Support Person', 'Owner group of the ticket', 'Owner', 'Incident: Creation Date',
                'Priority', 'Incident: Available Field 1', 'Urgency', 'Impact', 'SLA', 'Support Person',
                'Real topic (Full)'];
            $topics = [];
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load(AURA_DATA_PATH . 'files/requests/Incident_Catalog.xlsx');
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $cols = [];
            $row = 1;
            for ($row = 2; $row <= $highestRow; ++$row) {
                $topic = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                $new_topic = $objWorksheet->
                    getCellByColumnAndRow(7, $row)->
                    getValue() . '/' . $objWorksheet->
                    getCellByColumnAndRow(8, $row)->getValue();
                $topics[str_replace('//', '/', str_replace('///', '/', $topic))] = $new_topic;
            }
            foreach ($topics as $key => $value) {
                unset($topics[$key]);
                $key = explode('/', $key);
                unset($key[0]);
                $key = implode('/', $key);
                $topics[$key] = $value;
            }

            foreach ($files as $key => $value) {
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load(AURA_DATA_PATH . 'files/requests/' . $value);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                $cols = [];
                $row = 1;
                for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                    $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                    if (in_array($value, $headers, true)) {
                        $cols[$value] = $col;
                    }
                }
                $filter = [];
                $file_num = $key;
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $number = $objWorksheet->getCellByColumnAndRow($cols['Number'], $row)->getValue();
                    foreach ($cols as $key => $value) {
                        if ($key == 'Comments (Action)' && !empty($qko[$number][$key])) {
                            continue;
                        }
                        $qko[$number][$key] = $objWorksheet->getCellByColumnAndRow($value, $row)->getValue();
                        if ($key == 'Real topic (Full)') {
                            $topic = explode('/', $qko[$number][$key]);
                            unset($topic[0], $topic[2], $topic[1]);
                            $topic = implode('/', $topic);
                            @$qko[$number][$key] = $topics[$topic . '/'];
                        } elseif ($key == 'Incident: Creation Date') {
                            $qko[$number][$key] = \date(
                                $format = 'Y-m-d',
                                PHPExcel_Shared_Date::ExcelToPHP(
                                    $qko[$number][$key]
                                )
                            );
                        }
                    }
                    if (@$qko[$number]['Group'] != 'RUS L2 - Helpdesk') {
                        $filter[] = $number;
                    }
                }
                foreach ($filter as $key => $value) {
                    unset($qko[$value]);
                }
            }
            file_put_contents(AURA_DATA_PATH . 'files/requests/qko.txt', json_encode($qko));
        }

        return $this->render('QKO', ['qko' => $qko]);
    }

    public function test()
    {
        echo ProcessData::translitirate('заявка.xlsx');
    }

    public  function dumpAssets()
    {
        //Подключаем файл с массивом путей к ассетам проекта
        require_once AURA_CONFIGS_PATH.'assets.conf.php';

        // Создаем менеджер всех ассетов
        $auraAssets = new AssetManager();

        //На основе массива $Assets создаем AssetCollection'ы
        // и добавляем в AssetManager

        foreach ($Assets as $typeOfAssets => $pathToAssetArray) {

            // определяем расширение итогового файла
            $extension = Utils::isinStr($typeOfAssets, 'css') ? 'css' : 'js';
            foreach ($pathToAssetArray as $path) {
                ${$typeOfAssets . 'FileAsset'}[] = new FileAsset($path);
            }

            ${$typeOfAssets} = new AssetCollection(${$typeOfAssets . 'FileAsset'});
            ${$typeOfAssets}->setTargetPath($extension . '/' . $typeOfAssets . '.' . $extension);
            $auraAssets->set($typeOfAssets, ${$typeOfAssets});
        }

        $assetWriter = new AssetWriter(AURA_PUBLIC_PATH);
        $assetWriter->writeManagerAssets($auraAssets);
        echo 'dump ok';
    }
}
