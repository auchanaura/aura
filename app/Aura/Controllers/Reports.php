<?php

namespace Aura\Controllers;

use Aura\Core\Controllers;
use Aura\Core\Core;
use Aura\Core\Database;
use Aura\Models\MonitorModel;

/**
 * Class Reports
 * @package Aura\Controllers
 */
class Reports extends Controllers
{

    public $layout = 'main';

    public function index()
    {
        $this->title = 'Отчеты';
        $report_list = DataBase::select('REPORTS', "SELECT * FROM REPORT_LIST");
        $sites = DataBase::select('FIFY3', 'SELECT NUMBER, MAGAZ, GROUPE FROM MAGAZ');
        $user = Core::getLogin();
        $monitor = MonitorModel::select('FIFY3', "SELECT * FROM MONITOR2 WHERE 
convert(VARCHAR,lv) LIKE CASE (SELECT admin FROM MONITOR2 WHERE login = '" . $user . "') WHEN 2 THEN '%%' ELSE (SELECT convert(VARCHAR,lv) FROM MONITOR2 WHERE login = '" . $user . "') END
AND	groupe LIKE CASE (SELECT admin FROM MONITOR2 WHERE login ='" . $user . "') WHEN 2 THEN '%%' 
				ELSE (SELECT CASE WHEN substring((SELECT groupe  FROM MONITOR2 WHERE login = '" . $user . "' ),0,3) = 'ЦП' THEN 'ЦП%' 
					ELSE groupe END FROM MONITOR2 WHERE login ='" . $user . "' ) END
AND login LIKE CASE WHEN (SELECT admin FROM MONITOR2 WHERE login ='" . $user . "')=0 THEN'" . $user . "' ELSE '%%' END 
ORDER BY CASE WHEN groupe LIKE (SELECT CASE WHEN substring((SELECT groupe  FROM MONITOR2 WHERE login = '" . $user . "' ),0,3) = 'ЦП' THEN 'ЦП%' ELSE groupe END  
FROM MONITOR2 WHERE login = '" . $user . "' ) THEN '0' ELSE groupe END");

        return $this->render('index', ['report_list' => $report_list, 'monitor' => $monitor, 'sites' => $sites]);
    }

    public function getReport()
    {
        $report_list = DataBase::select('REPORTS', "SELECT * FROM REPORT_LIST");
        if (!empty($_POST)) {
            $data = [];
            foreach ($report_list as $key => $value) {
                if ($value['ID'] == $_POST['report']) {
                    $report = $value;
                }
            }
            $fields = explode('"', $report['PARAMETRS']);
            if (!empty($_POST['data'])) {
                foreach ($_POST['data'] as $key => $value) {
                    $key = $key * 4 + (4 - 1);
                    if ($fields[$key] == 'date') {
                        $data[] = "'" . $value . "'";
                    } else {
                        if (is_array($value)) {
                            $value = implode(',', $value);
                        }
                        $data[] = "'" . $value . "'";
                    }
                }
                $data = implode(', ', $data);
            } else {
                $data = "";
            }
            $info = DataBase::select('REPORTS', "SELECT * FROM " . $report['LINK'] . "(" . $data . ")");

        }

        foreach ($info as $key => $value) {
            foreach ($value as $key2 => $value2) {
                if (mb_detect_encoding($key2) != 'ASCII') {
                    unset($info[$key][$key2]);
                    if (mb_detect_encoding($key2) == 'cp1251') {
                        $info[$key][iconv('cp1251', 'UTF-8//TRANSLIT', $key2)] = rtrim($value2);
                    }
                    if (mb_detect_encoding($key2) == 'UTF-8') {
                        $info[$key][iconv('UTF-8', 'UTF-8//TRANSLIT', $key2)] = rtrim($value2);
                    }

                } else {
                    unset($info[$key][$key2]);
                    $info[$key][$key2] = rtrim($value2);
                }
            }
        }
        print_r(json_encode($info, JSON_UNESCAPED_UNICODE));
    }

    public function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$this->utf8ize($k)] = $this->utf8ize($v);
            }
        } elseif (is_string($d)) {
            return utf8_encode($d);
        }

        return $d;
    }
}
