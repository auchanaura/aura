<?php

namespace Aura\Controllers;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\AssetManager;
use Assetic\AssetWriter;
use Aura\Core\Utils;
use Fenom;

/**
 * Created by PhpStorm.
 * User: gena-
 * Date: 20.01.2017
 * Time: 0:57
 */
class Test
{
    public static function testAction()
    {




        $fenom = static::getFenom();
        $fenom->display(
            'test.tpl',
            [
                'title' => 'Тестовая страница',
                'content' => 'CONTENT',
                'version' => AURA_VERSION
            ]
        );

    }



    /**
     * @return bool|Fenom
     */
    public static function getFenom()
    {

        //Запускаем Fenom и Возвращаем объект Fenom
        return Fenom::factory(
            AURA_FENOM_TEMLATE_PATH,
            AURA_CACHE_PATH . 'tpl/',
            AURA_FENOM_OPTIONS
        );
    }

    public static function img()
    {

        //header('Content-type: img/png');
        $imgsource = imagecreatefromjpeg(AURA_PUBLIC_PATH . 'img/gallery/2.jpg');
        $imgOutputPath = AURA_DATA_PATH . '1.png';
        if (!file_exists($imgOutputPath)) {
            imagepng($imgsource, $imgOutputPath, 5);
        }

        return '<img src="/data/1.png"';
    }
}
