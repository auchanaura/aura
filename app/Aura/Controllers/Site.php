<?php

namespace Aura\Controllers;

use Aes;
use Aura\Core\Controllers;
use Aura\Core\Core;
use Aura\Core\Database;
use Aura\Models\ActionModel;
use Aura\Models\CoreModel;
use Aura\Models\MonitorModel;
use Aura\Models\RequestModel;
use Aura\Models\TimetableModel;
use Aura\Models\TopicsModel;
use Katzgrau\KLogger\Logger;

class Site extends Controllers
{


    /**
     * Site constructor.
     * @internal param string $layout
     */
    public function __construct()
    {
        $this->layout = 'main';
    }

    public function login()
    {
        $this->layout = 'empty';
        $this->title = 'Авторизация';
        $error = 0;
        if (!empty($_POST)) {
            $auth = CoreModel::login('FIFY3', $_POST);
            if ($auth[0]) {
                if ($auth[1] == 1) {
                    return static::redirect('view_request');
                } elseif ($auth[1] == 2) {
                    return static::redirect('home');
                } elseif ($auth[1] == -2) {
                    return static::redirect('search');
                }
                $error = 2;// return $this->redirect('main');
            } else {
                $error = 1; // Выводим сообщение об ошибке авторизации
            }
        }

        return $this->render('login', ['error' => $error]);
    }

    public function logout()
    {
        Core::logout();

        return static::redirect('login');
    }

    public function home()
    {
        $this->layout = 'main_extended';
        $this->title = 'Список заявок';
        $requests = RequestModel::getUserTickets('FIFY3', Core::getLogin());

        return $this->render('home', ['requests' => $requests]);
    }

    public function newRequest()
    {
        $this->layout = 'main_extended';
        $this->title = 'Создание заявки';
        $problems_list = TopicsModel::getProblemsList('FIFY3');
        $id = '';
        if (!empty($_POST)) {
            $email = explode(',', $_POST['email'])[0];
            $fio = Aes::decrypt($_COOKIE['username'], COOKIE_PASS);
            $data = [
                ['name' => 'login', 'value' => Core::getLogin()],
                ['name' => 'fio', 'value' => $fio],
                ['name' => 'email', 'value' => $_POST['email']],
                ['name' => 'site', 'value' => $_POST['department']],
                ['name' => 'department', 'value' => $_POST['site']],
                ['name' => 'real_site', 'value' => $_POST['real_site']],
                ['name' => 'phone', 'value' => $_POST['phone']],
                ['name' => 'type', 'value' => 1],
                ['name' => 'ticket_title', 'value' => $_POST['subject']],
                ['name' => 'problem_type', 'value' => @$_POST['type']],
                ['name' => 'description', 'value' => $_POST['description']],
                ['name' => 'status', 'value' => 4],
                ['name' => 'cim', 'value' => 0],
                ['name' => 'phys', 'value' => 0]
            ];
            $id = RequestModel::newRequest('FIFY_NEXT', $data, 0);
            $files = [];
            foreach ($_FILES['attachments']['tmp_name'] as $key => $value) {
                $files[] = ['file' => $value, 'name' => $_FILES['attachments']['name'][$key]];
            }
            $data = [
                'from' => $fio . ' <' . $email . '>',
                'to' => 'Support Center IT <it@auchan.ru>',
                'subject' => '=?utf-8?Q?=D0=97=D0=B0=D1=8F=D0=B2=D0=BA=D0=B0.?=',
                'body' => $_POST['description'], 'files' => $files
            ];
            Core::createEml($data, $id);
        }

        return $this->render('newRequest', ['problems_list' => $problems_list, 'id' => $id]);
    }

    public function background()
    {
        $this->viewRequest(['mode' => 2]);
    }

    /**
     * @param array $params
     */
    public function viewRequest($params = [])
    {

        $logger = new Logger(AURA_LOGS_PATH.'/viewrequest.php.log');

        if ($_COOKIE['spec'] != 1) {
            return static::redirect('/login');
        }
        $tab_number = Core::checkLogin();

        if (!empty($params['id'])) {
            $id = $params['id'];
        }
        if (empty($params['mode'])) {
            $params['mode'] = 0;
        }

        $this->title = 'Заявка';
        $real_sites = Database::select('FIFY3', 'SELECT NUMBER, MAGAZ FROM MAGAZ');

        $request = RequestModel::getCurrentTicket('FIFY3', $tab_number, $params['mode']);
        $user = $request[1];
        $request = $request[0];
        $problems_list = TopicsModel::getProblemsList('FIFY3');
        if (!empty($_POST)) {
            if ($_POST['ticket_number'] == $request['NUMBER']) {
                RequestModel::updateRequest($request, $_POST, $problems_list[0]);

                return static::redirect('view_request');
            }
        }

        return $this->render('viewRequest', ['request' => $request, 'problems_list' => $problems_list, 'tab_number'
        => $tab_number, 'real_sites' => $real_sites, 'user' => $user]);
    }

    public function monitor()
    {
        if ($_COOKIE['spec'] != 1) {
            return static::redirect('/login');
        }
        $this->title = 'Монитор';
        $monitor = MonitorModel::getMonitorScheet();

        return $this->render('monitor', ['monitor' => $monitor]);
    }

    public function qualityImprovement()
    {
        $this->title = 'Отчет';
        $this->layout = 'main';
        $info = explode(
            PHP_EOL,
            iconv(
                'cp1251',
                'utf-8',
                file_get_contents(AURA_DATA_PATH . 'files/requests/Quality_improvement.csv')
            )
        );
        foreach ($info as $key => $value) {
            if (!empty($value)) {
                $value = \str_replace("\t\t", '', $value);
                $value = \str_replace("\t\"\t", "\"\t", $value);
                $info[$key] = explode("\t", $value);
            } else {
                unset($info[$key]);
            }
        }
        $info[0][4] = 'test jd';
        foreach ($info as $key => $value) {
            foreach ($value as $key2 => $value2) {
                if ($key != 0) {
                    $info[$key][$info[0][$key2]] = $value2;
                    unset($info[$key][$key2]);
                }
            }
        }
        $info = json_encode($info);

        return $this->render('qualityImprovement', ['info' => $info]);
    }

    public function curation($data = []) {
        $data=$_GET;
        if ($_COOKIE['spec'] != 1) {
            return static::redirect('/login');
        }
        if (empty($data['mode'])) {
            $data['mode'] = 0;
        }
        $tab_number = Core::checkLogin();

        $workers = MonitorModel::getRedirectWorkers();
        $this->title = 'Курирование';
        $real_sites = Database::select('FIFY3', 'SELECT NUMBER, MAGAZ, GROUPE FROM MAGAZ');
        $problems_list = TopicsModel::getProblemsList('FIFY3');
        $pool = RequestModel::getCurationPool('FIFY3', $data['mode']);

        return $this->render(
            'curation',
            [
                'pool' => $pool[0], 'count' => $pool[1], 'real_sites' => $real_sites,
                'user' => $pool[2], 'workers' => $workers, 'problems_list' => $problems_list
            ]
        );
    }

    public function search($data)
    {
        $data=$_GET;
        if ($_COOKIE['spec'] != 1 && $_COOKIE['spec'] != -2) {
            return static::redirect('/login');
        }
        $tab_number = Core::checkLogin();
        $this->title = 'Поиск';
        $real_sites = Database::select('FIFY3', 'SELECT NUMBER, MAGAZ FROM MAGAZ');
        $pool = [];
        $problems_list = [];
        $user = '';
        if (!empty($data['request'])) {
            $pool = RequestModel::searchTickets('FIFY3', trim($data['request']));
            $problems_list = TopicsModel::getProblemsList('FIFY3');
            $user = MonitorModel::getCurrentUser('FIFY3');
        }

        return $this->render(
            'search',
            [
                'pool' => $pool, 'real_sites' => $real_sites, 'user' => $user, 'problems_list' => $problems_list
            ]
        );
    }

    public function timetable()
    {
        $data=$_GET;
        $logger =new Logger('data/logs');
        $logger->debug($data);
        if ($_COOKIE['spec'] != 1) {
            return static::redirect('/login');
        }
        $tab_number = Core::checkLogin();
        $month = date('m');
        $year = date('Y');
        if (!empty($data['month'])) {
            $month = $data['month'];
        }
        if (!empty($data['year'])) {
            $year = $data['year'];
        }
        if (!empty($_POST)) {
            TimetableModel::createUser($_POST);
        }
        $this->layout = 'main';
        $this->title = 'Расписание отдела';
        $timetable = TimetableModel::getTimetable('FIFY3', $month,$year);

        return $this->render('timetable', ['timetable' => $timetable, 'month' => $month,'year'=>$year]);
    }

    public function demand()
    {
        $this->layout = 'empty';
        $this->title = 'Demand';

        return $this->render('demand');
    }

    public function appeal()
    {
        if ($_COOKIE['spec'] != 1) {
            return static::redirect('/login');
        }
        $tab_number = Core::checkLogin();
        $this->title = 'Обращения';
        $actions = ActionModel::getAppeals();

        return $this->render('appeal', ['actions' => $actions]);
    }

    public function config()
    {
        //TODO Убрать потом комментарии
        //if ($_COOKIE['spec'] != 1) {
        //    return $this->redirect('/login');
        //}

        if (!empty($params['id'])) $id = $params['id'];
        if (empty($params['mode'])) $params['mode'] = 0;
        $this->title = 'Администрирование';

        return $this->render('config');
    }
}


?>
