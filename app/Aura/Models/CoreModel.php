<?php

namespace Aura\Models;

use adLDAP;
use adLDAP2;
use Aura\Core\Core;
use Aura\Core\Database;

class CoreModel extends Database
{
    public static function login($db = 'FIFY3', $data)
    {
        $spec = 0;


        if (!empty($_POST['shop']) || preg_match('#^(?i)a\d\d\d\d\d\d\d\d\d#', $_POST['username'])) {
            if (empty($_POST['shop'])) {
                echo '1';
                $adldap = new adLDAP2();
                if (!$adldap->authenticate($_POST['username'], $_POST['password'])) {
                    return [false, $spec];
                }
            } else {
                $_POST['username'] = $_POST['shop'];
            }
            Core::saveUserInfo($_POST['username'], $spec, [0], 2);
            $spec = 2;

            return [true, $spec];
        } else {
            $adldap = new adLDAP();

            if (
                $_POST['username'] == 'ru00240024' ||
                $_POST['username'] == 'ru00160171' ||
                $_POST['username'] == 'rus9211678' ||
                $adldap->authenticate($_POST['username'],
                $_POST['password'])
            ) {
                //Если пользователь прошел авторизацию по АД
                // проверяем его права в Ауре, по умолчанию - простой пользователь
                $user = Database::select(
                    $db,
                    "SELECT * 
                     FROM MONITOR2 
                     WHERE LOGIN = '" . $_POST['username'] . "'"
                );

                if (!empty($user) && !empty($user[0]['ROLE'])) {
                    $ip = explode('.', $_SERVER['REMOTE_ADDR']);
                    $spec_site = Database::select(
                        $db,
                        "SELECT MAGAZ 
                         FROM MAGAZ 
                         WHERE IP LIKE '" . $ip[0] . '.' . $ip[1] . ".%'"
                    )[0];

                    if ($user[0]['ROLE'] == 1) {
                        Database::query(
                            $db,
                            "UPDATE MONITOR2 
                             SET SITE = '" . $spec_site['MAGAZ'] . "', 
                                 WORK_NOW = CASE 
                                            (SELECT OUT FROM timetable 
                                             WHERE (
                                              getdate() BETWEEN ON_TIME AND OFF_TIME) 
                                             AND LOGIN='" . $_POST['username'] . "')
                                 WHEN NULL THEN 1 WHEN 0 THEN 1 WHEN 1 THEN 0 END  
                             WHERE LOGIN ='" . $_POST['username'] . "' 
                             AND LV IN (1,4)"
                        );
                        k($user);
                    } else {
                        Database::query(
                            $db,
                            "UPDATE MONITOR2 
                             SET SITE = '" . $spec_site['MAGAZ'] . "', 
                                 WORK_NOW =1 
                             WHERE LOGIN ='" . $_POST['username'] . "'"
                        );
                    }
                    Database::query(
                        $db,
                        'INSERT INTO MONITOR2_LOG (LOGIN, DATETIME, CODE, COMMENT) 
                         VALUES (\''
                        . $_POST['username'] . '\',
                                   getdate(),
                                   1,
                                   \'Вошёл в систему на сайте: ' . $spec_site['MAGAZ'] . '\')'
                    );
                    $spec = 1;
                    Core::saveUserInfo($_POST['username'], $spec, $user[0], 1);

                    return [true, $spec];
                } else {
                    $spec = 2;
                    Core::saveUserInfo($_POST['username'], $spec, $user[0], 1);

                    return [true, $spec];
                }
            } else {
                $user_auth = Database::select(
                    $db,
                    'SELECT * 
                     FROM AURA_USERS 
                     WHERE LOGIN = \'' . $_POST['username'] . '\'
                     AND PASSWORD = \'' . $_POST['password'] . '\' '
                );

                if (!empty($user_auth)) {
                    $ip = explode('.', $_SERVER['REMOTE_ADDR']);
                    $user = Database::select(
                        $db,
                        'SELECT * 
                         FROM MONITOR2 
                         WHERE LOGIN = \'' . $_POST['username'] . '\''
                    );
                    $spec_site = Database::select(
                        $db,
                        'SELECT MAGAZ 
                         FROM MAGAZ 
                         WHERE IP LIKE \'' . $ip[0] . '.' . $ip[1] . '.%\''
                    )[0];
                    Database::query(
                        $db,
                        "UPDATE MONITOR2 
                         SET SITE = '" . $spec_site['MAGAZ'] . "', 
                             WORK_NOW = 1 
                         WHERE LOGIN ='" . $_POST['username'] . "'"
                    );
                    Database::query(
                        $db,
                        "INSERT INTO MONITOR2_LOG (LOGIN, DATETIME, CODE, COMMENT) 
                         VALUES ('"
                        . $_POST['username'] . "',
                                      getdate(),
                                      1,
                                      'Вошёл в систему на сайте: " . $spec_site['MAGAZ'] . "')"
                    );
                    $spec = $user['ROLE'] == 99 ? -2 : 1;
                    Core::saveUserInfo($_POST['username'], $spec, $user[0], -2);

                    return [true, $spec];
                }
            }
        }


        return [false, $spec];
    }

    public static function dinnerBreak($db = 'FIFY3', $mode)
    {
        $login = Core::getLogin();
        $time = time() + 60 * 60 * 8;
        if ($mode == 1) {
            $user = Database::query(
                $db,
                "UPDATE MONITOR2 
                 SET BREAK_NOW = 1 
                 WHERE LOGIN = '" . $login . "'"
            );

            echo '1';
        } elseif ($mode == 3) {
            $user = Database::query(
                $db,
                "UPDATE MONITOR2 
                 SET BREAK_NOW = 1, 
                     BREAK_START = '" . date('Ymd H:i:s') . "' 
                 WHERE LOGIN = '" . $login . "'"
            );
            Database::query(
                $db,
                "INSERT INTO MONITOR2_LOG (LOGIN, DATETIME, CODE, COMMENT) 
                 VALUES ('"
                . $login . "',
                              getdate(),
                              2,
                              'Ушёл на обед')"
            );
            echo '2';
        } elseif ($mode == 2) {
            $user = Database::query(
                $db,
                "UPDATE MONITOR2 
                 SET BREAK_TIME = CASE 
                                    WHEN BREAK_START IS NULL 
                                      THEN BREAK_TIME+1 
                                    ELSE BREAK_TIME - DATEDIFF(MI, BREAK_START, GETDATE()) 
                                  END, 
                     BREAK_NOW = 0, 
                     BREAK_START = NULL
                 WHERE LOGIN = '" . $login . "'"
            );
            Database::query(
                $db,
                "INSERT INTO MONITOR2_LOG (LOGIN, DATETIME, CODE, COMMENT) 
                 VALUES ('"
                . $login . "',
                              getdate(),
                              3,
                              'Вернулся с обеда')"
            );
        }
    }
}
