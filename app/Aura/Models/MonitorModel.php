<?php

namespace Aura\Models;

use Aura\Core\Core;
use Aura\Core\Database;
use DateTime;

class MonitorModel extends Database
{

    public static function getMonitorScheet($db = 'FIFY3')
    {
        $user = Core::getLogin();
        $monitor = Database::select(
            $db,
            "SELECT * 
             FROM MONITOR2 
             WHERE groupe LIKE CASE
                                WHEN 
                                  (SELECT admin 
                                   FROM MONITOR2 
                                   WHERE login = '" . $user . "') > 0 
                                   THEN '%%' 
			                ELSE CASE 
			                        WHEN 
			                          substring((
			                            SELECT groupe  
			                            FROM MONITOR2 
			                            WHERE login = '" . $user . "' ),0,3) = 'ЦП' 
			                            THEN 'ЦП%' 
			                ELSE (SELECT groupe  
			                        FROM MONITOR2 
			                        WHERE login = '" . $user . "' ) 
			                      END 
			                    END
			 ORDER BY groupe DESC"
        );
        foreach ($monitor as $key => $value) {
            if (!empty($value['ON_TIME'])) {
                $time = new DateTime($value['ON_TIME']);
                $monitor[$key]['ON_TIME'] = $time->format('H:i:s');
                $time = new DateTime($value['OFF_TIME']);
                $monitor[$key]['OFF_TIME'] = $time->format('H:i:s');
            } else {
                $monitor[$key]['ON_TIME'] = '';
                $monitor[$key]['OFF_TIME'] = '';
            }
            if (!empty($value['WORK_NOW'])) {
                $monitor[$key]['WORK_NOW'] = true;
            } else {
                $monitor[$key]['WORK_NOW'] = false;
            }
        }

        return $monitor;
    }

    public static function setReady($ready)
    {
        $user = Core::getLogin();
        $query = Database::query(
            'FIFY3',
            "UPDATE MONITOR2 
             SET READY = " . $ready . " 
             WHERE LOGIN = '" . $user . "'"
        );
    }

    public static function getRedirectWorkers($db = 'FIFY3')
    {
        $user = MonitorModel::getCurrentUser();
        $workers = Database::select(
            $db,
            "SELECT * 
             FROM MONITOR2 
             WHERE GROUPE = '" . $user['GROUPE'] . "'"
        );

        return $workers;
    }

    public static function getCurrentUser($db = 'FIFY3')
    {
        $user = Core::getLogin();
        @$user = Database::select(
            $db,
            "SELECT * 
             FROM MONITOR2 
             WHERE LOGIN = '" . $user . "'"
        )[0];

        return $user;
    }

    public static function takeJobLV3($number, $owner)
    {
        Database::connect('FIFY_NEXT');
        $tsql_callSP = '{call TakeJobLv3(?, ?, ?)}';
        $params = [];
        $params[] = [$number, SQLSRV_PARAM_IN];
        $params[] = [$owner, SQLSRV_PARAM_IN];
        $params[] = [&$result, SQLSRV_PARAM_INOUT, SQLSRV_PHPTYPE_INT];
        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            die(json_encode(sqlsrv_errors()));
        }
        sqlsrv_free_stmt($stmt);
        print_r($result);
    }
}
