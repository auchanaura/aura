<?php

namespace Aura\Models;

use Aura\Core\Core;
use Aura\Core\Database;
use DateTime;

class TimetableModel extends Database
{

    public static function getTimetable($db = 'FIFY3', $month, $year)
    {
        $date = new DateTime($year.'-'.$month);
        $users = Database::select($db, "SELECT DISTINCT LOGIN, FIO, GROUPE FROM TIMETABLE ");
        $timetable = Database::select(
            $db,
            "SELECT * 
             FROM TIMETABLE 
             WHERE DATEPART(MM,DATE) = convert(INT," . $month . ") 
             ORDER BY GROUPE DESC, FIO"
        );
        foreach ($timetable as $key => $value) {
            @$date = new DateTime($value['DATE']);
            @$timetable[$value['FIO']][$date->format('Y-m-d')] = $value;
            $timetable[$value['FIO']]['GROUPE'] = $value['GROUPE'];
            unset($timetable[$key]);
        }
        foreach ($users as $key => $value) {
            if (empty($timetable[rtrim($value['FIO'])])) {
                $timetable[$value['FIO']] = ['GROUPE' => $value['GROUPE']];
            }
        }

        return $timetable;
    }

    public static function updateTimetable($db, $data)
    {
        if (empty($data['SUPERVISOR'])) {
            $data['SUPERVISOR'] = 0;
        }
        $login = Core::getLogin();
        if (!empty($data['id'])) {
            $request = Database::query(
                $db,
                "UPDATE TIMETABLE 
                 SET TOUR='" . $data["TOUR"] . '\',
                     COMMENT=\'' . $data['COMMENT'] . '\',
                     SUPERVISOR=\'' . $data['SUPERVISOR'] . '\',
                     OUT=\'' . $data['OUT'] . '\' 
                 OUTPUT Inserted.ID, deleted.TOUR, deleted.COMMENT, deleted.SUPERVISOR, deleted.OUT 
                 WHERE ID=\'' . $data['id'] . '\''
            );
        } else {
            $request = Database::query(
                $db,
                "INSERT INTO TIMETABLE 
                 OUTPUT Inserted.ID VALUES 
                                    ((SELECT LOGIN 
                                      FROM MONITOR2 
                                      WHERE FIO LIKE '%" . $data['FIO'] . "%'),
                                                      '" . $data['FIO'] . "',
                                                      '" . $data['GROUPE'] . "',
                                                      '" . $data['DATE'] . "',
                                                      '" . $data['TOUR'] . "',
                                                           NULL,
                                                           NULL,
                                                      '" . $data['SUPERVISOR'] . "',
                                                      '" . $data['COMMENT'] . "',0)"
            );
            Database::query(
                'FIFY3',
                "UPDATE TIMETABLE 
                 SET ON_TIME=convert(DATE,DATE,112)
                     +cast((
                       SELECT START 
                       FROM SMENA 
                       WHERE NAMEE='" . $data['TOUR'] . "') 
                       AS DATETIME), 
                       OFF_TIME=dateadd(
                                HH,
                                (SELECT DURATION 
                                 FROM SMENA 
                                 WHERE NAMEE='" . $data['TOUR'] . "'),
                                (CONVERT(DATE,DATE,112)
                                +cast((
                                  SELECT START 
                                  FROM SMENA 
                                  WHERE NAMEE='" . $data['TOUR'] . "') 
                                  AS DATETIME))) 
                WHERE ID='" . $request[0]['ID'] . "'"
            );
        }
        if (!empty($data['TOUR'])) {
            Database::query(
                'FIFY3',
                "UPDATE TIMETABLE 
                 SET ON_TIME=convert(DATE,DATE,112)+cast
                    ((SELECT START 
                      FROM SMENA 
                      WHERE NAMEE='" . $data['TOUR'] . "') AS DATETIME),
                            OFF_TIME=dateadd(
                                            HH,
                                            (SELECT DURATION 
                                             FROM SMENA 
                                             WHERE NAMEE='" . $data['TOUR'] . "'),
                                                   (CONVERT(DATE,DATE,112)+cast((
                                                                           SELECT START 
                                                                           FROM SMENA 
                                                                           WHERE NAMEE='" . $data['TOUR'] . "') 
                                                                            AS DATETIME))) 
                                             WHERE ID='" . $data['id'] . "'"
            );
        } else {
            Database::query(
                'FIFY3',
                "UPDATE TIMETABLE 
                 SET ON_TIME=NULL 
                 WHERE ID='" . $data['id'] . "'"
            );
        }

        if (@$data['TOUR'] != @$request[0]['TOUR']) {
            @$tour = (!empty($request[0]['TOUR']) ? $request[0]['TOUR'] : '');
            Database::query(
                'FIFY3',
                "INSERT INTO TIMETABLE_LOG 
                 VALUES (
                          '" . date('Ymd H:i:s') . "',
                          '" . $login . "',
                           " . $request[0]['ID'] . ",
                          '" . $tour . "',
                          '" . $data['TOUR'] . "',
                               'TOUR')"
            );
        }
        if (@$data['COMMENT'] != @$request[0]['COMMENT']) {
            @$COMMENT = (!empty($request[0]['COMMENT']) ? $request[0]['COMMENT'] : '');
            Database::query(
                'FIFY3',
                "INSERT INTO TIMETABLE_LOG 
                 VALUES (
                          '" . date('Ymd H:i:s') . "',
                          '" . $login . "',
                          " . $request[0]['ID'] . ",
                          '" . $COMMENT . "',
                          '" . $data['COMMENT'] . "',
                               'COMMENT')"
            );
        }
        if (@$data['SUPERVISOR'] * 1 != @$request[0]['SUPERVISOR'] * 1) {
            @$SUPERVISOR = (!empty($request[0]['SUPERVISOR']) ? $request[0]['SUPERVISOR'] : '');
            Database::query(
                'FIFY3',
                "INSERT INTO TIMETABLE_LOG 
                 VALUES (
                         '" . date('Ymd H:i:s') . "',
                         '" . $login . "',
                          " . $request[0]['ID'] . ",
                         '" . $SUPERVISOR . "',
                         '" . $data['SUPERVISOR'] . "',
                              'SUPERVISOR')"
            );
        }
        if (@$data['OUT'] * 1 != @$request[0]['OUT'] * 1) {
            @$OUT = (!empty($request[0]['OUT']) ? $request[0]['OUT'] : '');
            Database::query(
                'FIFY3',
                "INSERT INTO TIMETABLE_LOG 
                 VALUES (
                          '" . date('Ymd H:i:s') . "',
                          '" . $login . "',
                           " . $request[0]['ID'] . ",
                          '" . $OUT . "',
                          '" . $data['OUT'] . "',
                          'OUT')"
            );
        }
    }

    public static function createUser($data)
    {
        $login = Core::getLogin();
        $output = Database::query(
            'FIFY3',
            "if (
                 select count(*) 
                 from TIMETABLE 
                 where LOGIN='" . $data['new_login'] . "')=0 
             INSERT INTO TIMETABLE OUTPUT 
                                inserted.ID,
                                inserted.GROUPE,
                                inserted.FIO VALUES ('" . $data['new_login'] . "',
                                (SELECT RTRIM(FIO) 
                                  from MONITOR2 
                                  where LOGIN='" . $data['new_login'] . "'),
                                (SELECT RTRIM(GROUPE) from MONITOR2 where LOGIN='" . $data['new_login'] . "'),
                                '20140101 12:00:00',
                                '1',
                                NULL,
                                NULL,
                                '0',
                                '',
                                0)"
        );
        Database::query(
            'FIFY3',
            "INSERT INTO TIMETABLE_LOG VALUES (
                                               '" . date('Ymd H:i:s') . "',
                                               '" . $login . "',
                                                " . $output[0]['ID'] . ",
                                                    '',
                                               '" . $output[0]['FIO'] . "',
                                               'FIO')"
        );
        Database::query(
            'FIFY3',
            "INSERT INTO TIMETABLE_LOG VALUES (
                                                '" . date('Ymd H:i:s') . "',
                                                '" . $login . "',
                                                 " . $output[0]['ID'] . ",
                                                     '',
                                                '" . $output[0]['GROUPE'] . "',
                                                     'GROUPE')"
        );
    }
}
