<?php
//Модель для работы с заявками

namespace Aura\Models;

use Aura\Core\Core;
use Aura\Core\Database;
use DateTime;

class RequestModel extends Database
{
    /**
     * Получает информацию о количестве выполненных действий
     *
     * @param string $db
     * @param        $number
     *
     * @return array
     */
    public static function getCurrentNumberOfUserAction()
    {
        $login = Core::getLogin();
        $request = Database::select(
            'FIFY3',
            "SELECT TICKET_PER_DAY 
             FROM MONITOR2 
             WHERE LOGIN = '" . $login . "'"

        )[0]['TICKET_PER_DAY'];


        return $request;
    }


    /**
     *  Получает информацю по заявке авторизированного пользователя
     *
     * @param string $db
     * @param        $indent
     * @param int    $mode
     *
     * @return array
     */
    public static function getCurrentTicket($db = 'FIFY3', $indent, $mode = 0)
    {

        $ticket_info = '';
        // Работающий человек из монитора
        @$user = Database::select(
            $db,
            "SELECT * 
             FROM MONITOR2 
             WHERE LOGIN = '" . $indent . "'"
        )[0];
        @$ticket['number'] = $user['ACTIVE_TICKET'];
        if ($mode == 2) {
            $ticket['number'] = $user['SECOND_TICKET'];
        }
        Core::saveUserInfo($indent, -1, $user);
        if ($ticket['number'] != 0) {
            $ticket_info = RequestModel::getFullTicketInfo($db, $ticket['number']);
        }

        return [$ticket_info, $user];
    }


    /**
     * Получает информацию по номера тикета
     *
     * @param string $db
     * @param        $ticket
     *
     * @return mixed
     */
    public static function getFullTicketInfo($db = 'FIFY3', $ticket)
    {
        $ticket_info = RequestModel::getRequestsInfo($db, $ticket)[0];
        $ticket_info['history'] = ActionModel::getTicketsHistory($db, $ticket);
        $eml = RequestModel::getEmlInfo($ticket);
        $ticket_info['attachments'] = $eml['attachments'];
        @$date = date_diff(new DateTime('NOW'), new DateTime($value['STATUS0']));
        if ($date->format('%d') == 0) {
            if ($date->format('%H') == 0) {
                $ticket_info['STATUS0_FORMATTED'] = $date->format('%i м.');
            } else {
                $ticket_info['STATUS0_FORMATTED'] = $date->format('%H ч. %i м.');
            }
        } else {
            $ticket_info['STATUS0_FORMATTED'] = $date->format('%d д.');
        }
        if (!empty($eml['html'])) {
            $ticket_info['TICKET_DESCRIPTION_DEFAULT'] = $ticket_info['TICKET_DESCRIPTION'];
            $ticket_info['TICKET_DESCRIPTION'] = $eml['html'];// подменяем plaintext из базы текстом письма
        }

        return $ticket_info;
    }


    /**
     * Получает информацию о заявках по номеракм
     *
     * @param string $db
     * @param        $number
     *
     * @return array
     */
    public static function getRequestsInfo($db = 'FIFY3', $number)
    {
        if (is_array($number)) {
            $number = implode(',', $number);
        }
        @$request = Database::select(
            $db,
            "SELECT * 
             FROM ACTIVE_TICKET 
             WHERE NUMBER 
             IN (" . $number . ")"
        );
        $request = TopicsModel::appendProblemName($request);

        return $request;
    }

    /**
     * //Прикрепляем к заявке содержание письма из файла
     *
     * @param $indent
     *
     * @return array
     */
    public static function getEmlInfo($indent)
    {


        $mail_file = 'c:\FIFY3\TICKET\\' . $indent . '\\' . $indent . '.eml';

        $eml = Core::parseEml($mail_file, $indent);

        if (empty($eml['html'])) {
            $request = Database::select(
                'FIFY3',
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE NUMBER 
                 IN (" . $indent . ")"
            );
            $eml['hmtl'] = $request[0]['TICKET_DESCRIPTION'];
        }

        return ['html' => $eml['html'], 'attachments' => $eml['attachments']];
    }

    public static function getUserTickets($db = 'FIFY3', $user)
    {
        $request = Database::select(
            $db,
            "SELECT * 
             FROM ACTIVE_TICKET 
             WHERE USER_LOGIN='" . $user . "' 
             AND STATUS0>=(getdate()-30) 
             ORDER BY NUMBER DESC "
        );
        $request = TopicsModel::appendProblemName($request);

        /*foreach ($request as $key => $value) {
            //$requests[$key]['history'] = ActionModel::getTicketsHistory($db,$value['NUMBER']);
            $eml = RequestModel::getEmlInfo($value['NUMBER']);
            $requests[$key]['attachments'] = $eml['attachments'];
            if (!empty($eml['html'])) {
                $requests[$key]['TICKET_DESCRIPTION_DEFAULT'] = $value['TICKET_DESCRIPTION'];
                $requests[$key]['TICKET_DESCRIPTION'] = $eml['html'];// подменяем plaintext из базы текстом письма
            }
        }*/

        return $request;
    }

    /**
     * @param $request
     * @param $data
     * @param $problem_list
     */
    public static function updateRequest($request, $data, $problem_list)
    {

        $variables = $GLOBALS['variables'];
        $number = $data['ticket_number'];
        Database::connect('FIFY_NEXT');
        //подготавливаем вызов процедуры
        $tsql_callSP = '{call SetTreatmentData(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)}';
        $real_site = ($request['REAL_SITE'] != $data['REAL_SITE'] ? $data['REAL_SITE'] : null);
        @$treatment_type = ($request['TYPE'] != $data['treatment_type'] ? $data['treatment_type'] : null);
        @$problem_id = ($request['PROBLEM_ID'] != $data['problem_type'] ? $data['problem_type'] : null);
        $escalation_number = ($request['NUMBER_EVENT'] != $data['number'] ? $data['number'] : null);
        $cim = ($request['CIM'] != $data['cim'] ? $data['cim'] : null);
        $news = $data['news'];
        $phys = $data['physical'];
        $ticket_date = $request['STATUS0'];
        if ($request['PROBLEM_NAME'] != $problem_list[$data['problem_type']]['SHOW_NAME_RUS']) {
            $problem_name = ($problem_list[$data['problem_type']]['SHOW_NAME_RUS']);
        } else {
            $problem_name = (null);
        }
        if (@$data['type'] == 'comment') {
            $data['status_global'] = $variables['STATUS_GLOBAL']['Комментарий']['value'];
        }
        @$global_Status = $data['status_global'];
        if (@$data['status_global'] == $variables['STATUS_GLOBAL']['Отложить']['value'] || @$data['status_global']
            == $variables['STATUS_GLOBAL']['Аутсорс']['value']
        ) {
            $data['time_up'] = explode(':', $data['time_up']);
            $data['time_up'][0] += $data['date_diff'];
            $data['time_up'] = implode(':', $data['time_up']);
            $date_up = $data['date_up'] . ' ' . $data['time_up'];
            $date_up = new DateTime($date_up);
            $date_up = $date_up->format('Ymd H:i:s');
        } else {
            $date_up = null;
        }
        @$comment_mail = nl2br($data['user_answer']);
        @$comment_tech = nl2br($data['tech_comment']);
        $comment_mail = (empty($comment_mail) ? null : $comment_mail);
        $comment_tech = (empty($comment_tech) ? null : $comment_tech);
        if ($data['status_global'] == $variables['STATUS_GLOBAL']['Мобильная_группа']['value']
            || $data['status_global'] == $variables['STATUS_GLOBAL']['Хелпдеск']['value']
            || $data['status_global'] == $variables['STATUS_GLOBAL']['Аутсорс']['value']
            || $data['status_global'] == $variables['STATUS_GLOBAL']['Отложить']['value']
        ) {
            $temp = $comment_tech;
            $comment_tech = $comment_mail;
            $comment_mail = $temp;
        } elseif (!empty($comment_mail) && empty($comment_tech)) {
            $comment_tech = $comment_mail;
        }
        if (empty($data['justify'])) {
            $data['justify'] = null;
        }

        $params = [];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$data['ticket_number'], SQLSRV_PARAM_IN];
        $params[] = [$real_site, SQLSRV_PARAM_IN];
        $params[] = [$treatment_type, SQLSRV_PARAM_IN];
        $params[] = [$problem_id, SQLSRV_PARAM_IN];
        $params[] = [$problem_name, SQLSRV_PARAM_IN];
        $params[] = [$global_Status, SQLSRV_PARAM_IN];
        $params[] = [$escalation_number, SQLSRV_PARAM_IN];
        $params[] = [$cim, SQLSRV_PARAM_IN];
        $params[] = [$news, SQLSRV_PARAM_IN];
        $params[] = [$comment_mail, SQLSRV_PARAM_IN];
        $params[] = [$comment_tech, SQLSRV_PARAM_IN];
        $params[] = [$date_up, SQLSRV_PARAM_IN];
        $params[] = [$ticket_date, SQLSRV_PARAM_IN];
        $params[] = [$phys, SQLSRV_PARAM_IN];
        $params[] = [$data['justify'], SQLSRV_PARAM_IN];
        if (!empty($_FILES['attachments']['name'][0])) {
            $params[] = [implode(',', $_FILES['attachments']['name']), SQLSRV_PARAM_IN];
        } else {
            $params[] = [null, SQLSRV_PARAM_IN];
        }

        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            die(json_encode(sqlsrv_errors()));
        }
        if (@$data['status_global'] == $variables['STATUS_GLOBAL']['Решить']['value']) {
            Core::sendMail(
                'done',
                [
                    'text' => $comment_mail,
                    'number' => $number,
                    'title' => $request['TICKET_TITLE'],
                    'description' => $request['TICKET_DESCRIPTION_DEFAULT']
                ],
                $_FILES['attachments'],
                [
                    $request['USER_FIO'] => $request['USER_MAIL']
                ]
            );
        } elseif ($data['type'] == 'comment') {
        } elseif (
            @$data['status_global'] != 11
            && @$data['status_global'] != 14
            && $data['type'] == 'escalation'
            && @$data['status_global'] != $variables['STATUS_GLOBAL']['Мобильная_группа']['value']
            && @$data['status_global'] != $variables['STATUS_GLOBAL']['Центр_поддержки']['value']
            && @$data['status_global'] != $variables['STATUS_GLOBAL']['В спул']['value']
            && @$data['status_global'] != $variables['STATUS_GLOBAL']['ATAK']['value']
        ) {
            Core::sendMail(
                $data['type'],
                [
                    'text' => $comment_mail,
                    'number' => $number,
                    'escalation_number' => $escalation_number,
                    'title' => $request['TICKET_TITLE'],
                    'description' => $request['TICKET_DESCRIPTION_DEFAULT']
                ],
                $_FILES['attachments'],
                [
                    $request['USER_FIO'] => $request['USER_MAIL']
                ]
            );
        } elseif (
            ($data['type'] == 'hold'
                && $data['status_global'] != $variables['STATUS_GLOBAL']['Отложить']['value']
                && $data['status_global'] != $variables['STATUS_GLOBAL']['Аутсорс']['value'])
            || ($comment_mail != $comment_tech && !empty($comment_mail))
        ) {
            Core::sendMail(
                $data['type'],
                [
                    'status_global' => $data['status_global'],
                    'text' => $comment_mail,
                    'number' => $number,
                    'title' => $request['TICKET_TITLE'],
                    'description' => $request['TICKET_DESCRIPTION_DEFAULT'],
                    'date_up' => $date_up
                ],
                $_FILES['attachments'],
                [
                    $request['USER_FIO'] => $request['USER_MAIL']
                ]
            );
        }
        sqlsrv_free_stmt($stmt);
    }

    public static function getCurationPool($db = 'FIFY3', $pool_type = 0)
    {
        $user = MonitorModel::getCurrentUser($db);
        if ($user['ROLE'] == 2) {
            if ($pool_type != 6 && $pool_type != 5) {
                $pool_type = 2;
            }
        } elseif ($user['ROLE'] == 3) {
            $count = [
                // АТАК - На группе
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE STATUS_JOB IN (0,1,3) 
                     AND LV=5 
                     AND PROBLEM_ID NOT IN 
                                        (SELECT ID 
                                         FROM TOPICS 
                                         WHERE DOMAIN='1C')"
                ),
                // АТАК - Профиль
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE STATUS_JOB IN (0,1,3) 
                     AND LV=5 
                     AND PROBLEM_ID NOT IN 
                                        (SELECT ID 
                                         FROM TOPICS 
                                         WHERE DOMAIN='1C') 
                     AND PROBLEM_ID IN 
                                    (SELECT 
                                     Topic_ID 
                                     FROM SKILL 
                                     WHERE LOGIN='" . $user['LOGIN'] . "' 
                                     AND SKILL>5)"
                ),
                // АТАК - 1C
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE STATUS_JOB IN (0,1,3,2) 
                     AND LV=5 
                     AND PROBLEM_ID IN 
                                    (SELECT ID 
                                     FROM TOPICS 
                                     WHERE DOMAIN='1C')"
                ),
                DataBase::Select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET
                     WHERE WORKER=
                                (SELECT FIO 
                                 FROM MONITOR2 
                                 WHERE LOGIN='" . $user['LOGIN'] . "') 
                                 AND STATUS_JOB IN (1,3) 
                                 AND LV=5"
                ),
                DataBase::Select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET
                     WHERE STATUS_JOB 
                     IN (3) 
                     AND LV=5
                     AND PROBLEM_ID
                     NOT IN 
                         (SELECT ID FROM TOPICS WHERE DOMAIN='1C')"
                )
            ];
            if ($pool_type != 4 && $pool_type != 9 && $pool_type != 10 && $pool_type != 11) {
                $pool_type = 3;
            }
        } elseif ($user['ROLE'] == 1) {
            if ($pool_type != 1) {
                $pool_type = 0;
            }
        } elseif ($user['ROLE'] == 4) {
            $count = [
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE STATUS_JOB IN (2) 
                     AND LV=5 
                     AND PROBLEM_ID IN 
                                    (SELECT ID 
                                     FROM TOPICS 
                                     WHERE DOMAIN='1C')"
                ),
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE STATUS_JOB IN (0,1,3) 
                     AND LV=5 
                     AND PROBLEM_ID IN 
                                    (SELECT ID 
                                     FROM TOPICS 
                                     WHERE DOMAIN='1C')"
                ),
                Database::select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET 
                     WHERE WORKER=
                            (SELECT FIO 
                             FROM MONITOR2
                     WHERE LOGIN='" . $user['LOGIN'] . "') 
                     AND STATUS_JOB IN (1,3,2)"
                )
            ];
            // 0 - В работе, 1 - Спул, 2 - Мои заявкм
            if ($pool_type != 8 && $pool_type != 10) {
                $pool_type = 7;

            }
        } else if ($user['ROLE'] == 5) {
            $count = [DataBase::Select(
                $db,
                "SELECT COUNT(*)
                 FROM ACTIVE_TICKET
                 WHERE STATUS_JOB IN (2) 
                 AND LV=8
                 "),
                DataBase::Select(
                    $db,
                    "SELECT COUNT(*) 
                     FROM ACTIVE_TICKET
                     WHERE STATUS_JOB 
                     IN (0,1,3) 
                     AND LV=8"
                ),
                DataBase::Select(
                    $db,
                    "SELECT COUNT(*)
                     FROM ACTIVE_TICKET
                     WHERE WORKER=
                              (SELECT FIO 
                               FROM MONITOR2 
                               WHERE LOGIN='" . $user['LOGIN'] . "') 
                               AND STATUS_JOB 
                               IN (1,3,2)"
                )];
            // 0 - В работе, 1 - Спул, 2 - Мои заявкм
            if ($pool_type != 8 && $pool_type != 10) {
                $pool_type = 7;

            }
        } else {
            exit();
        }
        if ($pool_type == 0) {
            //Центр поддержки - новые
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_GLOBAL=0 OR STATUS_GLOBAL=2 
                 AND STATUS_JOB NOT IN (1,2) 
                 ORDER BY STATUS0"
            );
            $count = Database::select(
                $db,
                "SELECT COUNT(*) 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB=1 
                 AND LV NOT IN (3,5,8)"
            );
        } elseif ($pool_type == 1) {
            // Центр поддержки - спул
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB=1 
                 AND LV NOT IN (3,5,8) 
                 ORDER BY STATUS0 DESC"
            );
            // счетчик для новых заявок в спуле
            $count = Database::select(
                $db,
                "SELECT COUNT(*) 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_GLOBAL=0 AND lv IN (0) 
                 OR STATUS_GLOBAL=2 AND lv IN (0)"
            );
        } elseif ($pool_type == 2) {
            // МГ - на группе
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET  
                 WHERE REAL_SITE IN 
                                  (SELECT MAGAZ 
                                   FROM MAGAZ 
                                   WHERE GROUPE='" . $user['GROUPE'] . "') 
                 AND STATUS_JOB IN (1,3) 
                 AND LV=3 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
            $count =
                [
                    -1,
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET 
                         WHERE WORKER=
                                (SELECT FIO 
                                 FROM MONITOR2 
                                 WHERE LOGIN='" . $user['LOGIN'] . "') 
                         AND STATUS_JOB IN (1,3,2)"
                    ),
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET  
                         WHERE REAL_SITE IN
                                          (SELECT MAGAZ 
                                           FROM MAGAZ 
                                           WHERE GROUPE='" . $user['GROUPE'] . "') 
                         AND STATUS_JOB IN (2) 
                         AND LV=3"
                    )
                ];
        } elseif ($pool_type == 3) {
            // АТАК - на группе
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (0,1,3) 
                 AND LV=5 
                 AND PROBLEM_ID NOT IN 
                                    (SELECT ID 
                                     FROM TOPICS 
                                     WHERE DOMAIN='1C') 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 4) {
            // АТАК - Профиль
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (0,1,3) 
                 AND LV=5 
                 AND PROBLEM_ID NOT IN 
                                    (SELECT ID 
                                     FROM TOPICS 
                                     WHERE DOMAIN='1C') 
                 AND PROBLEM_ID IN 
                                (SELECT Topic_ID 
                                 FROM SKILL 
                                 WHERE LOGIN='" . $user['LOGIN'] . "' 
                                 AND SKILL>5) 
                ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 9) {
            // АТАК - 1С
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (0,1,3,2) 
                 AND LV=5 
                 AND PROBLEM_ID IN 
                                (SELECT ID 
                                 FROM TOPICS 
                                 WHERE DOMAIN='1C') 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 5) {
            // МГ - мои
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE WORKER=
                        (SELECT FIO 
                         FROM MONITOR2 
                         WHERE LOGIN='" . $user['LOGIN'] . "') 
                 AND STATUS_JOB IN (1,3,2) 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
            $count =
                [
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET  
                         WHERE REAL_SITE IN 
                                         (SELECT MAGAZ 
                                          FROM MAGAZ 
                                          WHERE GROUPE='" . $user['GROUPE'] . "') 
                         AND STATUS_JOB IN (1,3) 
                         AND LV=3"
                    ),
                    Database::select(
                        $db,
                        "SELECT * 
                         FROM ACTIVE_TICKET 
                         WHERE WORKER=
                                (SELECT FIO 
                                 FROM MONITOR2 
                                 WHERE LOGIN='" . $user['LOGIN'] . "') 
                                 AND STATUS_JOB IN (1,3,2)"
                    ),
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                             FROM ACTIVE_TICKET  
                             WHERE REAL_SITE IN 
                                             (SELECT MAGAZ 
                                              FROM MAGAZ 
                                              WHERE GROUPE='" . $user['GROUPE'] . "') 
                              AND STATUS_JOB IN (2) 
                              AND LV=3"
                    )
                ];
        } elseif ($pool_type == 6) {
            // МГ - в работе
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET  
                 WHERE REAL_SITE IN 
                                 (SELECT MAGAZ 
                                  FROM MAGAZ 
                                  WHERE GROUPE='" . $user['GROUPE'] . "') 
                 AND STATUS_JOB IN (2) 
                 AND LV=3 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
            $count =
                [
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET  WHERE REAL_SITE IN 
                                                             (SELECT MAGAZ 
                                                              FROM MAGAZ 
                                                              WHERE GROUPE='" . $user['GROUPE'] . "') 
                         AND STATUS_JOB IN (1,3) 
                         AND LV=3"
                    ),
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET 
                         WHERE WORKER=
                                (SELECT FIO 
                                 FROM MONITOR2 
                                 WHERE LOGIN='" . $user['LOGIN'] . "') 
                         AND STATUS_JOB IN (1,3,2)"
                    ),
                    Database::select(
                        $db,
                        "SELECT COUNT(*) 
                         FROM ACTIVE_TICKET  
                         WHERE REAL_SITE IN 
                                         (SELECT MAGAZ 
                                          FROM MAGAZ 
                                          WHERE GROUPE='" . $user['GROUPE'] . "') 
                         AND STATUS_JOB IN (2) 
                         AND LV=3"
                    )
                ];
        } elseif ($pool_type == 7) {
            //1C
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (0,1,3) 
                 AND LV=5 
                 AND PROBLEM_ID IN 
                                 (SELECT ID 
                                  FROM TOPICS 
                                  WHERE DOMAIN='1C') 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 8) {
            //1C в работе
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (2) 
                 AND LV=5 
                 AND PROBLEM_ID IN 
                                (SELECT ID 
                                 FROM TOPICS 
                                 WHERE DOMAIN='1C') 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 10) {
            //1C Мои зявки
            $pool = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE WORKER=
                        (SELECT FIO 
                         FROM MONITOR2 
                         WHERE LOGIN='" . $user['LOGIN'] . "') 
                 AND STATUS_JOB IN (1,3,2) 
                 AND LV=5 
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        } elseif ($pool_type == 11) {
            //АТАК холд
            $pool = DataBase::Select(
                $db,
                "SELECT * 
                 FROM ACTIVE_TICKET 
                 WHERE STATUS_JOB IN (3) 
                 AND LV=5 
                 AND PROBLEM_ID 
                 NOT IN 
                     (SELECT ID
                      FROM TOPICS
                      WHERE DOMAIN='1C')
                 ORDER BY STATUS_JOB, PHYSICAL DESC, CIM DESC, STATUS0 DESC"
            );
        }


        if (!empty($pool)) {
            $pool = TopicsModel::appendProblemName($pool);
            foreach ($pool as $key => $value) {
                $pool[$value['NUMBER']] = $value;
                unset($pool[$key]);
            }
            foreach ($pool as $key => $value) {
                @$pool[$key]['TICKET_DESCRIPTION'] = str_replace('', ' ', $pool[$key]['TICKET_DESCRIPTION']);
                @$text = $pool[$key]['TICKET_DESCRIPTION'];
                @$text = str_replace('*', '&lowast;', $text);
                @$text = preg_replace("/(\w|\,)\r\n/", '$1 ', $text);
                @$pool[$key]['TICKET_DESCRIPTION_LOW'] = $text . '...';
                @$date = date_diff(new DateTime('NOW'), new DateTime($pool[$key]['STATUS0']));
                if ($date->format('%d') == 0) {
                    if ($date->format('%H') == 0) {
                        $pool[$key]['STATUS0_FORMATTED'] = $date->format('%i м.');
                    } else {
                        $pool[$key]['STATUS0_FORMATTED'] = $date->format('%H ч. %i м.');
                    }
                } else {
                    $pool[$key]['STATUS0_FORMATTED'] = $date->format('%d д.');
                }
            }
        }

        return [$pool, $count, $user];
    }

    public static function searchTickets($db = 'FIFY3', $param)
    {
        $search = Database::select(
            $db,
            "SELECT TOP 200 * 
             FROM ACTIVE_TICKET 
             WHERE NUMBER  LIKE '%" . $param . "%' 
             OR USER_LOGIN LIKE '%" . $param . "%' 
             OR USER_MAIL  LIKE '%" . $param . "%' 
             OR TICKET_DESCRIPTION LIKE '%" . $param . "%' 
             OR NUMBER_EVENT LIKE '%" . $param . "%' 
             ORDER BY STATUS0 DESC"
        );
        $search = TopicsModel::appendProblemName($search);
        foreach ($search as $key => $value) {
            @$text = $value['TICKET_DESCRIPTION'];
            @$text = str_replace('*', '', $text);
            @$text = preg_replace("/(\w|\,)\r\n/", '$1 ', $text);
            @$text = preg_replace("/\r\n/", '<br>', $text);
            @$date = new DateTime($value['STATUS0']);
            @$search[$key]['CREATE_DATE'] = $date->format('d.m.Y');
            @$search[$key]['TICKET_DESCRIPTION_LOW'] = $text . '...';
            if (stripos($value['NUMBER'], $param) != false) {
                @$search[$key]['group'] = 'NUMBER';
            } elseif (stripos($value['USER_LOGIN'], $param) != false) {
                @$search[$key]['group'] = 'USER_LOGIN';
            } elseif (stripos($value['USER_MAIL'], $param) != false) {
                @$search[$key]['group'] = 'USER_MAIL';
            } elseif (stripos($value['TICKET_DESCRIPTION'], $param) != false) {
                @$search[$key]['group'] = 'TICKET_DESCRIPTION';
            } else {
                @$search[$key]['group'] = 'OTHER';
            }

            $search[$value['NUMBER']] = $search[$key];
            unset($search[$key]); // тут происходит сортировка элементов по возрастанию
        }

        return $search;
    }

    public static function newRequest($db = 'FIFY_NEXT', $data, $answer_type = 1)
    {
        Database::connect($db);
        foreach ($data as $key => $value) {
            $data[$value['name']] = $value['value'];
        }
        $problem = explode('##', $data['problem_type']);
        $tsql_callSP = "{call NewTicket(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        $params = [];
        $answer = null;
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$data['login'], SQLSRV_PARAM_IN];
        $params[] = [$data['fio'], SQLSRV_PARAM_IN];
        $params[] = [$data['email'], SQLSRV_PARAM_IN];
        $params[] = [$data['site'], SQLSRV_PARAM_IN];
        $params[] = [$data['department'], SQLSRV_PARAM_IN];
        $params[] = [$data['real_site'], SQLSRV_PARAM_IN];
        $params[] = [$data['type'], SQLSRV_PARAM_IN];
        $params[] = [$problem[0], SQLSRV_PARAM_IN];
        @array_push($params, [$problem[1], SQLSRV_PARAM_IN]);
        $params[] = [$data['description'], SQLSRV_PARAM_IN];
        $params[] = [$data['status'], SQLSRV_PARAM_IN];
        $params[] = [$data['cim'], SQLSRV_PARAM_IN];
        $params[] = [$data['phys'], SQLSRV_PARAM_IN];
        $params[] = [$data['ticket_title'], SQLSRV_PARAM_IN];
        if (empty($data['groupe'])) {
            $groupe = 0;
        } else {
            $groupe = $data['groupe'];
        };
        array_push($params, [$groupe, SQLSRV_PARAM_IN]);
        array_push($params, [$data['phone'], SQLSRV_PARAM_IN]);
        array_push($params, [&$answer, SQLSRV_PARAM_INOUT, SQLSRV_PHPTYPE_INT]);
        $stmt = sqlsrv_query(DataBase::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt === false) {
            die(json_encode(sqlsrv_errors()));
        }
        sqlsrv_free_stmt($stmt);
        if ($answer_type) {
            echo('Номер созданной заявки: ');
        }
        if ($data['status'] == 1 || $data['status'] == 4) {
            Core::sendMail(
                'new',
                [
                    'text' => $data['description'],
                    'number' => $answer,
                    'title' => '',
                    'description' => $data['description']
                ],
                null,
                [
                    $data['fio'] => $data['email']
                ]
            );
        } else {
            Core::sendMail(
                'done',
                [
                    'text' => $data['description'],
                    'number' => $answer,
                    'title' => '',
                    'description' => $data['description']
                ],
                null,
                [
                    $data['fio'] => $data['email']
                ]
            );
        }

        return $answer;
    }

    public static function userComment($number, $owner, $comment, $action, $star1 = 0, $star2 = 0, $star3 = 0, $star4 = 0)
    {
        DataBase::connect('FIFY_NEXT');
        $tsql_callSP = "{call User_Action(?, ?, ?, ?, ?, ?, ?, ? )}";
        $params = [];
        array_push($params, [$number, SQLSRV_PARAM_IN]);
        array_push($params, [$action, SQLSRV_PARAM_IN]);
        array_push($params, [$owner, SQLSRV_PARAM_IN]);
        array_push($params, [$comment, SQLSRV_PARAM_IN]);
        array_push($params, [$star1, SQLSRV_PARAM_IN]);
        array_push($params, [$star2, SQLSRV_PARAM_IN]);
        array_push($params, [$star3, SQLSRV_PARAM_IN]);
        array_push($params, [$star4, SQLSRV_PARAM_IN]);
        $stmt = sqlsrv_query(DataBase::$connects['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt === false) {
            die(json_encode(sqlsrv_errors()));
        }
        sqlsrv_free_stmt($stmt);
    }

    public static function redirectToWorker($number, $login)
    {
        Database::connect('FIFY_NEXT');
        $tsql_callSP = '{call GiveJobLv3(?, ?, ?)}';
        $params = [];
        $problem_name = explode(' <span', $problem_name)[0];
        if (empty($login)) {
            $login = null;
        }
        $params[] = [$number, SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$login, SQLSRV_PARAM_IN];
        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            echo 0;
            die(json_encode(sqlsrv_errors()));
        }
        sqlsrv_free_stmt($stmt);
        echo 1;
    }

    public static function changeTopic($number, $problem_id, $problem_name)
    {
        Database::connect('FIFY_NEXT');
        $tsql_callSP = '{call ChangeTopic(?, ?, ?, ?)}';
        $params = [];
        $problem_name = explode(' <span', $problem_name)[0];
        $params[] = [$number, SQLSRV_PARAM_IN];
        $params[] = [Core::getLogin(), SQLSRV_PARAM_IN];
        $params[] = [$problem_id, SQLSRV_PARAM_IN];
        $params[] = [$problem_name, SQLSRV_PARAM_IN];
        $stmt = sqlsrv_query(Database::$connections['FIFY_NEXT'], $tsql_callSP, $params);
        if ($stmt == false) {
            echo 0;
            die(json_encode(sqlsrv_errors()));
        }
        sqlsrv_free_stmt($stmt);
        echo 1;
    }
}
