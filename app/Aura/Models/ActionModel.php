<?php

namespace Aura\Models;

//Модель для работы с действиями по заявкам

use Aura\Core\Core;
use Aura\Core\Database;
use DateTime;

class ActionModel extends Database
{

    /**
     * Все действия по указанным заявкам
     *
     * @param string $db
     * @param $number
     * @param int $mode
     */
    public static function getTicketsHistory($db = 'FIFY3', $number, $mode = 0)
    {
        if (is_array($number)) {
            $ids = [];
            foreach ($number as $key => $value) {
                $ids[] = $value['NUMBER'];
            }
            $number = implode("', '", $ids);
        }
        if ($mode == 0) {
            $actions = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_ACTION 
                 WHERE NUMBER IN ('" . $number . "') 
                 ORDER BY DATETIME DESC"
            );
        } else {
            $actions = Database::select(
                $db,
                "SELECT * 
                 FROM ACTIVE_ACTION 
                 WHERE NUMBER IN ('" . $number . "') 
                 AND ACTION_CODE 
                        IN ( 2, 3, 4, 6, 7, 8, 9, 11, 31, 32,33, 66,88,71, 94, 95, 99, 110,111,112,113,114,119,215)
                  ORDER BY DATETIME DESC"
            );
        }
        $actions = ActionModel::appendEml($actions);

        return $actions;
    }

    public static function appendEml($actions)
    {
        foreach ($actions as $key => $value) {
            $attachments = [];
            if (!empty($value['EMAIL'])) { //Существует ли письмо по действию
                $mail_file = 'c:\FIFY3\TICKET\\' . $value['NUMBER'] . '\\' . $value['EMAIL'] . '.eml';
                $eml = Core::parseEml($mail_file, $value['NUMBER'] . '&action=' . $value['EMAIL']);
                $actions[$key]['COMMENT_TECH'] = $eml['html'];
                if (!empty($eml['attachments'])) {
                    foreach ($eml['attachments'] as $key2 => $value2) {
                        $attachments[] = '<a href="/manage/downloadMailAttachment?indent=' .
                            $value['NUMBER'] . '&action=' . $value['EMAIL'] . '&img=' . $key2 . '">' .
                            $value2->filename . '</a>';
                    }
                }
            }
            if (!empty($attachments)) {
                $actions[$key]['COMMENT_TECH'] = preg_replace(
                    '/<script\b[^>]*>(.*?)<\/script>/is',
                    '',
                    $actions[$key]['COMMENT_TECH']
                );
                $actions[$key]['COMMENT_TECH'] = 'Вложения: ' . implode(', ', $attachments) . "\r\n" .
                    $actions[$key]['COMMENT_TECH'];
            }
            $actions[$key] = ActionModel::formatAction($actions[$key]);
        }

        return $actions;
    }

    public static function formatAction($action)
    {
        $date = new DateTime($action['DATETIME']);
        $time = $action['DATETIME'];
        $action['DATETIME'] = $date->format('d.m.y в H:i');
        //Преобразуем отображение даты и сохраняем исходный вариант.
        $action['DATETIME_INIT'] = $date->format('Y-m-d H:i:s');
        $action['COMMENT_TECH'] = str_replace("<br />\r\n", '<br>', $action['COMMENT_TECH']);
        //Из-за переноса строк возникают проблемы с JS, заменяем на html перенос.
        $action['COMMENT_TECH'] = str_replace("\r\n", '', $action['COMMENT_TECH']);
        $action['COMMENT_TECH'] = str_replace('"', ' ', $action['COMMENT_TECH']);
        $action['COMMENT_USERMAIL'] = str_replace("<br />\r\n", '<br>', $action['COMMENT_USERMAIL']);
        //Из-за переноса строк возникают проблемы с JS, заменяем на html перенос.
        $action['COMMENT_USERMAIL'] = str_replace("\r\n", '', $action['COMMENT_USERMAIL']);
        $action['COMMENT_USERMAIL'] = str_replace('"', ' ', $action['COMMENT_USERMAIL']);
        if (!empty($action['KERNEL_JOB_TIME_END'])) {
            $kernel_date_start = new DateTime($action['KERNEL_JOB_TIME_START']);
            $datetime = new DateTime($time);
            $kernel_date_end = new DateTime($action['KERNEL_JOB_TIME_END']);
            $time_diff = date_diff($datetime, $kernel_date_start);

            if ($time_diff->format('%h') == 0) {
                if ($datetime < $kernel_date_end) {
                    $action['TIME_SPEND'] = $time_diff->format('Затрачено %i минут');
                } else {
                    $action['TIME_SPEND'] = $time_diff->format('<span style=\'color:red;\'>Затрачено %i минут</span>');
                }
            } else {
                $action['TIME_SPEND'] = $time_diff->format('Затрачено %h часов %i минут');
            }
        } else {
            $action['TIME_SPEND'] = '';
            $action['COMPLAINT'] = '';
        }
        if ($action['OWNER'] != 'FIFY3' && $action['OWNER'] != 'Система' && $action['OWNER'] != 'Aura') {
            $action['COMPLAINT'] = 'Пожаловаться на действие';
        }

        return $action;
    }


    /**
     *
     * какое-то конкретное действие по конкретной заявке заявке
     *
     * @param string $db
     * @param $number
     * @param $action_number
     */
    public static function getAction($db = 'FIFY3', $number, $action_number)
    {
        $actions = Database::select(
            $db,
            "SELECT * 
             FROM ACTIVE_ACTION 
             WHERE NUMBER ='" . $number . "'
             AND ACTION_NUMBER='" . $action_number . "'"
        );
        $actions = ActionModel::appendEml($actions);

        return $actions;
    }

    public static function getAppeals($db = 'FIFY3')
    {
        $actions = Database::select('FIFY_NEXT', "SELECT * FROM GetReclamation()");
        //$actions = ActionModel::appendEml($actions);
        foreach ($actions as $key => $value) {
            if (!empty($value['JUSTIFY'])) {
                $actions[$key]['appeal_type'] = $GLOBALS['variables']['APPEAL']['justify']['value'];
            } elseif ($value['ACTION_CODE'] == 211) {
                $actions[$key]['appeal_type'] = $GLOBALS['variables']['APPEAL']['complaint']['value'];
            } elseif ($value['ACTION_CODE'] == 119) {
                $actions[$key]['appeal_type'] = $GLOBALS['variables']['APPEAL']['reclamation']['value'];
            } /*else if ($value['ACTION_CODE'] == 213) {
				$actions[$key]['appeal_type'] = $GLOBALS['variables']['APPEAL']['speed_up']['value'];
			}*/
        }

        return $actions;
    }
}
