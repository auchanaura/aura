<?php

namespace Aura\Models;

use Aes;
use Aura\Core\Database;

//Модель для работы с типами проблем

class TopicsModel extends Database
{
    /**
     *  Замена ID проблемы в заявке на реальный топик
     *
     * @param $tickets
     *
     * @return mixed
     */
    public static function appendProblemName($tickets)
    {
        if (Aes::decrypt($_COOKIE['role'], COOKIE_PASS) != 0) {
            $request = Database::select('FIFY3', "SELECT ID,SHOW_NAME_RUS FROM TOPICS");
        } else {
            $request = Database::select('FIFY3', "SELECT ID,SHOW_NAME_RUS FROM TOPICS WHERE SYSTEM=1");
        }
        $problem_list = Database::select('FIFY3', "SELECT ID,SHOW_NAME_RUS FROM TOPICS");
        $problems = [];
        foreach ($problem_list as $key => $value) {
            $problems[$value['ID']] = $value['SHOW_NAME_RUS'];
        }
        foreach ($tickets as $key => $value) {
            if (!empty($problem[$value['PROBLEM_ID']])) {
                $tickets[$key]['PROBLEM_NAME'] = $problem[$value['PROBLEM_ID']]['SHOW_NAME_RUS'];
            } else {
                $tickets[$key]['PROBLEM_NAME'] = '';
            }
        }

        return $tickets;
    }

    /**
     * Список всех топиков + список форматированный для поля ввода
     *
     * @param string $db
     *
     * @return array
     */
    public static function getProblemsList($db = 'FIFY3')
    {
        if (Aes::decrypt($_COOKIE['role'], COOKIE_PASS) != 0) {
            $request = Database::select($db, "SELECT * FROM TOPICS");
        } else {
            $request = Database::select($db, "SELECT * FROM TOPICS WHERE SYSTEM=1 OR SHOW_NAME_RUS LIKE 'Финансовые приложения%'");
        }
        $unformed_list = [];
        foreach ($request as $key => $value) {
            $unformed_list[$value['ID']] = $value;
        }
        $list = [];
        foreach ($unformed_list as $key => $value) {
            if ($value['PER_ID'] == 0) {
                $list[$value['ID']] = [];
            } else {
                if (isset($list[$value['PER_ID']])) {
                    $list[$value['PER_ID']][$value['ID']] = [];
                } elseif ($unformed_list[$value['PER_ID']]['PER_ID'] != 0) {
                    $list[$unformed_list[$value['PER_ID']]['PER_ID']][$value['PER_ID']][] = $value['ID'];
                }
            }
        }

        return [$unformed_list, $list];
    }

    // Список всех топиков + список форматированный для поля ввода
    public static function getTreeProblemsList($db = 'FIFY3')
    {
        $request = DataBase::Select($db, "SELECT * FROM TOPICS ORDER BY PER_ID");

        return $request;
    }

}
