<?php

namespace Aura\Core;

class ProcessData extends Controllers
{

    public static function pregEscape($value)
    {
        if (is_array($value)) {
            foreach ($value as $key2 => $value2) {
                $value[$key2] = self::pregEscape($value2);
            }
        } else {
            $value = preg_replace('|\\\\|', '&frasl;', $value);
            $value = preg_replace('|"|', '&quot;', $value);
            $value = preg_replace("/\r\n/", '<br>', $value);
            $value = preg_replace("/\r/", '<br>', $value);
            $value = preg_replace('/	/', ' ', $value);
            $value = preg_replace('/\'/', '&quot', $value);
            $value = preg_replace('//', '&quot', $value);
            //$value = preg_replace("//*/",'&lowast;',$value);
        }

        return $value;
    }

    public static function filterData($spread, $pool, array $filter_fields = [])
    {

        $filters = [
            'NUMBER' =>
                [
                    'Номер', 'Номер заявки', 'text'
                ],
            'CIM' =>
                [
                    'Срочность', 'Степень срочности', 'dropdown'
                ]
        ];
        $filters['LV'] = ['Группа', 'Группа назначения', 'dropdown'];
        $filters['USER_LOGIN'] = ['User', 'Пользователь', 'text'];
        $filters['STATUS_GLOBAL'] = ['Статус', 'Статус', 'dropdown'];
        $filters['REAL_SITE'] = ['Сайт', 'Сайт заявителя', 'dropdown'];
        $filters['WORKER'] = ['Исполнитель', 'Специалист', 'dropdown'];
        $filter_values = [];
        foreach ($pool as $key => $value) {
            $filter_values['REAL_SITE'][$value['REAL_SITE']] = $value['REAL_SITE'];
            @$filter_values['CIM'][$value['CIM']] = $spread[0][$value['CIM']];
            $filter_values['STATUS_GLOBAL'][$value['STATUS_GLOBAL']] = $value['STATUS_GLOBAL'];
            $filter_values['WORKER'][$value['WORKER']] = $value['WORKER'];
            @$filter_values['LV'][$value['LV']] = $spread[1][$value['LV']];
        }
        foreach ($filter_fields as $key => $value) {
            $result_filters[$value] = $filters[$value];
            @$result_filter_values[$value] = $filter_values[$value];
        }
        foreach ($result_filter_values as $key => $value) {
            @asort($result_filter_values[$key]);
        }

        return [$result_filters, $result_filter_values];
    }

    public static function translitirate($str)
    {
        $tr = ["А" => "a", 'Б' => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e", "Ё" => "yo", "Ж" => "zh",
            "З" => "z", "И" => "i", "Й" => "j", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n", "О" => "o", "П" => "p",
            "Р" => "r", "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "kh", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo", "ж" => "zh",
            "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "kh", "ц" => "ts", "ч" => "ch",
            "ш" => "sh", "щ" => "sch", "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            ' ' => "_", "." => ".", "," => ",", "/" => "-", ":" => "", ";" => "", "—" => "", "–" => "-"];

        return strtr($str, $tr);
    }
}
