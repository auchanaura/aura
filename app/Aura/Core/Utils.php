<?php
/**
 * Created by PhpStorm.
 * User: RU00160171
 * Date: 017 17.02.17
 * Time: 15:59
 */

namespace Aura\Core;


use DateTime;

class Utils
{
    public static function isinStr($str, $substr): bool
    {
        return strpos($str, $substr) !== false;
    }

    /**
     * @param DateTime      $start
     * @param DateTime|null $end
     *
     * @return string
     */
    public static function formatDateDifftoNow($start)
    {
        if (!($start instanceof DateTime)) {
            $start = new DateTime($start);
        }

        $now = new DateTime('NOW');
        $interval = $now->diff($start);


        $format = [];
        if ($interval->y !== 0) {
            $format[] = '%y г.';
        }
        if ($interval->m !== 0) {
            $format[] = '%m м.';
        }
        if ($interval->d !== 0) {
            $format[] = "%d д.";
        }
        if ($interval->h !== 0) {
            $format[] = "%h ч.";
        }
        if ($interval->i !== 0) {
            $format[] = "%i мин.";
        }
        if ($interval->s !== 0) {
            if (!count($format)) {
                return "меньше минуты";
            } else {
                $format[] = "%s сек.";
            }
        }


        $format = count($format) > 1 ? array_shift($format) . " и " . array_shift($format) : array_pop($format);


        return $interval->format($format);
    }
}
