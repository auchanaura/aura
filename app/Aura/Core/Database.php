<?php

namespace Aura\Core;

class Database
{
    public static $connections = [];

    public static function select($databasename, $query)
    {
        return self::query($databasename, $query);
    }

    public static function query($DataBaseName, $query)
    {
        if (empty(self::$connections[$DataBaseName])) {
            self::connect($DataBaseName);
        }
        $stmt = sqlsrv_query(self::$connections[$DataBaseName], $query);
        $result = [];
        if ($stmt == false) {
            die(print_r(sqlsrv_errors(), true));
        }
        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
            $result[] = $row;
        }
        sqlsrv_free_stmt($stmt);

        return $result;
    }

    public static function connect($databasename)
    {
        // все соединения записываются в один массив соединений, для дальнейшего их использования.
        if (empty(self::$connections[$databasename])) {
            require_once AURA_CONFIGS_PATH . strtolower($databasename) . '.conf.php';
            self::$connections[$databasename] = self::newConnect($hostname, $connectionInfo);
        }
    }

    public static function newConnect($hostname, $connectionInfo)
    {
        $connect = sqlsrv_connect($hostname, $connectionInfo);
        if ($connect == false) {
            exit('Problem with SQL connection.' . print_r(sqlsrv_errors()));
        }
        return $connect;
    }

    public static function _DESCTUCT()
    {
        foreach (self::$connections as $key => $value) {
            sqlsrv_close($value);
        }
    }
}
