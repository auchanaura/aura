<?php

namespace Aura\Core;

use adLDAP;
use adLDAP2;
use Aes;
use Aura\Models\MonitorModel;
use DateTime;
use Katzgrau\KLogger\Logger;
use MimeMailParser;
use PHPMailer;

class Core
{

    public static function sendMail($template = 'done', $ticket,  $files = [],  $mails = [])
    {
        $variables = $GLOBALS['variables'];
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = '146.240.32.141';
        $mail->SMTPAuth = false;
        $mail->Port = 25;

        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->CharSet = 'UTF-8';

        $mail->setFrom('it@auchan.ru', 'Центр Поддержки IT');
        foreach ($mails as $key => $value) {
            $addresses = explode(',', $value);
            foreach ($addresses as $key2 => $value2) {
                if (!empty($value2)) {
                    $mail->addAddress($value2, $key);
                }
            }
        }
        $mail->addAddress('it@auchan.ru', 'Центр Поддержки IT');
        $mail->addReplyTo('it@auchan.ru', 'Центр Поддержки IT');

        $mail->isHTML(true);
        switch ($template) {
            case 'done':
                $mail->Subject = 'По вашей заявке были проведены работы. Заявка №' . $ticket['number'];
                break;
            case 'comment':
                $mail->Subject = 'Комментарий IT специалиста. Заявка №' . $ticket['number'];
                break;
            case 'escalation':
                $mail->Subject = 'Статус вашей заявки №' . $ticket['number'] . ' изменился.';
                break;
            case 'hold':
                $mail->Subject = 'Выполнение Вашей заявки №' . $ticket['number'] . ' приостановлено.';
                break;
            case 'new':
                $mail->Subject = 'Создана новая заявка №' . $ticket['number'] . '.';
                break;
        }


        $template = file_get_contents(AURA_VIEWS_PATH . '/mail_templates/' . $template . '.html');
        $template = str_replace('[TEXT]', $ticket['text'], $template);
        $template = str_replace('[NUMBER]', $ticket['number'], $template);
        @$template = str_replace('[TICKET]', $ticket['escalation_number'], $template);
        @$link = 'http://aura.ru.auchan.com/home?password="' .
            Aes::encrypt($ticket['USER_LOGIN'] . ';' . $ticket['number'], COOKIE_PASS) . '"';
        @$template = str_replace('[LINK]', $link, $template);
        if (@$ticket['status_global'] == $variables['STATUS_GLOBAL']['Недостаточно_данных']['value']) {
            @$template = str_replace('[REASON]', 'недостаточно данных', $template);
        };
        if (@$ticket['status_global'] == $variables['STATUS_GLOBAL']['Требуется_подтверждение']['value']) {
            @$template = str_replace('[REASON]', 'требуется подтверждение', $template);
        };
        if (@$ticket['status_global'] == $variables['STATUS_GLOBAL']['Отложить']['value']) {
            $ticket['date_up'] = new Datetime($ticket['date_up']);
            @$template = str_replace('[REASON]', 'отложено до ' . $ticket['date_up']->format('d.m.Y H:i:s'), $template);
        };
        if (@$ticket['status_global'] == $variables['STATUS_GLOBAL']['Аутсорс']['value']) {
            $ticket['date_up'] = new Datetime($ticket['date_up']);
            @$template = str_replace('[REASON]', 'отложено до ' . $ticket['date_up']->format('d.m.Y H:i:s'), $template);
        };
        $mail->Body = $template;
        $original = tmpfile();
        fwrite($original, $ticket['description']);
        $metaDatas = stream_get_meta_data($original);
        $tmpFilename = $metaDatas['uri'];
        $mail->addAttachment($tmpFilename, 'Оригинал_заявки.txt');
        if (!empty($files)) {
            foreach ($files['name'] as $key => $value) {
                $mail->addAttachment($files['tmp_name'][$key], $value);
            };
        }
        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            exit();
        } else {
            //echo 'Message has been sent';
        }
        fclose($original);
    }

    public static function saveUserInfo($indent, $spec = 0, array $user = [], $ad = 0)
    {

        $time = time() + 60 * 60 * 24 * 365;

        if ($ad > 0) {
            if ($ad == 1) {
                $adldap = new adLDAP();
            } elseif ($ad == 2) {
                $adldap = new adLDAP2();
            }
            $user_info = $adldap->user()->info($indent);
            $name = Aes::encrypt($user_info[0]['displayname'][0], COOKIE_PASS);
            $email = Aes::encrypt($user_info[0]['mail'][0], COOKIE_PASS);
            setcookie('name', $name, $time, '/');
            setcookie('email', $email, $time, '/');
        } elseif ($ad == -2) {
            $name = Aes::encrypt($indent, COOKIE_PASS);
            $email = Aes::encrypt($indent, COOKIE_PASS);
            setcookie('name', $name, $time, '/');
            setcookie('email', $email, $time, '/');
        }
        $time = time() + 60 * 60 * 9;
        $username = Aes::encrypt($indent, COOKIE_PASS);
        $ticket = Aes::encrypt($user['ACTIVE_TICKET'], COOKIE_PASS);
        $role = Aes::encrypt($user['ROLE'], COOKIE_PASS);
        $admin = Aes::encrypt($user['ADMIN'], COOKIE_PASS);
        $group = Aes::encrypt($user['GROUPE'], COOKIE_PASS);
        if ($user['BREAK_NOW'] == 1 && $user['BREAK_START'] != null) {
            $datediff = date_diff(new DateTime('NOW'), new DateTime($user['BREAK_START']));
            $datediff = $datediff->format('%i') * 60 + $datediff->format('%s');
            $dinner_time = $user['BREAK_TIME'] * 60 - $datediff;
            setcookie('dinner', $dinner_time, $time, '/');
            setcookie('dinner_active', '2', $time, '/');
        } elseif ($user['BREAK_NOW'] == 1 && empty($user['BREAK_START'])) {
            setcookie('dinner_active', '1', $time, '/');
            setcookie('dinner', $user['BREAK_TIME'] * 60, $time, '/');
        } else {
            setcookie('dinner', $user['BREAK_TIME'] * 60, $time, '/');
        }
        if ($spec != -1) {
            setcookie('spec', $spec, $time, '/');
        }
        setcookie('username', $username, $time, '/');
        setcookie('role', $role, $time, '/');
        setcookie('ticket', $ticket, $time, '/');
        setcookie('admin', $admin, $time, '/');
        setcookie('group', $group, $time, '/');
    }

    public static function logout()
    {
        Database::query(
            'FIFY3',
            "UPDATE MONITOR2 
             SET WORK_NOW=0 
             WHERE LOGIN='" . Aes::decrypt($_COOKIE['username'], COOKIE_PASS) . "'"
        );
        Database::query(
            'FIFY3',
            "INSERT INTO MONITOR2_LOG (LOGIN, DATETIME, CODE, COMMENT) 
             VALUES ('"
            . Aes::decrypt($_COOKIE['username'], COOKIE_PASS) . '\',
                     getdate(),
                     4,
                     \'Выход из системы (пользователь)\')'
        );
        setcookie('username', '', time() - 1, '/');
        setcookie('spec', '', time() - 1, '/');
        setcookie('dinner', '', time() - 1, '/');
        setcookie('ticket', '', time() - 1, '/');
        setcookie('dinner_active', '', time() - 1, '/');
        setcookie('name', '', time() - 1, '/');
        setcookie('role', '', time() - 1, '/');
        setcookie('email', '', time() - 1, '/');
        setcookie('admin', '', time() - 1, '/');
        setcookie('group', '', time() - 1, '/');
    }

    public static function renderMailAttachment($indent, $img, $action = "")
    {
         header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header('Content-Type: image/png');
        if (empty($action)) {
            $action = !empty($_GET['action']) ? $_GET['action'] : $indent;
        }
        //TODO $mail_file ссылается на папку на сервере АУРЫ c:\fify3\tickets
        //необходимо как то сослаться на нее
        $mail_file = 'c:\FIFY3\TICKET\\' . $indent . '\\' . $action . '.eml';

        $Parser = new MimeMailParser();
        $Parser->setPath($mail_file);
        $attachments = $Parser->getAttachments();
        $img = $attachments[$img]->getContent();
        $etag = md5($img);
        header("Etag: ".$etag);
        print_r($img);
    }

    public static function downloadMailAttachment($indent, $img, $action = "")
    {
        if (empty($action)) {
            if (!empty($_GET['action'])) {
                $action = $_GET['action'];
            } else {
                $action = $indent;
            }
        }
        //TODO $mail_file ссылается на папку на сервере АУРЫ c:\fify3\tickets
        //необходимо как то сослаться на нее
        $mail_file = 'c:\FIFY3\TICKET\\' . $indent . '\\' . $action . '.eml';
        $Parser = new MimeMailParser();

        $Parser->setPath($mail_file);
        $attachments = $Parser->getAttachments();

        $name = $attachments[$img]->getFilename();
        if (strpos($name, 'koi8-r') != false) {
            $name = str_replace('=?koi8-r?Q?', '', $name);
            $name = str_replace('?=', '', $name);
            $name = str_replace('?% ', '', $name);
        } else {
            $name = str_replace('=', '%', $attachments[$img]->getFilename());
            $name = str_replace('%?utf-8?Q?', '', $name);
            $name = str_replace('?% ', '', $name);
            $name = str_replace('?%', '', $name);
        }
        $file = $attachments[$img]->getContent();
        $name_visible = $name;
        @file_put_contents(AURA_DATA_PATH . 'files/' . $name, $file);
        //header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream; charset=utf-8');
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $agent = $_SERVER['HTTP_USER_AGENT'];
            if (strlen(strstr($agent, 'Firefox')) > 0) {
                $name_visible = $attachments[$img]->getFilename();
            }
        }
        header('Content-Disposition: attachment; filename="' . $name_visible . '"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . strlen($file));
        if (file_exists(AURA_DATA_PATH . 'files/' . $name)) {
            $file = @fopen(AURA_DATA_PATH . 'files/' . $name, 'rb');
            while (!feof($file)) {
                print(@fread($file, 1024 * 8));
                ob_flush();
                flush();
            }
            @fclose($file);
            unlink(AURA_DATA_PATH . 'files/' . $name);
        } else {
            print_r($file);
        }
    }

    public static function leftMenu($links)
    {
        $leftmenu = '';
        $user = MonitorModel::getCurrentUser();
        foreach ($links as $key => $value) {
            $class = '';
            if ($_SERVER['REQUEST_URI'] == $value['link']) {
                $class = 'active';
            }
            if (!empty($value['condition'])) {
                foreach ($value['condition'] as $key2 => $value2) {
                    if ($value2[1]) {
                        if ($user[$key2] != $value2[0]) {
                            continue 2;
                        }
                    } else {
                        if ($user[$key2] == $value2[0]) {
                            continue 2;
                        }
                    }
                }
            }

            if ($value['link'] == '/curation?mode=5') {
                $count_my = Database::select(
                    'FIFY3',
                    "SELECT count(*)
                     FROM ACTIVE_TICKET
                     WHERE WORKER=
                            (SELECT FIO
                             FROM MONITOR2
                             WHERE LOGIN='" . $user['LOGIN'] . "') 
                             AND STATUS_JOB
                             IN (1,3,2)"
                );
                $label = '';
                if (!empty($count_my[0][''])) {
                    $label = '<span class="label label-primary ">' . $count_my[0][''] . '</span>';
                }
                $leftmenu .= '<li><a href="/curation?mode=5" class="waves-effect ' . $class . '">
                              <i style="color: #green" class="' . $value['symbol'] . '"></i>' . $label .
                    '<span>Мои заявки</span></a></li>';
            } elseif ($value['link'] == '/curation?mode=10') {
                $count_my = DataBase::select(
                    'FIFY3',
                    "SELECT count(*) 
                     FROM ACTIVE_TICKET 
                     WHERE WORKER=
                                  (SELECT FIO 
                                   FROM MONITOR2 
                                   WHERE LOGIN='" . $user['LOGIN'] . "') 
                                   AND STATUS_JOB IN (1,3,2)"
                );
                $label = '';
                if (!empty($count_my[0][''])) {
                    $label = '<span class="label label-primary ">' . $count_my[0][''] . '</span>';
                }
                $leftmenu .= '<li><a href="/curation?mode=10" class="waves-effect ' .
                    $class . '"><i style="color: #green" class="' .
                    $value['symbol'] . '"></i>' .
                    $label . '<span>Мои заявки</span></a></li>';
            } else {
                $leftmenu .= '<li><a href="' . $value['link'] . '" class="waves-effect ' . $class . '">
                              <i class="' . $value['symbol'] . '"></i><span>' . $value['text'] . '</span></a></li>';
            }
            //}
            if ($value['link'] == '/view_request' && !empty($user['SECOND_TICKET']) && $user['SECOND_TICKET'] != 0) {
                $class = $_SERVER['REQUEST_URI'] == '/background' ? 'active' : '';
                $leftmenu .= '<li><a href="/background" class="waves-effect ' . $class . '">
                              <i style="color: #ffbd4a" class="' . $value['symbol'] . '"></i>
                              <span class="label label-primary ">1</span><span>Фоновая заявка</span></a></li>';
            }

        }

        return $leftmenu;
    }

    public static function parseEml($file, $indent)
    {
        $mail_file = $file;
        $html = '';
        $attachments = '';
        if (file_exists($mail_file)) {
            $Parser = new MimeMailParser();
            $Parser->setPath($mail_file);
            $html = $Parser->getMessageBody('html');

            $login = Core::getLogin();
            $coding = $Parser->getHeaders()['subject']; // Работающий человек из монитора
            if (!empty($coding)) {
                $coding = explode('?Q', $coding)[0];
                @$coding = explode('?', $coding)[1];
            }
            if (strtolower($coding) != 'utf-8' && !empty($coding)) {
                @$html = str_replace('charset=', '', $html);
                @$html = str_replace('<html><head>', '', $html);
                //if ($coding != 'koi8-r') {
                $html = mb_convert_encoding($html, 'UTF-8', $coding);
                //} else {
                //$html = iconv('UTF-8','KOI8-R//IGNORE', $html);
                //}
            }
            $attachments = $Parser->getAttachments();

            foreach ($attachments as $key => $value) {
                //Имя файла может быть указано как URL ссылка - переводим обратно на кирилицу
                $name = str_replace('=', '%', $value->filename);
                $name = str_replace('%?utf-8?Q?', '', $name);
                $name = str_replace('?% ', '', $name);
                $name = str_replace('?%', '', $name);
                if (strpos($name, '%?koi8-r?Q?') !== false) {
                    $name = explode('%?koi8-r?Q?', $name)[1];
                    $name = mb_convert_encoding($name, 'UTF-8', 'KOI8-R');
                    $name = 'Ссылка на файл'; //json вылетает с ошибкой
                } else {
                    //TODO Узнать ошибка ли это
                    $name = $name;
                }
                $attachments[$key]->filename = urldecode($name);

                if ($name == 'winmail.dat') {
                    @$html = @str_get_html($attachments[$key]->getContent());
                    if (is_object($html)) {
                        @$html = @$html->find('div[class="WordSection1"]', 0)->outertext;
                    } else {
                        @$html = '';
                    }
                }
            }

            $pattern = '/src="cid:(.*?)"/'; //В теле письма заменяем ссылки на изображения
            $html = preg_replace("/(<br\s*\/?>\s*)+/", "<br>", $html);
            //$html = preg_replace("/2000/","",$html);
            $html = str_replace('width=2000', '', $html);
            $html = str_replace('width=1000', '', $html);
            $html = str_replace('width=1440', '', $html);
            $html = str_replace("<base href=\"https://e.mail.ru/\" target=\"_self\">", "", $html);
            $html = str_replace('width:1500.0pt', '', $html);
            $html = str_replace('width:1080.0pt', '', $html);
            $html = str_replace('width:750.0pt', '', $html);
            $html = str_replace('min-width:2000px', '', $html);
            $html = preg_replace_callback($pattern, function ($match) use ($indent, $attachments) {
                $i = '';

                foreach ($attachments as $key => $value) {

                    if ($value->headers['x-attachment-id'] == $match[1]) {
                        $i = $key;
                        break;
                    }
                }


                return 'src="/manage/renderMailAttachment?indent=' .
                    $indent . '&img=' . $i . '" style="max-width: 100%;"
                     class="photo_preview" data-src="/manage/renderMailAttachment?indent=' .
                    $indent . '&img=' . $i++ . '" data-name="' . $i . '"';
            }, $html);
        }


        return ['html' => $html, 'attachments' => $attachments];
    }

    public static function getLogin()
    {
        return self::checkLogin();
    }

    public static function checkLogin()
    {

        if (!empty($_COOKIE['username'])) {
            $username = Aes::decrypt($_COOKIE['username'], COOKIE_PASS);

            if (!empty($username)) {
                return $username;
            }
        }

        return Controllers::redirect('/login');
    }

    public static function createEml($data, $id)
    {
        $content = file_get_contents(AURA_VIEWS_PATH . 'mail_templates/Template.eml');
        $content = str_replace('TEMPLATE_FROM_ADDRESS', $data['from'], $content);
        $content = str_replace('TEMPLATE_TO_ADDRESS', $data['to'], $content);
        $content = str_replace('TEMPLATE_SUBJECT', $data['subject'], $content);
        $content = str_replace('TEMPLATE_BODY', base64_encode($data['body']), $content);
        $attachments = '';
        if (!empty($data['files'][0]['file'])) {
            foreach ($data['files'] as $key => $value) {
                $template = 'Content-Type: application/octet-stream;name="TEMPLATE_ATTACH_FILENAME"
Content-Transfer-Encoding: base64
Content-Disposition: attachment;filename="TEMPLATE_ATTACH_FILENAME"

TEMPLATE_ATTACH_CONTENT

--080107000800000609090108
';
                $file_name = $value['name'];
                $file_content = file_get_contents($value['file']);
                $template = str_replace('TEMPLATE_ATTACH_FILENAME', $file_name, $template);
                $template = str_replace('TEMPLATE_ATTACH_CONTENT', base64_encode($file_content), $template);
                $attachments .= $template;
            }
        }
        $content = str_replace('TEMPLATE_ATTACH_CONTENT', $attachments, $content);
        $path = 'C:/FIFY3/TICKET/' . $id . '/' . $id . '.eml';
        if (!file_exists('C:/FIFY3/TICKET/' . $id)) {
            mkdir('C:/FIFY3/TICKET/' . $id, 0777, true);
        }
        file_put_contents($path, $content);
    }

    //TODO Переделать этот ужас
    public static function renderFilter($name, $filter, $filter_values)
    {
        ?>
        <div class="input-group input-group filter_input col-sm-12 col-md-6 col-xl-4" data-field="<?php echo $name ?>">
            <label class="input-group-addon"
                   for="<?php echo $name ?>"<?php if ($name == "WORKER") { ?> style="font-size: 9px;"<?php }
            ?>><?php echo $filter[0] ?></label>
            <?php if ($filter[2] == 'text') { ?>
            <input type="text" class="form-control filter_page_input" id="<?php echo $name ?>"
                   placeholder="<?php echo $filter[1] ?>">
            <?php } elseif ($filter[2] == 'dropdown') { ?>
                <select class="form-control filter_page_input" id="<?php echo $name ?>" multiple="multiple">
                    <option value=""></option>
                    <?php foreach ($filter_values[$name] as $key => $value): ?>
                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                    <?php endforeach ?>
                </select>
                <script>
                    $("#<?php echo $name ?>").select2({
                        placeholder: "<?php echo $filter[1] ?>",
                        theme: "bootstrap"
                    });
                </script>
            <?php } ?>
            <span class="input-group-btn search_sort_buttons_container">
				<button class="btn btn-default search_page_sort __left" data-order="ASC" type="button"><span
                            class="glyphicon glyphicon-arrow-up"></span></button>
				<button class="btn btn-default search_page_sort __right" data-order="DESC" type="button"><span
                            class="glyphicon glyphicon-arrow-down"></span></button>
			</span>
        </div> <?php
    }



}
