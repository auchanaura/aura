<?php

namespace Aura\Core;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Assetic\AssetManager;
use Assetic\AssetWriter;
use Bramus\Router\Router;
use Katzgrau\KLogger\Logger;
use Ubench;

class Controllers
{
    public $layout;
    public $basepath = AURA_BASE_PATH;
    public $connects = [];
    public $title;
    public $js = [];

    /**
     * Controllers constructor.
     */


    public static function redirect($link)
    {
        header('Location: ' . $link);
    }


    /**
     * @param string $config
     */
    public static function routeRun($config = 'routes.conf.php')
    {

        // Подключаем массив маршрутов
        require_once AURA_CONFIGS_PATH . $config;

        // Создаем роутер
        $router = new Router();

        //Добавляем роутеру маршруты проекта из конфигурации routes.conf.php
        foreach ($auraRoutes as $route) {
            $router->match('GET|POST|PUT|OPTIONS|HEAD', $route['url'], $route['controller']);
        }

        // Запускаем роутер
        $router->run();
    }








    public function render($name, array $attr = [])
    {

        $logger =new Logger(AURA_LOGS_PATH);

        $classname = explode('\\', get_called_class());

        $view_body = AURA_VIEWS_PATH . strtolower($classname[2]) . '/' . $name . '.php';

        foreach ($attr as $key => $value) {
            $$key = $value;
        }
        $scripts = '';


        include AURA_VIEWS_PATH . 'layout/' . $this->layout . '.php';

        if (!empty($this->js)) {
            foreach ($this->js as $key => $value) {
                if ($key === 'plain') {
                    $scripts .= $value . "\r\n";
                } else {
                    $scripts .= file_get_contents(AURA_PUBLIC_PATH . $value) . "\r\n";
                }
            }
            file_put_contents(AURA_PUBLIC_PATH . 'js/minified/' . $name . '.js', $scripts);
            $logger->debug($view_body);
        }


    }
}
