<?php
/**
 * В этом файле прописываются все css и js используемые в проекте
 * путь к каждому файлу.
 * Используется библиотека assetic (kriswallsmith/assetic)
 *
 *
 * User: g.lebedev@auchan.ru
 * Date: 017 17.02.17
 * Time: 12:00
 */

/* Каждый ключ массива $Assets это имя набора ресурсов сгруппированных по какому либо признаку
 * На основе этого имени формируется имя выходного файла.
 * Например для 'vendorcss' получим vendorcss.css.
 * 'css' или 'js' в конце имени обязательны, они формируют расширение файла.
 *
 * Порядок ресурсов в массиве аналогичен тому как бы их подключали напрямую в HTML!
 *
*/
$Assets = [
    'main_vendor_css' =>
        [
            AURA_BASE_PATH . 'vendor/twbs/bootstrap/dist/css/bootstrap.min.css',

            AURA_FRONTEND_PATH . 'css/vendor/jquery-ui.css',
            AURA_FRONTEND_PATH . 'css/vendor/ion.rangeSlider.css',
            AURA_FRONTEND_PATH . 'css/vendor/ion.rangeSlider.skinFlat.css',
            AURA_FRONTEND_PATH . 'css/vendor/bootstrap-slider.min.css',
            AURA_FRONTEND_PATH . 'css/vendor/footable.core.css',
            AURA_FRONTEND_PATH . 'css/vendor/switchery.min.css',
            AURA_FRONTEND_PATH . 'css/vendor/bootstrap-select.min.css',
            AURA_FRONTEND_PATH . 'css/vendor/bootstrap-timepicker.min.css',
            AURA_FRONTEND_PATH . 'css/vendor/sweetalert.css',
            AURA_FRONTEND_PATH . 'css/vendor/core.css',
            AURA_FRONTEND_PATH . 'css/vendor/components.css',
            AURA_FRONTEND_PATH . 'css/vendor/icons_min.css',
            AURA_FRONTEND_PATH . 'css/vendor/pages.css',
            AURA_FRONTEND_PATH . 'css/vendor/responsive.css',
            AURA_FRONTEND_PATH . 'css/vendor/morris.css'
        ],
    'main_css' =>
        [
            AURA_FRONTEND_PATH . 'css/main/aura.css',
            AURA_FRONTEND_PATH . 'css/main/main.css'
        ],
    'main_vendor_js' =>
        [
            AURA_FRONTEND_PATH . 'js/vendor/jquery.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.core.js',
            AURA_FRONTEND_PATH . 'js/vendor/switchery.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-timepicker.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-datepicker.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/notify.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/notify-metro.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-select.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-slider.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-filestyle.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/sweetalert.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/footable.all.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/select2.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/date_format.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery-ui.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/scope.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-table.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/detect.js',
            AURA_FRONTEND_PATH . 'js/vendor/fastclick.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.slimscroll.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.blockUI.js',
            AURA_FRONTEND_PATH . 'js/vendor/waves.js',
            AURA_FRONTEND_PATH . 'js/vendor/wow.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.nicescroll.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.scrollTo.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.peity.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.waypoints.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.counterup.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/raphael-min.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.knob.js',
            AURA_FRONTEND_PATH . 'js/vendor/jquery.app.js',
            AURA_FRONTEND_PATH . 'js/vendor/foo.js'

        ],
    'main_js' =>
        [
            AURA_FRONTEND_PATH . 'js/main/preview.js',
            AURA_FRONTEND_PATH . 'js/main/modal_windows.js'
        ],
    'qko_vendor_js' =>
        [
            AURA_FRONTEND_PATH . 'js/vendor/Chart.bundle.min.js'
        ],
    'reports_vendor_css' =>
        [
            AURA_FRONTEND_PATH . 'css/vendor/bootstrap-table.min.css'
        ],
    'reports_vendor_js' =>
        [
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-table.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/bootstrap-table-export.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/tableExport.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/FileSaver.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/xlsx.core.min.js',
            AURA_FRONTEND_PATH . 'js/vendor/Chart.bundle.min.js'
        ],
    'appeal_js' =>
        [
            AURA_FRONTEND_PATH . 'js/global/resize.js',
            AURA_FRONTEND_PATH . 'js/appeal/full_info.js',
            AURA_FRONTEND_PATH . 'js/global/ticket_select.js',
        ],
    'search_js' =>
        [
            AURA_FRONTEND_PATH . 'js/global/ticket_select.js',
            AURA_FRONTEND_PATH . 'js/global/filter.js',
            AURA_FRONTEND_PATH . 'js/global/resize.js',
            AURA_FRONTEND_PATH . 'js/global/send_curation_spool.js'

        ],
    'curation_js' =>
        [
            AURA_FRONTEND_PATH . 'js/global/resize.js',
            AURA_FRONTEND_PATH . 'js/curation/send_curation.js',
            AURA_FRONTEND_PATH . 'js/curation/see_new.js',
            AURA_FRONTEND_PATH . 'js/curation/menu_for_cur.js',
            AURA_FRONTEND_PATH . 'js/global/ticket_select.js',
            AURA_FRONTEND_PATH . 'js/global/filter.js',
            AURA_FRONTEND_PATH . 'js/global/send_curation_spool.js'

        ],
    'monitor_vendor_css' =>
        [
            AURA_FRONTEND_PATH . 'css/vendor/bootstrap-table.min.css'
        ]

];
