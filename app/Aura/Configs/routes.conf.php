<?php

/*
 * Таблица маршрутов проекта
 * url 2 вида:
 * - Статические маршруты (например /login)
 * - Динамические маршруты с рег. выражениями (/views/site/([_0-9a-z-]+))

* Для статических
 * controller - Класс контроллера и метод для обработки url
 *
 * Для динамических
 *   controller - анонимная функция с параметром $action
 *                запускаем метод $action у нужного контроллера
 * */
$auraRoutes = [
    [
        'url' => '/login($)',
        'controller' => 'Aura\Controllers\Site@login'
    ],
    [
        'url' => '/home($)',
        'controller' => 'Aura\Controllers\Site@home'
    ],
    [
        'url' => '/monitor($)',
        'controller' => 'Aura\Controllers\Site@monitor'
    ],
    [
        'url' => '/quality_improvement($)',
        'controller' => 'Aura\Controllers\Site@qualityImprovement'
    ],
    [
        'url' => '/Site/view_request($)',
        'controller' => 'Aura\Controllers\Site@viewRequest'
    ],
    [
        'url' => '/view_request($)',
        'controller' => 'Aura\Controllers\Site@viewRequest'
    ],
    [
        'url' => '/newrequest($)',
        'controller' => 'Aura\Controllers\Site@newRequest'
    ],
    [
        'url' => '/background($)',
        'controller' => 'Aura\Controllers\Site@background'
    ],
    [
        'url' => '/logout($)',
        'controller' => 'Aura\Controllers\Site@logout'
    ],
    [
        'url' => '/curation($)',
        'controller' => 'Aura\Controllers\Site@curation'
    ],
    [
        'url' => '/search($)',
        'controller' => 'Aura\Controllers\Site@search'
    ],
    [
        'url' => '/timetable($)',
        'controller' => 'Aura\Controllers\Site@timetable'
    ],
    [
        'url' => '/appeal($)',
        'controller' => 'Aura\Controllers\Site@appeal'
    ],
    [
        'url' => '/demand($)',
        'controller' => 'Aura\Controllers\Site@demand'
    ],
    [
        'url' => '/config($)',
        'controller' => 'Aura\Controllers\Site@config'
    ],
    [
        'url' => '/views/site/([_0-9a-z-]+)',
        'controller' =>
            function ($action) {
                $class = new \Aura\Controllers\Site();
                if (method_exists($class, $action)) {
                    $class->$action();
                } else {
                    http_response_code(404);
                }
            }
    ],
    [
        'url' => '/manage/([^/]+)',
        'controller' =>
            function ($action) {
                $class = new \Aura\Controllers\Manage();
                if (method_exists($class, $action)) {
                    $class->$action();
                } else {
                    http_response_code(404);
                }
            }
    ],
    [
        'url' => '/reports/([^/]+)',
        'controller' =>
            function ($action) {
                $class = new \Aura\Controllers\Reports();
                if (method_exists($class, $action)) {
                    $class->$action();
                } else {
                    http_response_code(404);
                }
            }
    ],
    [
        'url' => '/ajax/([^/]+)',
        'controller' =>
            function ($action) {
                $class = new \Aura\Controllers\Ajax();
                if (method_exists($class, $action)) {
                    $class->$action();
                } else {
                    http_response_code(404);
                }
            }
    ],
    [
        'url' => '/new_request($)',
        'controller' => 'Aura\Controllers\Site@newRequest'
    ],

    [
        'url' => '/reports($)',
        'controller' => 'Aura\Controllers\Reports@index'
    ],
    [
        'url' => '/test/action($)',
        'controller' => 'Aura\Controllers\Test@testAction'
    ],
    [
        'url' => '/dump($)',
        'controller' => 'Aura\Controllers\manage@dumpAssets'
    ],
    //Маршрут для всех ненайденых URL
    [
        'url' => '.*',
        'controller' => 'Aura\Controllers\Site@login'
    ]
];
