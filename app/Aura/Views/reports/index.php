<link href="/public/css/reports_vendor_css.css" rel="stylesheet" type="text/css" />
<script src="/public/js/reports_vendor_js.js"></script>

<style>
    .fields_container .input-group {
        float: left;
    }
    .filter_input {
        clear: none!important;
    }
    .bootstrap-table .table, .bootstrap-table .table>tbody>tr>td, .bootstrap-table .table>tbody>tr>th, .bootstrap-table .table>tfoot>tr>td, .bootstrap-table .table>tfoot>tr>th, .bootstrap-table .table>thead>tr>td {
        padding: 4px!important;
        letter-spacing: -0.4px;
        line-height: 130%;
    }
    .table_text {
        max-height: 62px;
        display: block;
        overflow: hidden;
        text-decoration: underline;
        cursor: pointer;
    }
</style>
<div class="row" style="padding: 0 0 0 20px;">
    <div class="col-sm-12 " style="background: #ffffff; padding: 10px 10px 10px 10px;">
        <div class="form-group" >
            <label for="problem_type" class="col-sm-1 control-label">Тип отчета</label>
            <div class="col-sm-4">
                <select class="form-control request" id="request" name="request" placeholder="Отчет">
                </select>
            </div>
            <button class="col-sm-2 btn btn-default btn-lg-p GetReport pull-right">Создать</button>
            <label class="switch autoresresh pull-right" style="margin-right:20px">
                <input type="checkbox" data-plugin="switchery" data-color="#5fbeaa" >
                <!--<div class="switchery switchery-small"></div>-->
            </label>
        </div>
    </div>
    <div class="col-sm-12 fields_container " style="background: #ffffff; padding: 10px 10px 0 5px;">

    </div>
    <div class="col-sm-12" style="background: #ffffff; padding: 0px 10px 10px 10px;">

    </div>
</div>

<div style="padding-top: 10px;">
    <canvas id="canvas" style="display: none;"></canvas>
</div>
<div class="col-sm-12 card-box table-responsive" style="margin-left: 10px;margin-right: 5px; min-height: 0px;">
    <table data-toggle="table"  data-show-export="true" data-toolbar="#demo-delete-row" data-search="true" data-show-toggle="true" data-show-columns="true" data-sort-name="id" data-page-list="[500]" data-page-size="500" data-pagination="true" class="table-bordered table table-striped table-sm" style="margin-top:10px; font-size: 12px;">
        <thead></thead>
        <tbody></tbody>
    </table>
</div>

</div>
<!-- прдключение компанентов-->
<!-- <script src="js\Report_constructor.js"></script>-->
<script>
    var Data_array = [];
    // массив под поля отчета
    var  field_store_array = [];
    // массив под данные отчета
    var action='load';
    // переменная под историю полей
    var chek_field=[];
    //функция сравнения массивов
    diff = function (a1, a2) {
        return a1.filter(i=>a2.indexOf(i)<0)
    }
        // переменная под текущие поля
    var field_table=[];
</script>




<!-- прдключение компанентов-->
<script>
    $(document).ready(function() {
        reports = <?php use Aura\Models\TopicsModel;print_r(json_encode($report_list)); ?>;
        $('.request').change(function(){
            $('.fields_container').html('');
            request_id = $(this).val();
            i = 1;
            $.each(reports,function(key,value){
                if (value['ID'] == request_id) {
                    fields = eval('(' + value['PARAMETRS'] + ')');
                    $.each(fields,function(key, value) {
                        render_field(i,value,key);
                        i++;
                    })
                }
            });
        });

        $(".request").select2();
        function formatResult(node) {
            var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
            return $result;
        };
        var data = [{id:"",text:""},<?php foreach ($report_list as $key => $value) {
            echo '{id: "'.$value['ID'].'", text: "'.$value['NAME'].'", level : 0},';
        } ?>];
        $(".request").select2({
            placeholder: 'Выберите отчет',
            data: data,
            formatSelection: function(item) {
                return 'test';
            },
            formatResult: function(item) {
                return item.text;
            },
            escapeMarkup: function(markup) {
                return markup;
            },
            templateResult: formatResult,
            theme: "bootstrap",
        });

        info = '';
        // функция сохранения конфигурации визуализации в историю и применения ее при обновлении
        function checkFiltr(action) {
            console.log('привести в соответствие');
            // применение фильтров (прятать столбец Sort_Id) и настроек с прошлого построения таблицы
            $('.dropdown-menu label [data-field]').each( function(){
                    var label = this.labels['0'].innerText.trim(); // имя столбца
                    var label_val=this.value; // номер элемента input
                    // перебираем хранилище с загруженными элементами
                                field_store_array.forEach( function(rec){
                                    if (label==rec.name.trim()) {
                            // если еть несоответствие с хранилищем - приводим в соответствие
                            var a = $('.dropdown-menu label [data-field="'+label_val+'"]').is(':checked');
                                        var b = rec.checked;
                            if (a==b){
                                //console.log('соответствие ');
                            } else {
                                //console.log('несоответствие '+ action);
                                if (action=='load') {
                                    //console.log('имулируем клак');
                                    $('.dropdown-menu label [data-field="'+label_val+'"]').click();
                                } else {
                                    if (label=='Sort_Id' && a) {
                                        $('.dropdown-menu label [data-field="'+label_val+'"]').click();
                                    } else {
                                        // console.log('сохраняем в базу '+$('.dropdown-menu label [data-field="'+label_val+'"]').is(':checked'));
                                                    rec.checked = $('.dropdown-menu label [data-field="'+label_val+'"]').is(':checked');
                                                    console.log('результат: '+rec.checked);
                                    }
                                }
                            }
                        }
                    });
                }
            );
        };
        // дествие при нажатии кнопки Создать
        $('.GetReport').click(function(){
            data = [];
            $('.fields').each(function(){
                data.push($(this).val());
            });
            $.ajax({
                method: "post",
                url: "/reports/getReport",
                data: {report: $('.request').val(), data: data}
            }).done(function(data) {
                //console.log('заполнение хранилища историческими данными о таблице '+action);
                        if (action!='load') {
                            checkFiltr(action);
                        }
                // уничтожение таблицы
                $('.table').bootstrapTable('destroy');
                $('.table tbody').html('');
                $('.table thead').html('');
                config.data.datasets = [];
                config.data.labels = [];
                info = JSON.parse(data);
                // создание таблицы заново с новыми данными
                tr = '<tr>';
                $.each(info[0],function(key2,value2){
                    tr +='<th  data-sortable="true" >'+key2+'</th>';
                    field_table.push(key2); // наполнение полями для хранилища
                    if (key2 != 'Группа' && key2 != 'Специалист' && key2 != 'Всего решено') {
                        config.data.labels.push(key2);
                    }
                });
                // применяем полученные поля для хранилища
                console.log('наполнение фильтра');
                // нужно добавить условие сравнение полученных именований столбцов с хранилищем
                chek_field=[];
                        field_store_array.forEach( function(rec){
                                chek_field.push(rec.name);
                    }
                );
                action = diff(chek_field,field_table);
                console.log(action);
                // если хранилище пусто (этот отчет ни разу не загружался)
                        if (field_store_array.length==0 || action!='') {
                    console.log('первый раз');
                    $.each(field_table, function (key2,value2) {
                        if (value2=='Sort_Id') {
                            Data_array.push({
                                name: value2,
                                checked: false,
                                sort: false
                            });
                        } else {
                            Data_array.push({
                                name: value2,
                                checked: true,
                                sort: false
                            });
                        }
                    });
                    //очистка хранилища
                            field_store_array=[];
                            //console.log(field_store_array);
                    //наполнение хранилища
                            Data_array.forEach(function (rec){
                                field_store_array.push(rec);
                            });
                            //console.log(field_store_array);
                } else {
                    // иначе параметр для команды применения настроек визуализации
                    action='load';
                }
                tr += '</tr>';
                $('.table thead').append(tr);
                $.each(info,function(key,value){
                    tr = '<tr>';
                    values = [];

                    $.each(reports,function(key,value){
                        if (value['ID'] == $('.request').val()) {
                            mode = value['MODE'];
                            chart_param = eval('(' + value['CHART_PARAM'] + ')');
                        }
                    });
                    $.each(value,function(key2,value2){
                        if (value2 != null && value2.length >= 230) {
                            tr +='<td><span class="table_text">'+value2+'</span></td>';
                        } else {
                            tr += '<td>'+value2+'</td>';
                        }
                        if (mode >= 2) {
                            if (chart_param['dataset'].indexOf(key2) != -1) {
                                values.push(value2);
                            }
                        }
                    });
                    tr += '</tr>';
                    $('.table tbody').append(tr);
                    if (mode >= 2) {
                        color = randomColor(0.5);
                        config.data.datasets.push({
                            label:value[chart_param['datalabel']]
                            ,fill: false
                            ,data:values
                            ,background:color
                            ,borderColor:color
                            ,backgroundColor:color
                            ,pointBorderColor:color
                            ,pointBackgroundColor:color
                            ,pointBorderWidth:1
                        });
                    }
                });
                $('.table').bootstrapTable();

                // применение фильтров (прятать столбец Sort_Id) и настроек с прошлого построения таблицы
                // console.log('восстановление из истории');
                checkFiltr(action);

                // если необходимо рисование, то рисуем

                if (mode >= 2) {
                    $('#canvas').show();
                    window.myRadar = new Chart(document.getElementById("canvas"), config);
                } else {
                    $('#canvas').hide();
                }
            });



        });
        // функция автообновления
        autorefresh= setInterval(function(){
                if ($('.autoresresh input').is(':checked')) {
                    // вставка под функцию автообновления
                    console.log('автообновление запущено');
                    $('.GetReport').click();
                } else { // если кнопка отжата,
                    console.log('автообновление не запущено');
                }
            }, 300000 // раз в 5 минут (300 секунд)
        );
    });



    function render_field(i,key,name) {
        field = '';
        if (key == 'date') {
            field += '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><input type="text" class="form-control a'+i+' fields" name="data[a'+i+']"></div>';
            $('.fields_container').append(field);
            jQuery('.a'+i+'').datepicker({
                format: "yyyymmdd",
                autoclose: true,
                todayHighlight: true,
            });
        } else if (key == 'groupe') {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><select class="form-control a'+i+' fields filter_page_input filter_page_input_groupe" name="data[a'+i+']" multiple="multiple"></select></div>'
            $('.fields_container').append(field);
            $(".a"+i+"").select2({
                placeholder: 'Выберите группу',
                data: filters_values[2],
                theme: "bootstrap",
            });
        } else if (key == 'spec') {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><select class="form-control a'+i+' fields filter_page_input filter_page_input_groupe" name="data[a'+i+']" multiple="multiple"></select></div>'
            $('.fields_container').append(field);
            $(".a"+i+"").select2({
                placeholder: 'Выберите специалиста',
                data: filters_values[1],
                theme: "bootstrap",
            });
        } else if (key == 'type') {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><select class="form-control a'+i+' fields filter_page_input filter_page_input_groupe" name="data[a'+i+']" multiple="multiple"></select></div>'
            $('.fields_container').append(field);
            $(".a"+i+"").select2({
                placeholder: 'Выберите тип',
                data: [{id:1, text: "Инцидент"}, {id:2, text: "Запрос"}],
                theme: "bootstrap",
            });
        } else if (key == 'site') {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><select class="form-control a'+i+' fields filter_page_input filter_page_input_groupe" name="data[a'+i+']" multiple="multiple"></select></div>'
            $('.fields_container').append(field);
            $(".a"+i+"").select2({
                placeholder: 'Выберите сайт',
                data: filters_values[3],
                theme: "bootstrap",
            });
        } else if (key == 'system') {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><select class="form-control a'+i+' fields filter_page_input filter_page_input_groupe" name="data[a'+i+']" multiple="multiple"></select></div>'
            $('.fields_container').append(field);
            $(".a"+i+"").select2({
                placeholder: 'Выберите систему',
                data: filters_values[4],
                theme: "bootstrap",
            });
        } else {
            field = '<div class="input-group filter_input col-sm-3"><label class="input-group-addon">'+name+'</label><input type="text" class="form-control a'+i+' fields" name="data[a'+i+']"></div>'
            $('.fields_container').append(field);
        }
    }

</script>

<script>
    var randomColorFactor = function() {
        return Math.round(Math.random() * 255);
    };
    var randomColor = function(opacity) {
        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
    };
    var config = {
        type: 'line',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            responsive: true,
            legend: {
                display: false,
                position: 'bottom',
            },
            hover: {
                mode: 'label'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Value'
                    }
                }]
            },
            title: {
                display: false,
                text: 'Тест'
            }
        }
    };
    $.each(config.data.datasets, function(i, dataset) {
        var background = randomColor(0.5);
        dataset.borderColor = background;
        dataset.backgroundColor = background;
        dataset.pointBorderColor = background;
        dataset.pointBackgroundColor = background;
        dataset.pointBorderWidth = 1;
    });
    window.onload = function() {
        //window.myRadar = new Chart(document.getElementById("canvas"), config);
    };
</script>
<script>
    filters_values = [];
    filters_values[1] = [];
    filters_values[2] = [];
    filters_values[3] = [];
    filters_values[4] = []; // заполняем системами
    <?php $used_values = [[],[],[],[]]; ?>
    <?php foreach ($monitor as $key => $value) { ?>
    <?php if (empty($value['WORK_NOW'])) {
        $monitor[$key]['WORK_NOW'] = 0;
        $value['WORK_NOW'] = 0;
    } ?>
    <?php if (!in_array($value['FIO'], $used_values[0])) { ?>
    <?php $used_values[0][] = $value['FIO'] ?>
    filters_values[1].push({'id':"<?php echo rtrim($value['FIO']) ?>",'text':'<?php echo rtrim($value['FIO']) ?>'});
    <?php } ?>
    <?php if (!in_array($value['GROUPE'], $used_values[1])) { ?>
    <?php $used_values[1][] = $value['GROUPE'] ?>
    filters_values[2].push({'id':"<?php echo $value['GROUPE'] ?>",'text':'<?php echo $value['GROUPE'] ?>'});
    <?php } ?>
    <?php } ?>
    <?php foreach ($sites as $key => $value) { ?>
    filters_values[3].push({'id':"<?php echo $value['MAGAZ'] ?>",'text':"<?php echo $value['MAGAZ'] ?>"});
    <?php } ?>
        // создаю и наполняю переменную с инфой по проблемам (хранится вся информация)
        var tree = <?php echo json_encode(TopicsModel::getTreeProblemsList('FIFY3'));?>;
        tree.forEach(function(rec) {
                // если система или корневые узлы, то ложим в фильтр
                if (rec.System== 1 || rec.PER_ID== 0 ) {
                    filters_values[4].push(rec.TOPIC_RUS);

        }
    });
</script>
<script>
    $(document).ready(function(){
        $('.table').on('click','.table_text',function(){
            $(this).removeClass('table_text');
        });
    });
</script>
