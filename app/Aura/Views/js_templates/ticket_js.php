<?php
//Шаблон используется в view_request,curation, search
?>

Templates.ticket = '<?php print_r(str_replace("\r\n", '', file_get_contents(AURA_VIEWS_PATH.'js_templates/ticket.php'))); ?>';
function add_ticket(target,value,user,spread,ik) {
	if (value['CHANGED'] == null) {
		value['CHANGED'] = '';
	}
	template = Templates.ticket;
	if (value.PROBLEM_ID != 0 && user.ROLE == 3) {
		template = template.split('%1%').join('list-group-item-title-success');
	} else {
		template = template.split('%1%').join('');
	}
	if (value.SEE_NEW == 0 && value.LV == 3) {
		template = template.split('%2%').join('color:red');
		template = template.split('%3%').join('label-danger');
		template = template.split('%4%').join('NEW');
	} else {
		template = template.split('%2%').join('');
		template = template.split('%3%').join('');
		template = template.split('%4%').join('');
	}
	if (value.REAL_SITE == '-') {
		template = template.split('%5%').join('<span class="ticket_site" >(' +value.USER_SITE+')');
	} else {
		template = template.split('%5%').join('<span class="ticket_site" style="color:black;">('+value.REAL_SITE+')');
	}
	if (value.WORKER != '-') {
		template = template.split('%WORKER%').join(value.WORKER);
	} else {
		template = template.split('%WORKER%').join('');
	}
	if (value.PHYSICAL == 1) {
		template = template.split('%6%').join('checked');
	} else {
		template = template.split('%6%').join('');
	}
	if (value.CIM == 1) {
		template = template.split('%7%').join('btn-danger');
	} else {
		template = template.split('%7%').join('btn-link');
	}
	if (user.ADMIN != 0 && getUrlParameter('mode') != 6 && getUrlParameter('mode') != 4) {
		template = template.split('%disabled%').join('');
	} else {
		template = template.split('%disabled%').join('disabled');
	}
	if (getUrlParameter('mode') != 6 ) {
		string = '<div class="btn-group';
		if (ik < 0 ) {
			string += ' dropup ';
		}
		string += '"  style="width: 100px; margin-top:27px"><button type="button" style="width: 100%; font-size: 11px" class="manage_list_button btn ';
		if (value.KERNEL_JOB_TIME_LIMIT != 0 && value.LV != 0) {
			string += 'btn-success ';
		} else {
			string += 'btn-warning ';
		}
		string +='dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Управление <span class="caret"></span></button><ul class="dropdown-menu manage_list manage_list_left"></ul></div>';
		template = template.split('%CONTORLS%').join(string);
	} else {
		template = template.split('%CONTORLS%').join('');
	}

	SPREAD0 = '';
	$.each(spread[0],function(key2, value2){
		if (key2 == value.CIM) {
			SPREAD0 += '<option selected value="'+ key2 +'">'+ value2 +'</option>';
		} else {
			SPREAD0 += '<option value="'+ key2 +'">'+ value2 +'</option>';
		}
	});
	template = template.split('%SPREAD0%').join(SPREAD0);

	SPREAD1 = '';
	$.each(spread[1],function(key2, value2){
		if (key2 != 3) {
			if (key2 == value.LV) {
				SPREAD1 += '<option selected value="'+ key2 +'">'+ value2 +'</option>';
			} else {
				SPREAD1 += '<option value="'+ key2 +'">'+ value2 +'</option>';
			}
		}
	});
	if (value.LV == 3 && value.REAL_SITE == '-') {
		SPREAD1 += '<option selected value="'+ value.USER_SITE +'">'+ value.USER_SITE +'</option>';
	} else if (value.LV == 3) {
		SPREAD1 += '<option selected value="'+ value.REAL_SITE +'">'+ value.REAL_SITE +'</option>';
	}
	template = template.split('%SPREAD1%').join(SPREAD1);

	SPREAD2 = '';
	ij = 0;
	$.each(spread[2],function(key2, value2){
		if (value2*1 == value.KERNEL_JOB_TIME_LIMIT) {
			ij = 1;
			SPREAD2 += '<option selected value="'+ value2 +'">'+ value2 +' минут</option>';
		} else {
			SPREAD2 += '<option value="'+ value2 +'">'+ value2 +' минут</option>';
		}
	});
	if (!ij && value.KERNEL_JOB_TIME_LIMIT != 0) {
			SPREAD2 += '<option selected value="'+ value.KERNEL_JOB_TIME_LIMIT +'">'+ value.KERNEL_JOB_TIME_LIMIT +' минут</option>';
	}
	template = template.split('%SPREAD2%').join(SPREAD2);

	if (typeof(value.history) != 'undefined') {
	template = template.split('%loaded%').join('loaded');
	} else {
		template = template.split('%loaded%').join('');
	}

	template = template.split('%NUMBER%').join(value.NUMBER);
	template = template.split('%STATUS_TITLE%').join(value.STATUS_TITLE);
	template = template.split('%STATUS_GLOBAL%').join(value.STATUS_GLOBAL);
	template = template.split('%STATUS0_FORMATTED%').join(value.STATUS0_FORMATTED);
	template = template.split('%TICKET_DESCRIPTION_LOW%').join(value.TICKET_DESCRIPTION_LOW);
	template = template.split('%USER_FIO%').join(value.USER_FIO);

	target.append(template);
}
