<li class="list-group-item curator_i %1% %loaded%" data-number="%NUMBER%">
	<div class="tb_container">
		<div class="ticket_body" >	
			<div style="width: 76px; float: left; margin-top: -2px;%2%">
					<span class="number_desc" >%NUMBER%</span>
			</div>
				<div style="margin-left: 80px;%2%">
					<span class="ticket_title">%STATUS_TITLE%</span>
				</div>
				<span class="ticket_date" style="float: right; font-size: 10px; margin-top: -16px;">
				%STATUS0_FORMATTED%
				</span>
			<div class="action_icon_container">
				<span class="label %3%" style="display: none; float:left;">%4%</span>
				<img style="width: 100%;" class="media-object action_icon_image" src="public/img/ticket/%STATUS_GLOBAL%.png">
			</div>
			<div class="ticket_description" style="clear:both;">%TICKET_DESCRIPTION_LOW%</div>
			<div style="line-height: 120%;">
				<span class="ticket_fio">%USER_FIO%</span>
				%5%	
				<span class="kernel_last_time">
					
				</span>
			</div>
			<span class="worker_name" style="font-weight: bold;float: right; font-size: 10px; margin-top: -16px;">
			%WORKER%
			</span>
		</div>
		<div class="tuco">
			<div class="col-sm-6" style="padding: 2px 5px;">
				<span class="checkbox checkbox-pink" style="margin-top: 0px; margin-bottom: 2px; font-size: 12px; ">
					<input id="phys[%NUMBER%]" name="PHYSICAL" value="1" class="phys ticket_update_options" type="checkbox" %6% %disabled%>
					<label for="phys[%NUMBER%]" class="mini_checkbox">Физическая</label>
				</span>
				%CONTORLS%				
			</div>
			<div class="col-sm-6" style="padding: 0px 8px 0px 5px;">
				<div style="margin-bottom: 2px;margin-top: -1px" class="ticket_update_container">
						<select name="CIM" class="form-control &7& dropdown-toggle waves-effect waves-light ticket_update_options btn-xs" %disabled%>
						<option>---</option>
						%SPREAD0%
					</select>
				</div>
				<div style="margin-bottom: 2px;" class="ticket_update_container">
					<select name="LV" class="form-control btn-link dropdown-toggle waves-effect waves-light btn-xs ticket_update_options ticket_update_options_select2" %disabled%>
						<option>Группа</option>
						%SPREAD1%
					</select>
				</div>	
				<div class="ticket_update_container">
					<select name="KERNEL_JOB_TIME_LIMIT" class="form-control btn-link dropdown-toggle waves-effect waves-light  ticket_update_options" %disabled%>
						<option>Время</option>
						%SPREAD2%
					</select>
				</div>
			</div>
			
			
		</div>
	</div>
	<div class="detailed_info">
	</div>
</li>
