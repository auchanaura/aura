<?php
//Шаблон используется в view_request,curation, search
 ?>
	Templates = {};
	Templates.event = '<?php  print_r(str_replace("\r\n", '', file_get_contents(AURA_VIEWS_PATH.'js_templates/event.php'))); ?>';
	function event(target,value,fields = [1,2,3,11,4,5,6,7,8,9,10,'img'], mode = 'append') {
		if (value['CHANGED'] == null) {
			value['CHANGED'] = '';
		}
		template = Templates.event;
		if (fields.indexOf(1) != -1) {
			if (value['ACTION_NAME']!='Жалоба на действие') {template = template.replace('%1%',value['OWNER'])} else {template = template.replace('%1%','')};
		} else {
			template = template.replace('%1%','');
		}
		if (fields.indexOf(2) != -1) {
			template = template.replace('%2%',value['ACTION_NAME']);
		} else {
			template = template.replace('%2%','');
		}
		if (fields.indexOf(3) != -1) {
			template = template.replace('%3%',value['COMMENT_TECH']);
		} else {
			template = template.replace('%3%','');
		}
		if (fields.indexOf(11) != -1) {
			if (value['COMMENT_USERMAIL'] == null) {
				value['COMMENT_USERMAIL'] = '';
			}
			if (fields.indexOf(3) != -1) {
				if (value['COMMENT_USERMAIL'] != value['COMMENT_TECH'] && value['COMMENT_USERMAIL'] != '') {
					value['COMMENT_USERMAIL'] = '<br>Ответ пользователю:<br>' + value['COMMENT_USERMAIL'];
				} else {
					value['COMMENT_USERMAIL'] = '';
				}
			}
			template = template.replace('%11%',value['COMMENT_USERMAIL']);
		} else {
			template = template.replace('%11%','');
		}
		if (fields.indexOf(4) != -1) {
			template = template.replace('%4%',value['DATETIME']);
		} else {
			template = template.replace('%4%','');
		}
		if (fields.indexOf(5) != -1) {
			template = template.replace('%5%',value['NUMBER']);
		} else {
			template = template.replace('%5%','');
		}
		if (fields.indexOf(6) != -1) {
			template = template.replace('%6%',value['ACTION_NUMBER']);
		} else {
			template = template.replace('%6%','');
		}
		if (fields.indexOf(7) != -1) {
			template = template.replace('%7%',value['TIME_SPEND']);
		} else {
			template = template.replace('%7%','');
		}
		if (fields.indexOf(8) != -1) {
			template = template.replace('%8%',value['COMPLAINT']);
		} else {
			template = template.replace('%8%','');
		}
		if (fields.indexOf(9) != -1) {
			template = template.replace('%9%',value['CHANGED']);
		} else {
			template = template.replace('%9%','');
		}
		if (fields.indexOf(10) != -1) {
			template = template.replace('%10%',value['ACTION_NUMBER']);
		} else {
			template = template.replace('%10%','');
		}
		template = template.replace('%img%',value['ACTION_CODE']);
		if (mode == 'append') {
			target.append(template);
		} else {
			target.prepend(template);
		}
}
