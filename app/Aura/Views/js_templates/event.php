<li class="list-group-item" data-action="%10%">
	<div class="action_icon_container">
		<img class="media-object action_icon_image" src="/public/img/action/%img%.png">
	</div>
	<div style="margin-left: 35px; overflow: hidden; ">
		<div class="list-group-item-heading" style="margin-top:-3px">
			<div style="font-size: 10px; vertical-align:middle; text-align:right; float: right;">
				<div>%4%</div>
				<div class="pull-right" style="font-size: 10px; margin-top:2px;    margin-bottom: 5px; color:#89a397;">%1%</div>
			</div>
			<div style="font-size: 11px; margin-left: 10px; font-weight: bold; text-align:left">%2%</div>
		</div>
		<p style="font-size: 11px;text-align:left; margin-left: 10px; color:#a6a6a6;white-space: normal; min-height: 7px; margin-bottom: 5px;">%9%</p>
		<div class="list-group-item-text" style="font-size: 11px;">%3%%11%</div>
		<div>
			<span class="pull-left" style="font-size: 11px; margin-left: 10px; color:#907f27" >%7%</span>
			<a class="pull-right open_modal_window" style="font-size: 11px;" data-window="complain" data-number="%5%" data-actionnumber="%6%">%8%</a>
		</div> 		
	</div>
</li>
