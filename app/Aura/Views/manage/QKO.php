<?php
$headers = ['Number'=>'Номер','Group'=>'Группа','Comments (Action)'=>'Комментарий',"[Groupe de l'action Origine (test JD)]"=>'Группа','Support Person'=>'Поддержка','Owner group of the ticket'=>'Владелец группы','Owner'=>'Владелец', 'Incident: Creation Date'=>'Дата создания','Priority'=>'Приоритет','Incident: Available Field 1'=>'Поле 1','Urgency'=>'Urgency','Impact'=>'Impact','SLA'=>'SLA','Support Person'=>'Поддержка','Real topic (Full)'=>'Топик'];
 ?>
 <style type="text/css">
 .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top {
 	margin-top: 58px;
 }
 </style>
 <?php $topics = []; ?>

 <div class="input-group input-group filter_input col-sm-12 col-md-6 col-xl-6" >
	<label class="input-group-addon" for="comment_select">Комментарий</label>
	<select class="form-control charts_filter" multiple="multiple" id="comment_select">
		<option value="TN">TN - Нетипичная</option>
		<option value="TY">TY - Типичная</option>
		<option value="KN">KN - Нет инструкции</option>
		<option value="KY">KY - Есть инструкция</option>
		<option value="QOK;">QOK - Качество соблюдено</option>
		<option value="QKO1">QKO1 - Неверная квалификация тикета (тип, топик и тд)</option>
		<option value="QKO2">QKO2 - Неверная квалификация важности тикета</option>
		<option value="QKO3">QKO3 - Отсутствие необходимых и достаточных данных для решения</option>
		<option value="QKO4">QKO4 - Копирование текста заявки в тело тикета</option>
		<option value="QKO5">QKO5 - Выполнение тикета в компетенции ЦП</option>
		<option value="QKOi">QKOi - Факультативно. Ответ по тикету содержит информацию, которая может быть добавлена в БЗ ЦП для дальнейшего использования.</option>
		<option value="RUE">RUE - Ошибка пользователя</option>
		<option value="RSE">RSE - Системная Ошибка</option>
		<option value="RLK">RLK - Мало знаний</option>
		<option value="RSC">RSC - Ошибка ЦП</option>
		<option value="RFE">RFE - Ошибка Французской команды</option>
		<option value="RUR">RUR - Запрос пользователя на измения, не доступные самому пользователю</option>
	</select>
	<script>
		$("#comment_select").select2({
			placeholder: "Комментарий",
			theme: "bootstrap"
		});
	</script>
</div>
<div class="form-group col-sm-12 col-md-6 col-xl-6">
	<label class="control-label col-sm-2" style="font-weight: bold;
    font-size: 11px;
    padding: 9px;
    text-align: right;">Период</label>
	<div class="col-sm-10">
		<div class="input-daterange input-group" id="date-range">
			<input type="text" class="form-control charts_filter start" name="start" />
			<span class="input-group-addon bg-custom b-0 text-white">to</span>
			<input type="text" class="form-control charts_filter end" name="end" />
		</div>
	</div>
</div>
<div class="input-group input-group filter_input col-sm-12 col-md-6 col-xl-6" >
	<label class="input-group-addon" for="comment_select">Специалист</label>
    <input class="form-control charts_filter spec" name="spec"/>
</div>
<script>
	jQuery('#date-range').datepicker({
        toggleActive: true,
        format: "yyyy-mm-dd",

    });
</script>
<div style=" overflow: hidden; clear: left; margin-bottom: 10px;">
	<div class="radar_container2" style="width:50%; float: left;">
	    <canvas id="canvas2"></canvas>
	</div>
	<div class="radar_container" style="width:50%; float: left;">
	    <canvas id="canvas"></canvas>
	</div>
</div>
<div class="col-sm-12" style="overflow: auto;">
	<table class="table table-striped table-bordered" style="font-size: 10px;">
		<thead>
			<?php foreach ($headers as $key => $value): ?>
				<th><?php echo $value ?></th>
			<?php endforeach ?>
		</thead>
		<tbody>
			
		</tbody>
	</table>
</div>
<script src="/vendor/nnnick/chartjs/dist/Chart.bundle.min.js"></script>
<script>
	qko = <?php echo json_encode($qko) ?>;
</script>
<script>
	var config = {
        type: 'radar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Chart.js Radar Chart'
            },
            scale: {
              reverse: false,
              ticks: {
                beginAtZero: true
              }
            }
        }
    };
    var config2 = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [],
                backgroundColor: ["#F7464A","#F7464A","#F7464A","#F7464A","#F7464A"],
                label: 'Dataset 3'
            },{
                data: [],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                    "#FDB45C",
                    "#949FB1",
                    "#4D5360",
                ],
                label: 'Dataset 1'
            }],
            labels: []
        },
        options: {
            responsive: true,
            legend: {
            	display: false,
                position: 'top',
            },
            title: {
                display: false,
                text: 'Chart.js Doughnut Chart'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
</script>
<script>
	topics = {};
	colors = {'Коммерция':"#F7464A",'Инфраструктура':"#46BFBD",'Финансы':"#FDB45C",'Пусто':"#949FB1",'Логистика':"#4D5360"};
	function updateCharts() {
		topics = {};
		table = '';
		config.data.labels = [];
		config.data.datasets = [{backgroundColor:"rgba(121,57,57,0.3)",pointBackgroundColor:"rgba(121,57,57,1)",data:[],label:"---"}];
		config2.data.labels = [];
		config2.data.datasets = [{backgroundColor:['','','','',''],data:[0,0,0,0,0]},{backgroundColor:[],data:[]}];
		$.each(qko,function(key,value) {
			skip = 0;
			comment = value['Comments (Action)'];
			if ($('#comment_select').val() != null) {
				$.each($('#comment_select').val(),function(key2,value2){
					if (comment == null || comment.indexOf(value2) == -1) {
						skip = 1;
					}
				});
			}
			if ($('.spec').val() != '') {
				if (value['Owner'].toUpperCase().indexOf($('.spec').val().toUpperCase()) == -1) {
					skip = 1;
				}
			}
			if ($('.start').val() != '') {
				if (value['Incident: Creation Date'] < $('.start').val()) {
					skip = 1;
				}
			}
			if ($('.end').val() != '') {
				if (value['Incident: Creation Date'] > $('.end').val()) {
					skip = 1;
				}
			}
			if (skip == 1) {
				return true;
			}
			if (config.data.labels.indexOf(value['Owner']) == -1) {
				config.data.labels.push(value['Owner']);
				config.data.datasets[0].data.push(1);
			} else {
				config.data.datasets[0].data[config.data.labels.indexOf(value['Owner'])]++;
			}
			if (value['Real topic (Full)'] == 'null' || value['Real topic (Full)'] == null || value['Real topic (Full)'] == 'undefined') {
				value['Real topic (Full)'] = 'Пусто/Пусто';
			}
			topic = value['Real topic (Full)'].split('/');
			if (typeof(topics[topic[0]]) == 'undefined') {
				topics[topic[0]] = {count:1,sections:{}};
			} else {
				topics[topic[0]].count++;
			}
			if (typeof(topics[topic[0]].sections[topic[1]]) == 'undefined') {
				topics[topic[0]].sections[topic[1]] = 1;
			} else {
				topics[topic[0]].sections[topic[1]]++;
			}
			table += '<tr><td>'+value['Number']+'</td><td>'+value['Group']+'</td><td>'+value['Comments (Action)']+'</td><td>'+value['[Groupe de l\'action Origine (test JD)]']+'</td><td>'+value['Support Person']+'</td><td>'+value['Owner group of the ticket']+'</td><td>'+value['Owner']+'</td><td>'+value['Incident: Creation Date']+'</td><td>'+value['Priority']+'</td><td>'+value['Incident: Available Field 1']+'</td><td>'+value['Urgency']+'</td><td>'+value['Impact']+'</td><td>'+value['SLA']+'</td><td>'+value['Real topic (Full)']+'</td></tr>';
		});
		$('.table tbody').html(table);
		$.each(topics,function(key,value){
			config2.data.labels.push(key);
			config2.data.datasets[1].data.push(value.count);
			config2.data.datasets[1].backgroundColor.push(colors[key]);
		});
		$.each(topics,function(key2,value){
			$.each(value.sections,function(key,value){
				config2.data.labels.push(key);
				config2.data.datasets[0].data.push(value);
				config2.data.datasets[0].backgroundColor.push(colors[key2]);
			});
		});
		window.myRadar.update();
		window.myRadar2.update();
	}
	window.onload = function() {
        window.myRadar = new Chart(document.getElementById("canvas"), config);
        window.myRadar2 = new Chart(document.getElementById("canvas2"), config2);
		updateCharts();
    };

	$('.charts_filter').change(function(){
		updateCharts();
	});
</script>
