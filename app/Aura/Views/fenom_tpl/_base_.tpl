<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        {block 'title'}{/block}
    </title>
    {block 'css'}
        <link href="/public/css/bootstrap.min.css" rel="stylesheet"/>
    {/block}
</head>
<body>
{block 'topbar-left'}
    {insert 'topbar-left.tpl'}
{/block}
{block 'content'}
{/block}

</body>
<footer>
    {block 'js'}

        
        <script src="/public/js/vendor.js?{$version}"></script>

    {/block}
</footer>
</html>
