<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title><?php use Aura\Core\Core;
        use Aura\Models\RequestModel;

        echo $this->title ?></title>

    <link rel="shortcut icon" href="/public/img/favicon_1.ico">


    <link href="/public/css/main_vendor_css.css?verison=<?php echo AURA_VERSION ?>" rel="stylesheet">

    <!-- select2.css подключаем отдельно, конфликтует с остальными vendor.css -->
    <link href="/public/css/select2.min.css" rel="stylesheet">


    <link href="/public/css/main_css.css?verison=<?php echo AURA_VERSION ?>" rel="stylesheet">

    <script src="/public/js/main_vendor_js.js?verison=<?php echo AURA_VERSION ?>"></script>











</head>

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper" class="forced enlarged">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="" class="logo"><img alt="AURA" height="20px"
                                             style="margin-top:-13px;margin-left:15px;margin-right:40px;"
                                             src="/public/img/logo_aura_min2.png"></a>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation" style="background-color:#777296; height:40px">
            <div class="container">
                <div class="">
                    <!--<div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="ion-navicon"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>-->

                    <form role="search" class="navbar-left app-search pull-left hidden-xs">
                        <input type="text" placeholder="Поиск..." class="form-control search" name="search"
                               style="background-color:#EBEFF2;margin-left:40px; color:black;">
                        <a class="search_button"><i class="fa fa-search search_button"></i></a>
                    </form>


                    <div class="search_results" style="display: none; z-index: 999999">
                        <span class="preview_close_small">×</span>
                        <ul class="search_ticket_template" style="display: none;">
                            <li class="search_ticket">
                                <a class="search_ticket_link" href="/search?request=%request%&number=%NUMBER%"
                                   target="_blank">
                                    <div class="col-sm-1" style="padding: 0; text-align: center;"><img
                                                src="/public/img/ico/ic_copy2.png"></div>
                                    <div class="col-sm-3"
                                         style="line-height: 21px; padding-left: 5px; font-size: 13px; white-space: nowrap; overflow: hidden;">
                                        <div>%USER_FIO%</div>
                                        <div>%USER_MAIL%</div>
                                    </div>
                                    <div class="col-sm-6"
                                         style="padding-right: 5px;max-height: 42px; overflow: hidden;">
                                        <div>%TICKET_DESCRIPTION%</div>
                                    </div>
                                    <div class="col-sm-2" style="line-height: 21px; text-align: right;">
                                        <div>%NUMBER%</div>
                                        <div>%CREATE_DATE%</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="search_results_container">
                            <div class="src_header" data-type="NUMBER">По номеру заявки: <span
                                        class="label label-purple" style="position: absolute;right: 20px;"></span></div>
                            <ul class="search_results_list" data-type="NUMBER" style="display: none;"></ul>
                            <div class="src_header" data-type="USER_LOGIN">По логину пользователя: <span
                                        class="label label-purple" style="position: absolute;right: 20px;"></span></div>
                            <ul class="search_results_list" data-type="USER_LOGIN" style="display: none;"></ul>
                            <div class="src_header" data-type="USER_MAIL">По адресу почты заявителя: <span
                                        class="label label-purple" style="position: absolute;right: 20px;"></span></div>
                            <ul class="search_results_list" data-type="USER_MAIL" style="display: none;"></ul>
                            <div class="src_header" data-type="TICKET_DESCRIPTION">По описанию заявки: <span
                                        class="label label-purple" style="position: absolute;right: 20px;"></span></div>
                            <ul class="search_results_list" data-type="TICKET_DESCRIPTION" style="display: none;"></ul>
                            <div class="src_header" data-type="OTHER">Прочие: <span class="label label-purple"
                                                                                    style="position: absolute;right: 20px;"></span>
                            </div>
                            <ul class="search_results_list" data-type="OTHER" style="display: none;"></ul>
                        </div>
                    </div>

                    <ul class=  "nav navbar-nav navbar-right pull-right">

                        <li class="dropdown hidden-xs action_history">
                            <a href="#" data-target="#" class="waves-effect waves-light ">
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-lg "
                                style="width: 50vw; max-height:500px;overflow:auto">
                                <!--<button type="button" class="btn btn-primary waves-effect waves-light btn-xs" style="float: right" >Добавить новость</button>-->
                                <li class="notifi-title">История моих действий</li>
                                <table id="foo-history" class="table m-b-0 toggle-arrow-tiny" data-page-size="50">
                                    <thead>
                                    <tr>
                                        <th data-type="numeric" data-toggle="true" width="80"
                                            style="color:black;font-size:12px"> Время
                                        </th>
                                        <th data-toggle="true" style="color:black;font-size:12px"> Номер</th>
                                        <th data-toggle="true" style="color:black;font-size:12px"> Действие</th>
                                        <!--<th style="color:black;font-size:12px"></th>-->
                                        <th data-hide="all" style="color:black;font-size:12px"> Затрачено</th>
                                        <th data-hide="all" style="color:black;font-size:12px"> Комментарий</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </ul>
                        </li>

                        <li class="dropdown hidden-xs news">
                            <a href="#" data-target="#" class="waves-effect waves-light ">
                                <i class="glyphicon glyphicon-bullhorn"></i> <span
                                        class="badge badge-xs badge-danger"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-lg "
                                style="width: 50vw; max-height:500px;overflow:auto">
                                <!--<button type="button" class="btn btn-primary waves-effect waves-light btn-xs" style="float: right" >Добавить новость</button>-->
                                <li class="notifi-title">Новости
                                    <div class="button-list empty" data-toggle="buttons" style="float:right">
                                        <label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
                                            <input type="radio" value="open" name="news_type" id="news_open" checked>Открытые
                                        </label>
                                        <label class="btn btn-inverse btn-xs btn-custom waves-effect waves-light">
                                            <input type="radio" value="close" name="news_type" id="news_close">Закрытые
                                        </label>
                                    </div>

                                </li>
                                <table id="demo-foo-accordion" class="table m-b-0 toggle-arrow-tiny"
                                       data-page-size="50">
                                    <thead>
                                    <tr>
                                        <th data-type="numeric" data-toggle="true" width="80"
                                            style="color:black;font-size:12px"> Дата
                                        </th>
                                        <th data-toggle="true" style="color:black;font-size:12px"> Сайт</th>
                                        <th data-toggle="true" style="color:black;font-size:12px"> Описание</th>
                                        <th style="color:black;font-size:12px"></th>
                                        <th data-hide="all" style="color:black;font-size:12px"> Инициатор</th>
                                        <th data-hide="all" style="color:black;font-size:12px"> Обращение AURA</th>
                                        <th data-hide="all" style="color:black;font-size:12px"> Ивент</th>
                                        <th data-hide="all" style="color:black;font-size:12px"> Сайт</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </ul>

                        </li>

                        <!-- <li class="hidden-xs">
                              <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a><br>
                          </li> -->

                        <li class="dropdown">


                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img
                                        src="<?php $filename = AURA_BASE_PATH . 'public/img/profile/' . Aes::decrypt($_COOKIE['username'], COOKIE_PASS) . '.jpg';
                                        if (file_exists($filename)) {
                                            echo($filename);
                                        } else {
                                            echo('/public/img/ico/bellboy.png');
                                        } ?> " alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <div>
                                    <small class="font-600"
                                           style="margin-left:20px;margin-bottom:15px;"><?php echo Aes::decrypt($_COOKIE['name'], COOKIE_PASS); ?></small>
                                </div>
                                <li><a href="/home"><i class="icon-info m-r-5"></i> Свои заявки</a></li>
                                <li><a href="#" class="break_activate"><i class=" icon-cup m-r-5"></i> Перерыв
                                        (<?php echo floor($_COOKIE['dinner'] / 60); ?> мин.)</a></li>
                                <li><a href="/logout"><i class="ti-power-off m-r-5"></i> Выход</a></li>
                            </ul>
                        <li  class="badge badge-info"  ><?php echo RequestModel::getCurrentNumberOfUserAction(); ?></li>
                        </li>


                    </ul>

                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ======= Left Sidebar Start ======= -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <!--<li class="text-muted menu-title">Navigation</li>-->
                    <?php
                    $links = [];
                    $links[] = ['link' => '/search', 'symbol' => 'icon-magnifier', 'text' => 'Поиск'];
                    $links[] = ['link' => '/view_request', 'symbol' => ' icon-fire', 'text' => 'Рабочий процесс', 'condition' => ['ROLE' => ['99', 0]]];
                    $links[] = ['link' => '/curation?mode=5', 'symbol' => ' icon-layers', 'text' => 'Мои заявки', 'condition' => ['GROUPE' => ['МГ - ЦО Москва', 1]]];
                    $links[] = ['link'=>'/curation?mode=10','symbol'=>' icon-layers','text'=>'Мои заявки','condition'=>['GROUPE'=>['АТАК',1]]];
                    $links[] = ['link' => '/curation', 'symbol' => ' icon-eye', 'text' => 'Курирование', 'condition' => ['ROLE' => ['99', 0]]];
                    $links[] = ['link' => '/timetable', 'symbol' => ' icon-calender', 'text' => 'Расписание отдела', 'condition' => ['ROLE' => ['99', 0]]];
                    $links[] = ['link' => '/monitor', 'symbol' => ' icon-speedometer', 'text' => 'Монитор нагрузки', 'condition' => ['ROLE' => ['99', 0]]];
                    $links[] = ['link' => '/reports', 'symbol' => ' icon-chart', 'text' => 'Статистика'];
                                $links[] =  ['link'=>'/config','symbol'=>' icon-eye','text'=>'Администрирование'];
                    ?>
                    <?php echo Core::leftMenu($links); ?>
                    <li>
                        <a class="waves-effect open_modal_window" data-window="new_request" >
                            <i class=" icon-plus"></i>
                            <span> "Создать заявку</span>
                        </a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ========================================== -->
    <!-- Start right Content here -->
    <!-- ========================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <?php include($view_body); ?>

        </div> <!-- content -->
    </div>
</div>
<!-- END wrapper -->

<script src="/public/js/main_js.js?verison=--><?php echo AURA_VERSION ?><!--"></script>
<script>
    $(document).ready(function () {
        $('.search_button').click(function () {
            Search();
        });
        $('.app-search').keydown(function (e) {
            if (e.which == 13) {
                Search();
            }
        });
        $('.app-search').submit(function (e) {
            Search();
            return false;
        });
        $('.preview_close_small').click(function () {
            $('.search_results').hide();
        });
        $('.src_header').click(function () {
            $('.search_results_list[data-type="' + $(this).data('type') + '"]').toggle();
        });
    });
    function Search() {
        $.ajax({
            method: "POST",
            url: "/ajax/search",
            data: {search: $('.search').val()}
        }).done(function (data) {
            data = JSON.parse(data);
            $('.search_results').show();
            template = $('.search_ticket_template').html();
            $('.search_results_list').html('');
            $('.borderless').removeClass('borderless');
            $.each(data, function (key, value) {
                ticket = template;
                ticket = ticket.split('%NUMBER%').join(value['NUMBER']);
                ticket = ticket.replace('%TICKET_DESCRIPTION%', value['TICKET_DESCRIPTION_LOW']);
                ticket = ticket.replace('%CREATE_DATE%', value['CREATE_DATE']);
                ticket = ticket.replace('%USER_MAIL%', value['USER_MAIL']);
                ticket = ticket.replace('%USER_FIO%', value['USER_FIO']);
                ticket = ticket.replace('%request%', $('.search').val());
                //console.log(value);
                $('.search_results_list[data-type="' + value['group'] + '"]').show();
                $('.src_header[data-type="' + value['group'] + '"]').show();
                $('.search_results_list[data-type="' + value['group'] + '"]').append(ticket);
            });
            //$('.src_header[style*="display: block"]').first().addClass('borderless');
            $('.search_results_list').each(function () {
                $('.src_header[data-type="' + $(this).data('type') + '"] .label.label-purple').html($(this).find('li').length);
            });
        });
    }
</script>
<script>

    $('document').ready(function () {
        date = localStorage.news_date
        if (typeof(localStorage.news_date) == 'undefined') {
            date = '20160701 10:00:00';
        }
        $.ajax({
            method: "POST",
            data: {date: date},
            url: "/ajax/freshNews",
        }).done(function (data) {
            if (data > 0) {
                $('.news .badge').html(data);

            }
        });
    });
    function NewNews() {
        $.ajax({
            method: "POST",
            url: "/ajax/news",
        }).done(function (data) {
            data = JSON.parse(data);
            console.log(data);
            date = new Date();
            localStorage.setItem('news_date', date.format('yyyymmdd H:mm:ss'));

            $.each(data, function (key, value) {
                var now = new Date()
                var day = new Date(value['DATETIME']);
                var today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf()
                var other = day.valueOf()
                if (other < today) {
                    show = day.format("dd.mm.yy");
                } else {
                    show = day.format("HH:MM");
                }
                var labes;
                switch (value['STATUS']) {
                    case 0:
                        labes = '<span class="label label-default">Нормальная</span>';
                        break;
                    case 1:
                        labes = '<span class="label label-warning">Высокая</span>';
                        break;
                    case 2 :
                        labes = '<span class="label label-danger">Крит</span>';
                        break;
                    case 3 :
                        labes = '<span class="label label-default">Инфо</span>';
                        break;
                }
                $('.news tbody').append('<tr><td data-value="' + day.getTime() + '" style="color:black;font-size:12px; ">' + show + '</td><td style="color:black;font-size:12px">' + value['SIT'] + '</td><td style="color:black;font-size:12px">' + value['NEWS'] + '</td><td>' + labes + '</td><td style="color:black;font-size:12px">' + value['OWNER'] + '</td><td style="color:black;font-size:12px">' + value['TICKET'] + '</td><td style="color:black;font-size:12px">' + value['IVENT'] + '</td><td style="color:black;font-size:12px">' + value['SIT'] + '</td></tr>');
            });
            $(window).resize();
            $('#demo-foo-accordion').trigger('footable_redraw').trigger('footable_redraw');
        });
    }
    function OldNews() {
        $.ajax({
            method: "POST",
            url: "/ajax/oldNews",
        }).done(function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, value) {
                var day = new Date(value['DATETIME']);
                var labes;
                switch (value['STATUS']) {
                    case 0:
                        labes = '<span class="label label-default">Нормальная</span>';
                        break;
                    case 1:
                        labes = '<span class="label label-warning">Высокая</span>';
                        break;
                    case 2 :
                        labes = '<span class="label label-danger">Крит</span>';
                        break;
                    case 3 :
                        labes = '<span class="label label-default">Инфо</span>';
                        break;
                }
                $('.news tbody').append('<tr><td data-value="' + day.getTime() + '" style="color:black;font-size:12px; ">' + show + '</td><td style="color:black;font-size:12px">' + value['SIT'] + '</td><td style="color:black;font-size:12px">' + value['NEWS'] + '</td><td>' + labes + '</td><td style="color:black;font-size:12px">' + value['OWNER'] + '</td><td style="color:black;font-size:12px">' + value['TICKET'] + '</td><td style="color:black;font-size:12px">' + value['IVENT'] + '</td><td style="color:black;font-size:12px">' + value['SIT'] + '</td></tr>');
            });
            $(window).resize();
            $('#demo-foo-accordion').trigger('footable_redraw').trigger('footable_redraw');
        });

    }
    $('body').click(function (e) {
        target_obj = $(e.target).parents('.news');
        if (!$(e.target).parents('.dropdown-menu').parent().hasClass('news')) {
            if ($('.news').hasClass('open')) {
                $('.news').removeClass('open').addClass('closed');
            }
        }
        if (target_obj.hasClass('news')) {
            if (!target_obj.hasClass('closed') && !target_obj.hasClass('open')) {
                NewNews();
                $('.news').addClass('open');
            } else {
                if ($('.news').hasClass('closed')) {
                    $('.news').removeClass('closed').addClass('open');
                }
            }
        }
    });
    $('body').click(function (e) {
        target_obj = $(e.target).parents('.action_history');
        if (!$(e.target).parents('.dropdown-menu').parent().hasClass('action_history')) {
            if ($('.action_history').hasClass('open')) {
                $('.action_history').removeClass('open').addClass('closed');
            }
        }
        if (target_obj.hasClass('action_history')) {
            if (!target_obj.hasClass('closed') && !target_obj.hasClass('open')) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/history",
                }).done(function (data) {
                    data = JSON.parse(data);
                    $.each(data, function (key, value) {
                        var day = new Date(value['DATETIME']);
                        var Comment;
                        if (value['COMMENT_TECH'] == null) {
                            Comment = 'Отсутствует'
                        } else {
                            Comment = value['COMMENT_TECH']
                        }
                        ;
                        $('.action_history tbody').append('<tr><td data-value="' + day.getTime() + '" style="color:black;font-size:12px; ">' + day.format("HH:MM") + '</td><td style="color:black;font-size:12px">' + value['number'] + '</td><td style="color:black;font-size:12px">' + value['ACTION_NAME'] + '</td><td>' + value['TIME_ON_ACTION'] + '</td><td style="color:black;font-size:12px">' + Comment + '</td></tr>');
                    });
                    $(window).resize();
                    $('#foo-history').trigger('footable_redraw').trigger('footable_redraw');
                });
                $('.action_history').addClass('open');
            } else {
                if ($('.action_history').hasClass('closed')) {
                    $('.action_history').removeClass('closed').addClass('open');
                }
            }
        }
    });


    $('document').ready(function () {
        $('input[name="news_type"]').change(function () {
            $('.news tbody').empty();
            if ($('input[name="news_type"]:checked').val() == "close") {
                OldNews();
            } else {
                NewNews();
            }

        });
    });


</script>

<script>
    var resizefunc = [];
</script>



<script type="text/javascript">

    /*jQuery(document).ready(function($) {
     $('.counter').counterUp({
     delay: 100,
     time: 1200
     });

     $(".knob").knob();

     });
     try {
     socket = new WebSocket('ws://146.240.32.57:6431');
     } catch(err) {

     }
     (function () {
     var init = function () {

     socket.onopen = connectionOpen;
     socket.onmessage = messageReceived;

     };


     function connectionOpen() {
     socket.send('hello');
     }

     function messageReceived(e) {
     answer = JSON.parse(e.data);
     //if (answer['type'] == 'alert') {
     //    swal(answer['text']);
     //}
     if (typeof(answer['command']) != 'undefined') {
     eval(answer['command']);
     }
     console.log(answer);
     }

     function connectionClose() {
     socket.close();
     }

     return {
     load : function () {
     window.addEventListener('load', function () {
     init();
     }, false);
     }
     }
     })().load();*/

    $('.break_activate').click(function () {
        time = getCookie('dinner');
        mode = 1;
        if (window.location.pathname == '/view_request' && $('.view_request_main_form').length == 0) {
            mode = 3;
        }
        if (time > 0) {
            $.ajax({
                method: "post",
                url: "/ajax/dinnerBreak",
                data: {mode: mode}
            }).done(function (data) {
                if (data == 2) {
                    window.location.replace("/view_request");
                } else if (data == 1) {
                    swal('Перерыв начнется после завершения текущей заявки'
                    );
                }
            });
        } else {
            swal('Закончилось время перерыва!'
            );
        }
    });

    <?php  ?>
    dinner_active = getCookie('dinner_active');
    // if (dinner_active == 2) {
    //    StartBreak();
    //}

    function StartBreak() {
        time = getCookie('dinner');
        time_string = Math.floor(time / 60) + ':' + time % 60;
        swal({
            title: "Обед",
            text: "Осталось времени: " + time_string,
            imageUrl: "/public/img/ico/ic_hamburger.png"

        });
        dinner_timer = setInterval(function () {
            time--;
            time_string = Math.floor(time / 60) + ':' + time % 60;
            $('.sweet-alert p').html('Осталось времени: ' + time_string);
            setCookie('dinner', time, 1)
        }, 1000);

        $('.sweet-alert button.confirm').click(function () {
            clearInterval(dinner_timer);
            $.ajax({
                method: "post",
                url: "/ajax/dinnerBreak",
                data: {mode: 2}
            }).done(function (data) {
                window.location.replace("/view_request");
            });
        });
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

</script>
<script>
    $(document).ready(function () {
        $.fn.OneClickSelect = function () {
            return $(this).on('dblclick', function () {
                span = $(this).find('span')[1];
                var range, selection;

                if (window.getSelection) {
                    selection = window.getSelection();
                    range = document.createRange();
                    range.selectNodeContents(span);
                    selection.removeAllRanges();
                    selection.addRange(range);
                } else if (document.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(span);
                    range.select();
                }
                document.execCommand('copy');
            });
        };

        // Apply to these elements
        $('.fixx .info').OneClickSelect();
    });
</script>
<script>
    $(window).keydown(function (e) {
        if (e.keyCode == 40) {
            number = $('.curator_i').eq(($('.curator_i.list-group-item-info').index() + 1)).data('number');
            if (typeof(Tickets[number].htmlbody) == 'undefined') {
                download_info(number, 1, 1);
            } else {
                $('.curator_i').eq(($('.curator_i.list-group-item-info').index() + 1)).find('.ticket_body').click();
            }
            $('.tickets_container').scrollTop($('.tickets_container').scrollTop() + 76);
            return false;
        } else if (e.keyCode == 38) {
            number = $('.curator_i').eq(($('.curator_i.list-group-item-info').index() - 1)).data('number');
            if (typeof(Tickets[number].htmlbody) == 'undefined') {
                download_info(number, 1, 1);
            } else {
                $('.curator_i').eq(($('.curator_i.list-group-item-info').index() - 1)).find('.ticket_body').click();
            }
            $('.tickets_container').scrollTop($('.tickets_container').scrollTop() - 76);
            return false;
        }
    });
</script>
<script>
    $('base').remove();
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};
</script>

<?php if (!empty($this->js)): ?>
    <script src="/public/js/<?php echo $name.'_js' ?>.js?verison=<?php echo AURA_VERSION ?>"></script>
<?php endif ?>

</body>
</html>
