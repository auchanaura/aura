<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->title ?></title>

    <link href="/public/css/main_vendor_css.css?verison=<?php echo AURA_VERSION ?>" rel="stylesheet">
    <link href="/public/css/main_css.css?verison=<?php echo AURA_VERSION ?>" rel="stylesheet">
    <script src="/public/js/main_vendor_js.js?verison=<?php echo AURA_VERSION ?>"></script>
</head>
<body>
<?php include($view_body); ?>

</body>
</html>
