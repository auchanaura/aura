<?php use Aura\Core\Core;
use Aura\Core\ProcessData;

$variables = $GLOBALS['variables'] ?>
<script>
    $('title').text('Рабочий процесс');


</script>
<?php if (!empty($request['NUMBER'])) { ?>
    <style>
        .select2 {
            width: 100%!important;
        }
        .button-list .btn.btn-xs {
            min-width: 70px;
            margin-left: 0px;
            margin-bottom: 5px;
        }
        .form-group {
            margin-bottom: 10px;
        }
        .datepicker-dropdown {
            margin-top: 63px;
        }
    </style>
    <input style="display: none;" class="clipboard"/>
    <form method="post" enctype="multipart/form-data" class="view_request_main_form">
        <input type="hidden" value="<?php echo $request['NUMBER'] ?>" name="ticket_number">
        <div style="overflow: auto;">
            <div class="col-sm-7">
                <div class="form-horizontal panel panel-color panel-custom"> <!-- Левый верхний раздел -->
                    <div class="panel-heading">
                        <div class="panel-title"><?php echo $request["STATUS_TITLE"] ?>
                            <?php if (Aes::decrypt($_COOKIE['role'],COOKIE_PASS) == 1) {?>
                                <?php
                                $time_diff = date_diff(new DateTime('NOW'), new DateTime($request['KERNEL_JOB_TIME_START']));
                                $time_diff = $time_diff->format('%s')+$time_diff->format('%i')*60+$time_diff->format('%h')*3600;
                                $time_diff = $request['KERNEL_JOB_TIME_LIMIT']*60 - $time_diff;
                                $m = floor($time_diff/60);
                                $s = abs($time_diff%60);

                                if (strlen($m) == 1) {$m = '0'.$m;}
                                if (strlen($s) == 1) {$s = '0'.$s;}
                                $class = 'purple';
                                if ($m < 0) { $class = 'danger';} else if ($m < $request['KERNEL_JOB_TIME_LIMIT']/5 ) {$class = 'warning';}
                                ?>
                                <span style="float: right; margin-top: 6px;" class="kernel_job_tome_limit_label label label-<?php echo $class; ?>"><?php echo $m.':'.$s ?></span>


                                <script>
                                    time_kernel = <?php echo $time_diff ?>;
                                    kernel_job_tome_limit_interval = setInterval(function(){
                                        time_kernel--;
                                        m = Math.floor(time_kernel/60);
                                        s = Math.abs(time_kernel%60);
                                        if (s == 0) { m = m-1;}
                                        if (String(s).length == 1) {s = '0'+s;}
                                        kernel_class = 'purple';
                                        btn_class = 'default';
                                        if (m <= 0) {
                                            btn_class = 'danger';
                                            if ($("textarea[name='justify']").val() != '') {
                                                btn_class = 'default';
                                            }
                                            kernel_class = 'danger';
                                            $('textarea[name="justify"]').show();
                                            $('.justify_l').show();
                                        } else if( m < <?php echo $request['KERNEL_JOB_TIME_LIMIT']/5 ?>) {
                                            kernel_class = 'warning';
                                            btn_class = 'warning';
                                            if ($("textarea[name='justify']").val() != '') {
                                                btn_class = 'default';
                                            }
                                        }
                                        $('.submit_form').removeClass('btn-default').removeClass('btn-warning').removeClass('btn-danger').addClass('btn-'+btn_class);
                                        $('.kernel_job_tome_limit_label').removeClass('label-purple').removeClass('label-warning').removeClass('label-danger').addClass('label-'+kernel_class).html(m+':'+s);
                                        $('.delay_minutes').html(Math.abs(m));
                                    }, 1000);
                                </script>
                            <?php }?>
                            <?php if (@$_GET['mode'] == 2) { ?>
                                <span style="float: right;margin-top: 6px; margin-right: 10px;" class="label label-warning">Фоновая заявка</span>
                            <?php } else { ?>
                                <span style="float: right;margin-top: 6px; margin-right: 10px;" class="label label-warning">Основная заявка</span>
                            <?php } ?>
                        </div>
                    </div>


                </div>

                <div class="panel panel-default" style="overflow: auto; max-height: 550px;"> <!-- HTML письмо -->
                    <div class="panel-body">
                        <?php print_r($request['TICKET_DESCRIPTION']); ?>
                    </div>
                </div>

                <div class="panel-group " id="accordion-test-2" style="margin-bottom: 10px;">
                    <div class="panel panel-custom panel-color">
                        <div class="panel-heading " >
                            <h4 class="panel-title ">
                                <a style="text-decoration: none; color: #ffffff;" data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-2" aria-expanded="false" class="collapsed">
                                    Тип проблемы и топик
                                </a>
                            </h4>
                        </div>
                        <!--<div id="collapseOne-2" class="panel-collapse collapse"> -->
                        <div class="panel-body">
                            <div class="form-group"  >
                                <label for="treatment_type" class="col-md-3 col-sm-4 col-xl-2 control-label">Тип обращения</label>
                                <div class="button-list col-sm-8 col-md-9 col-xl-10" data-toggle="buttons" style="margin-bottom:0px">
                                    <label class="btn btn-default btn-xs btn-custom waves-effect waves-light  <?php echo ($request['TYPE']==1 ? 'active' : ''); ?>">
                                        <input type="radio" value="1" name="treatment_type" id="treatment_type" autocomplete="off" <?php echo ($request['TYPE']==1 ? 'checked' : ''); ?>> Инцидент
                                    </label>
                                    <label class="btn btn-default btn-xs btn-custom waves-effect waves-light <?php echo ($request['TYPE']==2 ? 'active' : ''); ?>">
                                        <input type="radio" value="2" name="treatment_type" id="treatment_type" autocomplete="off" <?php echo ($request['TYPE']==2 ? 'checked' : ''); ?>> Запрос
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="problem_type" class="col-md-3 col-sm-4 col-xl-2 control-label">Тип проблемы</label>
                                <div class="col-sm-8 col-md-9 col-xl-10" style="margin-bottom:7px">
                                    <select class="form-control problem_name" id="problem_type" name="problem_type" placeholder="Задание">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <label for="problem_type" class="col-md-3 col-sm-4 col-xl-2 control-label">Реальный сайт</label>
                                <div class="col-sm-8 col-md-9 col-xl-10">
                                    <select class="form-control REAL_SITE" id="REAL_SITE" name="REAL_SITE" placeholder="Реальный сайт">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>
                </div>

                <div class="panel panel-default panel-color panel-custom" style="margin-bottom: 0px;"> <!-- Раздел Работы -->
                    <div class="panel-heading">
                        <h3 class="panel-title">Работа</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-top: 0px;">Статус</label>
                                    <div class="button-list col-sm-9" data-toggle="buttons">
                                        <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                            <input type="radio" name="type" value="escalation" autocomplete="off"> Передача в группу
                                        </label>
                                        <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                            <input type="radio" name="type" value="hold" autocomplete="off"> Холд
                                        </label>
                                        <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                            <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Решить']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Решить']['title'] ?>
                                        </label>
                                        <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                            <input type="radio" name="type" value="comment" autocomplete="off"> Комментарий
                                        </label>
                                        <?php if ($request['LAST_STATUS_JOB'] >= 0 || true): ?>
                                            <label class="btn btn-warning btn-xs btn-custom waves-effect waves-light">
                                                <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Игнорировать']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Игнорировать']['title'] ?>
                                            </label>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="form-group"  style="display: none; margin-bottom: 15px;">
                                    <div class="col-sm-9 col-sm-offset-3 escalation" style="display: none;float: initial;">
                                        <div class="button-list" data-toggle="buttons">
                                            <?php if ($user['LV'] == 3 ) { ?>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Центр_поддержки']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Центр_поддержки']['title'] ?>
                                                </label>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['ATAK']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['ATAK']['title'] ?>
                                                </label>
                                            <?php } else if ($user['LV'] == 1 || $user['LV'] == 4) { ?>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Мобильная_группа']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Мобильная_группа']['title'] ?>
                                                </label>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['ATAK']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['ATAK']['title'] ?>
                                                </label>
                                            <?php } ?>

                                            <?php if ($user['LV'] == 4 ) { ?>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Центр_компетенций']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Центр_компетенций']['title'] ?>
                                                </label>
                                            <?php } else if ($user['LV'] == 1) { ?>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Контакный_Центр']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Контакный_Центр']['title'] ?>
                                                </label>
                                            <?php } ?>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                <input name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Хелпдеск']['value'] ?>" type="radio" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Хелпдеск']['title'] ?>
                                            </label>
                                            <?php if ($user['LV'] == 5) { ?>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Центр_компетенций']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Центр_компетенций']['title'] ?>
                                                </label>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Контакный_Центр']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Контакный_Центр']['title'] ?>
                                                </label>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Мобильная_группа']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Мобильная_группа']['title'] ?>
                                                </label>
                                                <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
                                                    <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['В спул']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['В спул']['title'] ?>
                                                </label>
                                            <?php } ?>

                                            <!--<label class="btn btn-default btn-sm btn-custom btn-rounded waves-effect waves-light <?php echo ($request['STATUS_GLOBAL']==1 ? 'active' : ''); ?>">
												<input type="radio" name="status_global" value="13" autocomplete="off" <?php echo ($request['STATUS_GLOBAL']==1 ? 'checked' : ''); ?>> Центр Поддержки
											</label>-->
                                        </div>
                                    </div>

                                    <div class="col-sm-9 col-sm-offset-3 hold" style="display:none">
                                        <div class="button-list" data-toggle="buttons">
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light" style="margin-left: 0px;">
                                                <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Отложить']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Отложить']['title'] ?>
                                            </label>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light" style="margin-left: 0px;">
                                                <input type="radio" name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Аутсорс']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Аутсорс']['title'] ?>
                                            </label>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light" style="margin-left: 0px;">
                                                <input type="radio"   name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Недостаточно_данных']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Недостаточно_данных']['title'] ?>
                                            </label>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light" style="margin-left: 0px;">
                                                <input type="radio"  name="status_global" value="<?php echo $variables['STATUS_GLOBAL']['Требуется_подтверждение']['value'] ?>" autocomplete="off"> <?php echo $variables['STATUS_GLOBAL']['Требуется_подтверждение']['title'] ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group time_up" style="display: none;">
                                    <label for="time_up" class="col-sm-3 control-label ">Отложить до...</label>
                                    <div class="col-sm-9">
                                        <div class="input-daterange bootstrap-timepicker input-group" id="date-range">
                                            <input type="text" class="form-control" name="date_up" placeholder="dd.mm.yyyy" id="datepicker-autoclose3">
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-calendar"></i></span>
                                            <input type="text" class="form-control" name="time_up" placeholder="hh:ii:ss" id="timepicker">
                                            <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>

                                            <input type="hidden" name="date_diff" value="<?php echo date('H'); ?>">
                                            <script>
                                                $('input[name="date_diff"]').val($('input[name="date_diff"]').val()-new Date().getHours());
                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    jQuery('#datepicker-autoclose3').datepicker({
                                        format: "dd.mm.yyyy",
                                        autoclose: true,
                                        todayHighlight: true,
                                        minDate: 0, maxDate: "+1M +10D"
                                    });
                                    jQuery('#timepicker').timepicker({
                                        minuteStep : 5,
                                        showMeridian : false
                                    }).on('changeTime.timepicker', function(e) {
                                        var h= e.time.hours;
                                        var m= e.time.minutes;
                                        var mer= e.time.meridian;
                                        //convert hours into minutes
                                        m+=h*60;
                                        date_time = new Date();
                                        date_time = date_time.getHours()*60 + Math.floor(date_time.getMinutes()/5)*5+5;
                                        //10:15 = 10h*60m + 15m = 615 min
                                        if(mer=='AM' && m<(date_time)) {
                                        date_time = new Date();
                                            $('#timepicker').timepicker('setTime', date_time.getHours()+':'+(Math.floor(date_time.getMinutes()/5)*5+5)+' AM');
                                        }
                                    });
                                </script>
                                <div class="form-group num"  style="display: none;">
                                    <label for="number" class="col-sm-3 control-label ">Номер</label>
                                    <div class="col-sm-9">
                                        <?php $number = ''; if (!empty($request['NUMBER_EVENT']) && $request['NUMBER_EVENT'] != '-') {
                                            $number = $request['NUMBER_EVENT'];
                                        } else if (!empty($request['NUMBER_BEELINE']) && $request['NUMBER_BEELINE'] != '-') {
                                            $number = $request['NUMBER_BEELINE'];
                                        } ?>
                                        <input name="number" type="text" class="form-control " id="number" placeholder="Номер" value="<?php echo $number ?>">
                                    </div>
                                </div>
                                <div class="form-group crit" style="display: none;">
                                    <label class="col-sm-3 control-label" style="padding-top: 0px;">Критичность</label>
                                    <div class="col-sm-9">
                                        <div class="button-list" data-toggle="buttons">
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light <?php echo ($request['CIM']==0 ? 'active' : ''); ?>">
                                                <input type="radio" name="cim" value="0" autocomplete="off" <?php echo ($request['CIM']==0 ? 'checked' : ''); ?>> Нормальная
                                            </label>
                                            <label class="btn btn-warning btn-xs btn-custom waves-effect waves-light <?php echo ($request['CIM']==1 ? 'active' : ''); ?>">
                                                <input type="radio" name="cim" value="1" autocomplete="off" <?php echo ($request['CIM']==1 ? 'checked' : ''); ?>> Срочная
                                            </label>
                                            <label class="btn btn-danger btn-xs btn-custom waves-effect waves-light <?php echo ($request['CIM']==2 ? 'active' : ''); ?>">
                                                <input type="radio" name="cim" value="2" autocomplete="off" <?php echo ($request['CIM']==2 ? 'checked' : ''); ?>> Критичная
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3  control-label"  style="padding-top: 0px;">Физическая проблема</label>
                                    <div class="col-sm-9">
                                        <div class="button-list" data-toggle="buttons">
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light <?php echo ($request['PHYSICAL']==1 ? 'active' : ''); ?>">
                                                <input type="radio" name="physical" value="1" autocomplete="off" <?php echo ($request['PHYSICAL']==1 ? 'checked' : ''); ?>> Да
                                            </label>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light <?php echo ($request['PHYSICAL']==0 ? 'active' : ''); ?>">
                                                <input type="radio" name="physical" value="0" autocomplete="off" <?php echo ($request['PHYSICAL']==0 ? 'checked' : ''); ?>> Нет
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3  control-label"  style="padding-top: 0px;">Новость</label>
                                    <div class="col-sm-9">
                                        <div class="button-list" data-toggle="buttons">
                                            <label id="newsbutton" class="btn btn-default btn-xs btn-custom waves-effect waves-light ">
                                                <input type="radio" name="news" value="1" autocomplete="off" > Да
                                            </label>
                                            <label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
                                                <input type="radio" name="news" value="0" autocomplete="off" checked> Нет
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group user_answer_container2">
                                    <label style="margin-bottom: 10px;" class="control-label">Ответ пользователю</label>
                                    <textarea name="user_answer" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-custom user_answer_button" style="margin-bottom:5px;margin-top:-5px;">
                                        <input id="tech2"  name="tech2" type="checkbox" >
                                        <label class="second_comment" for="tech2">Требуется отдельный технический комментарий </label>
                                    </div>

                                    <div class="user_answer_container" style="display: none;">
                                        <label style="margin-bottom: 10px;" class="control-label">Технический комментарий</label>
                                        <textarea id="tech_comment" class="form-control" name="tech_comment" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="file" class="filestyle" data-buttonbefore="true" multiple="true" name="attachments[]">
                                </div>
                                <script>
                                    $(":file").filestyle({buttonText: "Выберите файл"});
                                </script>
                                <?php
                                $time_diff = date_diff(new DateTime('NOW'), new DateTime($request['KERNEL_JOB_TIME_START']));
                                $time_diff = $time_diff->format('%s')+$time_diff->format('%i')*60+$time_diff->format('%h')*3600;
                                $time_diff = $request['KERNEL_JOB_TIME_LIMIT']*60 - $time_diff;
                                $m = floor($time_diff/60);
                                $class = 'default';
                                if ($m < 0) { $class = 'danger';}
                                ?>
                                <div class="form-group">
                                    <label for="justify" class="justify_l" style="color:red; display: none;">Причина
                                        задержки (Просрочка: <span class="delay_minutes"><?php echo abs($m) ?></span> мин.)</label>
                                    <textarea style="margin-bottom: 10px; display: none; border-color: red;"
                                              name="justify" class="form-control"
                                              placeholder="Укажите причины задержки обработки заявки. В случае отсутствия обоснования, рассмотрение претензий к штрафу не принимается"></textarea>
                                    <button type="submit"
                                            class="btn btn-<?php echo $class ?> waves-effect waves-light btn-lg btn-lg-p btn-block submit_form">
                                        Отправить
                                    </button>
                                    <div class="alert alert-danger submit_form_alert" role="alert"
                                         style="display: none; margin-top: 8px">Необходимо выбрать Тип проблемы!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

            </div>

            <div class="col-sm-5 fixx">

                <div class="panel panel-border panel-custom" style="position: relative;" > <!-- Детали обращения -->
                    <div class="panel-heading request_details collapsed" style="cursor: pointer;" data-toggle="collapse" data-parent="#accordion-test-2" href="#collapse-ditail" aria-expanded="false" class="collapsed">
                        <div class="panel-title">
                            <a style="text-decoration: none;" >
                                Детали обращения
                            </a>

                        </div>
                    </div>
                    <div id="collapse-ditail" class="panel-collapse collapse">
                        <ul class="list-group" style="padding: 3px 10px 5px;">
                            <li class="list-group-item info"><span style="font-weight:bold;">Номер:</span> <span><?php echo $request['NUMBER'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Время рег.:</span> <span><?php echo $request['STATUS0'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Заявитель:</span> <span><?php echo $request['USER_FIO'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Емейл:</span> <span><?php echo $request['USER_MAIL'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Сайт:</span> <span><?php echo $request['USER_SITE'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Логин:</span> <span><?php echo $request['USER_LOGIN'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Должность:</span> <span><?php echo $request['USER_POST'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Номер ивента:</span> <span><?php echo $request['NUMBER_EVENT'] ?></span></li>
							<li class="list-group-item info"><span style="font-weight:bold;">Телефон:</span>
                                <span><?php echo $request['USER_PHONE'] ?></span></li>
                            <li class="list-group-item info"><span style="font-weight:bold;">Вложения:</span>
                                <?php if (!empty($request['attachments'])): ?>
                                <?php foreach ($request['attachments'] as $key => $value): ?>
                                    <?php if ($key != 0): ?>
                                        ,
                                    <?php endif ?>
                                    <a href="/manage/DownloadMailAttachment?indent=<?php echo $request['NUMBER'] ?>&img=<?php echo $key ?>"><?php echo $value->filename ?></a>
                                <?php endforeach ?></li>
                            <?php endif ?>

                        </ul>
                    </div>
                    <div class="panel panel-default panel-custom" style="width:100%;position: absolute; position: absolute; max-height: calc(100vh - 100% - 80px); margin-top: 10px;overflow: auto;"> <!-- Cgbcjr ltqcndbq -->
                        <div class="panel-heading">
                            <h3 class="panel-title">Действия</h3>
                        </div>

                        <ul class="list-group events_list" style="max-height: 100%;">
                            <!-- col-sm-5 col-sm-offset-7  Список действий формируется JS, шаблон в конце документа -->
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </form>

<?php } else { ?>
    <?php if ($user['BREAK_START'] == NULL) { ?>
        <div class="container" style="height: calc(100vh - 100px);">
            <div class="col-xs-6 col-xs-offset-3  v-center ">
                <h4 style="text-align: center; <?php if (($user['LV'] == 3 || Core::GetLogin() == 'ru00240024') && $user['READY'] == 0): ?>display: none;<?php endif ?>" class="ready_on" >
                    Ожидание новой заявки...
                </h4>
                <div class="progress progress-lg m-b-5 ready_on" style="<?php if (($user['LV'] == 3 || Core::GetLogin() == 'ru00240024') && $user['READY'] == 0): ?>display: none;<?php endif ?>">

                    <div  class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; vertical-align: middle" >
                        <span class="sr-only">Поиск заявки...</span>
                    </div>
                </div>
                <?php if ($user['LV'] == 3 || Core::GetLogin() == 'ru00240024'): ?>
                    <div class="button-list" data-toggle="buttons" style="text-align: center; padding-top: 10px;">
                        <label class="btn btn-default btn-lg btn-custom waves-effect waves-light ready <?php if (($user['LV'] == 3 || Core::GetLogin() == 'ru00240024') && $user['READY'] == 1): ?>active<?php endif ?>">
                            <input type="radio" name="ready" value="1" autocomplete="off"> <span class="ready_text"><?php if (($user['LV'] == 3 || Core::GetLogin() == 'ru00240024') && $user['READY'] == 0) { ?>Начать поиск<?php } else { ?>Отменить поиск<?php } ?></span>
                        </label>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php } else { ?>
        <div style="background: #ffffff; width: 450px; text-align: center; padding: 40px 0px 20px 0px; border-radius: 5px; position: absolute; top: 50%; margin-top: -200px; left: 50%; margin-left: -225px;">
            <img src="/public/img/ico/ic_hamburger.png" style="width: 80px;">
            <h2>Обед</h2>
            <div style="margin-bottom: 20px; font-size: 14px;">Осталось времени: <span class="time_left" style="font-weight: bold;"><?php
                    $datediff = date_diff(new DateTime('NOW'),new DateTime($user['BREAK_START']));
                    $datediff = $datediff->format('%i')*60+$datediff->format('%s');
                    $dinner_time = $user['BREAK_TIME']*60 - $datediff;
                    $s = $dinner_time%60;
                    if (strlen($s) == 1) {
                        $s = '0'.$s;
                    }
                    echo floor($dinner_time/60) .':'. $s;
                    ?></span></div>
            <button style="margin-bottom: 10px; font-size: 16px;" class="btn btn-default stop_dinner">Прекратить обед</button>
            <script>
                time = <?php echo $dinner_time ?>;
                dinner_timer = setInterval(function(){
                    time--;
                    s = time%60;
                    if (String(s).length == 1) {
                        s = '0'+s;
                    }
                    time_string = Math.floor(time/60) + ':' + s;
                    $('.time_left').html(time_string);
                    setCookie('dinner',time,1)
                }, 1000);

                $('.stop_dinner').click(function(){
                    clearInterval(dinner_timer);
                    $.ajax({
                        method: "post",
                        url: "/ajax/dinnerBreak",
                        data: {mode: 2}
                    }).done(function(data) {
                        window.location.replace("/view_request");
                    });
                });
            </script>
        </div>
    <?php } ?>
<?php } ?>

<?php if (!empty($request['NUMBER'])) { ?>

    <script src="/public/js/view_request_logic.js"></script>
    <script src="/public/js/resize.js"></script>
    <script >
        $('.escalation .btn').click(function(){
            $('.hold .active').removeClass('active');
        });
        $('.hold .btn').click(function(){
            $('.escalation .active').removeClass('active');
        });

        $(document).ready(function() {
            $(".problem_name").select2();
            $('.REAL_SITE').select2();
        });

        $(document).on("ready", function() { //Подгружаем список типов проблем
            var data = [<?php foreach ($problems_list[1] as $key => $value) {
                echo '{id: "'.$key.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].'</span>", level : 0, disabled: '.($problems_list[0][$key]['Get'] == 0 ? 1 : 0).'},';
                foreach ($value as $key2 => $value2) {
                    echo '{id: "'.$key2.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].'</span>", level : 1, disabled: '.($problems_list[0][$key2]['Get'] == 0 ? 1 : 0).'},';
                    foreach ($value2 as $key3 => $value3) {
                        echo '{id: "'.$value3.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].' - '.$problems_list[0][$value3]['TOPIC'].'</span>", level : 2, disabled: '.($problems_list[0][$value3]['Get'] == 0 ? 1 : 0).'},';
                    }
                }
            } ?>];

            function formatResult(node) {
                var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
                return $result;
            };

            $(".problem_name").select2({
                placeholder: 'Выберите тип проблемы',
                data: data,
                formatSelection: function(item) {
                    return 'test';
                },
                formatResult: function(item) {
                    return item.text;
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateResult: formatResult,
                theme: "bootstrap"
            });

            var data = [<?php foreach ($real_sites as $key => $value) {
                echo '{id: "'.$value['MAGAZ'].'", text: "'.$value['MAGAZ'].'", level : 0},';
            } ?>];
            $('.REAL_SITE').select2({
                placeholder: 'Выберите реальный сайт',
                data: data,
                formatSelection: function(item) {
                    return item.text
                },
                formatResult: function(item) {
                    return item.text
                },
                templateResult: formatResult,
                theme: "bootstrap"
            });

            $('.problem_name').val('<?php echo $request['PROBLEM_ID'] ?>').trigger('change');
            $('.REAL_SITE').val('<?php echo $request['REAL_SITE'] ?>').trigger('change');
        });
        $('.user_answer_button').change(function(){
            $('.user_answer_container').toggle();
        });

        events = [];
        <?php foreach ($request['history'] as $key => $value) { //Обрабатываем от нежелательных символов JSON информацию по заявке
        $value = ProcessData::pregEscape($value);	?>
        events[<?php echo $key ?>] = JSON.parse('<?php echo json_encode($value) ?>');
        <?php } ?>
        $(document).ready(function(){
            $.each(events,function(key,value){
                event($('.events_list'),value);
            });
        });
        <?php require(AURA_VIEWS_PATH.'js_templates/events_js.php') ?>

        function Validate() { // При обновлении ключевых полей - проверяем выполнение всех условий
            alert = '';
            if ($('.problem_name').val() == null) {
                alert += 'Необходимо выбрать тип проблемы!';
            }
            if ($('.REAL_SITE').val() == null) {
                if (alert != '') { alert += '<br>'; }
                alert += 'Необходимо выбрать реальный сайт!';
            }
            if (($('textarea[name="user_answer"]').val() == '' || $('textarea[name="user_answer"]').val().length < 1) && $('input[name="status_global"]:checked').val() != 4 && $('input[name="status_global"]:checked').val() != 9 && $('input[name="status_global"]:checked').val() != 99) {
                if (alert != '') { alert += '<br>'; }
                alert += user_answer_text;
            }
            if ($('input[name="status_global"]:checked').val() == 35 || $('input[name="status_global"]:checked').val() == 94) {
                date1 = new Date();
                date_val = $('input[name="date_up"]').val().split('.');
                time_val = $('input[name="time_up"]').val().split(':');
                date2 = new Date(date_val[2],date_val[1]-1,date_val[0],time_val[0],time_val[1]);
                if (date1 > date2 || $('input[name="date_up"]').val() == '') {
                    if (alert != '') { alert += '<br>'; }
                    alert += 'Некорректные дата и время!';
                }
            }
            if ($('input[name="status_global"]:checked').val() == 4) {
                event_number_symbol = '';
                if (typeof($('input[name="number"]').val()) != 'undefined') {
                    event_number_symbol = $('input[name="number"]').val()[0].toLowerCase();
                }
                if ((event_number_symbol != 's' || event_number_symbol != 'i')  && $('input[name="number"]').val().length != 14) {
                    if (alert != '') { alert += '<br>'; }
                    alert += 'Необходимо указать номер ивента!';
                }
            }
            if (typeof($('input[name="status_global"]:checked').val()) == 'undefined' && $('input[name="type"]:checked').val() != 'comment' ) {
                if (alert != '') { alert += '<br>'; }
                alert += 'Необходимо выбрать статус';
            }
            if (alert != '') {
                $('.submit_form').attr('disabled',true);
                $('.submit_form_alert').html(alert).show();
            } else {
                $('.submit_form').attr('disabled',false);
                $('.submit_form_alert').hide();
            }
        }

        $('.problem_name').change(function(){
            Validate();
        });
        $('input[name="date_up"]').change(function(){
            Validate();
        });
        $('input[name="time_up"]').change(function(){
            Validate();
        });
        $('input[name="number"]').change(function(){
            Validate();
        });
        $('input[name="status_global"]').change(function(){
            Validate();
        });
        $('input[name="type"]').change(function(){
            Validate();
        });
        $('.REAL_SITE').change(function(){
            Validate();
        });
        $('textarea[name="user_answer"]').keyup(function(){
            Validate();
        });
    </script>
<?php } else if ($user['BREAK_START'] == NULL) { ?>
    <script>
        $('.ready').click(function(){
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('.ready_text').html('Начать поиск');
                $('.ready_on').hide();
                ready = 0;
                $.ajax({
                    method: "POST",
                    url: "/ajax/setReady",
                    data: { ready: ready}
                })
                return false;
            } else {
                $('.ready_on').show();
                $(this).find('.ready_text').html('Отменить поиск');
                ready = 1;
                $.ajax({
                    method: "POST",
                    url: "/ajax/setReady",
                    data: { ready: ready}
                })
            }

        });
        ready = 0;
        <?php if ($user['LV'] != 3) {
            echo "ready = 1;";
        } ?>
        search = setInterval(function(){
            if (ready == 1) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/checkTicket",
                    data: { name: "<?php echo $tab_number ?>"}
                }).done(function(data) {
                    if (data != 0) {
                        console.log(data);
                        $('.search_progress').append('<br>Найдена заявка №' + data);
                        clearInterval(search);
                        window.location.replace('/view_request');
                    } else {
                        console.log(data);
                        $('.search_progress').append('<br>Идет поиск заявки...');
                    }
                });
            }
        }, 2000);
    </script>
<?php } ?>
<script>
    $(document).ready(function(){
        $('.list-group-item-text').each(function() {
            if ($(this).height() > 48) {
                $(this).addClass('collapsed_text');
            }
        });
    });
    $('body').on('click','.collapsed_text',function(){
        $(this).removeClass('collapsed_text');
    });
    $(document).ready(function () {
        $('base').remove();
    });
</script>
