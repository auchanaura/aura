<?php
/**
 * Created by PhpStorm.
 * User: ru00160012
 * Date: 01.02.2017
 * Time: 15:13
 */
$variables = $GLOBALS['variables'];
$disabled = '';//($user['ADMIN'] != 0 && @$_GET['mode'] != 6 && @$_GET['mode'] != 4 ? '' : 'disabled');
?>
<script>
    $('title').text('Администрирование');
</script>

<?php if ($disabled!=="disabled") { ?>
    <style>
        .select2 {
            width: 100% !important;
        }

        .button-list .btn.btn-xs {
            min-width: 70px;
            margin-left: 0px;
            margin-bottom: 5px;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .datepicker-dropdown {
            margin-top: 63px;
        }
    </style>
    <input style="display: none;" class="clipboard"></input>
    <form method="post" enctype="multipart/form-data" class="view_request_main_form">
        <input type="hidden" value="" name="">
        <div style="overflow: auto;">
            <div class="col-sm-7">
                <div class="form-horizontal panel panel-color panel-custom"> <!-- Левый верхний раздел -->
                    <div class="panel-heading">
                        <div class="panel-title">контент

                        </div>
                    </div>


                </div>

                <div class="panel panel-default" style="overflow: auto; max-height: 550px;"> <!-- HTML письмо -->
                    <div class="panel-body">
                        грид ( табличка )
                    </div>
                </div>

                <div class="panel-group " id="accordion-test-2" style="margin-bottom: 10px;">
                    <div class="panel panel-custom panel-color">
                        <div class="panel-heading ">
                            <h4 class="panel-title ">
                                <a style="text-decoration: none; color: #ffffff;" data-toggle="collapse"
                                   data-parent="#accordion-test-2" href="#collapseOne-2" aria-expanded="false"
                                   class="collapsed">
                                    функционал управления гридом
                                    <div class="form-group">

                                    </div>
                                </a>
                            </h4>
                        </div>
                        <!--<div id="collapseOne-2" class="panel-collapse collapse"> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <button type="submit"
                                                class="btn btn-<?php echo $class ?> waves-effect waves-light btn-lg btn-lg-p btn-block submit_form">
                                            Отправить
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>
                </div>

                <div class="panel panel-default panel-color panel-custom" style="margin-bottom: 0px;">
                    <!-- Раздел Работы -->
                    <div class="panel-heading">
                        <h3 class="panel-title">Логирование</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-top: 0px;"> таблица с логами</label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-sm-5 fixx">

                <div class="panel panel-border panel-custom" style="position: relative;"> <!-- Детали обращения -->
                    <div class="panel-heading request_details collapsed" style="cursor: pointer;" data-toggle="collapse"
                         data-parent="#accordion-test-2" href="#collapse-ditail" aria-expanded="false"
                         class="collapsed">
                        <div class="panel-title">
                            <a style="text-decoration: none;">
                                функционал администрирования
                            </a>

                        </div>
                    </div>
                    <div id="collapse-ditail" class="panel-collapse collapse">

                    </div>
                    <div class="panel panel-default panel-custom"
                         style="width:100%;position: absolute; position: absolute; max-height: calc(100vh - 100% - 80px); margin-top: 10px;overflow: auto;">
                        <!-- Cgbcjr ltqcndbq -->
                        <div class="panel-heading">
                            <h3 class="panel-title">элементы управления</h3>
                        </div>

                        <ul class="list-group events_list" style="max-height: 100%;">
                            <!-- col-sm-5 col-sm-offset-7  Список действий формируется JS, шаблон в конце документа -->
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </form>

<?php } ?>

<?php if (!empty($request['NUMBER'])) { ?>

    <script src="/public/js/view_request_logic.js"></script>
    <script src="/public/js/resize.js"></script>
    <script>
        $('.escalation .btn').click(function () {
            $('.hold .active').removeClass('active');
        });
        $('.hold .btn').click(function () {
            $('.escalation .active').removeClass('active');
        });

        $(document).ready(function () {
            $(".problem_name").select2();
            $('.REAL_SITE').select2();
        });

        $(document).on("ready", function () { //Подгружаем информацию







        });
        $('.user_answer_button').change(function () {
            $('.user_answer_container').toggle();
        });

        events = [];

        $(document).ready(function () {
            $.each(events, function (key, value) {
                event($('.events_list'), value);
            });
        });



        /*
        search = setInterval(function () {
            if (ready == 1) {
                $.ajax({
                    method: "POST",
                    url: "/ajax/CheckTicket",
                    data: {name: "<?php echo $tab_number ?>"}
                }).done(function (data) {
                    if (data != 0) {
                        console.log(data);
                        $('.search_progress').append('<br>Найдена заявка №' + data);
                        clearInterval(search);
                        window.location.replace('/view_request');
                    } else {
                        console.log(data);
                        $('.search_progress').append('<br>Идет поиск заявки...');
                    }
                });
            }
        }, 2000);
        */
    </script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('.list-group-item-text').each(function () {
            if ($(this).height() > 48) {
                $(this).addClass('collapsed_text');
            }
        });
    });
    $('body').on('click', '.collapsed_text', function () {
        $(this).removeClass('collapsed_text');
    });
    $(document).ready(function () {
        $('base').remove();
    });
</script>
