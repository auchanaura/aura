
<?php
use Aura\Core\Core;
use Aura\Core\ProcessData;

$cim = $lv = $TIME_LIMIT = $filter_values = [];
	foreach ($GLOBALS["variables"]['CIM'] as $key => $value) {
		$cim[$value['value']] = $value['title'];
	}
	foreach ($GLOBALS["variables"]['LV'] as $key => $value) {
		$lv[$value['value']] = $value['title'];
	}
	foreach ($GLOBALS["variables"]['TIME_LIMIT'] as $key => $value) {
		$TIME_LIMIT[$value['value']] = $value['title'];
	}
	$spread = [$cim,$lv,$TIME_LIMIT];
	$filters = ProcessData::filterData($spread,$pool,['NUMBER','CIM','LV','USER_LOGIN','STATUS_GLOBAL','REAL_SITE','WORKER']);
	$filter_values = $filters[1];
	$filters = $filters[0];

	$disabled = ($user['ADMIN'] != 0 && @$_GET['mode'] != 6 && @$_GET['mode'] != 4 ? '' : 'disabled');
?>
<!-- дублирование глобальных данных в меню распреда на js-->
<script>
    // TODO в дольнейшем это нужно вынести в store.js
    var data_for_menu = JSON.parse('<?php echo json_encode($spread); ?>');
</script>
<script>
	$('title').text('Курирование');
</script>

<!-- Контер для собственного контекстного меню. По умолчания - скрыт. -->
<div id="contextMenuId" style="position:absolute; top:0; left:0; border:3px solid #5fbeaa; border-width:2px; border-top-style:groove; background-color:#ebeff2; display:none; float:inherit; text-align:center;"></div>
<!-- ФИЛЬТР -->
<div class=" col-sm-7">
	<div class="panel panel-color panel-custom">
	
		<div class="panel-heading filter_container" style="position: relative;">
			<div class="panel-title">Фильтр</div>
			<span class="label label-purple" style="float: right; position: absolute; right: 10px; top: 7px;"><?php echo count($pool) ?></span></div>

		<div class="panel-body filter_body" style="display: none;">
			<?php foreach ($filters as $key => $value) {
				Core::renderFilter($key,$value,$filter_values);
			} ?>
		
		</div>
	</div>
</div>

<!-- НАВИГАЦИЯ ПО КУРИРОВАНИЮ, ВЕРХНЕЕ МЕНЮ -->
<ul class="nav nav-tabs" style="float: left; display: block;margin-left: 11px; clear: left;">
<!-- если Роль 2 ( роль для МГ ), то соответствующий интерфейс (вкладки с данными) -->	
<?php if ($user['ROLE'] == 2) { ?>
		<li role="presentation" style="background-color: #5fbeaa;">
			<a class="waves-effect waves-light send_new">
				<div style="color: #ffffff;">Просмотренно</div>
			</a>
		</li>
		<li role="presentation" <?php if (empty($_GET['mode']) || $_GET['mode'] == 2) {echo 'class="active"'; } ?>><a href="/curation">В ожидании<span class="badge"><?php if (empty($_GET['mode']) || $_GET['mode'] == 2) {echo count($pool);} else {echo $count[0][0][''];} ?></span></a></li>
		<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 6) {echo 'class="active"'; } ?>><a href="/curation?mode=6">В работе<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 6) {echo count($pool);} else {echo $count[2][0][''];} ?></span></a></li>
		<?php if ($user['GROUPE'] == 'МГ - ЦО Москва'): ?>
			<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 5 ) {echo 'class="active"'; } ?>><a href="/curation?mode=5">Мои заявки<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 5) {echo count($pool);} else {echo $count[1][0][''];} ?></span></a></li>
		<?php endif ?>
<!-- если ЦП, то соответствующий интерфейс (вкладки с данными) -->	
<?php } else if (!$count[0] != -1 && $user['ROLE'] != 3 && $user['ROLE'] != 4) { ?>

	<?php if (empty($_GET['mode']) || $_GET['mode'] != 1) { ?>
		<li role="presentation" style="background-color: #5fbeaa;">
			<a class="waves-effect waves-light send_curation">
				<div style="color: #ffffff;">Распределить</div>
			</a>
		</li>
	<?php } ?>
	
		<li role="presentation" <?php if (empty($_GET['mode']) || $_GET['mode'] != 1) {echo 'class="active"'; } ?>><a href="/curation">новые <span class="badge"><?php if (empty($_GET['mode']) || $_GET['mode'] != 1) {echo count($pool); } else {echo $count[0][''];} ?></span></a></li>
		<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 1) {echo 'class="active"'; } ?>><a href="/curation?mode=1">Спул <span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 1) {echo count($pool);} else {echo $count[0][''];} ?></span></a></li>

        <!--<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 101) {
            echo 'class="active"';
        } ?>><a href="/curation?mode=101">Спул 1 уровень <span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 101) {
            echo count($pool);
        } else {
            echo $count[0][''];
        } ?></span></a></li>
		<!--<li><a href="/appeal">Обращения</a></li>-->
	
<?php } else if ($user['ROLE'] == 3) { ?>
	<li role="presentation" <?php if (empty($_GET['mode']) || ($_GET['mode'] != 4 && $_GET['mode'] != 9 && $_GET['mode']!= 10  && $_GET['mode']!= 11)) {echo 'class="active"'; } ?>><a href="/curation">Активные <span class="badge"><?php if (empty($_GET['mode']) || ($_GET['mode'] != 4 && $_GET['mode'] != 9 && $_GET['mode'] != 10 && $_GET['mode'] != 11)) {echo count($pool); } else {echo $count[0][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 11) {echo 'class="active"'; } ?>><a href="/curation?mode=11">Холд<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 11) {echo count($pool);} else {echo $count[4][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 4) {echo 'class="active"'; } ?>><a href="/curation?mode=4">Мой профиль<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 4) {echo count($pool);} else {echo $count[1][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 10) {echo 'class="active"'; } ?>><a href="/curation?mode=10">Мои заявки<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 10) {echo count($pool);} else {echo $count[3][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 9) {echo 'class="active"'; } ?>><a href="/curation?mode=9">1C <span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 9) {echo count($pool);} else {echo $count[2][0][''];} ?></span></a></li>
	<!-- если Роль 4 ( ??? ), то соответствующий интерфейс (вкладки с данными)-->	
<?php } else if ($user['ROLE'] == 4) { ?>
	<li role="presentation" <?php if (empty($_GET['mode']) || ($_GET['mode'] != 8 && $_GET['mode'] != 10)) {echo 'class="active"'; } ?>><a href="/curation">Спул 1C<span class="badge"><?php if (empty($_GET['mode']) || ($_GET['mode'] != 8 && $_GET['mode'] != 10)) {echo count($pool); } else {echo $count[1][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 8) {echo 'class="active"'; } ?>><a href="/curation?mode=8">В работе 1C<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 8) {echo count($pool);} else {echo $count[0][0][''];} ?></span></a></li>
	<li role="presentation" <?php if (!empty($_GET['mode']) && $_GET['mode'] == 10 ) {echo 'class="active"'; } ?>><a href="/curation?mode=10">Мои заявки<span class="badge"><?php if (!empty($_GET['mode']) && $_GET['mode'] == 10) {echo count($pool);} else {echo $count[2][0][''];} ?></span></a></li>
<?php } ?>
</ul>
<? $even = 0; ?>
<!-- Контер для собственного контекстного меню. По умолчания - скрыт. -->
<div id="contextMenuId"
     style="position:absolute; top:0; left:0; border:1px solid #ebeff2; background-color:#ebeff2; display:none; float:left;"></div>
<!-- СПИСОК ЗАЯВОК -->
<ul class="list-group col-sm-7 tickets_container" oncontextmenu="return menu(1, event);" style="margin-bottom: 5px;clear: both;overflow-y: scroll;height: calc(100vh - 163px);" >
							</ul>
<div class="btn-group tickets_paginator" style="float: left;clear: both;padding-left: 10px;padding-right: 10px;text-align: center;width: 58.3%; margin-bottom: 10px;">
						</div>
						
<!-- ПРАВАЯ СТОРОНА -->
<div class="col-sm-5 fixx">
	<div class="panel panel-border panel-custom" style="position: relative; background: #ebeff2;">
		<div class="panel-heading">
			<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Содержание</div>
		</div>
		<div class="panel-body ticket_eml_body" style="box-shadow: red 0px -2px 0px 0px inset; background:#ffffff;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;height:calc(50vh - 80px); padding: 0px 0px 2px 20px; overflow:hidden;max-height: calc(100vh - 80px)">
			<div class="ticket_eml_body_container" style="overflow-y: scroll; height: 100%; padding-right: 10px; padding-bottom: 18px"></div>
		</div>

		<div class="panel panel-border panel-custom" style="width:100%;position: absolute; position: absolute;  margin-top: 10px;overflow: auto;">
			<div class="panel-heading">
				<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Действия</div>
			</div>	
			<div class="panel-body event_body" style="height:calc(50vh - 80px);max-height: calc(100vh - 80px);overflow: hidden; padding:0px 0px 0px 0px;    margin-left: -1px;">
			<div class="event_body_container" style="overflow-y: scroll; height: 100%; padding-right: 0px; margin-right: -1px;"></div>
			</div>
		</div>
	</div>
</div>

<!-- ШАБЛОНЫ ДЛЯ ВЫПАДАЮЩИХ МЕНЮ -->
<?php $this->js[] = 'js/resize.js'; ?>
<ul class="manage_list_template" style="display: none;">
<?php if (@$_GET['mode'] != 6 ): ?>
	<?php if ($user['ADMIN'] != 0): ?>
		<?php foreach ($spread[0] as $key2 => $value2): ?>
	    	<li class="dropdown-submenu">
	    	<a tabindex="-1" class="curation_update_select" data-cim="<?php echo $key2 ?>"><?php echo $value2 ?></a>
	        	<ul class="dropdown-menu">
	        	<?php foreach ($spread[1] as $key3 => $value3): ?>
	        		<?php if ($key3 == 3) {
	        			$group = '%USER_SITE%';
	        		} else {
	        			$group = $key3;
	    			} ?>
	        		<li class="dropdown-submenu">
	            		<a tabindex="-1" class="curation_update_select" data-cim="<?php echo $key2 ?>" data-group="<?php echo $group ?>"><?php echo $value3 ?></a>
	                	<ul class="dropdown-menu">
	                	<?php foreach ($spread[2] as $key4 => $value4): ?>
		                	<li>
	                    		<a class="curation_update_select" data-cim="<?php echo $key2 ?>" data-group="<?php echo $group ?>" data-min="<?php echo $value4 ?>"><?php echo $value4 ?> минут</a>
	                		</li>
	                	<?php endforeach ?>
	                	</ul>
	            	</li>
	        	<?php endforeach ?>
	        	</ul>
	    	</li>
		<?php endforeach ?>
	<li role="separator" class="divider"></li>
	<?php endif ?>
	<?php if ((($user['ROLE'] == 2 && $user['GROUPE'] == 'МГ - ЦО Москва') || $user['ROLE'] == 4 || $user['ROLE'] == 3) && $user['ADMIN'] >= 1): ?>
		<li class="dropdown-submenu">
	    	<a tabindex="-1">Назначить</a>
    			
	        	<ul class="dropdown-menu" style="margin-top: -112%;">

	        	<?php foreach ($workers as $key4 => $value4): ?>
	            	<li>
	            		<a style="cursor: pointer;" class="redirect_to_worker" data-name="<?php echo $value4['FIO'] ?>" data-worker="<?php echo $value4['LOGIN'] ?>"><?php echo $value4['FIO'] ?></a>
	        		</li>
	        	<?php endforeach ?>
	        	</ul>
		</li>
		<li style="%WORKER%">
    		<a class="redirect_to_worker" data-name="" data-worker="">Снять с человека</a>
		</li>
		<?php if ($user['ROLE'] == 3 || $user['ROLE'] == 4): ?>
			<li>
	    		<a class="redirect_to_worker" data-name="<?php echo $user['FIO'] ?>" data-worker="<?php echo $user['LOGIN'] ?>">Назначить на себя</a>
			</li>
		<?php endif ?>
	<?php endif ?>
	
	<li>
		<a class="open_modal_window" data-window="comment" tabindex="-1">Комментарий</a>
	</li>
	<li>
		<a class="open_modal_window" data-window="to_work" tabindex="-1">Поднять в работу</a>
	</li>
	<!--<li>
		<a class="open_modal_window" data-window="close_ticket" tabindex="-1">Закрыть</a>
	</li>-->
	<?php if ($user['LV'] == 3 || $user['LV']==0 || $user['LV']==5) {?>
	<li>
		<?php if ($user['ACTIVE_TICKET'] == 0 || @$_GET['mode'] == 5): ?>
    		<a class="take_job" tabindex="-1" style="color:green">Взять основной</span></a>
		<?php endif ?>
		<?php if ($user['SECOND_TICKET'] == 0 || @$_GET['mode'] == 5): ?>
			<a class="take_fon" tabindex="-1" style="color:orange">Взять фоновой</span></a>
		<?php endif ?>
    </li>
	<?php } ?>
<?php endif ?>
</ul>
<!-- поля для деталей по заявке-->
<div class="detailed_info_template" style="display: none;">
	<ul class="list-group" style="margin-bottom: 0px;">
		<li class="list-group-item info list-info number">Номер: </li>
		<li class="list-group-item info status0">Время рег.: </li>
		<li class="list-group-item info user_fio">Заявитель: </li>
		<li class="list-group-item info problem_list" data-blocked="<?php if ($user['ADMIN'] >= 1): ?>1<?php endif ?>">Тема: </li>
		<li class="list-group-item info user_site">Сайт: </li>
		<li class="list-group-item info user_mail">Емейл: </li>
		<li class="list-group-item info user_phone">Телефон: </li>
		<li class="list-group-item info user_login">Логин: </li>
		<li class="list-group-item info user_post">Должность: </li>
		<li class="list-group-item info event_number">Номер ивента: </li>
		<li class="list-group-item info attachments">Вложения: </li>
	</ul>
</div>
<script>
	
	
	function formatResult(node) {
		var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
		return $result;
	};
	var problem_data = [<?php foreach ($problems_list[1] as $key => $value) {
	echo '{id: "'.$key.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].'</span>", level : 0, disabled: '.($problems_list[0][$key]['Get'] == 0 ? 1 : 0).'},';
	foreach ($value as $key2 => $value2) {
		echo '{id: "'.$key2.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].'</span>", level : 1, disabled: '.($problems_list[0][$key2]['Get'] == 0 ? 1 : 0).'},';
		foreach ($value2 as $key3 => $value3) {
			echo '{id: "'.$value3.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].' - '.$problems_list[0][$value3]['TOPIC'].'</span>", level : 2, disabled: '.($problems_list[0][$value3]['Get'] == 0 ? 1 : 0).'},';
		}
	}
	} ?>];

</script>
<select class="real_sites_template" style="display: none;">
	<optgroup label="Мобильная группа">
		<?php foreach ($real_sites as $key2 => $value2): ?>
			<option value="<?php echo $value2['MAGAZ'] ?>"><?php echo $value2['MAGAZ'] ?></option>
		<?php endforeach ?>
	</optgroup>
</select>
<?php if ((empty($_GET['mode']) || $_GET['mode'] != 1) && !$count[0] != -1) {
	$this->js[] = 'js/send_curation.js';
} ?>
<?php if ($user['ADMIN'] == 2 && $user['ROLE'] == 2) {
	$this->js[] = 'js/see_new.js';
} ?>

<script>
	user = <?php echo(json_encode($user)) ?>;
	spread = <?php echo(json_encode($spread)) ?>;
	<?php require(AURA_VIEWS_PATH.'js_templates/events_js.php') ?>
	<?php require(AURA_VIEWS_PATH.'js_templates/ticket_js.php') ?>
	Tickets = [];
	old_Tickets = [];
	<?php $ticket_list = '['; ?>
	<?php foreach ($pool as $key => $value) {
		$ticket_list .= $value['NUMBER'] .','; 
		$value = ProcessData::pregEscape($value); ?>
		Tickets['<?php echo $value['NUMBER'] ?>'] = JSON.parse('<?php echo json_encode($value) ?>');
		old_Tickets['<?php echo $value['NUMBER'] ?>'] = JSON.parse('<?php echo json_encode($value) ?>');
	<?php } ?>
	<?php $ticket_list .= ']'; echo 'ticket_list = ' . $ticket_list; ?>
</script>
<script src="/public/js/menu_for_cur.js"></script>
<script src="/public/js/ticket_select.js"></script>
<?php $this->js[] = 'js/filter.js'; ?>
<script>
	$('body').on('change','.ticket_update_options',function(){
		number_r = $(this).parents('.curator_i').data('number');
		ManageTicket(number_r);
	});
	real_sites = [];
	<?php foreach ($real_sites as $key => $value): ?>
		real_sites[<?php echo $value['NUMBER']*1 ?>] = '<?php echo $value['GROUPE'] ?>';
	<?php endforeach ?>
	changed_tickets = [];
	function ManageTicket(number_r) {
		<?php if ((empty($_GET['mode']) || $_GET['mode'] != 1) && $user['ROLE'] == 1) { ?>
		$.ajax({
			method: "POST",
			url: "/ajax/manageTicket",
			data: { ticket: number_r, CIM: Tickets[number_r].CIM, PHYSICAL: Tickets[number_r].PHYSICAL, KERNEL_JOB_TIME_LIMIT: Tickets[number_r].KERNEL_JOB_TIME_LIMIT, LV: Tickets[number_r].LV, GROUPE: real_sites[parseInt(String(Tickets[number_r].LV).replace(/\D+/g,""))]}
		}).done(function(data) {
			console.log(data);
		});
	<?php } else { ?>
		if (changed_tickets.indexOf(String(number_r)) == -1) {
			changed_tickets.push(String(number_r));
		}
	<?php } ?>
	}
</script>
<?php if ((!empty($_GET['mode']) && ($_GET['mode'] != 0 || ($count[0] == -1 && $user['ADMIN'] != 0)) || $user['ROLE'] != 1)) {
	$this->js[] = 'js/send_curation_spool.js';
} ?>
<script>
//Вынести в отдельный модуль
	$('body').on('click','.redirect_to_worker',function(){
		container = $(this);
		number = $(this).parents('.curator_i').data('number');
		login = $(this).data('worker');
		name = $(this).data('name');
		$.ajax({
		method: "POST",
		url: "/ajax/redirectToWorker",
		data: {number: number, login: login}
		}).done(function(data) {
			if (!data){
				$.Notification.notify('error','top center','Невозможно назначить заявку','');			
			} else { 
				$.Notification.notify('success','top center','Заявка '+number+' успешно назначена!','');
				container.parents('.curator_i').find('.worker_name').html(name);
			}
		});
	});
	$('body').on('change','.problems_list',function(){
		container = $(this);
		number = $(this).parents('.curator_i').data('number');
		if (Tickets[number].PROBLEM_ID != $(this).val() && $(this).val() != 0 && $(this).val() != null) {
			Tickets[number].PROBLEM_ID = $(this).val();
			$.ajax({
			method: "POST",
			url: "/ajax/changeTopic",
			data: {number: number, problem_id: $(this).val(), problem_name: $(this).find("option:selected").text()}
			}).done(function(data) {
				if (!data){
					$.Notification.notify('error','top center','Невозможно изменить тему','');			
				} else { 
					$.Notification.notify('success','top center','Тема заявки '+number+' успешно изменена!','');
					container.parents('.curator_i').addClass('list-group-item-success');
				}
			});
		}
	});
	$('body').on('click','.take_job',function(){
		number = $(this).parents('.curator_i').data('number');
		$.ajax({
		method: "POST",
		url: "/ajax/takeJobLV3",
		data: {ticket: number}
		}).done(function(data) {
            console.log(data);
			if (data==0){
					$.Notification.notify('error','top center','Нельзя забрать заявку себе в работу!','У Вас уже есть заявка в работе. Закройте текущую заявку и повторите попытку.');			
			} else if (data == 1) { 
				$.Notification.notify('success','top center','Заявка '+number+' успешно забрана!','Переход на страницу рабочего процесса...');
				setTimeout(function(){window.location.replace('/view_request');}, 1000);				
			} else {
				$.Notification.notify('success','top center','Заявка '+data+' заменена заявкой '+number,'Переход на страницу рабочего процесса...');
				setTimeout(function(){window.location.replace('/view_request');}, 1000);	
			}
		});
	});
	$('body').on('click','.take_fon',function(){
		number = $(this).parents('.curator_i').data('number');
		$.ajax({
		method: "POST",
		url: "/ajax/takeFon",
		data: {ticket: number}
		}).done(function(data) {
			if (data==0){
					$.Notification.notify('error','top center','Нельзя забрать заявку себе в работу!','У Вас уже есть заявка в работе. Закройте текущую заявку и повторите попытку.');			
			} else { 
					$.Notification.notify('success','top center','Заявка '+number+' успешно взята в работу!','Переход на страницу фоновой заявки...');
					setTimeout(function(){window.location.replace('/background');}, 1000);
			}
		});
	});
</script>
<script>
	page_num = 0;
	num_of_tickets_per_page = Math.floor($('.tickets_container').height()/74);
	filtered_ticket_list = ticket_list;
	function render_ticket_list(page) {
		num_of_tickets_per_page = Math.floor($('.tickets_container').height()/74);
		page_num = page;
		ticket_number = page*num_of_tickets_per_page;
		$('.tickets_container').html('');
		while (ticket_number < page*num_of_tickets_per_page + num_of_tickets_per_page) {
			if (ticket_number >= filtered_ticket_list.length) {
				break;
			}
			add_ticket($('.tickets_container'),Tickets[filtered_ticket_list[ticket_number]],user,spread,num_of_tickets_per_page/2 - ticket_number%num_of_tickets_per_page);
			ticket_number++;
		}
		paginator = '<button style="display: inline-block; float:none;" data-page="prev" type="button" class="btn btn-white waves-effect select_ticket_page"><span class="glyphicon glyphicon-arrow-left"></span></button>';
		if (page == 0) {
			paginator = '<button disabled style="display: inline-block; float:none;" data-page="prev" type="button" class="btn btn-white waves-effect select_ticket_page"><span class="glyphicon glyphicon-arrow-left"></span></button>';
		}

		if (page - 3 >= 0) {
			paginator += '<button style="width: 34px; display: inline-block; float:none; margin-left: 0px;" data-page="'+(page-3)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page-2)+'</button>';
		}
		if (page - 2 >= 0) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+(page-2)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page-1)+'</button>';
		}
		if (page - 1 >= 0) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+(page-1)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page)+'</button>';
		}
		if (page >= 0) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+page+'" type="button" class="btn btn-default waves-effect select_ticket_page">'+(page+1)+'</button>';
		}
		if (page + 1 < (filtered_ticket_list.length/num_of_tickets_per_page)) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+(page+1)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page+2)+'</button>';
		}
		if (page + 2 < ((filtered_ticket_list.length/num_of_tickets_per_page))) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+(page+2)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page+3)+'</button>';
		}
		if (page + 3 < ((filtered_ticket_list.length/num_of_tickets_per_page))) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="'+(page+3)+'" type="button" class="btn btn-white waves-effect select_ticket_page">'+(page+4)+'</button>';
		}
		if (page < ((filtered_ticket_list.length/num_of_tickets_per_page) - 1)) {
			paginator += '<button style="width: 34px;display: inline-block; float:none; margin-left: 0px;" data-page="next" type="button" class="btn btn-white waves-effect select_ticket_page"><span class="glyphicon glyphicon-arrow-right"></span></button>'
		} else {
			paginator += '<button disabled style="display: inline-block; float:none; margin-left: 0px;" data-page="next" type="button" class="btn btn-white waves-effect select_ticket_page"><span class="glyphicon glyphicon-arrow-right"></span></button>'

		}
		$('.tickets_paginator').html('');
		$('.tickets_paginator').append(paginator);
	}
	$('body').on('click','.select_ticket_page',function(){
		if (Number.isInteger($(this).data('page'))) {
			page_num = $(this).data('page');
		} else if ($(this).data('page') == 'prev') {
			page_num--;
		} else if ($(this).data('page') == 'next') {
			page_num++;
		}

		render_ticket_list(page_num);
	});
	$(document).ready(function(){
		render_ticket_list(0);
	});
	$(window).keydown(function(e){
	if (e.keyCode ==37) {
		if (page_num >0 ) {
			page_num--;
			render_ticket_list(page_num);
		}
		return false;
	} else if (e.keyCode == 39) {
		if (page_num <ticket_list.length/num_of_tickets_per_page - 1) {
			page_num++;
			render_ticket_list(page_num);
		}
		return false;
	}
});
</script>
