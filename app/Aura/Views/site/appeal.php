<?php

	$appeal = [];
	foreach ($GLOBALS["variables"]['APPEAL'] as $key => $value) {
		$appeal[$value['value']] = $value['title'];
	}
?>

<style>
    
	.slider.slider-vertical {
		height: 75px;
	}
	.slider.slider-horizontal {
		width: 100%;
		margin: 2px 0;
	}
	.slider.slider-success {
		width: 100%;
	}
	.slider_label {
	    margin-bottom: 0;
	    font-size: 13px;
	    font-weight: 700;
	    margin-top: -3px;
	    display: block;
	    color: #333333;
	}
</style>

<ul class="list-group col-sm-7 tickets_container" style="clear: both;overflow-y: scroll;height: calc(100vh - 75px);" >
<?php $even = 0;?>
	<?php foreach ($actions as $key => $value): ?>
		<li class="list-group-item curator_i" data-number="<?php echo $value['NUMBER'] ?>" data-action="<?php echo $value['ACTION_NUMBER'] ?>" style="<?if ($even++ % 2 == 0){}?>"  >
	<div class="tb_container">
		<div class="ticket_body" style="margin-right: 120px;">
			<div style="width: 76px; float: left; margin-top: -2px">
				<span class="number_desc" ><?php echo $value['NUMBER'] ?></span>
			</div>
			<div style="margin-left: 80px;">
				<span class="ticket_title"><?php echo $appeal[$value['appeal_type']]?> (<?php echo $value['ACTION_NAME']; ?>)</span>
			</div>
			<span class="ticket_date" style="float: right; font-size: 10px; margin-top: -16px;">
				<?php @$date = date_diff(new DateTime('NOW'), new DateTime($value['DATETIME_INIT']));
					if ($date->format('%d') == 0) {
						if ($date->format('%H') == 0) {
							echo $date->format('%i м.');
						} else {
							echo $date->format('%H ч. %i м.');
						}
					} else {
						echo $date->format('%d д.');
					} ?>
		    </span>
			<div class="action_icon_container">
				<img style="width: 100%;" class="media-object action_icon_image" src="/public/img/ticket/<?php echo $value['ACTION_CODE']?>.png">
			</div>
			<div class="ticket_description" style="clear:both;">
			<?php if (!empty($value['JUSTIFY'])) { ?>
				<?php echo nl2br($value['JUSTIFY']) ?>
			<?php } else { ?>
				<?php echo nl2br($value['COMMENT_TECH']) ?>
			<?php } ?></div>
			<div style="line-height: 120%;">
				<span class="ticket_fio"><?php echo ($value['OWNER']) ?> (<?php echo $value['TIME_SPEND'] ?>)</span>
			</div>
		</div>
		<div class="tuco" style="width: 120px;">
			<?php render_appeal_buttons($value) ?>
		</div>
	</div>
	<div class="detailed_info">
	</div>
	<div class="row complaint_comment" style="display: none; margin-top: 7px; padding-top: 7px; border-top: 1px solid #dfdfdf;">
		<div style=""></div>
		<div class="col-sm-9" style="">
			<textarea class="form-control comment"></textarea>
		</div>
		<div class="col-sm-3" style="height: 89px; position: relative;">
		<?php if ($value['appeal_type'] == $GLOBALS['variables']['APPEAL']['justify']['value']) { ?>
			<label class="slider_label" style="display: none;">Выберите бонус: <span class="slider_value">0</span></label>
			<div class="slider slider-success" style="display: none;">
	            <input type="text" class="bootstrapSlider" value="0" data-slider-orientation="horizontal" data-slider-min="0" data-slider-max="<?php echo $value['KERNEL_JOB_TIME_LIMIT'] ?>" data-slider-value="0" data-slider-selection="before" data-slider-reversed="false">
	        </div>
		<?php } else if ($value['appeal_type'] == $GLOBALS['variables']['APPEAL']['speed_up']['value']) { ?>
			<select class="selectpicker" data-style="btn-primary btn-custom" style="display: none;" title="Выберите CIM">
				<?php foreach ($GLOBALS['variables']['CIM'] as $key => $value): ?>
					<option value="<?php echo $value['value'] ?>"><?php echo $value['title'] ?></option>
				<?php endforeach ?>
			</select>
		<?php } ?>
			<button type="button" class="btn btn-default btn-lg waves-effect waves-light submit_button" disabled style="left: 10px;margin-top: 4px; width: calc(100% - 20px); position: absolute; bottom: 0;">Отправить</button>
		</div>
	</div>
	<div class="ticket_event_container" style="margin-left:-10px;margin-right:-10px; border-width: 1px; border-style: solid; border-color:#b5b3b3;"></div>
</li>
	<?php endforeach ?>
</ul>
<script>
	$('.curator_i .comment').change(function(){
		if ($(this).val() != '') {
			$(this).parents('.curator_i').find('.submit_button').prop('disabled',false);
		}
	});
	$('.submit_button').click(function(){
		$(this).parents('.curator_i').addClass('list-group-item-success').removeClass('list-group-item-info');
		$(this).parents('.curator_i').find('.complaint_comment').hide();
		$(this).parents('.curator_i').find('.ticket_event_container').hide();
	});
	$('input[name="appeal"]').change(function(){
		if ($('input[name="appeal"]:checked').val() == 3) {
			$(this).parents('.curator_i').find('.bootstrapSlider').slider();
			$(this).parents('.curator_i').find('.slider').show();
			$(this).parents('.curator_i').find('.slider_label').show();
		} else if ($('input[name="appeal"]:checked').val() == 8) {
			$(this).parents('.curator_i').find('.selectpicker').show();
		} else {
			$(this).parents('.curator_i').find('.slider_label').hide();
			$(this).parents('.curator_i').find('.selectpicker').hide();
			$(this).parents('.curator_i').find('.slider').hide();
		}
		$(this).parents('.curator_i').find('.complaint_comment').show();
	});
</script>
</ul>
<div class="col-sm-5 fixx">
	<div class="panel panel-border panel-custom" style="position: relative; background: #ebeff2;">
		<div class="panel-heading">
			<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Содержание</div>
		</div>
		<div class="panel-body ticket_eml_body" style="box-shadow: red 0px -2px 0px 0px inset; background:#ffffff;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;height:calc(50vh - 80px); padding: 0px 0px 2px 20px; overflow:hidden;max-height: calc(100vh - 80px)">
			<div class="ticket_eml_body_container" style="overflow-y: scroll; height: 100%; padding-right: 10px; padding-bottom: 18px"></div>
		</div>

		<div class="panel panel-border panel-custom" style="width:100%;position: absolute; position: absolute;  margin-top: 10px;overflow: auto;">
			<div class="panel-heading">
				<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Действия</div>
			</div>	
			<div class="panel-body event_body" style="height:calc(50vh - 80px);max-height: calc(100vh - 80px);overflow: hidden; padding:0px 0px 0px 0px;    margin-left: -1px;">
			<div class="event_body_container" style="overflow-y: scroll; height: 100%; padding-right: 0px; margin-right: -1px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="detailed_info_template" style="display: none;">
	<ul class="list-group" style="margin-bottom: 0px;">
		<li class="list-group-item info list-info number">Номер: </li>
		<li class="list-group-item info status0">Время рег.: </li>
		<li class="list-group-item info user_fio">Заявитель: </li>
		<li class="list-group-item info problem_list" data-blocked="0">Тема: </li>
		<li class="list-group-item info user_site">Сайт: </li>
		<li class="list-group-item info user_mail">Емейл: </li>
		<li class="list-group-item info user_login">Логин: </li>
		<li class="list-group-item info user_post">Должность: </li>
		<li class="list-group-item info event_number">Номер ивента: </li>
		<li class="list-group-item info attachments">Вложения: </li>
	</ul>
</div>
<?php $this->js[] = 'js/resize.js'; ?>
<script>
	Tickets = [];
	<?php foreach ($actions as $key => $value) { ?>
		Tickets[<?php echo $value['NUMBER'] ?>] = [];
	<?php } ?>
</script>
<?php $this->js[] = 'js/ticket_select.js'; ?>
<?php $this->js['plain'] = "full_info = 1;" ?>
<?php //$this->js[] = 'js/filter.js'; ?>
<script>
	<?php require(AURA_VIEWS_PATH.'js_templates/events_js.php') ?>
</script>
<?php function render_appeal_buttons($action) { ?>
	<?php if ($action['appeal_type'] == $GLOBALS['variables']['APPEAL']['justify']['value']) { ?>
		<div class="col-sm-12" style="padding-left: 2px;">
			<div class="button-list " style="text-align: center;" data-toggle="buttons">
				<label class="btn btn-danger btn-custom waves-effect waves-light btn-xs" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="1" autocomplete="off"> Виноват
				</label>
				<label class="btn btn-default btn-custom waves-effect waves-light btn-xs" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="2" autocomplete="off"> Оправдан
				</label>
				<label class="btn btn-success btn-custom waves-effect waves-light btn-xs" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="3" autocomplete="off"> Молодец
				</label>
			</div>
		</div>
	<?php } else if ($action['appeal_type'] == $GLOBALS['variables']['APPEAL']['complaint']['value']) { ?>
		<div class="col-sm-12" style="padding-left: 2px;">
			<div class="button-list " style="text-align: center;" data-toggle="buttons">
				<label class="btn btn-default btn-custom waves-effect waves-light btn-sm" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="4" autocomplete="off"> Подтвердить
				</label>
				<label class="btn btn-warning btn-custom waves-effect waves-light btn-sm" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="5" autocomplete="off"> Отклонить
				</label>
			</div>
		</div>
	<?php } else if ($action['appeal_type'] == $GLOBALS['variables']['APPEAL']['reclamation']['value']) { ?>
		<div class="col-sm-12" style="padding-left: 2px;">
			<div class="button-list " style="text-align: center;" data-toggle="buttons">
				<label class="btn btn-primary btn-custom waves-effect waves-light btn-sm" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="6" autocomplete="off"> Написать ответ
				</label>
			</div>
		</div>
	<?php } else if ($action['appeal_type'] == $GLOBALS['variables']['APPEAL']['speed_up']['value']) { ?>
		<div class="col-sm-12" style="padding-left: 2px;">
			<div class="button-list " style="text-align: center;" data-toggle="buttons">
				<label class="btn btn-warning btn-custom waves-effect waves-light btn-sm" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="7" autocomplete="off"> Отклонить
				</label>
				<label class="btn btn-default btn-custom waves-effect waves-light btn-sm" style="margin-bottom: 3px;">
					<input type="radio" name="appeal" value="8" autocomplete="off"> Ускорить
				</label>
			</div>
		</div>
	<?php } ?>
<?php } ?>
<script>



	$('.bootstrapSlider').change(function(){
		$(this).parent().parent().find('.slider_value').html($(this).parent().parent().find('.tooltip-inner').html());
	});


</script>
