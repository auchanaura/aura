
<div id="grid_array"></div>
<script>
data = <?php echo $info ?>;

$(function(){
	function pqDatePicker(ui) {
        var $this = $(this);
        $this
            .css({ zIndex: 3, position: "relative" })
            .datepicker({
                yearRange: "-20:+0", //20 years prior to present.
                changeYear: true,
                changeMonth: true,
                //showButtonPanel: true,
                onClose: function (evt, ui) {
                    $(this).focus();
                }
            });
        //default From date
        $this.filter(".pq-from").datepicker("option", "defaultDate", new Date("01/01/1996"));
        //default To date
        $this.filter(".pq-to").datepicker("option", "defaultDate", new Date("12/31/1998"));
    }
    var obj = {};

    var data = <?php echo $info ?>;
    settings = JSON.parse(localStorage.getItem('info'));
    
    if (settings == null) {
        obj.colModel = [];
        $.each(data[0],function(key,value){
            obj.colModel[key] = {title:value, width:180, dataType:"string", dataIndx: value, filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }};
        });

        obj.width = 1150;
        obj.height = 550;
    } else {
        obj.colModel = settings;
        obj.width = localStorage.getItem('info_grid_width');
        obj.height = localStorage.getItem('info_grid_height');
    }

    obj.title = 'Монитор';
    obj.resizable = true;
    obj.showBottom = false;
    obj.editable = false;
    obj.filterModel = { on: true, mode: "AND", header: true };
    obj.beforeTableView = function(event,ui) {
        localStorage.setItem('info',JSON.stringify($( "#grid_array" ).pqGrid( "getColModel" )));
        localStorage.setItem('info_grid_height',$( "#grid_array" ).height());
        localStorage.setItem('info_grid_width',$( "#grid_array" ).width());
    };

    data = data.splice(1,data.length);
    obj.dataModel = {data:data};
    $("#grid_array").pqGrid( obj );                                
 
});        
</script>  