<link href="/public/css/monitor_vendor_css.css" rel="stylesheet" type="text/css" />


<script>
	$('title').text('Монитор');
</script>
<style>
    .visible {
        display: table-row!important;
    }
    .bars.pull-left {
        width: calc(100% - 120px);
    }
    .card-box {
        padding: 0 10px 10px 10px;
    }
    .td_name {
        max-width: 160px;
    }
</style>

<script>
    filters_values = [];
    filters_values[1] = [];
    filters_values[2] = [];
    filters_values[3] = [];
    filters_values[4] = [];
    <?php $used_values = [[],[],[],[]]; ?>
    <?php $statuses = ['Не работает','Работает','Обед'] ?>
    <?php foreach ($monitor as $key => $value) { ?>
        <?php if (empty($value['WORK_NOW'])) {
            $monitor[$key]['WORK_NOW'] = 0;
            $value['WORK_NOW'] = 0;
        } ?>
        <?php if (!in_array($value['FIO'], $used_values[0])) { ?>
            <?php $used_values[0][] = $value['FIO'] ?>
            filters_values[1].push({'id':"<?php echo rtrim($value['FIO']) ?>",'text':'<?php echo rtrim($value['FIO']) ?>'});
        <?php } ?>
        <?php /*if (!in_array($value['GROUPE'], $used_values[1])) { */?><!--
            <?php /*$used_values[1][] = $value['GROUPE'] */?>
            filters_values[2].push({'id':"<?php /*echo $value['GROUPE'] */?>",'text':'<?php /*echo $value['GROUPE'] */?>'});
        --><?php /*} */?>
        <?php if (!in_array($value['WORK_NOW'], $used_values[2])) { ?>
            <?php $used_values[2][] = $value['WORK_NOW'] ?>
            filters_values[3].push({'id':"wn_<?php echo $value['WORK_NOW'] ?>",'text':'<?php echo $statuses[$value['WORK_NOW']] ?>'});
        <?php } ?>
        <?php if (!in_array($value['ACTIVE_TICKET'], $used_values[3])) { ?>
            <?php $used_values[3][] = $value['ACTIVE_TICKET'] ?>
            filters_values[4].push({'id':"lv_<?php echo $value['ACTIVE_TICKET'] ?>",'text':'<?php echo $value['ACTIVE_TICKET'] ?>'});
        <?php } ?>
    <?php } ?>
</script>
<div class="row" style="margin-left: 0px; margin-right: -20px;">
    <div class="col-sm-12">
    	<div class="card-box">
            <div id="demo-delete-row" style="margin-left: -5px;">
                <div class="input-group filter_input col-sm-3" style="clear: none; margin-bottom: 0px;">
                    <label for="request" class="input-group-addon" style="font-weight: bold;">ФИО</label>
                    <select class="form-control filter_page_input filter_page_input_name" data-name="name" multiple="multiple">
                        <option value=""></option>
                    </select>
                </div>
                <div class="input-group filter_input col-sm-3" style="clear: none; margin-bottom: 0px;">
                    <label for="request" class="input-group-addon" style="font-weight: bold;">Группа</label>
                    <select class="form-control filter_page_input filter_page_input_groupe" data-name="groupe" multiple="multiple">
                        <option value=""></option>
                    </select>
                </div>
                <div class="input-group filter_input col-sm-3" style="clear: none; margin-bottom: 0px;">
                    <label for="request" class="input-group-addon" style="font-weight: bold;">Статус</label>
                    <select class="form-control filter_page_input filter_page_input_status" data-name="status" multiple="multiple" >
                        <option value=""></option>
                    </select>
                </div>
                <div class="input-group filter_input col-sm-3" style="clear: none; margin-bottom: 0px;">
                    <label for="request" class="input-group-addon" style="font-weight: bold;">Активная</label>
                    <select class="form-control filter_page_input filter_page_input_request" data-name="request" multiple="multiple">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <table id="demo-custom-toolbar" data-toggle="table" data-toolbar="#demo-delete-row" data-show-toggle="true" data-show-columns="true" data-sort-name="id" data-page-list="[500]" data-page-size="500" data-pagination="true" data-show-pagination-switch="true" class="table-bordered table table-striped table-sm">
                <thead >
                    <tr style="font-weight: bold;">
        				<th data-sortable="true" data-field="name" style="width: 200px;">ФИО</th>
        				<th data-sortable="true" data-field="groupe" style="width: 200px;">Группа</th>
						<th data-sortable="true" data-field="site" style="width: 200px;">Сайт</th>
                        <th data-sortable="true" data-field="status" style="width: 95px;">Статус</th>
                        <th data-sortable="true" data-field="request" style="width: 125px; text-align: center">Активная</th>
        				<th data-sortable="true" style="width: 50px;">В работе</th>
						<th data-sortable="true" data-field="request_fon" style="width: 125px; text-align: center">Фоновая</th>
                        <th data-sortable="true" style="width: 100px;">Начало</th>
                        <th data-sortable="true" style="width: 100px;">Окончание</th>
                        <th data-sortable="true" style="width: 85px;">Перерыв</th>
						<th data-sortable="true" style="width: 85px;">Закрыто</th>
						<th data-sortable="true" style="width: 85px;">Обработано</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($monitor as $key => $value): ?>
                        <?php
                        $display = "";
                        if (Aes::decrypt($_COOKIE['group'],COOKIE_PASS) != $value['GROUPE'] ) {
                            $display = 'display: none;';
                        } ?>
                        <tr data-number="<?php echo $value['ACTIVE_TICKET'] ?>" style="<?php echo $display ?>">
                            <td data-field="name" class="td_name"><?php echo trim(str_replace("&nbsp;", '', $value['FIO'])) ?></td>
            				<td data-field="groupe" ><?php echo $value['GROUPE'] ?></td>
							<td data-field="site" ><?php echo $value['SITE'] ?></td>
                            <td data-field="status" class="text-center">
        					<?php if ($value['WORK_NOW'] == 1) {
                                if ($value['BREAK_NOW'] == 1) {
                                    echo '<span class="label label-warning">Обед</span>';
                            } else {
                                    echo '<span class="label label-success">Работает</span>';
                                }

                            } else {
								if ($value['OUT'] == 1) {echo '<span class="label label-danger">Отсутствует</span>';}
							} ?></td>
        					<td data-field="request" class="text-center">
        						<?php if (!empty($value['ACTIVE_TICKET'])) {?>
        						<div class="btn-group">
                                    <button type="button" class="btn <?php $str = $value['ACTIVE_TICKET'];
                                    if (strpos($str, '9') == 6) { ?>btn-warning<?php } else { ?>btn-primary<?php } ?> dropdown-toggle waves-effect waves-light btn-xs "
                                            data-toggle="dropdown"
                                            aria-expanded="false"><?php echo $value['ACTIVE_TICKET'] . ' ' ?><span
                                                class="caret"></span></button>
        							<ul class="dropdown-menu" role="menu">
        									<?php if (Aes::decrypt($_COOKIE['admin'],COOKIE_PASS) > 0) {?>
													<li><a href="#">Снять заявку</a></li>
                                        <?php } ?>
                                        <li><a class="search_link"
                                               href="/search?request=<?php echo $value['ACTIVE_TICKET'] ?>&number=<?php echo $value['ACTIVE_TICKET'] ?>"
                                               target="blank">Найти в поиске</a></li>
        							</ul>
        						</div>
                            <?php } else {
                                echo 'Нет';
                            } ?>
        					</td>
        					<td class="text-center"><?php
        						if (!empty( $value['ACTIVE_TICKET'])) {
        						$time_diff = date_diff(new DateTime($value['ACTIVE_TICKET_STARTJOB']), new DateTime('NOW') );
        						$time_diff = $time_diff->format('%H:%I');
        						$plus_limit = new DateTime($value['ACTIVE_TICKET_STARTJOB']);
                                if (!empty($value['ACTIVE_TICKET_TIMELIMIT'])) {
                                    $plus_limit->add(new DateInterval('PT'.$value['ACTIVE_TICKET_TIMELIMIT'].'M')); 
                                }
        						$plus_limit = $plus_limit->format('Y-m-d H:i:s');
        						$now = new DateTime('NOW');
        						$now = $now->format('Y-m-d H:i:s');
        						if ($now>$plus_limit) {
									$time_diff2 = date_diff(new DateTime($plus_limit), new DateTime('NOW') );
									$time_diff2 = $time_diff2->format('%H:%I');
        							echo $time_diff.' '.'<span class="label label-danger" style="font-size:11px;">'.$time_diff2.'</span>';
        							} else {echo $time_diff;};
        						}
        					?></td>
							<td data-field="request_fon" class="text-center">
        						<?php if (!empty($value['SECOND_TICKET'])) {?>
        						<div class="btn-group">
        							<button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light btn-xs " data-toggle="dropdown" aria-expanded="false"><?php echo $value['SECOND_TICKET'].' '?><span class="caret"></span></button>
        							<ul class="dropdown-menu" role="menu">
        									<?php if (Aes::decrypt($_COOKIE['admin'],COOKIE_PASS) > 0) {?>
													<li><a href="#">Снять заявку</a></li>
                                        <?php } ?>
                                        <li><a class="search_link"
                                               href="/search?request=<?php echo $value['SECOND_TICKET'] ?>&number=<?php echo $value['SECOND_TICKET'] ?>"
                                               target="blank">Найти в поиске</a></li>
        							</ul>
        						</div>
                            <?php } else {
                                echo 'Нет';
                            } ?>
        					</td>
                            <td class="text-center"><?php echo $value['ON_TIME'] ?></td>
                            <td class="text-center"><?php echo $value['OFF_TIME'] ?></td>
                            <td class="text-center"><?php if ($value['BREAK_TIME']>0) {echo $value['BREAK_TIME'];} else {echo '<span class="label label-danger" style="font-size:11px;">'.$value['BREAK_TIME'].'</span>';} ?></td>
                           
                            <td class="text-center"><?php echo $value['TICKET_CLOSE'] ?></td>
                            <td class="text-center"><?php echo $value['TICKET_PER_DAY'] ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<ul class="monitor_context_menu dropdown-menu" style="z-index: 500000;">
    <li>
        <a href="#">Снять заявку</a>
    </li>
    <li>
        <a href="#">Переназначить</a>
    </li>
    <li>
        <a class="search_link" href="/search" target="blank">Открыть в поиске</a>
    </li>
</ul>
<script>
    /*$('td').contextmenu(function(){
        $('.monitor_context_menu').appendTo($(this)).show();
        $('.monitor_context_menu .search_link').attr('href','/search?request='+$(this).parent().data('number')+'&number='+$(this).parent().data('number'));
        return false;
    });
	$('#search').on('click', function (e) {
		$('.search .search_link').attr('href','/search?request='+$(this).parent().data('number')+'&number='+$(this).parent().data('number'));
    });*/
   /* $('td').click(function(){
        $('.monitor_context_menu').hide();
    });*/
</script>

<script>
    $(document).ready(function(){
        $(".filter_page_input_name").select2({
            placeholder: 'Имя',
            data:  filters_values[1],
            theme: "bootstrap",
        });
        $(".filter_page_input_groupe").select2({
            placeholder: 'Группа',
            data:  filters_values[2],
            theme: "bootstrap",
        });
        $('.filter_page_input_groupe').val('<?php echo Aes::decrypt($_COOKIE['group'],COOKIE_PASS) ?>').trigger('change');
        $(".filter_page_input_status").select2({
            placeholder: 'Статус',
            data:  filters_values[3],
            theme: "bootstrap",
        });
        $(".filter_page_input_request").select2({
            placeholder: 'Выберите заявку',
            data:  filters_values[4],
            theme: "bootstrap",
        });
    });
    $('.filter_page_input').change(function(){
        values = {};
        $('.filter_page_input').each(function(){
            if ($(this).val() != null) {
                values[$(this).data('name')] = $(this).val();
            }
        });
        $('.table').bootstrapTable('filterBy',values);
    });
</script>

