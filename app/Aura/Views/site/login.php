
<script>
	$('title').text('Вход');

</script>
<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3" style="padding-top: 115px;">
	<form method="POST">
		<div class="form-group_aura" align = "center">
			<img height="80px" src="/public/img/logo_aura_min.png" style="display: block;  margin-right:0px"/><br>
		    <!-- <label for="username"></label> -->
		<?php if ($error==1) {
			?><p class="bg-danger_aura" style="padding: 5px;" align = "center"><b>Ошибка:</b>  Неправильное имя пользователя или пароль</p><?php
		} ?>
		<?php if ($error==2) {
			?><p class="bg-danger_aura" style="padding: 5px;" align = "center"><b>Ошибка:</b>  Нет доступа в систему. Ожидайте приглашения</p><?php
		} ?>			
		    <input type="text" style="" class="form-control_aura" name="username" id="username" placeholder="Логин...">
		</div>
		<div class="form-group_aura" align = "center">
	    	<!-- <label for="password"></label> -->
	    	<input type="password" style="" class="form-control_aura" name="password" id="password" placeholder="Пароль...">
		</div>
		<div align = "center">
			<button type="submit" class="btn btn-lg btn-lg-p btn-primary btn-block_aura">Войти</button></br>
			<p style="color:#a9a9a9">Введите ваш логин и пароль<br>для входа в компьютер
			<?php
			$ip = $_SERVER['REMOTE_ADDR'];
			if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			    $ip = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
			} else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
			    $ip = array_pop(explode(',', $_SERVER['HTTP_CLIENT_IP']));
			} else if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
			    $ip = array_pop(explode(',', $_SERVER['HTTP_X_REAL_IP']));
			}
			file_put_contents(AURA_DATA_PATH.'files/test.txt', $ip."\r\n", FILE_APPEND);
			$ip = explode('.',$ip);
			if ($ip[0] == 10 && $ip[1] == 50) {
				$shop_num = 500+$ip[2];
			} else if ($ip[0] == 156 && in_array($ip[1], [244,245,246,242,243,247]) && $ip[2] < 200) {
				$shop_num = ($ip[1] - 240)*100 + floor($ip[2]/2);
			} ?>
			<?php if (!empty($shop_num)) { ?>
			<?php print_r($shop_num) ?>
				<br> Или воспользуйтесь автоматической авторизацией для магазинов АТАК</p>
				<button type="submit" name="shop" value="<?php echo $shop_num ?>" class="btn btn-primary btn-block_aura btn-lg btn-lg-p">Вход для магазинов</button>
			<?php } else { echo '</p>'; } ?>
		</div>
	</form>
	<footer>
		<div class="footer_aura" align = "center">
			WEB client service desk <br>
		</div>
	</footer>
</div>
