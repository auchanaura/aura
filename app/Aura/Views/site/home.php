<style>
	.top_header .card-box {
		margin-bottom: 10px; padding: 15px; font-weight: bold; font-size: 14px;
	}
	.collapsed_view {
	    padding-left: 50px;
	    padding-right: 60px
	}
	.icon-arrow-down {
		font-size: 28px;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-top: -14px;
    margin-left: -15px;
	}
	.pointer_container {
		position: absolute; 
		right: 0px; 
		width: 60px; 
		top: 0; 
		bottom: 0; 
		text-align: center; 
		border-left: 1px 
		solid #ebeff2; 
		cursor: pointer; 
		background-color: #5fbeaa; 
		font-weight: bold; 
		color: #ffffff;
	}
	.date_container {
		float: left;
		overflow: visible;
		top: 0px;
		width: 170px;
		left: 26px;
		bottom: 0px;
	}
	.pointer_container:hover {
		background-color: #7fcfbb;
	}
	.tickets_list_i_opacity {
		opacity: 0.55;
	}
	.list-group-item {
		border: 1px solid #d8d8d8;
	}
	.request_content {
		padding-bottom: 5px;
	}
</style>
<!--<div class="col-sm-12 top_header" style="padding: 0;">
	<div class="col-sm-4" style="padding-right: 0;">
		<div class="card-box text-center">Заявки в Центр Поддержки</div>
	</div>
	<div class="col-sm-4" style="padding-right: 0;">
		<div class="card-box text-center">Новости отдела IT</div>
	</div>
	<div class="col-sm-4" style="padding-right: 0;">
		<div class="card-box text-center">Приложения АШАН</div>
	</div>
</div>-->
<div style="padding: 0px 0px 0px 10px;">
	<a href="/new_request" class="btn btn-success btn-block btn-lg waves-effect waves-light btn-lg-p">Создать новую заявку</a>
</div>
<ul  class="list-group tickets_list" style="margin-top: 10px; padding-left: 10px;">
<?php use Aura\Core\ProcessData;

foreach ($requests as $key => $value): ?>
	<li class="list-group-item tickets_list_i" data-number="<?php echo $value['NUMBER'] ?>" style="margin-bottom: 5px; position: relative; padding: 0;">
		<div style="position: relative; overflow: auto;padding: 10px 15px;">
			<div class="action_icon_container">
				<img style="width: 100%;" class="media-object action_icon_image" src="/public/img/ticket/<?php echo $value['STATUS_GLOBAL']?>.png">
			</div>
			<div class="pointer_container">
				<span class="icon-arrow-down"></span>
			</div>
			<div class="collapsed_view" style=" overflow: auto;">
				<div class="date_container">
				
					<div><span style="font-weight:bold; font-size:12px"><?php echo $value['NUMBER'] ?></span><span style="font-size:12px">  от <?php
						$date = new DateTime($value['STATUS0'] );
						echo $date->format('d.m.y');?></span></div>
					<div><span style="font-size:12px"><?php
						if (!empty($value['LAST_ACTION'])){
								$date2 = new DateTime($value['LAST_ACTION']);
								echo 'Действие от '.$date2->format('h:i d.m.y').':';
						} else {
								echo 'Действие от '.$date->format('h:i d.m.y').':';
						}
						?>
					</div>
				</div>
				<div >
					<div><?php echo $value['TICKET_TITLE'] ?></div>
					<div><span style="font-color: black"><?php echo $value['STATUS_TITLE'] ?></span></div>
				</div>
				
			</div>
		</div>
		<div class="main_view" style="display: none; border-top: 1px solid #ebeff2; overflow: auto; padding-top: 12px; margin-bottom: 8px;">
			<div class="col-sm-5 request_content"></div>
			<ul class="col-sm-7 request_history" style="max-height: 300px;overflow: auto; margin-bottom: 12px;">
				
			</ul>
			<div class="request_menu" style="overflow: auto; clear: both;border-top: 1px solid #ebeff2; overflow: auto; padding: 8px 10px 5px 10px;">
				<textarea  data-id="<?php echo $value['NUMBER'] ?>" style="margin-top: 10px; margin-bottom: 10px;" class="form-control" placeholder="Комментарий пользователя"></textarea>
				<div class="col-sm-12">
				
				<div class="button-list col-sm-12" data-toggle="buttons" style="text-align: center;overflow: auto; margin-bottom: 0px;">
					<?php if ($value['STATUS_JOB']!=4) {?>
					<label class="btn btn-lg btn-primary waves-effect waves-light send_comment" data-id="<?php echo $value['NUMBER'] ?>" data-type="comment">
						<input type="radio" value="comment" name="status" id="type"> Уточнение
					</label>
					<label class="btn btn-lg btn-danger waves-effect waves-light send_comment" data-id="<?php echo $value['NUMBER'] ?>" data-type="reclamation">
						<input type="radio" value="reclamation" name="status" id="type"> Рекламация
					</label>
					<?php } else if ($value['STATUS_GLOBAL']!=8) { ?>
					<label class="btn btn-lg btn-warning waves-effect waves-light send_comment" data-id="<?php echo $value['NUMBER'] ?>" data-type="back_to_work">
						<input type="radio" value="reclamation" name="status" id="type"> Вернуть в работу
					</label>
					<label class="btn btn-lg btn-success waves-effect waves-light open_modal_window" data-window="smiles" data-number="<?php echo $value['NUMBER'] ?>">
						<input type="radio" value="close" name="status" id="type"> Подтвердить закрытие
					</label>
					<?php }?>
				</div>
				</div>
			</div>
		</div>
	</li>
<?php endforeach ?>
</ul>

<script src="/public/js/ticket_select.js"></script>
<script>
	$('.send_comment').click(function(){
		if ($('textarea[data-id="'+number+'"]').val() == '') {
			if ($(this).data('type') == 'comment') {
				$.Notification.notify('error','top center','Ошибка!','Необходимо заполнить поле "комментарий пользователя"');
			} else if ($(this).data('type') == 'back_to_work') {
				$.Notification.notify('error','top center','Ошибка!','Укажите причину, по которой заявку необходимо вернуть в работу."');
			} else if ($(this).data('type') == 'reclamation') {
				$.Notification.notify('error','top center','Ошибка!','Укажите причину для рекламации."');
			}
			return false;
		}
		number = $(this).data('id');
		comment = {
			ACTION_CODE:215,ACTION_NAME:"Комментарий пользователя",ACTION_NUMBER:10,CHANGED:"",COMMENT_SMS:null,COMMENT_TECH:$('textarea[data-id="'+number+'"]').val(),COMMENT_USERMAIL:$('textarea[data-id="'+number+'"]').val(),COMPLAINT:"Пожаловаться на действие",COMPLAINT_AN:null,CURATION:null,DATETIME:dateFormat(Date.now(),'dd-mm-yyyy HH:MM:ss'),DATETIME_INIT:dateFormat(Date.now(),'dd-mm-yyyy HH:MM:ss'),EMAIL:null,EXT_TIME:0,JUSTIFY:null,JUSTIFY_COMMENT:null,JUSTIFY_RESULT:null,KERNEL_JOB_TIME_END:null,KERNEL_JOB_TIME_LIMIT:0,KERNEL_JOB_TIME_START:null,NUMBER:number,OWNER:"ru00240024",Q_CHECK:null,Q_COMMENT:null,Q_DONE:0,Q_REPLY:0,Q_STUPID:0,Q_TECH:0,Q_TYPE:0,TIME_SPEND:"",USER_CODEE:"0",U_BOOR:"no",U_LONGLY:"no",U_STUPID:"no",WHO:"-"};
		if ($(this).data('type') == 'comment') {
			action = 1;
			mark = 0;
		} else if ($(this).data('type') == 'reclamation') {
			action = 2;
			mark = 0;
			comment.ACTION_NAME = 'Рекламация';
			comment.ACTION_CODE = 216;
		} else if ($(this).data('type') == 'back_to_work') {
			action = 3;
			mark = 0;
			comment.ACTION_NAME = 'Возвращена в работу';
			comment.ACTION_CODE = 214;
		}
		$.ajax({
			method: "POST",
			url: "/ajax/userComment",
			data: { number: number, comment: $('textarea[data-id="'+number+'"]').val(),action: action, mark: mark}
		}).done(function(data) {
			console.log(data);
			$.Notification.notify('success','top center','Комментарий по заявке '+number+' успешно добавлен!','');
			$('textarea[data-id="'+number+'"]').val('');
			event($('.tickets_list_i[data-number="'+number+'"] .request_history'),comment,[2,4,5,6,'img',10,11], 'prepend');
		});
	});
	Tickets = [];
<?php require(AURA_VIEWS_PATH.'js_templates/events_js.php') ?>
	<?php foreach ($requests as $key => $value) {
		$value = ProcessData::pregEscape($value); ?>
		Tickets['<?php echo $value['NUMBER'] ?>'] = JSON.parse('<?php echo json_encode($value) ?>');
	<?php } ?>

	$('body').on('mouseenter','.tickets_list_i', function() {
		number = $(this).data('number');
		download_info(number,0,0,1);
	});

	$('.tickets_list_i').dblclick(function(){
		open_ticket($(this));
	});

	$('.pointer_container').click(function(){
		ticket_i = $(this).parents('.tickets_list_i');
		open_ticket(ticket_i);
	});

	function open_ticket(ticket_i) {
		number = ticket_i.data('number');
		if (ticket_i.find('.request_content').html() == '') {
			if (Tickets[number]['htmlbody'] == '') {
				ticket_i.find('.request_content').html(Tickets[number]['TICKET_DESCRIPTION']);
			} else {
				ticket_i.find('.request_content').html(Tickets[number]['htmlbody']);
			}
			if (typeof(Tickets[number].history) != 'undefined') {
				$.each(Tickets[number].history,function(key,value){
					event($('.tickets_list_i[data-number="'+number+'"] .request_history'),value,[2,4,5,6,'img',10,11]);
				});
			}
		}
		ticket_i.find('.main_view').toggle();
		if (ticket_i.find('.main_view').is(":visible")) {
			$('.tickets_list_i_opacity').removeClass('tickets_list_i_opacity');
			$('.tickets_list_i:not([data-number="'+number+'"])').addClass('tickets_list_i_opacity');
		} else {
			$('.tickets_list_i_opacity').removeClass('tickets_list_i_opacity');
		}
		$('.tickets_list_i:not([data-number="'+number+'"]) .main_view').hide();
	}
</script>
<?php if (!empty($_GET['number'])) { ?>
<script>
	$(document).ready(function(){
		$('.tickets_list_i[data-number="<?php echo $_GET['number'] ?>"] .pointer_container').click();
		ticket = $('.tickets_list_i[data-number="<?php echo $_GET['number'] ?>"]').addClass('tickets_list_i_selected');
		scroll = ticket.position().top;
		console.log(scroll);
		$('body').scrollTop(scroll);
	});
</script>
<?php } ?>
