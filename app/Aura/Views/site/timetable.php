<?php
	$this_month = new DateTime(date('Y-'.$month));

	$filters = [];
	//$filters['MONTH'] = ['Месяц','Месяц','dropdown'];
	$filters['GROUPE'] = ['Группа','Группа специалиста','dropdown'];
	$filters['FIO'] = ['ФИО','Имя специалиста','dropdown'];

	$filter_values = [];
	foreach ($timetable as $key => $value) {
	 	$filter_values['GROUPE'][$value['GROUPE']] = $value['GROUPE'];
	 	$filter_values['FIO'][$key] = $key;
	}
	$filter_values['MONTH'] = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
	@asort($filter_values['GROUPE']);
	@asort($filter_values['FIO']);
?>
<script>

	$('title').text('Расписание');
</script>
<div style="overflow: visible; margin-left: 10px; padding: 10px; background-color: #ffffff;">
	<div style=" position: relative;">
		<button type="button" style="height: 25px; float: left; margin-right: 10px;" class="btn btn-default add_user">
			Добавить нового специалиста
		</button>
		<form method="post" class="new_user_form" style="z-index: -10; position: absolute;top:38px;width: 300px;overflow: hidden;border: 1px solid rgb(221, 221, 221);border-radius: 5px;padding: 7px;background: rgb(255, 255, 255);">
			<!--<div class="input-group" style="margin-bottom: 10px;">
				<label style="width: 73px;" class="input-group-addon">Группа</label>
					<select class="form-control" name="new_group" id="new_group">
						<option value=""></option>
						<option value="ЦП - Новосибирск">ЦП - Новосибирск</option>
						<option value="ЦП - Санкт-Петербург">ЦП - Санкт-Петербург</option>
					</select>
					<script>
						$("#new_group").select2({
							placeholder: "Выберите группу",
							theme: "bootstrap"
						});
					</script>
			</div>-->
			<div class="input-group" style="width: 100%; margin-bottom: 10px;">
				<label  style="width: 73px;" class="input-group-addon">Логин</label>
				<input class="form-control" name="new_login">
			</div>
			<button class="btn btn-default" style="width: 100%;" type="submit">Добавить</button>
		</form>
	</div>
	<script>
		$('.new_user_form').toggle().css('z-index',10000);
		$('.add_user').click(function(){
			$('.new_user_form').toggle();
		});
	</script>
	<div class="btn-group" style="float: left; margin-right: 5px;">
		<button type="button" style="height: 25px;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Месяц<span class="caret"></span></button>
		<ul class="dropdown-menu" style="z-index: 500000;">
			<li class="dropdown-submenu">
                    <a tabindex="-1">2017</a>
                    <ul class="dropdown-menu">
                        <li>
                            <?php foreach ($filter_values['MONTH'] as $key => $value): ?>
                                <a href="/timetable?month=<?php echo $key + 1 ?>&year=2017"><?php echo $value; ?></a>
                            <?php endforeach ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown-submenu">
		    	<a tabindex="-1" >2016</a>
	        	<ul class="dropdown-menu">
		        	<li>
		        		<?php foreach ($filter_values['MONTH'] as $key => $value): ?>
                                <a href="/timetable?month=<?php echo $key + 1 ?>&year=2016"><?php echo $value; ?></a>
		        		<?php endforeach ?>
		    		</li>
	    		</ul>
			</li>
			<li class="dropdown-submenu">
		    	<a tabindex="-1" >2015</a>
	        	<ul class="dropdown-menu">
		        	<li>
		        		<?php foreach ($filter_values['MONTH'] as $key => $value): ?>
                                <a href="/timetable?month=<?php echo $key + 1 ?>&year=2015"><?php echo $value; ?></a>
		        		<?php endforeach ?>
		    		</li>
	    		</ul>
			</li>
		</ul>
	</div>
	<?php foreach ($filters as $key => $value) {
		renderFilter($key,$value,$filter_values);
	} ?>
	<div class="spacer" style="clear: both;"></div>
</div>
<?php
	$i = 290;
	$i_max = 610;
	$i_step = ($i_max-$i)/24;
	$color = [];
	while ($i <= $i_max) {
		$i = $i+ $i_step;
		$grd = $i;
		if ($i > 360) {
			$grd = $i - 360;
		}
		$result = HSVtoRGB([$grd/360,25/100,90/100]);
		$color[] = dechex($result[0]).dechex($result[1]).dechex($result[2]);
	}
 ?>
<form class="form-horizontal day_change" style="z-index: 10000; background: #ffffff; display: none; position: absolute; width: 135px;overflow: hidden;border: 1px solid #dddddd; border-radius: 5px; padding: 7px;">
	<div class="form-group" style="margin: 0px 0px 5px 0px;">
		<div class="input-group input-group-sm">
			<span class="input-group-addon" id="sizing-addon1">Смена</span>
			<input name="TOUR" type="text" maxlength="2" max="24" class="form-control" aria-describedby="sizing-addon1">
		</div>
	</div>
	<div class="btn-group-sm" data-toggle="buttons">
		<label class="btn btn-default btn-sm btn-custom waves-effect waves-light " style="width: 100%;margin-bottom: 5px;">
			<input name="SUPERVISOR" type="checkbox" name="type" value="1" autocomplete="off"> Супервизор
		</label>
	</div>
	<div class="btn-group-sm" data-toggle="buttons">
		<label class="btn btn-danger btn-sm btn-custom waves-effect waves-light " style="width: 100%;margin-bottom: 5px;">
			<input name="OUT" type="checkbox" name="type" value="1" autocomplete="off"> Отсутствует
		</label>
	</div>
	<textarea name="comment" class="form-control" rows="2" style="margin-bottom: 5px;"></textarea>
    <input type="hidden" name="id"/>
    <input type="hidden" name="group"/>
    <input type="hidden" name="fio"/>
    <input type="hidden" name="date"/>
	<div class="form-group" style="margin: 0px;">
		<div style="width: 80%; margin: auto;">
			<button type="submit" class="btn btn-primary btn-sm" style="width: 100%">Сохранить</button>
		</div>
	</div>
</form>
<div style="margin-left: 10px; background: #ffffff;" class="table-responsive">
	<table class="table table-condensed table-bordered table-hover fixed_headers timetable" style="position: relative; z-index: 1; margin-bottom: 0;">
		<thead>
			<tr style="font-weight: bold;">
				<th style="width: 155px;">Специалист</th>
				<?php while ($this_month->format('m') == $month) { ?>
				<?php
				$class = ''; 
				if ($this_month->format('w') == 0 || $this_month->format('w') == 6) {
					$class = '__weekend';
				}
				 ?>
					<th class="text-center <?php echo $class ?>"><?php echo $this_month->format('d'); ?></th>
					<?php $this_month->modify('+1 day');
				} ?>
			</tr>
		</thead>
		<?php foreach ($timetable as $key => $value): ?>
			<tr data-group="<?php echo $value['GROUPE'] ?>" data-name="<?php echo rtrim(str_replace("\n", "", $key)) ?>">
				<td style="width: 155px;"><?php echo $key ?></td>
			<?php
                    $this_month = new DateTime(date($year.'-' . $month));
				while ($this_month->format('m') == $month) { ?>
				<?php
				$class = ''; 
				if ($this_month->format('w') == 0 || $this_month->format('w') == 6) {
					$class .= '__weekend';
				}
				if (@$value[$this_month->format('Y-m-d')]['SUPERVISOR'] == 1) {
					$class .= ' __supervisor';
				}
				if (@$value[$this_month->format('Y-m-d')]['OUT'] == 1) {
					$class .= ' __out';
				}
				 ?>
					<td class="text-center day <?php echo $class ?>" data-out="<?php echo @$value[$this_month->format('Y-m-d')]['OUT'] ?>" data-SUPERVISOR="<?php echo @$value[$this_month->format('Y-m-d')]['SUPERVISOR'] ?>" data-date="<?php echo $this_month->format('Y-m-d') ?>" data-id="<?php echo @$value[$this_month->format('Y-m-d')]['ID'] ?>" data-comment="<?php echo @$value[$this_month->format('Y-m-d')]['COMMENT'] ?>">
						<div style="background: #<?php echo @$color[$value[$this_month->format('Y-m-d')]['TOUR'] - 1] ?>; position: absolute; z-index: -2; top:0; left: 0; right: 0; bottom: 0;"></div>
						<span><?php echo @$value[$this_month->format('Y-m-d')]['TOUR']; ?></span><?php if (!empty(@$value[$this_month->format('Y-m-d')]['COMMENT'])): ?><div class="tooltip tooltip_custom" title="<?php echo @$value[$this_month->format('Y-m-d')]['COMMENT'] ?>"></div></td><?php endif ?>
				<?php $this_month->modify('+1 day');
			} ?>
			</tr>
		<?php endforeach ?>
	</table>
</div>
<script>
	$('th').click(function(){
		list = [];
		row = 0;
		index = $(this).html()*1;
		$('tbody tr').each(function(){
			value = $(this).find('td span').eq(index - 1).html()*1;
			if (value != '') {
				list.push([$('tbody tr').eq(row),value]);
			}
			row++;
			console.log(row + ' ' + value + ' ' + index);
		});
		if (!$(this).hasClass('__ASC')) {
			list.sort(function(a,b) {
				return b[1]-a[1];
			});
			$(this).removeClass('__DESC');
			$(this).addClass('__ASC');
		} else {
			list.sort(function(a,b) {
				return a[1]-b[1];
			});
			$(this).addClass('__DESC');
			$(this).removeClass('__ASC');
		}
		$.each(list,function(key,value) {
			value[0].prependTo('tbody');
		});
	});
</script>
<?php function renderFilter($name,$filter,$filter_values) { ?>
	<div class="input-group filter_input col-sm-4 col-md-3 col-xl-3" data-field="<?php echo $name ?>" style="clear: none; margin-bottom: 0px;">
		<label class="input-group-addon" for="<?php echo $name ?>"><?php echo $filter[0] ?></label>
		<?php if ($filter[2] == 'text') { ?>
			<input type="text" class="form-control filter_page_input" id="<?php echo $name ?>" placeholder="<?php echo $filter[1] ?>">
		<?php } else if ($filter[2] == 'dropdown') { ?>
			<select class="form-control filter_page_input" id="<?php echo $name ?>" multiple="multiple">
				<option value=""></option>
				<?php foreach ($filter_values[$name] as $key => $value): ?>
					<option value="<?php echo $key ?>"><?php echo $value ?></option>
				<?php endforeach ?>
			</select>
			<script>
				$("#<?php echo $name ?>").select2({
					placeholder: "<?php echo $filter[1] ?>",
					theme: "bootstrap"
				});
			</script>
		<?php } ?>
	</div>
<?php } ?>
<script>
	$('#MONTH').change(function(){
		window.location.replace('/timetable?month='+($(this).val()*1+1));
	});
</script>
<script>
	$('#GROUPE').change(function(){
		value = $(this).val();
		$('tbody tr').show().each(function(){
			row = $(this);
			visible = 0;
			$.each(value,function(key,value){
				if (row.data('group') == value) {
					visible = 1;
				}
			});
			if (!visible) {
				$(this).hide();
			}
		});
	});
	$('#FIO').change(function(){
		value = $(this).val();
		$('tbody tr').show().each(function(){
			row = $(this);
			visible = 0;
			$.each(value,function(key,value){
				if (row.data('name') == value) {
					visible = 1;
				}
			});
			if (!visible) {
				$(this).hide();
			}
		});
	});
</script>
<script>
	$('.day').click(function(e){
            if (e.target !== this) {
    		return;
		}
		$('.day.active').removeClass('active');
		$('.day_change').hide();
	});
	$('.day').dblclick(function(e){
            if (e.target !== this && e.target.nodeName != 'SPAN') {
			console.log(e);
			test = e.target.nodeName;
    		return;
		}
		$(this).addClass('active');
		tour = $(this).find('span').html()*1;
		$('.day_change').appendTo(this).show().css('margin-left','-55px');
		$('.day_change input[name="TOUR"]').val(tour);
		$('.day_change input[name="group"]').val($(this).parents('tr').data('group'));
		$('.day_change input[name="fio"]').val($(this).parents('tr').data('name'));
		$('.day_change input[name="id"]').val($(this).data('id'));
		$('.day_change input[name="date"]').val($(this).data('date'));
		$('.day_change textarea[name="comment"]').val($(this).data('comment'));
		if ($(this).data('supervisor') == 1 && !$('.day_change input[name="SUPERVISOR"]:checked').val()) {
			$('.day_change input[name="SUPERVISOR"]').click();
		} else if ($(this).data('supervisor') != 1 && $('.day_change input[name="SUPERVISOR"]:checked').val()) {
			$('.day_change input[name="SUPERVISOR"]').click();
		}
		if ($(this).data('out') == 1) {
			$('.day_change input[name="OUT"]').click();
		}
		$('input[name="TOUR"]').focus();
		$('input[name="TOUR"]').select();
	});
	$('.day_change').submit(function(){
		$(this).hide();
		$('.day.active').removeClass('active');
		$.ajax({
			method: "post",
			url: "/ajax/timetableUpdate",
			data: {id: $('.day_change input[name="id"]').val(),
					TOUR: $('.day_change input[name="TOUR"]').val(),
					SUPERVISOR: $('.day_change input[name="SUPERVISOR"]:checked').val(),
					GROUPE: $('.day_change input[name="group"]').val(),
					FIO: $('.day_change input[name="fio"]').val(),
					OUT: $('.day_change input[name="OUT"]:checked').val(),
					DATE: $('.day_change input[name="date"]').val(),
					COMMENT: $('.day_change textarea[name="comment"]').val()}
		}).done(function(data) {
			console.log(data);
			$('tr[data-name="'+$('.day_change input[name="fio"]').val()+'"] .day[data-date="'+$('.day_change input[name="date"]').val()+'"]>span').html($('.day_change input[name="TOUR"]').val());
			day = $('tr[data-name="'+$('.day_change input[name="fio"]').val()+'"] .day[data-date="'+$('.day_change input[name="date"]').val()+'"]');
			if ($('.day_change input[name="SUPERVISOR"]:checked').val() == 1) {
				day.addClass('__supervisor');
			} else {
				day.removeClass('__supervisor');
			}
			if ($('.day_change input[name="OUT"]:checked').val() == 1) {
				day.addClass('__out');
			} else {
				day.removeClass('__out');
			}
			if ($('.day_change textarea[name="comment"]').val() != '') {
				day.append('<div class="tooltip" title="'+$('.day_change textarea[name="comment"]').val()+'"></div>');
			} else {
				day.find('.tooltip').remove();
			}
		});
		return false;
	});
	$(function () {
	  $('.tooltip').tooltip()
	})
</script>

<?php

function HSVtoRGB(array $hsv) {
    list($H,$S,$V) = $hsv;
    //1
    $H *= 6;
    //2
    $I = floor($H);
    $F = $H - $I;
    //3
    $M = $V * (1 - $S);
    $N = $V * (1 - $S * $F);
    $K = $V * (1 - $S * (1 - $F));
    //4
    switch ($I) {
        case 0:
            list($R,$G,$B) = array($V,$K,$M);
            break;
        case 1:
            list($R,$G,$B) = array($N,$V,$M);
            break;
        case 2:
            list($R,$G,$B) = array($M,$V,$K);
            break;
        case 3:
            list($R,$G,$B) = array($M,$N,$V);
            break;
        case 4:
            list($R,$G,$B) = array($K,$M,$V);
            break;
        case 5:
        case 6: //for when $H=1 is given
            list($R,$G,$B) = array($V,$M,$N);
            break;
    }
    return array($R*255, $G*255, $B*255);
}

 ?>
