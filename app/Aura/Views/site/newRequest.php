<?php
$username = Aes::decrypt($_COOKIE['username'],COOKIE_PASS);
$email = Aes::decrypt($_COOKIE['email'],COOKIE_PASS);
if (is_numeric($username)) {
	$email = $username . 'director@ataksupermarket.ru, '.$username . 'shop@ataksupermarket.ru';
}
 ?>
<div class="col-md-8" style="padding-right: 0px;">
	<div class="panel panel-border panel-custom">
		<div class="panel-heading"><h3 class="panel-title">Создание заявки</h3></div>
		<?php if (empty($id)) { ?>
			<div class="panel-body" style="padding-top: 5px;">
				<form role="form" method="post" enctype="multipart/form-data" class="new_request">
				    <div class="form-group">
				        <label for="type">Тип <span style="color: red">*</span></label>
				        <select type="text" id="TOPIC_RUS" class="form-control" name="type"></select>
				    </div>
				    <div class="form-group">
				        <label for="subject ">Тема <span style="color: red">*</span></label>
				        <input type="text" name="subject" class="form-control required">
				    </div>
				    <div class="form-group">
				        <label for="email">Email</label>
				        <input type="text" name="email" class="form-control email" value="<?php echo $email ?>">
				    </div>
				    <div class="form-group">
                        <label for="subject ">Отдел <span style="color: red">*</span></label>
                        <input type="text" name="department" class="form-control required" value="<?php echo $department ?>">
                    </div>
					<div class="form-group">
                        <label for="subject ">Сайт <span style="color: red">*</span></label>
                        <input type="text" name="site" class="form-control required" value="<?php echo $site ?>">
                    </div>
					<div class="form-group">
                        <label for="subject ">Телефон для связи<span style="color: red">*</span></label>
                        <input type="text" name="phone" class="form-control required">
                    </div>
                    <div class="form-group">
				        <label for="description">Описание <span style="color: red">*</span></label>
				        <textarea class="form-control required" name="description"></textarea>
				    </div>
				    <div class="form-group">
				        <label for="attachments">Вложения</label>
						<input type="file" place class="filestyle" data-buttonbefore="true" multiple="true" name="attachments[]">
					</div>
					<?php
					if (is_numeric($username)) {
					 	$real_site = $username . ' - АТАК';
					} else {
						$real_site = '921 - Auchan Central Office';
					} ?>
                    <input type="hidden" name="real_site" value="<?php echo $real_site ?>"/>
				    <button type="submit" class="submit_ticket btn btn-default btn-lg btn-block waves-effect waves-light btn-lg-p" disabled>Создать заявку</button>
				</form>
			</div>
		<?php } else { ?>
			<div class="col-sm-12">Заявка успешно создана.</div>
			<div class="col-sm-12">Номер созданной заявки: <strong><?php echo $id ?></strong></div>
		<?php } ?>
	</div>
</div>
<div style="display: none" class="select_data">
	<?php
		$array = [];
		foreach ($problems_list[1] as $key => $value) {
			$array[] = ['id'=>$key. '##' . $problems_list[0][$key]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].'&&&','level'=>0, 'disabled'=>($problems_list[0][$key]['Get'] == 0 ? true : false)];
			foreach ($value as $key2 => $value2) {
				$array[] = ['id'=>$key2. '##' . $problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].'&&&','level'=>1, 'disabled'=>($problems_list[0][$key2]['Get'] == 0 ? true : false)];
				foreach ($value2 as $key3 => $value3) {
					$array[] = ['id'=>$value3 . '##' . $problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].' - '.$problems_list[0][$value3]['TOPIC'].'&&&','level'=>2, 'disabled'=>($problems_list[0][$value3]['Get'] == 0 ? true : false)];
				}
			}
		}
		print_r(json_encode($array)) 
	?>
</div>
<script>
	function formatResult(node) {
		var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
		return $result;
	};
	$('.email').change(function(){
		text = $(this).val();
		text = text.replace(/(,| )*/g,"").replace(/(.*?@.*?\.ru|com)/g,"$1,");
		array = text.split(',');
		text = '';
		$.each(array,function(key,value){
			if (value.match(/(.*?@.*?\.ru|com)/)) {
				text += value+', ';
			}
		});
		$(this).val(text);
		if (text == '') {
			$('.submit_ticket').attr('disabled','disabled');
		} else {
			$('.submit_ticket').removeAttr('disabled');
		}
	});
	$('.required').change(function(){
		error = 0;
		$('.required').each(function(){
			if ($(this).val() == '') {
				$(this).parent().addClass('has-error');
				error = 1;
			} else {
				$(this).parent().removeClass('has-error');
			}
		});
		if (error == 0) {
			$('.submit_ticket').removeAttr('disabled')
		} else {
			$('.submit_ticket').attr('disabled','disabled')
		}
	});
	$(document).ready(function(){
		data = JSON.parse($('.select_data').html());
		$.each(data,function(key,value){
			data[key].text = value.text.replace('%%%','<span style="display: none">');
			data[key].text = value.text.replace('&&&','</span>');
		});
		$("#TOPIC_RUS").select2({
			placeholder: 'Выберите тип проблемы',
			data: data,
			formatSelection: function(item) {
				return item.text
			},
			formatResult: function(item) {
				return item.text
			},
			escapeMarkup: function(markup) {
				return markup;
			},
			templateResult: formatResult,
			theme: "bootstrap"
		});
		$('#TOPIC_RUS').val('248##Прочее').trigger('change');
	});
	$('.new_request').submit(function(){
		$('.submit_ticket').remove();
	});

</script>
