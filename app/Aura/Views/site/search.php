<?php
use Aura\Core\Core;
use Aura\Core\ProcessData;
use Aura\Core\Utils;

$cim = $lv = $TIME_LIMIT = $filter_values = [];
	foreach ($GLOBALS["variables"]['CIM'] as $key => $value) {
		$cim[$value['value']] = $value['title'];
	}
	foreach ($GLOBALS["variables"]['LV'] as $key => $value) {
		$lv[$value['value']] = $value['title'];
	}
	foreach ($GLOBALS["variables"]['TIME_LIMIT'] as $key => $value) {
		$TIME_LIMIT[$value['value']] = $value['title'];
	}
	$spread = [$cim,$lv,$TIME_LIMIT];
	$filters = ProcessData::filterData($spread,$pool,['NUMBER','CIM','LV','USER_LOGIN','STATUS_GLOBAL','REAL_SITE']);
	$filter_values = $filters[1];
	$filters = $filters[0];
	
?>
<script>
	$('title').text('Поиск');
</script>
<div class="col-sm-7">
	<form class="form-group" method="GET" style="width: 100%; margin-bottom: 10px;">
		<div class="input-group input-group">
			<label for="request" class="input-group-addon" style="font-weight: bold;">Поиск</label>
			<input type="text" class="form-control" id="request" name="request" placeholder="Критерий поиска" aria-describedby="sizing-addon1" value="<?php echo @$_GET['request'] ?>">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-default search_button" style="padding: 2px 12px; font-size: 12px; margin-top: -1px"><span class="glyphicon glyphicon-search"></span></button>
			</span>
		</div>
	</form>
	<?php if (!empty($pool)): ?>
		<div class="panel panel-color panel-custom">
			<div class="panel-heading filter_container" style="position: relative;">
				<div class="panel-title">Фильтр</div>
				<span class="label label-purple" style="float: right; position: absolute; right: 10px; top: 8px;"><?php echo count($pool) ?></span>
			</div>
			<div class="panel-body filter_body" style="display: none;">
				<?php foreach ($filters as $key => $value) {
					Core::renderFilter($key,$value,$filter_values);
				} ?>
			</div>
		</div>
	<?php endif ?>
</div>

<?php if (!empty($pool)): ?>
	<script>
		<?php require(AURA_VIEWS_PATH.'js_templates/events_js.php') ?>
		Tickets = [];
		old_Tickets = [];
		<?php $ticket_list = '['; ?>
		<?php foreach ($pool as $key => $value) {
			$ticket_list .= $value['NUMBER'] .','; 
			$value = ProcessData::pregEscape($value); ?>
		Tickets['<?php echo $value['NUMBER'] ?>'] = JSON.parse('<?php echo json_encode($value) ?>');
		old_Tickets['<?php echo $value['NUMBER'] ?>'] = JSON.parse('<?php echo json_encode($value) ?>');
	<?php } ?>
		<?php $ticket_list .= ']'; echo 'ticket_list = ' . $ticket_list; ?>
	</script>
	<ul class="list-group col-sm-7 search_tickets_container" style="clear: both;overflow-y: scroll;height: calc(100vh - 150px);">
		<?php if (!empty($_GET['number'])) {
			RenderTicket($spread,$pool[$_GET['number']],$user);
			unset($pool[$_GET['number']]);
		} ?>
		<?php foreach ($pool as $key => $value) {
			RenderTicket($spread,$value,$user);	
		} ?>
	</ul>
	<div class="col-sm-5 fixx">
		<div class="panel panel-border panel-custom" style="position: relative; background: #ebeff2;">
			<div class="panel-heading">
				<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Содержание</div>
			</div>
			<div class="panel-body ticket_eml_body" style="box-shadow: red 0px -2px 0px 0px inset; background:#ffffff;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;height:calc(50vh - 80px); padding: 0px 0px 2px 20px; overflow:hidden;max-height: calc(100vh - 80px)">
				<div class="ticket_eml_body_container" style="overflow-y: scroll; height: 100%; padding-right: 10px; padding-bottom: 18px"></div>
			</div>

			<div class="panel panel-border panel-custom" style="width:100%;position: absolute; position: absolute;  margin-top: 10px;overflow: auto;">
				<div class="panel-heading">
					<div class="panel-title" style="margin-top:-7px; margin-bottom:3px;">Действия</div>
				</div>	
				<div class="panel-body event_body" style="height:calc(50vh - 80px);max-height: calc(100vh - 80px);overflow: hidden; padding:0px 0px 0px 0px;    margin-left: -1px;">
				<div class="event_body_container" style="overflow-y: scroll; height: 100%; padding-right: 0px; margin-right: -1px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->js[] = 'js/ticket_select.js'; ?>
<?php $this->js[] = 'js/filter.js'; ?>
<?php $this->js[] = 'js/resize.js'; ?>
<?php endif ?>

<?php function RenderTicket($spread,$ticket,$user) { ?>
	<li class="list-group-item curator_i" data-number="<?php echo $ticket['NUMBER'] ?>">
			<div class="tb_container">
				<div class="ticket_body" >	
						<div style="width: 76px; float: left; margin-top: -2px">
								<span class="number_desc" ><?php echo $ticket['NUMBER'] ?></span>
						</div>
						<div style="margin-left: 80px;">
							<span class="ticket_title"><?php echo $ticket['STATUS_TITLE'] ?></span>
						</div>
					<div class="action_icon_container">
						<img style="width: 100%;" class="media-object action_icon_image" src="/public/img/ticket/<?echo $ticket['STATUS_GLOBAL']?>.png">
					</div>
					<div class="ticket_description" style="clear:both;"><?php echo nl2br($ticket['TICKET_DESCRIPTION_LOW']) ?></div>
					<div style="line-height: 120%;">
                    <span class="ticket_fio"><?php echo mb_strtolower($ticket['USER_FIO'], "utf-8") ?></span>
						
                    <?php if ($ticket['REAL_SITE'] == '-') {
                        echo('<span class="ticket_site" >(' . mb_strtolower($ticket['USER_SITE'], "utf-8") . ')');
                    } else {
                        echo('<span class="ticket_site" style="color:black;">(' . mb_strtolower($ticket['REAL_SITE'], "utf-8") . ')');
                    } ?></span>
						
						<span class="kernel_last_time">
							
						</span>
					</div>
					<span class="ticket_date" style="float: right; font-size: 10px; margin-top: -16px;">
						<?php echo Utils::formatDateDifftoNow(new DateTime($ticket['STATUS0']));
						 ?> назад.
					</span>
				</div>
				<?php $disabled = ($user['ADMIN'] >= 1 ? ' ' : 'disabled'); ?>
            <?php if ($user['ADMIN'] >= 1 || $user['LV']==4) { ?>
					<div class="tuco">
						<div class="col-sm-6" style="padding: 2px 5px;">
							<span class="checkbox checkbox-pink" style="margin-top: 0px; margin-bottom: 2px; font-size: 12px; ">
								<input id="phys[<?php echo $key ?>]" name="PHYSICAL" value="1" class="phys ticket_update_options" type="checkbox" <?php echo ($ticket['PHYSICAL'] == 1 ?  'checked' : '') ?> <?php echo $disabled ?>>
								<label for="phys[<?php echo $key ?>]" class="mini_checkbox">Физическая</label>
							</span>
							
							<div class="btn-group"  style="width: 100px; margin-top:27px">
								<button style="width: 100%; font-size: 11px" type="button" class="manage_list_button btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Управление <span class="caret"></span></button>
								<ul class="dropdown-menu manage_list">
									<li>
										<a class="open_modal_window" data-window="comment" tabindex="-1">Комментарий</a>
									</li>
									<li>
									<?php if ($ticket['STATUS_GLOBAL'] == 8) {
											echo '<a style="color: red;">Заявка выполнена</a>';
										} else if ($ticket['STATUS_JOB'] == 2) { ?>
											<a style="color:red;">Исполняется: <?php echo $ticket['WORKER']; ?></a>
										<?php } else  { ?>
											<a class="open_modal_window" data-window="to_work" tabindex="-1">Поднять в работу</a>
											<?php if (Aes::decrypt($_COOKIE['role'],COOKIE_PASS) != 99): ?>
												<a class="take_fon" tabindex="-1">Взять фоновой</a>
											<?php endif ?>
										<?php } ?>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6" style="padding: 0px 8px 0px 5px;">
							<div style="margin-bottom: 2px;margin-top: -1px" class="ticket_update_container">
                            <select name="CIM" class="form-control <?php if ($ticket['CIM'] == 1) {
                                echo('btn-danger');
                            } else {
                                echo('btn-link');
                            } ?> dropdown-toggle waves-effect waves-light ticket_update_options btn-xs" <?php if ($user['LV'] != 4) {echo $disabled;} ?>>
									<option>---</option>
									<?php foreach ($spread[0] as $key2 => $value2): ?>
										<?php if ($key2 == $ticket['CIM']) { ?>
											<option selected value="<?php echo $key2 ?>"><?php echo $value2 ?></option>
										<?php } else { ?>
											<option value="<?php echo $key2 ?>"><?php echo $value2 ?></option>
										<?php } ?>
									<?php endforeach ?>
								</select>
							</div>
							<div style="margin-bottom: 2px;" class="ticket_update_container">
								<select name="LV" class="form-control btn-link dropdown-toggle waves-effect waves-light btn-xs ticket_update_options ticket_update_options_select2" <?php echo $disabled ?>>
									<option>Группа</option>
									<?php foreach ($spread[1] as $key2 => $value2): ?>
										<?php if ($key2 != 3): ?>
											<?php if ($ticket['LV'] == $key2) { ?>
												<option selected value="<?php echo $key2 ?>"><?php echo $value2 ?></option>
											<?php } else { ?>
												<option value="<?php echo $key2 ?>"><?php echo $value2 ?></option>
											<?php } ?>
										<?php endif ?>
									<?php endforeach ?>l
									<?php if ($ticket['LV'] == 3 && empty($ticket['REAL_SITE'])) { ?>
										<option selected value="<?php echo substr($ticket['USER_SITE'],0,3) ?>"><?php echo $ticket['USER_SITE'] ?></option>
									<?php } else if ($ticket['LV'] == 3) { ?>
										<option selected value="<?php echo substr($ticket['REAL_SITE'],0,3) ?>"><?php echo $ticket['REAL_SITE'] ?></option>
									<?php } ?>
								</select>
							</div>	
							<div class="ticket_update_container">
								<select name="KERNEL_JOB_TIME_LIMIT" class="form-control btn-link dropdown-toggle waves-effect waves-light  ticket_update_options" <?php echo $disabled ?>>
									<option>Время</option>
									<?php $i = 0; ?>
									<?php foreach ($spread[2] as $key2 => $value2): ?>
										<?php if ($value2*1 == $ticket['KERNEL_JOB_TIME_LIMIT']*1) { $i = 1; ?>
											<option selected value="<?php echo $value2 ?>"><?php echo $value2 ?> минут</option>
										<?php } else { ?>
											<option value="<?php echo $value2 ?>"><?php echo $value2 ?> минут</option>
										<?php } ?>
									<?php endforeach ?>
									<?php if (!$i && $ticket['KERNEL_JOB_TIME_LIMIT'] != 0): ?>
										<option selected value="<?php echo $ticket['KERNEL_JOB_TIME_LIMIT'] ?>"><?php echo $ticket['KERNEL_JOB_TIME_LIMIT'] ?> минут</option>
									<?php endif ?>
									<?php unset($i); ?>
								</select>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="tuco">
						<div class="col-sm-6" style="padding: 2px 5px;">
							<div class="checkbox checkbox-pink number_desc" style="margin-top: 0px; margin-bottom: 2px; font-size: 12px;">
								<input id="phys[1606280582]" class="phys ticket_update_options" type="checkbox" disabled <?php echo ($ticket['PHYSICAL'] == 1 ?  'checked' : '') ?>>
								<label for="phys[1606280582]" class="mini_checkbox">Физическая</label>
							</div>

							<div class="btn-group"  style="width: 100px; margin-top:27px">
								<button style="width: 100%; font-size: 11px" type="button" class="manage_list_button btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Управление <span class="caret"></span></button>
								<ul class="dropdown-menu manage_list">
									<li>
										<a class="open_modal_window" data-window="comment" tabindex="-1">Комментарий</a>
									</li>
									<li>
									<?php if ($ticket['STATUS_GLOBAL'] == 8) {
											echo '<a style="color: red;">Заявка выполнена</a>';
										} else if ($ticket['STATUS_JOB'] == 2) { ?>
											<a style="color:red;">Исполняется: <?php echo $ticket['WORKER']; ?></a>
										<?php } else  { ?>
											<a class="open_modal_window" data-window="to_work" tabindex="-1">Поднять в работу</a>
											<?php if (Aes::decrypt($_COOKIE['role'],COOKIE_PASS) != 99): ?>
												<a class="take_fon" tabindex="-1">Взять фоновой</a>
											<?php endif ?>
										<?php } ?>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6" style="padding: 0px 8px 0px 5px;">
							<div class="ticket_update_container number_desc" style="margin-bottom: 2px; line-height: 22px; font-weight: 100;">
								<?php echo $spread[0][$ticket['CIM']]; ?>
							</div>
							<div class="ticket_update_container number_desc" style="margin-bottom: 2px; height:22px; line-height: 110%; overflow: hidden; font-weight: 100;">
								<?php if ($ticket['LV'] == 4) { ?>
									<?php echo $ticket['USER_SITE'] ?>
								<?php } else { ?>
									<?php echo @$spread[1][$ticket['LV']] ?>
								<?php } ?>
							</div>
							<div class="ticket_update_container number_desc" style="line-height: 22px; font-weight: 100;">
								<?php echo $ticket['KERNEL_JOB_TIME_LIMIT'] ?> минут
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		<div class="detailed_info">
		</div>
	</li>
<?php } ?>
<?php function renderFilter($name,$filter,$filter_values) { ?>
	<div class="input-group input-group filter_input col-sm-12 col-md-6 col-xl-4" data-field="<?php echo $name ?>">
		<label class="input-group-addon" for="<?php echo $name ?>"><?php echo $filter[0] ?></label>
		<?php if ($filter[2] == 'text') { ?>
			<input type="text" class="form-control filter_page_input" id="<?php echo $name ?>" placeholder="<?php echo $filter[1] ?>">
		<?php } else if ($filter[2] == 'dropdown') { ?>
			<select class="form-control filter_page_input" id="<?php echo $name ?>" multiple="multiple">
				<option value=""></option>
				<?php foreach ($filter_values[$name] as $key => $value): ?>
					<option value="<?php echo $key ?>"><?php echo $value ?></option>
				<?php endforeach ?>
			</select>
			<script>
				$("#<?php echo $name ?>").select2({
					placeholder: "<?php echo $filter[1] ?>",
					theme: "bootstrap"
				});
			</script>
		<?php } ?>
		<span class="input-group-btn search_sort_buttons_container">
			<button class="btn btn-default search_page_sort __left" data-order="ASC" type="button"><span class="glyphicon glyphicon-arrow-up"></span></button>
			<button class="btn btn-default search_page_sort __right" data-order="DESC" type="button"><span class="glyphicon glyphicon-arrow-down"></span></button>
		</span>
	</div>
<?php } ?>

<script>
function formatResult(node) {
		var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
		return $result;
	};
	var problem_data = [<?php foreach ($problems_list[1] as $key => $value) {
	echo '{id: "'.$key.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].'</span>", level : 0, disabled: '.($problems_list[0][$key]['Get'] == 0 ? 1 : 0).'},';
	foreach ($value as $key2 => $value2) {
		echo '{id: "'.$key2.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].'</span>", level : 1, disabled: '.($problems_list[0][$key2]['Get'] == 0 ? 1 : 0).'},';
		foreach ($value2 as $key3 => $value3) {
			echo '{id: "'.$value3.'", text: "'.$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'].' <span style=\'display:none\'>'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].' - '.$problems_list[0][$value3]['TOPIC'].'</span>", level : 2, disabled: '.($problems_list[0][$value3]['Get'] == 0 ? 1 : 0).'},';
		}
	}
	} ?>];
real_sites = [];
	<?php foreach ($real_sites as $key => $value): ?>
		real_sites[<?php echo $value['NUMBER']*1 ?>] = '<?php echo @$value['GROUPE'] ?>';
	<?php endforeach ?>
//Вынести в отдельный модуль
	$('body').on('click','.take_fon',function(){
		number = $(this).parents('.curator_i').data('number');
		$.ajax({
		method: "POST",
		url: "/ajax/takeFon",
		data: {ticket: number}
		}).done(function(data) {
			if (data==0){
					$.Notification.notify('error','top center','Нельзя забрать заявку себе в работу!','У Вас уже есть заявка в работе. Закройте текущую заявку и повторите попытку.');			
			} else { 
					$.Notification.notify('success','top center','Заявка '+number+' успешно забрана!','Переход на страницу фоновой заявки...');
					setTimeout(function(){window.location.replace('/background');}, 1000);				
			}
		});
	});
</script>
<script>
$('body').on('change','.problems_list',function(){
		container = $(this);
		number = $(this).parents('.curator_i').data('number');
		if (Tickets[number].PROBLEM_ID != $(this).val() && $(this).val() != 0 && $(this).val() != null) {
			Tickets[number].PROBLEM_ID = $(this).val();
			$.ajax({
			method: "POST",
			url: "/ajax/changeTopic",
			data: {number: number, problem_id: $(this).val(), problem_name: $(this).find("option:selected").text()}
			}).done(function(data) {
				if (!data){
					$.Notification.notify('error','top center','Невозможно изменить тему','');			
				} else { 
					$.Notification.notify('success','top center','Тема заявки '+number+' успешно изменена!','');
					container.parents('.curator_i').addClass('list-group-item-success');
				}
			});
		}
	});


</script>
<?php
$this->js[] = 'js/send_curation_spool.js';
?>
<select class="real_sites_template" style="display: none;">
	<optgroup label="Мобильная группа">
		<?php foreach ($real_sites as $key2 => $value2): ?>
			<option value="<?php echo $value2['MAGAZ'] ?>"><?php echo $value2['MAGAZ'] ?></option>
		<?php endforeach ?>
	</optgroup>
</select>
<div class="detailed_info_template" style="display: none;">
	<ul class="list-group" style="margin-bottom: 0px;">
		<li class="list-group-item info list-info number">Номер: </li>
		<li class="list-group-item info event_number">Номер ивента: </li>
		<li class="list-group-item info beeline_number">Номер аутсорс: </li>
		<li class="list-group-item info worker" >Исполняется: </li>
		<li class="list-group-item info status0">Время рег.: </li>
		<li class="list-group-item info user_fio">Заявитель: </li>
		<li class="list-group-item info problem_list" data-blocked="<?php if ($user['ADMIN'] >= 1): ?>1<?php endif ?>">Тема: </li>
		<li class="list-group-item info user_site">Сайт: </li>
		<li class="list-group-item info user_mail">Емейл: </li>
		<li class="list-group-item info user_login">Логин: </li>
		<li class="list-group-item info user_post">Должность: </li>
		<li class="list-group-item info attachments">Вложения: </li>

		
	</ul>
</div>
