<div class="new_request_form_container">
	<div class="new_request_form_overlay"></div>
	<div class="panel panel-default new_request_form">
		<div class="panel-heading">
			<h3 class="panel-title">Создание нового обращение</h3>
		</div>
		<form class="form-horizontal" method="POST">
			<div class="panel-body" style="padding:5px;">
				<h4 style="font-size: 14px; margin-bottom: 10px;" class="well well-sm" style="padding: 3px;">Заявитель

				<div style="float:right; margin-left: 15px; width: 56px; text-align: center;"><span class="user_id">0</span>/<span class="users_length">0</span></div>
				<div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group" style="float: right;">
					<button type="button" class="btn btn-default prev_user"><i class="fa fa-arrow-left"></i></button>
					<button type="button" class="btn btn-default find_users">Искать</button>
					<button type="button" class="btn btn-default next_user"><i class="fa fa-arrow-right"></i></button>
				</div>
				</h4>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Логин</label>
					<div class="col-sm-9">
						<input type="text" name="login" class="form-control ad_search_input" >
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Фамлия и Имя</label>
					<div class="col-sm-9">
						<input type="text" name="fio" class="form-control ad_search_input">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">E-mail*</label>
					<div class="col-sm-9">
						<input type="text" name="email" class="form-control ad_search_input requested_input">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Отдел</label>
					<div class="col-sm-9">
						<input type="text" name="site" class="form-control ad_search_input">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label for="inputEmail3" class="col-sm-3 control-label">Сайт</label>
					<div class="col-sm-9">
						<input type="text" name="department" class="form-control ad_search_input">
					</div>
				</div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="inputEmail3" class="col-sm-3 control-label">Телефон*</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control requested_input">
                    </div>
                </div>
				<h4 style="font-size: 14px; margin-bottom: 10px;" class="well well-sm" style="padding: 3px;border:none">Обращение</h4>
				<div class="form-group selet2_real_site">
					<label for="real_site" class="col-sm-3 control-label">Реальный сайт*</label>
					<div class="col-sm-9">
						<select class="form-control real_site requested_input" id="real_site" name="real_site" placeholder="Реальный сайт">
							<option></option>
							<?php foreach ($realSites as $key => $value): ?>
								<option value="<?php echo $value['MAGAZ'] ?>"><?php echo $value['MAGAZ'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					
				</div>
				<div class="checkbox checkbox-custom user_answer_button col-sm-offset-3" style="margin-bottom: 5px;margin-top:-5px;">
					<input value="1" id="groupe"  name="groupe" type="checkbox" >
					<label class="second_comment" for="groupe">Назначить на группу</label>
				</div>
				<div class="form-group">
					<label for="status" class="col-sm-3 control-label">Состояние</label>
					<div class="button-list  col-sm-9" data-toggle="buttons" style="margin-top:3px">
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
							<input type="radio" value="1" name="status" id="type" checked> Новая
						</label>
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
							<input type="radio" value="2" name="status" id="type"> Решено
						</label>
						
					</div>
				</div>
				<div class="form-group">
					<label for="type" class="col-sm-3 control-label">Критичность</label>
					<div class="button-list  col-sm-9" data-toggle="buttons" style="margin-top:3px">
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
							<input type="radio" value="0" name="cim" id="cim" checked> Нормальная
						</label>
						<label class="btn btn-warning btn-xs btn-custom waves-effect waves-light">
							<input type="radio" value="1" name="cim" id="cim"> Срочная
						</label>
						<label class="btn btn-danger btn-xs btn-custom waves-effect waves-light">
							<input type="radio" value="2" name="cim" id="cim"> Критичная
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="type" class="col-sm-3 control-label">Тип обращения</label>
					<div class="button-list  col-sm-9" data-toggle="buttons" style="margin-top:3px">
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
							<input type="radio" value="1" name="type" id="type" checked> Инцидент
						</label>
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
							<input type="radio" value="2" name="type" id="type"> Запрос
						</label>
						<?php if (Aes::decrypt($_COOKIE['admin'],COOKIE_PASS) > 1) { ?>
						<label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
							<input type="radio" value="3" name="type" id="type"> Задание
						</label>
						<?php } ?>
						<!--<label class="btn btn-default btn-xs btn-custom waves-effect waves-light" style"disable">
							<input type="radio"   value="3" name="type" id="type"> Задание
						</label>-->
					</div>
				</div>
				<div class="form-group">
									<label for="inputEmail3" class="col-sm-3  control-label"  style="padding-top: 0px;">Физическая проблема</label>
									<div class="col-sm-9">
										<div class="button-list" data-toggle="buttons">
											<label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
												<input type="radio" name="phys" value="1" autocomplete="off"> Да
											</label>
											<label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
												<input type="radio" name="phys" value="0" autocomplete="off" checked> Нет
											</label>
										</div>
									</div>
				</div>
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Тема</label>
					<div class="col-sm-9">
						<input type="text" class="form-control ticket_title" id="ticket_title" name="ticket_title">
					</div>
				</div>
				<div class="form-group" style="z-index: 20000;">
					<label for="inputEmail3" class="col-sm-3 control-label">Топик*</label>
					<div class="col-sm-9">
						<select class="form-control problem_name requested_input" id="TOPIC_RUS" name="problem_type">
						<option></option>
						</select>
					</div>
				</div>
				<!--<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Вложения</label>
					<div class="col-sm-9">
						<input type="file" multiple="" name="files[]" class="form-control input-sm">
					</div>
				</div>-->
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Описание обращения*</label>
					<div class="col-sm-9">
						<textarea class="form-control requested_input" name="description" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="btn btn-default close_modal_window">Отмена</div>
				<button type="submit" class="btn btn-default" data-action="newRequest" style="float: right;">Создать</button>
			</div>
		</form>
	</div>
	<div style="display: none" class="select_data">
		<?php
			$array = [];
			foreach ($problems_list[1] as $key => $value) {
				$array[] = ['id'=>$key. '##' . $problems_list[0][$key]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].'&&&','level'=>0,'disabled'=>($problems_list[0][$key]['Get'] == 0 ? true : false)];
				foreach ($value as $key2 => $value2) {
					$array[] = ['id'=>$key2. '##' . $problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].'&&&','level'=>1,'disabled'=>($problems_list[0][$key2]['Get'] == 0 ? true : false)];
					foreach ($value2 as $key3 => $value3) {
						$array[] = ['id'=>$value3 . '##' . $problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'],'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'] . '%%%'.$problems_list[0][$key]['TOPIC'].' - '.$problems_list[0][$key2]['TOPIC'].' - '.$problems_list[0][$value3]['TOPIC'].'&&&','level'=>2,'disabled'=>($problems_list[0][$value3]['Get'] == 0 ? true : false)];
					}
				}
			}
			print_r(json_encode($array)) 
		?>
	</div>
</div>
