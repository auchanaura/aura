<div class="new_request_form_container">
	<div class="new_request_form_overlay"></div>
	<div class="panel panel-default new_request_form" style="width: 900px;">
		<div class="panel-heading">
			<h3 class="panel-title">Комментарий к заявке <?php echo $request['NUMBER'] ?></h3>
		</div>
				<form method="post">
			<div class="panel-body" style="padding: 10px;">
						<div style="font-weight: bold; font-size: 16px; margin-bottom: 10px;margin-left:10px"><?php echo @$request['STATUS_TITLE'] ?></div>
					
					<div class="row" >
						<div class="col-sm-6 panel panel-default" style="max-height: 300px; overflow: auto;">
							<div class="panel-body">
								<?php echo nl2br($request['TICKET_DESCRIPTION']); ?>
							</div>
						</div>
						<div class="col-sm-6">
						<div class="panel panel-border panel-DEFAULT">
						<div class="panel-heading">
						<div class="panel-title">Детали обращения</div>
					</div>
					<ul class="list-group" style="padding: 3px 10px 5px;">
						<li class="list-group-item info"><span style="font-weight:bold;">Номер:</span> <span><?php echo $request['NUMBER'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Время рег.:</span> <span><?php echo $request['STATUS0'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Заявитель:</span> <span><?php echo $request['USER_FIO'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Емейл:</span> <span><?php echo $request['USER_MAIL'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Сайт:</span> <span><?php echo $request['USER_SITE'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Логин:</span> <span><?php echo $request['USER_LOGIN'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Должность:</span> <span><?php echo $request['USER_POST'] ?></span></li>
						<li class="list-group-item info"><span style="font-weight:bold;">Номер ивента:</span> <span><?php echo $request['NUMBER_EVENT'] ?></span></li>
						
						
						
					</ul>
						</div>
					</div>
					<h4 style="font-size: 16px; margin-bottom: 10px; clear: left; margin-left:5px;margin-right:5px" class="well well-sm">Действие</h4>
					
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Комментарий</label>
						<div class="col-sm-9">
							<textarea class="requested_input form-control input-sm" rows="5" name="comment"></textarea>
                            <input name="number" value="<?php echo $request['NUMBER'] ?>" type="hidden"/>
						</div>
					</div>
			</div>
			<div class="panel-footer">
				<div class="btn btn-default close_modal_window">Отмена</div>
			<div style="display: none;" class="result_msg">Комментарий к заявке добавлен</div>
				<button type="submit" class="btn btn-default" data-action="Comment" style="float: right;">Отправить</button>
			</div>
				</form>
		<div style="display: none" class="select_data">
			<?php
				$array = [];
				foreach ($problems_list[1] as $key => $value) {
					$array[] = ['id'=>$key,'text'=>$problems_list[0][$key]['TOPIC_RUS'],'level'=>0];
					foreach ($value as $key2 => $value2) {
						$array[] = ['id'=>$key2,'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'],'level'=>1];
						foreach ($value2 as $key3 => $value3) {
							$array[] = ['id'=>$value3,'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'],'level'=>2];
						}
					}
				}
				print_r(json_encode($array)) 
			?>
		</div>
	</div>
</div>
