<style>
	.smile {opacity: 0.8; width: 19%; text-align: center; padding: 0;}
	.smile:hover {opacity: 1;}
	.smile img {
		width: 96px;
	}
</style>
<div class="new_request_form_container">
	<div class="new_request_form_overlay"></div>
	<div class="panel panel-default new_request_form" style="width: 900px;">
		<div class="panel-heading">
			<h3 class="panel-title">Оцените выполнение заявки <?php echo $number ?></h3>
		</div>
        <form method="post" action="/ajax/userComment">
            <!-- <textarea style="display: none;" name="comment"><?php echo $info ?></textarea>
		<input type="hidden" name="number" value="<?php echo $number ?>">
		<input type="hidden" name="mark" class="smile_mark_value" value="0">
			<div class="panel-body">
				<h3 style="width: 100%; text-align: center;">Пожалуйста, оцените выполнение заявки</h3>
                <button class="btn btn-custom btn-rounded smile" type="submit" value="1" data-action="UserComment"
                        style="color: #333333!important;"><img src="/img/smiles/smiley_angry.png"><br>Очень плохо
                </button>
                <button class="btn btn-custom btn-rounded smile" type="submit" value="2" data-action="UserComment"
                        style="color: #333333!important;"><img src="/img/smiles/smiley_sad.png"><br>Плохо
                </button>
                <button class="btn btn-custom btn-rounded smile" type="submit" value="3" data-action="UserComment"
                        style="color: #333333!important;"><img src="/img/smiles/smiley_waiting.png"><br>Удовлетворительно
                </button>
                <button class="btn btn-custom btn-rounded smile" type="submit" value="4" data-action="UserComment"
                        style="color: #333333!important;"><img src="/img/smiles/smiley.png"><br>Хорошо
                </button>
                <button class="btn btn-custom btn-rounded smile" type="submit" value="5" data-action="UserComment"
                        style="color: #333333!important;"><img src="/img/smiles/smiley_grin.png"><br>Очень хорошо
                </button>
			</div>
			<script>
				$('.smile').click(function(){
					$('.smile_mark_value').val($(this).val());
				});
			</script>
            <div style="display: none;" class="result_msg">Спасибо! Ваше мнение очень важно для нас!</div> -->
            <div class="col-sm-12">
                            <textarea style="display: none;" name="comment"><?php echo $info ?></textarea>
                <input type="hidden" name="number" value="<?php echo $number ?>">
                <table class="table">
                    <tr>
                        <th>Ваша оценка</th>
                        <th style="text-align: center;" width="120">Ужасно</th>
                        <th style="text-align: center;" width="120">Плохо</th>
                        <th style="text-align: center;" width="120">Нормально</th>
                        <th style="text-align: center;" width="120">Отлично</th>
                        <th style="text-align: center;" width="120">Потрясающе</th>
                    </tr>
                    <tr>
                        <td>Понял ли специалист ИТ заявку?</td>
                        <td style="text-align: center;"><input type="radio" name="star[0]" value="1"></td>
                        <td style="text-align: center;"><input type="radio" name="star[0]" value="2"></td>
                        <td style="text-align: center;"><input type="radio" name="star[0]" value="3"></td>
                        <td style="text-align: center;"><input type="radio" name="star[0]" value="4"></td>
                        <td style="text-align: center;"><input type="radio" name="star[0]" value="5"></td>
                    </tr>
                    <tr>
                        <td>Удовлетворены ли вы качеством ответа по заявке?</td>
                        <td style="text-align: center;"><input type="radio" name="star[1]" value="1"></td>
                        <td style="text-align: center;"><input type="radio" name="star[1]" value="2"></td>
                        <td style="text-align: center;"><input type="radio" name="star[1]" value="3"></td>
                        <td style="text-align: center;"><input type="radio" name="star[1]" value="4"></td>
                        <td style="text-align: center;"><input type="radio" name="star[1]" value="5"></td>
                    </tr>
                    <tr>
                        <td>Удовлетворены ли вы скоростью решения заявки?</td>
                        <td style="text-align: center;"><input type="radio" name="star[2]" value="1"></td>
                        <td style="text-align: center;"><input type="radio" name="star[2]" value="2"></td>
                        <td style="text-align: center;"><input type="radio" name="star[2]" value="3"></td>
                        <td style="text-align: center;"><input type="radio" name="star[2]" value="4"></td>
                        <td style="text-align: center;"><input type="radio" name="star[2]" value="5"></td>
                    </tr>
                    <tr>
                        <td>Общее впечатление от обращения в отдел ИТ</td>
                        <td style="text-align: center;"><input type="radio" name="star[3]" value="1"></td>
                        <td style="text-align: center;"><input type="radio" name="star[3]" value="2"></td>
                        <td style="text-align: center;"><input type="radio" name="star[3]" value="3"></td>
                        <td style="text-align: center;"><input type="radio" name="star[3]" value="4"></td>
                        <td style="text-align: center;"><input type="radio" name="star[3]" value="5"></td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6" style="margin-bottom: 10px; text-align: center;"><button type="button" class="btn btn-lg btn-warning close_modal_window">Отмена</button></div>
            <div class="col-sm-6" style="margin-bottom: 10px; text-align: center;"><button type="submit" data-action="UserComment" class="btn btn-lg btn-success">Отправить</button></div>
            <div style="display: none;" class="result_msg">Спасибо!</div>
		</form>
	</div>
</div>

<div class="button-list" data-toggle="buttons">
    <label class="btn btn-default btn-xs btn-custom waves-effect waves-light">
        <input type="radio" name="physical" value="1" autocomplete="off">
        Да
    </label>
    <label class="btn btn-default btn-xs btn-custom waves-effect waves-light active">
        <input type="radio" name="physical" value="0" autocomplete="off" checked="">
        Нет
    </label>
</div>
