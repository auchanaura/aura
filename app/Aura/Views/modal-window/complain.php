<div class="new_request_form_container">
	<form method="post">
		<div class="new_request_form_overlay"></div>
		<div class="panel panel-default new_request_form" style="width: 900px;">
			<div class="panel-heading">
				<h3 class="panel-title">Пожаловаться на действие</h3>
			</div>
					<div class="panel-body">
						<h4 style="font-size: 16px; margin-bottom: 10px;" class="well well-sm">Информация по действию</h4>
						<div style="font-weight: bold; font-size: 16px; padding-left: 15px;"><?php echo $action['ACTION_NAME'] ?></div>
						<div class="row" style="padding: 15px 0px 0px 15px">
							<div class="col-sm-6 panel panel-default" style="max-height: 400px; overflow: auto;">
								<div class="panel-body">
									<?php echo nl2br($action['COMMENT_TECH']); ?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<ul class="list-group">
										<li class="list-group-item">Номер: <?php echo $action['NUMBER'] ?> </li>
										<li class="list-group-item">Время: <?php echo $action['DATETIME'] ?></li>
										<li class="list-group-item">Специалист: <?php echo $action['OWNER'] ?></li>
									</ul>
								</div>
							</div>
						</div>
						<h4 style="font-size: 16px; margin-bottom: 10px; clear: left;" class="well well-sm">Действие</h4>
						
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Жалоба</label>
							<div class="col-sm-9">
								<textarea class="requested_input form-control input-sm" rows="5" name="comment"></textarea>
                                <input name="number" value="<?php echo $action['NUMBER'] ?>" type="hidden"/>
                                <input name="action_number" value="<?php echo $action['ACTION_NUMBER'] ?>"
                                       type="hidden"/>
							</div>
						</div>
					</div>
			<div class="panel-footer">
				<div class="btn btn-default close_modal_window">Отмена</div>
				<div style="display: none;" class="result_msg">Жалоба зарегестрирована</div>
				<button type="submit" class="btn btn-default" style="float: right;" data-action="complaint">Отправить</button>
			</div>
		</div>
	</form>
</div>
