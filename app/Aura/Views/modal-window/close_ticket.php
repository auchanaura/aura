<div class="new_request_form_container">
	<div class="new_request_form_overlay"></div>
	<div class="panel panel-default new_request_form" style="width: 900px;">
		<div class="panel-heading">
			<h3 class="panel-title">Закрыть заявку <?php echo $request['NUMBER'] ?></h3>
		</div>
		<form method="post" action="/ajax/comment_action">
			<div class="panel-body">
				<h4 style="font-size: 16px; margin-bottom: 10px;" class="well well-sm">Информация по заявке</h4>
						<div style="font-weight: bold; font-size: 16px; margin-bottom: 10px;"><?php echo @$request['TICKET_TITLE'] ?></div>
					<?php if (!empty($request['TOPIC_RUS']) && $request['TOPIC_RUS'] != '-') { ?>
						<div style="font-weight: bold; font-size: 16px; padding-left: 15px;"><?php echo $request['TOPIC_RUS'] ?></div>
					<?php } else { ?>
						<div class="input-group input-group" >
							<label for="TOPIC_RUS" class="input-group-addon" style="font-weight: bold;">Задание</label>
							<select type="text" class="form-control" id="TOPIC_RUS" name="TOPIC_RUS"></select>
						</div>
					<?php } ?>
				<div class="row" style="padding: 15px 0px 0px 15px">
					<div class="col-sm-6 panel panel-default" style="max-height: 400px; overflow: auto;">
						<div class="panel-body">
							<?php echo nl2br($request['TICKET_DESCRIPTION']); ?>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<ul class="list-group">
								<li class="list-group-item">Номер: <?php echo $request['NUMBER'] ?> </li>
								<li class="list-group-item">Время рег.: <?php echo $request['STATUS0'] ?></li>
								<li class="list-group-item">Заявитель: <?php echo $request['USER_FIO'] ?></li>
								<li class="list-group-item">Сайт: <?php echo $request['USER_SITE'] ?></li>
								<li class="list-group-item">Логин: <?php echo $request['USER_LOGIN'] ?></li>
								<li class="list-group-item">Должность: <?php echo $request['USER_POST'] ?></li>
								<li class="list-group-item">Вложения: 
								<?php if (!empty($request['attachments'])): ?>
									<?php foreach ($request['attachments'] as $key => $value): ?>
										<?php if ($key != 0): ?>
											, 
										<?php endif ?>
										<a href="/manage/DownloadMailAttachment?indent=<?php echo $request['NUMBER'] ?>&img=<?php echo $key ?>"><?php echo $value->filename ?></a>
									<?php endforeach ?></li>
								<?php endif ?>
							</ul>
						</div>
					</div>
				</div>
				<h4 style="font-size: 16px; margin-bottom: 10px; clear: left;" class="well well-sm">Действие</h4>
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Комментарий</label>
					<div class="col-sm-9">
						<textarea class="form-control input-sm" rows="5"></textarea>
					</div>
				</div>
			</div>
		</form>
		<div class="panel-footer">
			<div class="btn btn-default close_modal_window">Отмена</div>
			<button type="submit" class="btn btn-default" style="float: right;">Закрыть</button>
		</div>
		<div style="display: none" class="select_data">
			<?php
				$array = [];
				foreach ($problems_list[1] as $key => $value) {
					$array[] = ['id'=>$key,'text'=>$problems_list[0][$key]['TOPIC_RUS'],'level'=>0];
					foreach ($value as $key2 => $value2) {
						$array[] = ['id'=>$key2,'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'],'level'=>1];
						foreach ($value2 as $key3 => $value3) {
							$array[] = ['id'=>$value3,'text'=>$problems_list[0][$key]['TOPIC_RUS'].' - '.$problems_list[0][$key2]['TOPIC_RUS'].' - '.$problems_list[0][$value3]['TOPIC_RUS'],'level'=>2];
						}
					}
				}
				print_r(json_encode($array)) 
			?>
		</div>
	</div>
</div>