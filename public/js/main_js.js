var outercontainer = 'body'; //куда добавится превью
var imgclass = '.photo_preview'; //клик на картинку данного класса
var overlayclass = 'preview_overlay'; //внешний фон
var centerblock = 'preview_container_photo'; //внутренний контейнер
var left = 'preview_left';
var right = 'preview_right';
var close = 'preview_close';
$(document).ready(function () {

    $('body').on('click', imgclass, function () {
        var src = $(this).data('src');
        var name = $(this).data('name');
        if (name == undefined) {
            name = '';
        } else {
            name = '<h2>' + name + '</h2>';
        }
        var desc = $(this).data('desc');
        if (desc == undefined) {
            desc = '';
        }
        var index = $(this).index(imgclass);
        var count = $(this).parents('ul').find(imgclass).length;
        $(outercontainer).append('<div class="preview">' +
            '<div class="' + overlayclass + '"></div>' +
            '<div class="' + centerblock + '"><span  class="preview_name">' + name + '</span>' +
            '<div class="img_container" data-index="' + index + '" data-count=' +
            '"' + count + '"><div class="loading"></div></div>' + desc +
            '<div class="' + left + '"></div><div class="' + right + '"></div><div class="' + close + '">&times;</div></div>' +
            '</div>');
        var i = new Image();
        i.src = src;
        $(i).load(function () {
            $('.img_container').html(this);
        });

        $('.' + left).click(function () {
            var index = $('.img_container').data('index') - 1;
            var count = $('.img_container').data('count');
            if (index < 0) {
                index = count - 1;
            }
            var src = $(imgclass).eq(index).data('src');
            var name = $(imgclass).eq(index).data('name');
            if (name == undefined) {
                name = '';
            } else {
                name = '<h2 class="preview_name">' + name + '</h2>';
            }
            var desc = $(imgclass).eq(index).data('desc');
            if (desc == undefined) {
                desc = '';
            }
            i.src = src;
            $('.img_container').append('<div class="loading"></div>');
            $(i).load(function () {
                $('.preview_name').html(name);
                $('.preview_container_photo p').html(desc);
                $('.img_container').html(this).data('index', index);
            });
        });
        $('.' + right).click(function () {
            var index = $('.img_container').data('index') + 1;
            var count = $('.img_container').data('count');
            if (index >= count) {
                index = 0;
            }
            var src = $(imgclass).eq(index).data('src');
            var name = $(imgclass).eq(index).data('name');
            if (name == undefined) {
                name = '';
            } else {
                name = '<h2 class="preview_name">' + name + '</h2>';
            }
            var desc = $(imgclass).eq(index).data('desc');
            if (desc == undefined) {
                desc = '';
            }
            i.src = src;
            $('.img_container').append('<div class="loading"></div>');
            $(i).load(function () {
                $('.preview_name').html(name);
                $('.preview_container_photo p').html(desc);
                $('.img_container').html(this).data('index', index);
            });
        });
        $('.' + overlayclass).click(function () {
            $('.preview').remove();
            $('.' + overlayclass).remove();
        });
        $('.' + close).click(function () {
            $('.preview').remove();
            $('.' + overlayclass).remove();
        });

    });
});
$('body').on('click', '.open_modal_window', function () {
    number = $(this).data('number');
    action_number = $(this).data('actionnumber');
    name = $(this).data('window');
    info = '';
    if (typeof(number) == 'undefined') {
        number = $(this).parents('.curator_i').data('number')
    }
    if (name == 'smiles') {
        info = $('textarea[data-id="' + number + '"]').val();
        if ($('textarea[data-id="' + number + '"]').val() == '') {
            $.Notification.notify('error', 'top center', 'Ошибка!', 'Пожалуйста, прокомментируйте прежде чем оценить.');
            return false;
        }
    } else {

    }
    $.ajax({
        method: "GET",
        url: "/ajax/modalWindow",
        data: {name: name, number: number, action_number: action_number, info: info}
    }).done(function (data) {
        $('body').append(data);

        function formatResult(node) {
            var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
            return $result;
        };
        if (name == 'new_request') {
            data = JSON.parse($('.select_data').html());
            $.each(data, function (key, value) {
                data[key].text = value.text.replace('%%%', '<span style="display: none">');
                data[key].text = value.text.replace('&&&', '</span>');
            });
            $("#TOPIC_RUS").select2({
                placeholder: 'Выберите тип проблемы',
                data: data,
                formatSelection: function (item) {
                    return item.text
                },
                formatResult: function (item) {
                    return item.text
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: formatResult,
                theme: "bootstrap"
            });
            $(".new_request_form .real_site").select2({
                placeholder: 'Выберите сайт',
                theme: "bootstrap"
            });
        }
    });
});

$('body').on('click', '.close_modal_window', function () {
    $(this).parents('.new_request_form_container').remove();
});

user_id = 0;
users = [];
$('body').on('click', '.find_users', function () {
    users = [];
    $('.find_users').html('<span class="glyphicon glyphicon-refresh spinning"></span>');
    $.ajax({
        method: "post",
        url: "/ajax/ADSearch",
        data: {
            name: $('.new_request_form input[name="login"]').val(),
            displayname: $('.new_request_form input[name="fio"]').val(),
            mail: $('.new_request_form input[name="email"]').val(),
            dn: $('.new_request_form input[name="site"]').val()
        }
    }).done(function (data) {
        console.log(data);
        data = JSON.parse(data);
        $.each(data, function (key, value) {
            users.push({'fio': value[0], 'login': key, 'email': value[1], 'department': value[3], 'site': value[2]});
        });
        $('.new_request_form input[name="department"]').val(users[user_id].department);
        $('.new_request_form input[name="fio"]').val(users[user_id].fio);
        $('.new_request_form input[name="login"]').val(users[user_id].login);
        $('.new_request_form input[name="email"]').val(users[user_id].email);
        $('.new_request_form input[name="site"]').val(users[user_id].site);
        $('.user_id').html(user_id);
        $('.users_length').html(users.length);
        $('.find_users').html("Искать");
    });
});

$('body').on('click', '.next_user', function () {
    user_id++;
    if (users.length <= user_id) {
        user_id = 0;
    }
    $('.user_id').html(user_id + 1);
    $('.users_length').html(users.length);
    $('.new_request_form input[name="department"]').val(users[user_id].department);
    $('.new_request_form input[name="fio"]').val(users[user_id].fio);
    $('.new_request_form input[name="login"]').val(users[user_id].login);
    $('.new_request_form input[name="email"]').val(users[user_id].email);
    $('.new_request_form input[name="site"]').val(users[user_id].site);
});

$('body').on('click', '.prev_user', function () {
    user_id--;
    if (user_id < 0) {
        user_id = users.length - 1;
    }
    $('.user_id').html(user_id + 1);
    $('.users_length').html(users.length);
    $('.new_request_form input[name="department"]').val(users[user_id].department);
    $('.new_request_form input[name="fio"]').val(users[user_id].fio);
    $('.new_request_form input[name="login"]').val(users[user_id].login);
    $('.new_request_form input[name="email"]').val(users[user_id].email);
    $('.new_request_form input[name="site"]').val(users[user_id].site);
});

//$('body').on('click','.new_request_form .ad_search_input',function(){
//	$(this).autocomplete( "search", "" );
//});

$('body').on('submit', '.new_request_form_container form', function (e) {
    error = 0;
    $('.requested_input').each(function () {
        if ($(this).val() == '' && $(this).attr('name') != 'site') {
            error = 1;
            $(this).parents('.form-group').addClass('has-error')
        }
    });
    if (error) {
        swal("Ошибка", 'Необходимо заполнить обязательные поля!', "error");
        return false;
    }
    action = $('.new_request_form_container button[type="submit"]').data('action');
    $.ajax({
        method: "post",
        url: "/ajax/" + action,
        data: {data: JSON.stringify($(this).serializeArray())}
    }).done(function (data) {
        result = 'Заявка создана!';
        if (typeof($('.result_msg').html()) != 'undefined') {
            result = $('.result_msg').html();
        }
        $('.new_request_form_container').remove();
        swal(result, data, "success");
    });
    return false;
});
