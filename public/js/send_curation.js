$('.send_curation').click(function () {
    $.ajax({
        method: "POST",
        url: "/ajax/sendCuration",
        data: {}
    }).done(function (data) {
        $.Notification.notify('success', 'top center', 'Новые заявки распределены!', 'Распределение заявок успешно подтверждено.')
        setTimeout(function () {
            window.location.replace('/curation');
        }, 2000);
    });
});
