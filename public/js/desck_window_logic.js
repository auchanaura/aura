$(function () {
    $(".drag").draggable({
        opacity: 0.5,
        handle: "header",
        stack: ".drag",
        containment: "parent",
        drag: function (event, ui) {
            ui.position.left = Math.max(0, ui.position.left);
            ui.position.top = Math.max(0, ui.position.top);
        }
    });
    $('.resizable').resizable();
});
