/**
 * Created by ru00160012 on 23.01.2017.
 */


// переменные для меню распреда
var number_select = "";
var sit = "";
var cim = "";
var pfys = "";
var elem_sit = "";
var elem = "";
// функция улавливания события вращения колеса над меню
if (document.getElementById("contextMenuId").addEventListener) {

    if ("onwheel" in document) {
        // IE9+, FF17+, Ch31+
        document.getElementById("contextMenuId").addEventListener("wheel", onWheel);
    } else if ("onmousewheel" in document) {
        // устаревший вариант события
        document.getElementById("contextMenuId").addEventListener("mousewheel", onWheel);
    } else {
        // Firefox < 17
        document.getElementById("contextMenuId").addEventListener("MozMousePixelScroll", onWheel);
    }
} else { // IE8-
    document.getElementById("contextMenuId").attachEvent("onmousewheel", onWheel);
}

// функция получение значения куда вращалось колесо
function onWheel(e) {
    e = e || window.event;
    // wheelDelta не дает возможность узнать количество пикселей
    var delta = e.deltaY || e.detail || e.wheelDelta;
    elem = $('.changetime');
    var nextElement = "";
    if (delta < 0) {
        // колесико вращалось вниз
        nextElement = $('.changetime [selected="selected"]').next();
        if (nextElement.length > 0) {
            $('.changetime [selected="selected"]').removeAttr("selected").next("option").attr("selected", "selected");
            $(".changetime").val($('.changetime [selected="selected"]').val());
        }
    } else {
        // колесико вращалось вверх
        nextElement = $('.changetime [selected="selected"]').prev();
        if (nextElement.length > 0) {
            $('.changetime [selected="selected"]').removeAttr("selected").prev("option").attr("selected", "selected");
            $(".changetime").val($('.changetime [selected="selected"]').val());
        }
    }
    e.preventDefault ? e.preventDefault() : (e.returnValue = false);
}

// Функция для определения координат указателя мыши
function defPosition(event) {
    var x = y = 0;
    if (document.attachEvent != null) { // Internet Explorer & Opera
        x = window.event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y = window.event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    } else if (!document.attachEvent && document.addEventListener) { // Gecko
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    } else {
        // Do nothing
    }
    return {x: x, y: y};
}
// функция распределения
function setRaspred(CIM, LV, pfys, number_select) {
    if (LV == 3||LV == '3') { // если морбильная группа
        sit = $('.changesit').val();
        // заполнение распределенными данными нужной заявки поля с боку (визуализация)
        // --- элемент LV
        Tickets[number_select].LV = LV;
        Tickets[number_select].REAL_SITE = sit;
        // удаляем выбранный элемент (название сита), если заявка уже была один раз распределена
        $('.curator_i[data-number="' + number_select + '"] [name="LV"] [value="' + sit + '"]').remove();
        // добавляем элемент (название сита), к списку элемента на морде
        $('.curator_i[data-number="' + number_select + '"] [name="LV"]').append($('<option value="' + sit + '">' + sit + '</option>'));
        // выбираем добавленный элемент
        $('.curator_i[data-number="' + number_select + '"] [name="LV"] [value="' + sit + '"]').attr("selected", "selected");
        // --- элемент KERNEL_JOB_TIME_LIMIT
        Tickets[number_select].KERNEL_JOB_TIME_LIMIT = $('.changetime').val();
        $('.curator_i[data-number="' + number_select + '"] [name="KERNEL_JOB_TIME_LIMIT"]').val($('.changetime').val());

        Tickets[number_select].CIM = CIM;
        CIM = CIM + ''; //преобразование в строку
        // --- элемент CIM
        $('.curator_i[data-number="' + number_select + '"] [name="CIM"]').val(CIM);
        // если нажата кнопка "физич",
        if (pfys == 'f') {
            // поставить метку
            // сделать проверку а была ли метка
            if ($('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]')[0].checked==false) {
                $('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]').click();
            } else {
                //  инициализация применения изменений в базу
                $('.curator_i[data-number="' + number_select + '"] [name="LV"]').change();
            }
            Tickets[number_select].PHYSICAL = 1;
        } else {
            // если уже был отсечен, то снять метку
            if ($('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]')[0].checked){
                $('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]').click();
            } else {
                //  инициализация применения изменений в базу
                $('.curator_i[data-number="' + number_select + '"] [name="LV"]').change();
            }
            Tickets[number_select].PHYSICAL = 0;
        }
    }
    else { // применение параметров для остальных
        Tickets[number_select].PHYSICAL = 0;
        // LV
        Tickets[number_select].LV = LV;
        LV = LV + ''; //преобразование в строку
        $('.curator_i[data-number="' + number_select + '"] [name="LV"]').val(LV);
        // CIM
        Tickets[number_select].CIM = CIM;
        CIM = CIM + ''; //преобразование в строку
        $('.curator_i[data-number="' + number_select + '"] [name="CIM"]').val(CIM);
        // limit
        $('.curator_i[data-number="' + number_select + '"] [name="KERNEL_JOB_TIME_LIMIT"]').val($('.changetime').val());
        Tickets[number_select].KERNEL_JOB_TIME_LIMIT = $('.changetime').val();
        // снятие галочки физ нажатие( тоже инициация) или инциация действия
        if ($('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]')[0].checked){
            $('.curator_i[data-number="' + number_select + '"] [name="PHYSICAL"]').click();
        } else {
            $('.curator_i[data-number="' + number_select + '"] [name="LV"]').change();
        }

    }
    console.log('Номер: ' + number_select + ';CIM:' + CIM + ';LV:' + LV + ';физ:' + pfys);
    document.getElementById("contextMenuId").style.display = "none";
};



// функция вызова меню на правой кнопке
function menu(type, evt) {
    // Блокируем всплывание события contextmenu
    evt = evt || window.event;
    evt.cancelBubble = true;
    // Показываем собственное контекстное меню
    var menu = document.getElementById("contextMenuId");
    var html = "";
    switch (type) {
        case (1) :
            html = "   Распределение " + number_select + ", от обычного к критичному; ESC - отмена";
            // создание меню управления
            for (iLv in data_for_menu[1]) {
                html += "<br>";
                //console.log(data_for_menu[1][iLv]+' - '+iLv);
                for (iCim in data_for_menu[0]) {
                    //console.log(data_for_menu[0][iCim]+' - '+iCim);
                    if (iLv != "3" || iLv != 3) {
                        html += '<button ' +
                            'type ="button" ' +
                            'id="CIM' + iCim + '_' + iLv + '" ' +
                            'value="CIM' + iCim + '_' + iLv + '" ' +
                            'style="width: 33%; font-size: 13px" ' +
                            'class="curation-menu-' + iCim + ' btn-link dropdown-toggle waves-effect waves-light btn-xs btnMenu">' +
                            data_for_menu[1][iLv] + '</button>';
                    } else {
                        html += '<button ' +
                            'type ="button" ' +
                            'id="CIM' + iCim + '_' + iLv + '" ' +
                            'value="CIM' + iCim + '_' + iLv + '" ' +
                            'style="width: 26%; font-size: 13px" ' +
                            'class="curation-menu-' + iCim + ' btn-link dropdown-toggle waves-effect waves-light btn-xs  group_f btnMenu">' +
                            data_for_menu[1][iLv] + '</button>';
                        html += '<button ' +
                            'type ="button" ' +
                            'id="CIM' + iCim + '_' + iLv + '_f" ' +
                            'value="CIM' + iCim + '_' + iLv + '_f" ' +
                            'style="width: 7%; font-size: 13px" ' +
                            'class="curation-menu-' + iCim + ' btn-link dropdown-toggle waves-effect waves-light btn-xs group_f btnMenu">' +
                            'Физ.</button>';
                    }
                }
            }

            html += '<br><select name="TIME_LIMIT" class="form-control btn-link dropdown-toggle waves-effect waves-light changemenu changetime" style="width: 33%; font-size: 11px">';
            for (iTime in data_for_menu[2]) {
                if (iTime == 10) {
                    // выбираем 10 минут по умолчанию
                    html += '<option selected="selected" value="' + iTime + '">' + iTime + ' минут</option>';
                } else {
                    html += '<option value="' + iTime + '">' + iTime + ' минут</option>';
                }
            }
            html += '</select>';
            html += '<select name="sites" class="form-control btn-link dropdown-toggle waves-effect waves-light changemenu changesit" style="width: 43%; font-size: 11px">';
            // находим все объекты 'optgroup option' -- названиния ситов -- и ложим их в перемнную
            $('optgroup option').each(
                function () {
                    var value = $(this).val();
                    html += '<option value="' + value + '">' + value + '</option>';
                });
            html += '</select>';
            html += '<button ' +
                'type ="button" ' +
                'id="buttonСontextMenuId" ' +
                'style="width: 23%; font-size: 13px" ' +
                'class="dropdown-toggle waves-effect waves-light btn-xs buttonСontextMenuId">' +
                'Отправить</button>';
            break;
        default :
            // Nothing
            break;
    }
    // Если есть что показать - показываем
    if (html) {
        menu.innerHTML = html;
        $('.changesit').val(''); // обнулить эдемент меню с ситами
        $(".btnMenu").each(function () {
            var a = this.value;
            // функция дуглирования кнопки
                document.getElementById(a).onclick = function() {
                    if (this.className.indexOf("group_f") < 0) {
                        //console.log(this.className.indexOf("group_f"));
                        // если кнопка не относится к МГ
                        // запомнить параметры для вызова распреда (значение)
                        //вызов функции распреда

                        setRaspred(this.value.substr(3,1),this.value.substr(5,1),this.value.substr(7,1),number_select);
                        //console.log(' cim ' + this.value.substr(3,1) +' LV ' + this.value.substr(5,1) +' F ' + this.value.substr(7,1) );
                    } else {
                        // если кнопка относится к МГ
                        if (this.className.indexOf(" active") < 0) {
                            // неактивна - добавть в класс активатор
                            $('.btnMenu.active').each(function(){this.className=this.className.slice(0, -7);});
                            this.className+=" active";
                            // запомнить параметры для вызова распреда (значение)
                            //console.log(' распред откладывается ' + this.value+' className:'+ this.className);
                        }
                        else {
                            // активна - убрать из класса активатор
                            this.className=this.className.slice(0, -7);
                            //console.log(' распред откладывается - ' + this.value+' className:'+ this.className);
                        }
                    }
                };
        });
        document.getElementById("buttonСontextMenuId").onclick = function (){
            // проверка на нажатие МГ
            var select_group = $('.btnMenu.active').val();
            if (typeof select_group!=="undefined") {
                // если выбрано кнопка с МГ группой (group_f)
            setRaspred(select_group.substr(3,1),select_group.substr(5,1),select_group.substr(7,1),number_select);
            } else {
                // иначе по умолчанию кинуть с 0 приоритетом на 1 уровень
                setRaspred(0,1,"",number_select);
            }

        };
        menu.style.zIndex = 1000;
        menu.style.top = defPosition(evt).y - 70 + "px";
        menu.style.left = defPosition(evt).x - 70 + "px";
        menu.style.display = "";
    }
    // Блокируем всплывание стандартного браузерного меню
    return false;
}
// Функция для добавления обработчиков событий
function addHandler(object, event, handler, useCapture) {
    if (object.addEventListener) {
        object.addEventListener(event, handler, useCapture ? useCapture : false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + event, handler);
    } else alert("Add handler is not supported");
}

// функция проверки где произошло событие (на меню или за его пределам)
function whereWasClick(x,y){
    var menuXmin = document.getElementById("contextMenuId").getBoundingClientRect().left;
    var menuXmax = document.getElementById("contextMenuId").getBoundingClientRect().right;
    var menuYmin = document.getElementById("contextMenuId").getBoundingClientRect().top;
    var menuYmax = document.getElementById("contextMenuId").getBoundingClientRect().bottom;
    if (menuXmin <= x && x <= menuXmax && menuYmin <= y && y <= menuYmax) {
        //console.log('false'+menuXmin +'<'+x+'<'+menuXmax+' и '+menuYmin+'<'+y+'<'+menuYmax);
        return false; // на меню
    }
    //console.log('true'+menuXmin +'<'+x+'<'+menuXmax+' и '+menuYmin+'<'+y+'<'+menuYmax);
    return true; // за его пределами
}
addHandler(document, "contextmenu", function () {
    // если правый клик за пределами меню и ниспадающих списков элементов меню, то прячим меню распреда
    console.log(event.target.className.indexOf("changemenu"));
    if (whereWasClick(event.clientX,event.clientY)&& event.target.className.indexOf("changemenu")<0) {
        document.getElementById("contextMenuId").style.display = "none";
    }

});
addHandler(document, "click", function (event) {
        // если клик за пределами и ниспадающих списков элементов меню, то прячим меню распреда
    console.log(event.target.className.indexOf("changemenu"));
    if (whereWasClick(event.clientX,event.clientY)&& event.target.className.indexOf("changemenu")<0) {
        document.getElementById("contextMenuId").style.display = "none";
    }

});
// если нажата кнопка интер
$('body').on('keypress', function (e) {
    /**
     * Определяет нажатие правой кнопки на клавиатуре
     *
     */
    if (e.which == 13 && $(".changesit").val() != "" && $(".changesit").val() != "undefined") {

        //console.log(" вызов функции распреда");
        document.getElementById("buttonСontextMenuId").click();
    } else {
        // иначе кинуть в поле для выбора сита
        $(".changesit").focus();
        console.log(" взять в фокус");
    }
});

// если нажата клафиша esc при открытом окне распреда
$("body").on("keyup", function (e) {
    if (e.which == 27) {
        document.getElementById("contextMenuId").style.display = "none";
    }
});
// если нажата правая кнопка мыши в контейнере с заявками
$("body").on("mousedown", ".tb_container", function (e, t, eOpts) {
    /**
     * Определяет нажатие правой кнопки в контейнере с заявкой
     *
     */
    if (e.button === 2) {
        //обнуление используемых переменных
        cim = "";
        pfys = "";
        sit = "";
        elem_sit = "";
        number_select = "";
        //---------
        number_select = $(this).parents(".curator_i").data("number"); // получить заявку, над которой щелкнули правой кнопкой мыши
        console.log("Номер выбранной заявки: " + number_select);
        $('.curator_i[data-number="' + number_select + '"] [name="LV"]').parents(".tb_container").children(".ticket_body").click();
    }

});
