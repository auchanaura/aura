function render_window(title, spec_class, body) {
    test = '<div class="desck_window resizable drag ' + spec_class + '">' +
        '<header class="window_header">' +
        '<span class="window_title">' + title + '</span>' +
        '<span class="window_managment">' +
        '<span class="window_managment_button _collapse glyphicon glyphicon-minus"></span>' +
        '</span>' +
        '</header>' +
        '<div class="window_body">' + body + '</div>' +
        '</div>';
    $('.workdesk').append(test);
}

$(document).ready(function () {
    $('.render_window').each(function () {
        render_window($(this).data('title'), $(this).data('class'), $(this).html());
        $(this).remove();
    });

    zindex = 0;
    $('.desck_window ').click(function () {
        z_this = $(this).css('z-index') * 1;
        if (zindex <= z_this) {
            zindex = z_this + 1;
        } else {
            zindex = zindex + 1;
        }
        $(this).css('z-index', zindex);
    });
    $('._collapse').click(function () {
        parent = $(this).parents('.desck_window');
        if (parent.hasClass('collapsed')) {
            $(this).removeClass('glyphicon-plus').addClass('glyphicon-minus');
            parent.removeClass('collapsed');
        } else {
            $(this).removeClass('glyphicon-minus').addClass('glyphicon-plus');
            parent.addClass('collapsed');
        }
    });
});