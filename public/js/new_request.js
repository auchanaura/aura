$('body').on('click', '.open_modal_window', function () {
    number = $(this).data('number');
    action_number = $(this).data('actionnumber');
    name = $(this).data('window');
    if (typeof(number) == 'undefined') {
        number = $(this).parents('.curator_i').data('number')
    }
    $.ajax({
        method: "GET",
        url: "/ajax/modalWindow",
        data: {name: name, number: number, action_number: action_number}
    }).done(function (data) {
        $('body').append(data);

        function formatResult(node) {
            var $result = $('<span style="padding-left:' + (20 * node.level) + 'px;">' + node.text + '</span>');
            return $result;
        };
        if (name == 'new_request') {
            data = JSON.parse($('.select_data').html());
            $("#TOPIC_RUS").select2({
                placeholder: 'Выберите тип проблемы',
                data: data,
                formatSelection: function (item) {
                    return item.text
                },
                formatResult: function (item) {
                    return item.text
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: formatResult,
                theme: "bootstrap"
            });
            $(".new_request_form .real_site").select2({
                placeholder: 'Выберите сайт',
                theme: "bootstrap"
            });
        }
    });
});

$('body').on('click', '.close_modal_window', function () {
    $(this).parents('.new_request_form_container').remove();
});

$('body').on('change', '.new_request_form .ad_search_input', function () {
    $.ajax({
        method: "post",
        url: "/ajax/ADSearch",
        data: {
            name: $('.new_request_form input[name="login"]').val(),
            displayname: $('.new_request_form input[name="fio"]').val(),
            mail: $('.new_request_form input[name="email"]').val(),
            dn: $('.new_request_form input[name="site"]').val()
        }
    }).done(function (data) {
        data = JSON.parse(data);
        fio = [];
        email = [];
        login = [];
        site = [];
        department = [];
        $.each(data, function (key, value) {
            if (fio.indexOf(value[0]) == -1) {
                fio.push(value[0]);
            }
            if (login.indexOf(key) == -1) {
                login.push(key);
            }
            if (email.indexOf(value[1]) == -1) {
                email.push(value[1]);
            }

        });
        if (department.length == 1) {
            $('.new_request_form input[name="department"]').val(department[0]);
        }
        if (fio.length == 1) {
            $('.new_request_form input[name="fio"]').val(fio[0]);
        }
        if (login.length == 1) {
            $('.new_request_form input[name="login"]').val(login[0]);
        }
        if (email.length == 1) {
            $('.new_request_form input[name="email"]').val(email[0]);
        }
        if (site.length == 1) {
            $('.new_request_form input[name="site"]').val(site[0]);
        }
        $('.new_request_form input[name="fio"]').autocomplete({
            source: fio,
            minLength: 0
        });
        $('.new_request_form input[name="login"]').autocomplete({
            source: login,
            minLength: 0
        });
        $('.new_request_form input[name="email"]').autocomplete({
            source: email,
            minLength: 0
        });
        $('.new_request_form input[name="department"]').autocomplete({
            source: department,
            minLength: 0
        });
        $('.new_request_form input[name="site"]').autocomplete({
            source: site,
            minLength: 0
        });
        console.log(data);
    });
});

$('body').on('click', '.new_request_form .ad_search_input', function () {
    $(this).autocomplete("search", "");
});

$('body').on('submit', '.new_request_form_container form', function (e) {
    error = 0;
    $('.requested_input').each(function () {
        if ($(this).val() == '' && $(this).attr('name') != 'site') {
            error = 1;
        }
    });
    if (error) {
        swal("Ошибка", 'Необходимо заполнить все поля', "error");
        return false;
    }
    action = $('.new_request_form_container button[type="submit"]').data('action');
    $.ajax({
        method: "post",
        url: "/ajax/" + action,
        data: {data: JSON.stringify($(this).serializeArray())}
    }).done(function (data) {
        result = 'Заявка создана!';
        if (typeof($('.result_msg').html()) != 'undefined') {
            result = $('.result_msg').html();
        }
        $('.new_request_form_container').remove();
        swal(result, data, "success");
    });
    return false;
});
