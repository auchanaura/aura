context_height = localStorage.getItem('context_height');
if (typeof(context_height) != 'undefined' && context_height != "") {
    console.log(1);
    $(".event_body").css("height", 'calc(' + (100 - context_height) + 'vh - 160px)');
    $(".ticket_eml_body").css("height", context_height + 'vh');
}
$(function () {
    $(".ticket_eml_body").resizable({
        handles: "s",
        alsoResize: '.event_body',
        resize: function (e, ui) {
            height = $(".ticket_eml_body").height() + 4;
            if (height < 110) {
                height = 110;
            }
            localStorage.setItem('context_height', height * 100 / window.innerHeight);
            $(".event_body").css({
                height: 'calc(' + (100 - height * 100 / window.innerHeight) + 'vh - 160px)',
                width: '100%'
            });
            $(".ticket_eml_body").css({height: height * 100 / window.innerHeight + 'vh', width: '100%'});
        }
    });
});
if (localStorage.getItem('request_details') == 1) {
    $(document).ready(function () {
        $('.request_details').click();
    });
}
$('.request_details').click(function () {
    hidden = 0;
    if ($(this).hasClass('collapsed')) {
        hidden = 1;
    }
    localStorage.setItem('request_details', hidden);
});