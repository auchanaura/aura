user_answer_text = 'Необходимо написать ответ пользователю!';

$('input[name="type"]').change(function () { // Переключение типа
    $('.second_comment').show();
    switch ($('input[name="type"]:checked').val()) {
        case 'hold':
            $('.hold').show();
            $('.hold').parent().show();
            $('.escalation').hide();
            $('.crit').show();
            $('.num').hide();
            $('input[name="status_global"]:checked').prop('checked', false).parent().removeClass('active');
            break;
        case 'escalation':
            $('.hold').hide();
            $('.escalation').show();
            $('.time_up').hide();
            $('.escalation').parent().show();
            $('.crit').show();
            $('.num').hide();
            $('input[name="status_global"]:checked').prop('checked', false).parent().removeClass('active');
            break;
        case 'comment':
            switch_user_comment(3);
            $('.hold').parent().hide();
            $('.num').show();
            $('.time_up').hide();
            $('.time_up').hide();
            $('.crit').show();
            $('input[name="status_global"]:checked').prop('checked', false).parent().removeClass('active');
            break;
    }
});

$('input[name="status_global"]').change(function () {
    $('.second_comment').show();
    switch ($('input[name="status_global"]:checked').val()) {
        case '35':
            $('.num').hide();
            $('.time_up').show();
            switch_user_comment(2);
            break;
        case '94':
            $('.num').show();
            $('.time_up').show();
            switch_user_comment(2);
            break;
        case '7':
            $('.hold').parent().hide();
            $('.num').hide();
            $('.time_up').hide();
            $('.crit').hide();
            switch_user_comment(1);
            $('input[name="type"]:checked').prop('checked', false);
            break;
        case '4':
            $('.num').show();
            $('.time_up').hide();
            switch_user_comment(2);
            break;
        case '9':
            $('.time_up').hide();
            switch_user_comment(2);
            break;
        case '99':
            $('.num').hide();
            $('.time_up').hide();
            switch_user_comment(2);
            break;
        case '32':
            $('.num').hide();
            $('.time_up').hide();
            switch_user_comment(1);
            break;
        case '33':
            $('.num').hide();
            $('.time_up').hide();
            switch_user_comment(1);
            break;
        case '-1':
            $('.num').hide();
            $('.time_up').hide();
            switch_user_comment(2);
            break;
        case '11':
            $('.num').hide();
            $('.time_up').hide();
            switch_user_comment(2);
            break;
        default:
            $('.time_up').hide();
            switch_user_comment(1);
            break;
    }
});

function switch_user_comment(mode) {
    if (mode == 1) {
        user_answer_text = 'Необходимо написать ответ пользователю!';
        $('.user_answer_container2 label').html('Ответ пользователю');
        $('.user_answer_container label').html('Технический комментарий');
        $('.second_comment').html('Требуется отдельный технический комментарий');
    } else if (mode == 2) {
        user_answer_text = 'Необходимо написать технический комментарий!';
        $('.user_answer_container2 label').html('Технический комментарий');
        $('.user_answer_container label').html('Ответ пользователю');
        $('.second_comment').html('Требуется отдельный ответ пользователю');
    } else if (mode == 3) {
        user_answer_text = 'Необходимо написать технический комментарий!';
        $('.user_answer_container2 label').html('Технический комментарий');
        $('.second_comment').hide();
    }

}