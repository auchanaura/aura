context_height = localStorage.getItem('context_height');
if (typeof(context_height) != 'undefined' && context_height != "") {
    console.log(1);
    $(".event_body").css("height", 'calc(' + (100 - context_height) + 'vh - 160px)');
    $(".ticket_eml_body").css("height", context_height + 'vh');
}
$(function () {
    $(".ticket_eml_body").resizable({
        handles: "s",
        alsoResize: '.event_body',
        resize: function (e, ui) {
            height = $(".ticket_eml_body").height() + 4;
            if (height < 110) {
                height = 110;
            }
            localStorage.setItem('context_height', height * 100 / window.innerHeight);
            $(".event_body").css({
                height: 'calc(' + (100 - height * 100 / window.innerHeight) + 'vh - 160px)',
                width: '100%'
            });
            $(".ticket_eml_body").css({height: height * 100 / window.innerHeight + 'vh', width: '100%'});
        }
    });
});
if (localStorage.getItem('request_details') == 1) {
    $(document).ready(function () {
        $('.request_details').click();
    });
}
$('.request_details').click(function () {
    hidden = 0;
    if ($(this).hasClass('collapsed')) {
        hidden = 1;
    }
    localStorage.setItem('request_details', hidden);
});
$('.send_curation').click(function(){
	$.ajax({
		method: "POST",
		url: "/ajax/send_curation",
		data: { }
	}).done(function(data) {
		$.Notification.notify('success','top center','Новые заявки распределены!', 'Распределение заявок успешно подтверждено.')
		setTimeout(function(){window.location.replace('/curation');}, 2000);
	});
});
$('.send_new').click(function(){
	$.ajax({
		method: "POST",
		url: "/ajax/see_new",
		data: { }
	}).done(function(data) {
		$.Notification.notify('success','top center','Новые заявки просмотрены', 'Новые заявки просмотрены')
		setTimeout(function(){window.location.replace('/curation');}, 1000);
	});
});
/**
* Создание и наполнение хранилища под названия ситов 
* @type Array|Object
*/
var sits = []; // переменная под все магазины
// находим все объекты 'optgroup option' -- названиния ситов -- и ложим их в перемнную 
sits = $('optgroup option').each(function(){sits.push($('optgroup option').next().val())}); 
var sites=sits.toArray(); // переделываем данную переменную с названиями в массив 
var data = []; // массив для хранилища ситов  
 // заполняем хранилище только названием сита (отбрасываем все лишнее)
for (var i = 0; i < sites.length; i++) {
	data.push(sites[i].value);
}
// создаем модель под поля хранилища
Ext.define('JobA', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'sit'}
            ]
        });
// создаем хранилище, в котором содержится массив с названиями магазинов
var sit_store = Ext.create('Ext.data.ArrayStore', {
                model: 'JobA',
                data: data,
                expandData: true // this is tied to ArrayStore#loadData only
});
// наполняем данными наше хранилище
sit_store.loadData(data); 

/**
* Создание и наполнение хранилища под логин 
* @type Array|Object
*/
var LOGIN_IN_AURA = [];







/**
* Создание меню для распреда
* @type Ext.menu.Menu
*/	
// переменные для меню распреда
var number_select = '';
var sit = '';
var id_button_mg='';
var cim = '';
var pfys = '';
var elem_sit= '';

// кнопки под распределение с приоритетом 0			 
   menu_for_raspred_0 = new Ext.menu.Menu({
	id: 'menu_for_raspred_0',	
	width: 160,
	glyph: 72,   
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM0_1',							 
                        text: 'Центр Поддержки',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Центр Поддержки',
                        handler: function(){
                           // console.log('вызов функции с параметрами CIM0_1'); 
                           set_raspred(0,1,pfys,number_select);
                           
                        }
                    },
                    { 
                        id: 'CIM0_4',							 
                        text: 'Контакный Центр',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Контакный Центр',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_4');
                            set_raspred(0,4,pfys,number_select);
                           
                        }
                    },
                    { 
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-0',
                        items:[
                            { 
                                id: 'CIM0_3',
                                text: 'Мобильная группа',
                                tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Мобильную группу',
                                xtype: 'button',
                                style: "background: #ffffff" ,
                                enableToggle: true,
                                toggleGroup: 'group_f',
                                height: 32,
                                handler: function(){
                                    //console.log('вызов функции с параметрами CIM0_3');
                                    Ext.getCmp('confurd').show();
                                    Ext.getCmp('sit_raspred').show();
									
                                }
                            },
                            {
                                id: 'CIM0_3_f',
                                xtype: 'button',
                                tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Мобильную группу с физической поломкой',
                                boxLabel: 'Физ',
                                cls: 'curation-menu-0',
                                name: 'phisical',
                                text: 'Физич.',
                                enableToggle: true,
                                height: 32,
                                toggleGroup: 'group_f',
                                handler: function(){
                                   // console.log('вызов функции с параметрами CIM0_3_f');
                                    Ext.getCmp('confurd').show();
                                    Ext.getCmp('sit_raspred').show();
									
									
                                }
                            }
                        ]
                    },
                    '-',
                    { 
                        id: 'CIM0_5',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом на 2 уровень поддержки АТАК',
                        cls: 'curation-menu-0',
                        text: 'АТАК',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_5');
                            set_raspred(0,5,pfys,number_select);
                           
                        }
                    },
                    { 
                        id: 'CIM0_6',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом на 2 уровень поддержки DataQuality',
                        text: 'DataQuality',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_6');
                            set_raspred(0,1,pfys,number_select);
                            
                        }
                    }
        ]
		 
});	

// кнопки под распределение с приоритетом 1
menu_for_raspred_1 = new Ext.menu.Menu({
	id: 'menu_for_raspred_1',
	text: 'Высокий',
	xtype: 'menu',
	glyph: 72,    
	width: 160,
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM1_1',							 
                        text: 'Центр Поддержки',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Центр Поддержки',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_1');
                            set_raspred(1,1,pfys,number_select);

                        }
                    },
                    { 
                        id: 'CIM1_4',
                        text: 'Контакный Центр',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Контакный Центр',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_4');
                            set_raspred(1,4,pfys,number_select);
                        }
                    },
                    { 
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-1',
                        items:[
                                {
                                    id: 'CIM1_3',
                                    text: 'Мобильная группа',
                                    tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Мобильную группу',
                                    xtype: 'button',
                                    style: "background: #ffde92" ,
                                    enableToggle: true,
                                    toggleGroup: 'group_f',
                                    name: 'group_f',
                                    height: 32,
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM1_3');
                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                },
                                {
                                    id: 'CIM1_3_f',
                                    xtype: 'button',
                                    boxLabel: 'Физ',
                                    tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Мобильную группу с физической поломкой',
                                    cls: 'curation-menu-1',
                                    name: 'group_f',
                                    name: 'phisical',
                                    text: 'Физич.',
                                    enableToggle: true,
                                    height: 32,
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                            //console.log('вызов функции с параметрами CIM1_3_f');
                                            Ext.getCmp('confurd').show();
                                            Ext.getCmp('sit_raspred').show();

                                    }
                            }
                        ]
                    },
                    '-',
                    { 
                        id: 'CIM1_5',
                        text: 'АТАК',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом на 2 уровень поддержки АТАК',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM10_5');
                            set_raspred(1,5,pfys,number_select);
                        }
                    },
                    { 
                        id: 'CIM1_6',
                        cls: 'curation-menu-1',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом на 2 уровень поддержки DataQuality',
                        text: 'DataQuality',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_6');
                            set_raspred(1,1,pfys,number_select);
                        }
                    }
        ]	 
});	
// кнопки под распределение с приоритетом 2
menu_for_raspred_2 = new Ext.menu.Menu({
	id: 'menu_for_raspred_2',
	text: 'Критичный',
	xtype: 'menu',
	width: 160,
	glyph: 72,
        cls: 'curation-menu-2',
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM2_1',							 
                        text: 'Центр Поддержки',
                        tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Центр Поддержки',
                        cls: 'curation-menu-2',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM2_1');
                            set_raspred(2,1,pfys,number_select);

                        }
                    },
                    { 
                        id: 'CIM2_4',
                        text: 'Контакный Центр',
                        tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Контакный Центр',
                        cls: 'curation-menu-2',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM2_4');
                            set_raspred(2,4,pfys,number_select);								 

                          }
                    },
                    { 
                        xtype: 'container',
			layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-2',
                        items:[
                                {
                                    id: 'CIM2_3',
                                    style: "background: #ffa292" ,
                                    text: 'Мобильная группа',
                                    cls: 'curation-menu-2',
                                    tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Мобильную группу',
                                    xtype: 'button',
                                    enableToggle: true,
                                    height: 32,
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM2_3');
                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                },
                                {
                                    id: 'CIM2_3_f',
                                    xtype: 'button',
                                    boxLabel: 'Физ',
                                    tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Мобильную группу с физической поломкой',
                                    cls: 'curation-menu-2',
                                    name: 'phisical',
                                    enableToggle: true,
                                    height: 32,
                                    text: 'Физич.',
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM2_3_f');

                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                }
                           ]
                        },
                        '-',
                        { 
                            id: 'CIM2_5',
                            text: 'АТАК',
                            tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом на 2 уровень поддержки АТАК',
                            cls: 'curation-menu-2',
                            handler: function(){
                                //console.log('вызов функции с параметрами CIM2_5');
                                set_raspred(2,5,pfys,number_select);
                            }
                        },
                        { 
                            id: 'CIM2_6',
                            text: 'DataQuality',
                            tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом на 2 уровень поддержки DataQuality',
                            cls: 'curation-menu-2',
                            handler: function(){
                               // console.log('вызов функции с параметрами CIM2_6');
                                set_raspred(2,1,pfys,number_select);
                            }
                        }
        ]
});

/**
 * хранилище времени для распределения
 * @type |||
 */
var times = Ext.create('Ext.data.Store', {
    fields: ['times_id', 'times_rus'],
    data : [
            {"times_id":"5", "times_rus":"Пять минут"},
            {"times_id":"8", "times_rus":"Восемь минут"},
            {"times_id":"10", "times_rus":"Десять минут"},
            {"times_id":"15", "times_rus":"Пятнадцать минут"}		
    ]
});

        /**
         * два новых метода для комбо бокса 
         */
Ext.define( "Ext.form.field.ComboBoxOverride", {
    "override": "Ext.form.field.ComboBox",
// берет след элемент хранилища
    "selectNextItem": function( suppressEvent ) {
        var store       = this.getStore();
        var value       = this.getValue();
        var index       = store.find( this.valueField, value );
        var next_index  = index + 1;
        var next_record = store.getAt( next_index );
        if( next_record ) {
            this.select( next_record );

            if( ! suppressEvent ) {
                this.fireEvent( 'select', this, [ next_record ] );
            }
        }
    },
// берет прошлый элемент хранилища
    "selectPrevItem": function( suppressEvent ) {
        var store       = this.getStore();
        var value       = this.getValue();
        var index       = store.find( this.valueField, value );
        var prev_index  = index - 1;
        var prev_record = store.getAt( prev_index );
        if( prev_record ) {
            this.select( prev_record );

            if( ! suppressEvent ) {
                this.fireEvent( 'select', this, [ prev_record ] );
            }
        }
    }
});

// функция распределения
function set_raspred(CIM,LV,pfys,number_select ){
            if (LV==3) { // если морбильная группа
            //получение элемента мобильной группы, который был нажат 
		GroupToggle.forEach( function (a) { if (a.pressed==true) { id_button_mg=a.id;} });
                // заполнение переменных данными	
                    CIM = id_button_mg.slice(3,4);
                    pfys = id_button_mg.slice(7,8);
                    sit=Ext.getCmp('sit_raspred').rawValue;
            // заполнение распределенными данными нужной заявки поля с боку (визуализация) 
                // если нажата кнопка "физич", 
                if ( pfys=='f' ) {                   
                    $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').attr("checked", "checked");
                    Tickets[number_select].PHYSICAL=1;
                } else {
                    $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').removeAttr('checked');
                    Tickets[number_select].PHYSICAL=0;
                } 
                // --- элемент LV
                Tickets[number_select].LV=LV;
                Tickets[number_select].REAL_SITE=sit;
                    // удаляем выбранный элемент (название сита), если заявка уже была один раз распределена
                $('.curator_i[data-number="'+number_select+'"] [name="LV"] [value='+sit.slice(0,3)+']').remove();
                    // добавляем элемент (название сита), к списку элемента на морде 
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').append($('<option value="'+sit.slice(0,3)+'">'+sit+'</option>'));
                    // выбираем добавленный элемент
                $('.curator_i[data-number="'+number_select+'"] [name="LV"] [value='+sit.slice(0,3)+']').attr("selected", "selected");
                // --- элемент KERNEL_JOB_TIME_LIMIT 
                Tickets[number_select].KERNEL_JOB_TIME_LIMIT=Ext.getCmp('times_raspred').getValue();
                $('.curator_i[data-number="'+number_select+'"] [name="KERNEL_JOB_TIME_LIMIT"]').val(Ext.getCmp('times_raspred').getValue());
                 
                Tickets[number_select].CIM=CIM;
                CIM=CIM+''; //преобразование в строку
                // --- элемент CIM
                $('.curator_i[data-number="'+number_select+'"] [name="CIM"]').val(CIM);               
            //  инициализация применения изменений в базу           
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').change();		
            }
            else 
            { // применение параметров для остальных
                // снятие галочки физ нажатие
                $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').removeAttr('checked');
                Tickets[number_select].PHYSICAL=0;
                // LV
                Tickets[number_select].LV=LV;
                LV=LV+''; //преобразование в строку
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').val(LV);
                // CIM
                Tickets[number_select].CIM=CIM;
                CIM=CIM+''; //преобразование в строку
                $('.curator_i[data-number="'+number_select+'"] [name="CIM"]').val(CIM);
                // limit
                $('.curator_i[data-number="'+number_select+'"] [name="KERNEL_JOB_TIME_LIMIT"]').val(Ext.getCmp('times_raspred').getValue());
                Tickets[number_select].KERNEL_JOB_TIME_LIMIT=Ext.getCmp('times_raspred').getValue();
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').change();                
            }
            console.log('Номер: '+number_select+';CIM:'+CIM+';LV:'+LV+';физ:'+pfys);
            menu_for_raspred.hide();
}

// переменная, в которую подвешивается функция-обработчик колесика
var wheel_handle = null;
			
// создание обработки собтия вращения колеса		
var mouse_wheel = function(event) {
        if (false == !!event) event = window.event;
        var direction = ((event.wheelDelta) ? event.wheelDelta/120 : event.detail/-3) || false;
        if (direction && !!wheel_handle && typeof wheel_handle == "function") {
                if (event.preventDefault) event.preventDefault();
                event.returnValue = false;
                wheel_handle(direction);
        }
}  

		/**
		*	окно с всеми элементами
		*/
menu_for_raspred = new Ext.Window({		
	id: 'menu_for_raspred',
	title: 'Распределение '+number_select+', от бычного к критичному',
	autoHeight: true,
        closeAction:"hide", //кнопочка на закрытие, которая прячит окно
	//closable: false,
	resizable: false, // возможность изменения размеров окна.
	draggable: true, // возможность перетаскивания окна.
	flex:1,
	autoScroll: true,
	hideHeaders: true,
	hideLabel: false,
	bodyStyle: 'padding:-13 -13 -13 -13', 
	layout:'vbox',
        
	items: [ 
		{
                    xtype: 'panel',
                    layout:'hbox',

                    bodyStyle: 'padding:-3 -3 -3 -3', 
                    items: [
                            menu_for_raspred_0,
                            menu_for_raspred_1,
                            menu_for_raspred_2
                    ]
		},
		{
                    xtype: 'panel',
                    layout:'hbox',			
                    bodyStyle: 'padding:-3 -3 -3 -3', 
                    items: [
                            { 
                                id: 'times_raspred', 
                                name: 'times_raspred', 
                                fieldLabel: 'Минуты',
                                labelWidth: 50,						
                                xtype: 'combobox', 
                                store: times, 
                                queryMode: 'local', 
                                displayField: 'times_rus', 
                                valueField: 'times_id', 
                                allowBlank: false,
                                editable: false,
                                multiSelect: false,
                                disabled: false,
                                hidden: false,
                                triggerAction: 'all',
                                value: "10",
                                style: "background: #e8e8e8",
                                selectOnFocus:true							
                            },
                            { 
                                id: 'sit_raspred', 
                                name: 'it_raspred', 
                                fieldLabel: 'Сит', 
                                xtype: 'combobox', 
                                labelWidth: 20,
                                store: sit_store, 
                                queryMode: 'local', 
                                displayField: 'sit', 
                                valueField: sit_store.data.items, 
                                allowBlank: false,
                                editable: true,
                                multiSelect: false,
                                disabled: false,
                                style: "background: #e8e8e8",
                                hidden: true,
                                triggerAction: 'all',
                                value: "",						
                                selectOnFocus:true,
                                listeners: {
                                        'change': function (combo, newval, odlval) {										
                                            var store = this.store;
                                            store.suspendEvents();
                                            store.clearFilter();
                                            store.resumeEvents();
                                            store.filter({
                                              property: 'sit',
                                              anyMatch: true,
                                              value   : this.getValue()
                                            });
                                        }
                                }
                            },
                            {
                                id: 'confurd',
                                xtype: 'button',
                                width: 65,
                                text: 'Отправить',									
                                handler: function(){
                                                // дублирования действий при нажатии enter											
                                                if (Ext.getCmp('sit_raspred').value!='') {
                                                      set_raspred(0,3,pfys,number_select);
                                                } else {
                                                        // иначе кинуть в поле для выбора сита
                                                        Ext.getCmp('sit_raspred').focus();
                                                }	
                                }
                            }

                    ]
		}
		
	],
	listeners: {
		
		afterRender: function() {
			var temp;
			document.body.onmouseover = document.body.onmouseout = handler;
                        function handler(event) {					
                                var str = event.target.id;
                                var el=str.slice(0,6);
                                if (event.type == 'mouseover' && event.target.id.search("CIM")!=-1) {
                                        temp=document.getElementById(el).style.background;
                                        document.getElementById(el).style.background = '#e8e8e8';
                                }	
                                if (event.type == 'mouseout' && event.target.id.search("CIM")!=-1) {
                                        document.getElementById(el).style.background = temp;
                                }

                        }
			
			// если вращение колеса было в искомом элементе, то обрабатываем, если нет, то функци иобработки не будет
			var set_handle = function(id, func) {
				document.getElementById(id).onmouseover = function() {
					wheel_handle = func;					
				}
				document.getElementById(id).onmouseout = function() {
					wheel_handle = null;
					handler = null;
				}
			}									
			
			 // распазнаем куда крутилось колесо  (значение переменной direction: "1" - в верх и "-1" вниз) 
             // и делаем соотвествующее изменение элемента комбо 
			var set_move_item = function(direction) {
				//console.log('значение '+ direction);
				if (direction==1) {Ext.getCmp('times_raspred').selectNextItem();}
				else {Ext.getCmp('times_raspred').selectPrevItem();}				
			}
			//ловля onmousewheel (события движения колесика мышки) для всего окна (объекта window)
			if (window.addEventListener) window.addEventListener("DOMMouseScroll", mouse_wheel, false);
			window.onmousewheel = document.onmousewheel = mouse_wheel;
			set_handle('menu_for_raspred', set_move_item);
			
		}
	} 
});	

		/**
		* переменная, которая содержит в себе все кнопки с эскалацией в мобильную группу
		*/
var GroupToggle = Ext.ComponentQuery.query('button[toggleGroup = group_f]');

// если нажата кнопка интер  
$('body').on('keypress',function(e){
            /**
            * Определяет нажатие правой кнопки на клавиатуре 
            * 
            */
    if (e.which == 13 && Ext.getCmp('sit_raspred').value!='') { 
		set_raspred(0,3,pfys,number_select);
	} else {
		// иначе кинуть в поле для выбора сита		
		 Ext.getCmp('sit_raspred').focus();		
	}
});

// если нажата клафиша esc при открытом окне распреда
$('body').on('keyup',function(e){
	if (e.which ==27 && menu_for_raspred.hidden==false) { menu_for_raspred.hide(); }
});

// если нажата правая кнопка мыши в контейнере с заявками
$('body').on('mousedown','.tb_container',function(e, t, eOpts){
            /**
            * Определяет нажатие правой кнопки в контейнере с заявкой 
            * 
            */
    if (e.button==2){
       $('.curator_i[data-number="'+number_select+'"] [name="LV"]').parents('.tb_container').children('.ticket_body').click();
        GroupToggle.forEach( function (a) { a.toggle(false)}) // отжать все кнопочки мобильной группы
                //обнуление используемых переменных
		id_button_mg='';
		cim = '';
		pfys = '';
		sit = '';
		elem_sit= '';
                number_select='';
                //---------
        number_select = $(this).parents('.curator_i').data('number'); // получить заявку, над которой щелкнули правой кнопкой мыши
	console.log('Номер выбранной заявки: '+ number_select);
        Ext.getCmp('confurd').hide(); // спрятать кнопку подвтерждения
        Ext.getCmp('sit_raspred').hide(); // спрятать список ввода сита
        Ext.getCmp('sit_raspred').setValue(''); // очистить значение списка ввода сита
        Ext.getCmp('times_raspred').setValue('10'); // значение времени по умолчанию
        menu_for_raspred.setTitle('Распределение '+number_select+', от бычного к критичному; ESC - отмена') // вывести номер заявки в заголовок
	menu_for_raspred.showAt(e.clientX-60,e.clientY-50); // координаты, где вывести окно
	
    }
    
});

//При клике на тело тикета или иконку - открываем подробное содержимое
	$('body').on('click','.ticket_body', function() {
		number = $(this).parents('.curator_i').data('number');
		action = $(this).parents('.curator_i').data('action');
		show_event(number,action);
		select_ticket($(this).parents('.curator_i'));
	});
	$('body').on('click','.curator_i .action_icon_container', function() {
		number = $(this).parents('.curator_i').data('number');
		action = $(this).parents('.curator_i').data('action');
		show_event(number,action);
		select_ticket($(this).parents('.curator_i'));
	});

	function select_ticket(element) {
		i_number = element.data('number');
		action_selector = element.data('action');
		if (typeof(action_selector) != 'undefined') {
			action_selector = '[data-action="'+action_selector+'"]';
		} else {
			action_selector = '';
		}
		if (!$('.curator_i[data-number="'+i_number+'"]'+action_selector).hasClass('loaded')) {
			setTimeout(function(){select_ticket(element)}, 50);
			$("body").css("cursor", "progress");
		} else {
			if ($('.ticket_event_container[style*="display: block;"]').length > 0) {
				if ($('.ticket_event_container[style*="display: block;"]').parent().position().top < $('.curator_i[data-number="'+i_number+'"]').position().top) {
					$('body').scrollTop($('body').scrollTop() - ($('.ticket_event_container[style*="display: block;"]').height() + 17));
				}
			}
			$('.curator_i:not([data-number="'+i_number+'"]'+action_selector+') .ticket_event_container').hide();
			$('.list-group-item-info').removeClass('list-group-item-info');
			element.addClass('list-group-item-info');
			
			if (typeof(Tickets[i_number].htmlbody) != 'undefined' && Tickets[i_number].htmlbody != '') {
				$('.ticket_eml_body_container').html(Tickets[i_number].htmlbody);
			} else {
				$('.ticket_eml_body_container').html(Tickets[i_number].TICKET_DESCRIPTION);
			}
			$("body").css("cursor", "default");
		}
	}
	full_info = 0;
	$('body').on('mouseenter','.curator_i', function() {
		number = $(this).data('number');
		download_info(number,0,full_info);
	});

	function download_info(number,render = 0, full_info = 0, mode = 0) {
		url = 'EmlInfo';
		if (full_info == 1) {
			url = 'GetFullInfo';
		}
		if (typeof(Tickets[number]) != 'undefined' && typeof(Tickets[number].htmlbody) == 'undefined') {
			$.ajax({
				method: "POST",
				url: "/ajax/"+url,
				data: { ticket: number}
			}).done(function(data) {
				//console.log(data);
				data = JSON.parse(data);
				Tickets[number].htmlbody = data.html;
				Tickets[number].attachments = data.attachments;
				if (full_info == 1) {
					$.each(data,function(key,value){
						Tickets[number][key] = value; 
					});
				}
				if (render) {
					$('.curator_i[data-number="'+number+'"] .ticket_body').click();
				}
				$('.curator_i[data-number="'+number+'"]').addClass('loaded');
			});
		}
		if (typeof(Tickets[number]) != 'undefined' && typeof(Tickets[number].history) == 'undefined') {
			$.ajax({
				method: "POST",
				url: "/ajax/TicketEvents",
				data: { ticket: number, mode: mode}
			}).done(function(data) {
				data = JSON.parse(data);
				Tickets[number].history = [];
				$.each(data,function(key,value) {
					Tickets[number].history[key] = value;
				});
			});
		}
	} 
	//При двойном клике - раскрываем список действий
	$('body').on('dblclick','.curator_i', function() {
		number = $(this).data('number');
		show_detailed_info(number);
	});
	function show_detailed_info(number) {
		$('.curator_i[data-number="'+number+'"] .detailed_info').html($('.detailed_info_template').html());
		$('.curator_i[data-number="'+number+'"] .number').html('<span style="font-weight:bold;">Номер:</span> <span>'+Tickets[number].NUMBER+'</span>');
		$('.curator_i[data-number="'+number+'"] .status0').html('<span style="font-weight:bold;">Время рег.:</span> <span>'+Tickets[number].STATUS0+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_fio').html('<span style="font-weight:bold;">Заявитель:</span> <span>'+Tickets[number].USER_FIO+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_site').html('<span style="font-weight:bold;">Сайт:</span> <span>'+Tickets[number].USER_SITE+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_login').html('<span style="font-weight:bold;">Логин:</span> <span>'+Tickets[number].USER_LOGIN+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_mail').html('<span style="font-weight:bold;">Емейл:</span> <span>'+Tickets[number].USER_MAIL+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_post').html('<span style="font-weight:bold;">Должность:</span> <span>'+Tickets[number].USER_POST+'</span>');
		$('.curator_i[data-number="'+number+'"] .event_number').html('<span style="font-weight:bold;">Номер ивента:</span> <span>'+Tickets[number].NUMBER_EVENT+'</span>');
		$('.curator_i[data-number="'+number+'"] .beeline_number').html('<span style="font-weight:bold;">Номер оутсорс:</span> <span>'+Tickets[number].NUMBER_BEELINE+'</span>');
		if (Tickets[number].STATUS_JOB==2) {$('.curator_i[data-number="'+number+'"] .worker').html('<span style="font-weight:bold;">Исполняется:</span> <span>'+Tickets[number].WORKER+'</span>');}
		else { $('.curator_i[data-number="'+number+'"] .worker').html('');}
		if ($('.curator_i[data-number="'+number+'"] .problem_list').data('blocked') == 1) {
			$('.curator_i[data-number="'+number+'"] .problem_list').html('<span style="font-weight:bold;" class="info_name">Тема:</span><span class="info_input"><select class="form-control problems_list" id="problems_list" name="problems_list" placeholder="Задание"></select></span>');
			$(document).ready(function() {
				$('.curator_i[data-number="'+number+'"] .problems_list').select2();
			});
			$('.curator_i[data-number="'+number+'"] .problems_list').select2({
				placeholder: 'Выберите тип проблемы',
				data: problem_data,
				formatSelection: function(item) {
					return 'test';
				},
				formatResult: function(item) {
					return item.text;
				},
				escapeMarkup: function(markup) {
					return markup;
				},
				templateResult: formatResult,
				theme: "bootstrap",
			});
		} else {
			$('.curator_i[data-number="'+number+'"] .problem_list').html('<span style="font-weight:bold;" class="info_name">Тема: </span><span> '+Tickets[number].PROBLEM+'</span>');
		}
		$('.curator_i[data-number="'+number+'"] .problems_list').val(Tickets[number].PROBLEM_ID).trigger('change');
		$('.curator_i[data-number="'+number+'"] .attachments').html('<span style="font-weight:bold;">Вложения:</span>  '+Tickets[number].attachments);
		
		$('.curator_i:not([data-number="'+number+'"]) .detailed_info').hide();
		$('.curator_i[data-number="'+number+'"] .detailed_info').toggle();
	}

	function show_event(number,action = 0) {
		if (action != 0) {
			action_selector = '[data-action="'+action+'"]';
		} else {
			action_selector = '';
		}
		//$('.curator_i:not([data-number="'+number+'"]'+action_selector+') .ticket_event_container').hide();
		//$('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').toggle();

		//if ($('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').html() == '') {
		$('.event_body_container').html('');
		$.each(Tickets[number].history,function(key,value){
			event($('.event_body_container'),value);
		});
		//}
		/*if (typeof(action) != 'undefined' && action != 0) {
			action = $('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container .list-group-item[data-action="'+action+'"]')
			margin = $('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container .list-group-item').length;
			action.addClass('list-group-item-success');
			scroll = action.position().top;
			$('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').scrollTop(scroll);
		}*/
		//$('body').scrollTop($('.curator_i[data-number="'+number+'"]').position().top + $('.list-group').position().top);
		$('.event_body_container .list-group-item-text').each(function() {
			if ($(this).height() > 48) {
				$(this).addClass('collapsed_text');
			}
		});	
	}

	//При загрузке страницы - выбирает первый тикет для подробного отображения и инициируем поля распределения
	$(document).ready(function(){
		number = $('.curator_i').first().data('number');
		download_info(number,1,1);
	});

	$('body').on('click','.manage_list_button',function(){
		number = $(this).parents('.curator_i').data('number');
		template = $('.manage_list_template').html();
		if (typeof(template) != 'undefined') {
			if (Tickets[number].REAL_SITE != '-' || Tickets[number].REAL_SITE != '' || typeof(Tickets[number].REAL_SITE) != 'undefined') {
				template = template.split('%USER_SITE%').join(Tickets[number].REAL_SITE);
			} else {
				template = template.split('%USER_SITE%').join(Tickets[number].USER_SITE);
			}
			$(this).parent().find('.manage_list').html(template);
		}
		if (!$(this).parents('.curator_i').find('.ticket_update_options_select2').hasClass('_updated')) {
			$(this).parents('.curator_i').find('.ticket_update_options_select2').append($('.real_sites_template').html()).addClass('_updated');
		}
	});
	//Распределение заявки
	$('body').on('click','.curation_update_select',function(){
		number = $(this).parents('.curator_i').data('number');
		if (typeof($(this).data('cim')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="CIM"]').val($(this).data('cim'));
			Tickets[number].CIM = $(this).data('cim');
		}
		if (typeof($(this).data('min')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="KERNEL_JOB_TIME_LIMIT"]').val($(this).data('min'));
			Tickets[number].KERNEL_JOB_TIME_LIMIT = $(this).data('min');
		}
		if (typeof($(this).data('group')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="LV"]').val($(this).data('group'));
			Tickets[number].LV = $(this).data('group');
			Tickets[number].GROUPE = real_sites[parseInt(String(Tickets[number].LV).replace(/\D+/g,""))];
		}
		$(this).parents('.curator_i').find('.ticket_update_options[name="KERNEL_JOB_TIME_LIMIT"]').change();
		if (Tickets[number].LV != 0 && Tickets[number].KERNEL_JOB_TIME_LIMIT != 0) {
			$(this).parents('.curator_i').addClass('list-group-item-success');
		}
	});

	$('.ticket_update_options').change(function(){
		number = $(this).parents('.curator_i').data('number');
		if ($(this).attr('name') == 'PHYSICAL') {
			Tickets[number][$(this).attr('name')] = $('.curator_i[data-number="'+number+'"] input[name="PHYSICAL"]:checked').val();
			if (typeof(Tickets[number][$(this).attr('name')]) == 'undefined') {
				Tickets[number][$(this).attr('name')] = 0;
			}
		} else {
			Tickets[number][$(this).attr('name')] = $(this).val();
		}
		if ($(this).attr('name') == 'LV') {
			Tickets[number].GROUPE = real_sites[parseInt(String(Tickets[number].LV).replace(/\D+/g,""))];
		}
		if (Tickets[number].LV != 0 && Tickets[number].KERNEL_JOB_TIME_LIMIT != 0) {
			$(this).parents('.curator_i').addClass('list-group-item-success');
		}
	});

	$('body').on('mousedown','.ticket_update_options_select2',function(){
		if (!$(this).hasClass('_updated')) {
			$(this).append($('.real_sites_template').html());
			$(this).addClass('_updated');
		}
	});

	$('body').on('click','.collapsed_text',function(){
		$(this).removeClass('collapsed_text').addClass('uncollapsed_text');
	});
	$('body').on('click','.uncollapsed_text',function(){
		if (window.getSelection().toString() == "") {
			$(this).removeClass('uncollapsed_text').addClass('collapsed_text');
		}
	});
filter = {};
	filter.numbers = ticket_list;
	filter.params = {'NUMBER':''};
	filter.sortBy = 'NUMBER';
	filter.orderBy = 'ASC';
	$(document).ready(function(){
		$('.filter_input .filter_page_input').change(function(){
			field = $(this).parents('.filter_input').data('field');
			result = $(this).val();
			if (result == null) {
				result = '';
			}
			filter.params[field] = result;
			ApplyFilter();
		});

		$('.search_page_sort').click(function(){
			$('.search_page_sort.active').removeClass('active');
			$(this).addClass('active');
			filter.sortBy = $(this).parents('.filter_input').data('field');
			filter.orderBy = $(this).data('order');
			ApplyFilter();
		});

		$('.filter_container').click(function(){
			$('.filter_body').toggle();
		});
	});

	function ApplyFilter() {
		filter.numbers = [];
		$.each(ticket_list,function(key,value) {
			i = 0;
			j = 0;
			$.each(filter.params,function(key2,value2) {
				j++;
				if (Array.isArray(value2)) {
					$.each(value2,function(key3,value3) { 
						if (Tickets[value][key2].indexOf(value3) != -1) {
							i++;
							return false;
						}
					});
				} else {
					if (Tickets[value][key2].indexOf(value2) != -1) {
						i++;
					}
				}
			});
			if (i == j) {
				filter.numbers.push(value);
			}
		});
		filter.numbers.sort(function(a, b){
			if (filter.sortBy == 'REAL_SITE' && Tickets[a][filter.sortBy] == '-') {
				a = Tickets[a]['USER_SITE'].toLowerCase();
			} else {
				a = Tickets[a][filter.sortBy].toLowerCase();
			}
			if (filter.sortBy == 'REAL_SITE' && Tickets[b][filter.sortBy] == '-') {
				b = Tickets[b]['USER_SITE'].toLowerCase();
			} else {
				b = Tickets[b][filter.sortBy].toLowerCase();
			}
			if (filter.orderBy == 'ASC') {
				if (a < b){
		    	    return -1;
			    } else if (a > b){
			    	return  1;
			    } else{
			    	return 0;
			    }
			} else {
				if (b < a){
		    	    return -1;
			    } else if (b > a){
			    	return  1;
			    } else{
			    	return 0;
			    }
			}
		});
		$('.curator_i').each(function(){
			if ($.inArray($(this).data('number'),filter.numbers) > -1 ) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		$.each(filter.numbers,function(key,value) {
			$('.curator_i[data-number="'+value+'"]').appendTo('.tickets_container');
		});
		$('.filter_container .label').html(filter.numbers.length);
	}
$(document).ready(function(){
		$('.ticket_update_options').change(function(){
			array = [];
			//$.each(changed_tickets,function(key,value){
			number = $(this).parents('.curator_i').data('number');
			array.push([Tickets[number],old_Tickets[number]]);
			//});
			array = JSON.stringify(array);
			$.ajax({
				method: "POST",
				url: "/ajax/send_curation_spool",
				data: {data: array}
			}).done(function(data) {
				$.Notification.notify('success','top center','Заявка №'+number+' распределена!', 'Распределение заявки успешно подтверждено.')
				//setTimeout(function(){window.location.replace('/curation?mode=1');}, 1500);
				old_Tickets[number].LV = Tickets[number].LV;
				old_Tickets[number].CIM = Tickets[number].CIM;
				old_Tickets[number].REAL_SITE = Tickets[number].REAL_SITE;
				old_Tickets[number].PHYSICAL = Tickets[number].PHYSICAL;
				old_Tickets[number].KERNEL_JOB_TIME_LIMIT = Tickets[number].KERNEL_JOB_TIME_LIMIT;
				old_Tickets[number].GROUPE = Tickets[number].GROUPE;
				console.log(data);
			});
		});
	});