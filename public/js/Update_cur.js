/**
 * Created by ru00160012 on 23.01.2017.
 */

// переменные для меню распреда
var number_select = '';
var sit = '';
var id_button_mg='';
var cim = '';
var pfys = '';
var elem_sit= '';


// Функция для определения координат указателя мыши
function defPosition(event) {
    var x = y = 0;
    if (document.attachEvent != null) { // Internet Explorer & Opera
        x = window.event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y = window.event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    } else if (!document.attachEvent && document.addEventListener) { // Gecko
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    } else {
        // Do nothing
    }
    return {x:x, y:y};
}

// формирование нового элеменета с кнопками отправки в соотвествующую группу


// функция вызова меню на правой кнопке
function menu(type, evt) {
    // Блокируем всплывание события contextmenu
    evt = evt || window.event;
    evt.cancelBubble = true;
    // Показываем собственное контекстное меню
    var menu = document.getElementById("contextMenuId");
    var html = "";
    switch (type) {
        case (1) :
            // тут и создать меню управления
            html = "Меню распределения";
            html += '<br><button type ="button" id="CIM0_1" value="CIM0_1" ' +
                'style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-0 btn-link dropdown-toggle waves-effect waves-light btn-xs">Центр Поддержки</button>';
            html += '<button type ="button" id="CIM1_1" value="CIM1_1" style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-1 btn-link dropdown-toggle waves-effect waves-light btn-xs">Центр Поддержки</button>';
            html += '<button type ="button" id="CIM2_1" value="CIM2_1" style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-2 btn-link dropdown-toggle waves-effect waves-light btn-xs">Центр Поддержки</button>';
            html += '<br><button type ="button" id="CIM0_4" value="CIM0_4" style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-0 btn-link dropdown-toggle waves-effect waves-light btn-xs">Контакт Центр</button>';
            html += '<button type ="button" id="CIM1_4" value="CIM1_4" style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-1 btn-link dropdown-toggle waves-effect waves-light btn-xs">Контакт Центр</button>';
            html += '<button type ="button" id="CIM2_4" value="CIM2_4" style="width: 33%; font-size: 11px" ' +
                'class="curation-menu-2 btn-link dropdown-toggle waves-effect waves-light btn-xs" >Контакт Центр</button>';
            html += '<br><button type ="button" id="CIM0_3" value="CIM0_3" style="width: 20%; font-size: 11px" ' +
                'class="curation-menu-0 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f">Мобил.Группа</button>';
            html += '<button type ="button" id="CIM0_3" value="CIM0_3_f" style="width: 13%; font-size: 11px" ' +
                'class="curation-menu-0 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f">Физ.</button>';
            html += '<button type ="button" id="CIM1_3" value="CIM1_3" style="width: 20%; font-size: 11px" ' +
                'class="curation-menu-1 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f">Мобил.Группа</button>';
            html += '<button type ="button" id="CIM1_3" value="CIM1_3_f" style="width: 13%; font-size: 11px" ' +
                'class="curation-menu-1 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f">Физ.</button>';
            html += '<button type ="button" id="CIM2_3" value="CIM2_3" style="width: 20%; font-size: 11px" ' +
                'class="curation-menu-2 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f">Мобил.Группа</button>';
            html += '<button type ="button" id="CIM2_3" value="CIM2_3_f" style="width: 13%; font-size: 11px" ' +
                'class="curation-menu-2 btn-link dropdown-toggle waves-effect waves-light btn-xs group_f" >Физ.</button>';

            break;
        default :
            // Nothing
            break;
    }
    // Если есть что показать - показываем
    if (html) {
        var limit = $('.curator_i[data-number="'+number_select+'"] [name="KERNEL_JOB_TIME_LIMIT"]').parents('.ticket_update_container');

        menu.innerHTML = html;
        menu.style.zIndex=1000;
        menu.style.top = defPosition(evt).y-70 + "px";
        menu.style.left = defPosition(evt).x-70 + "px";
        menu.style.display = "";
    }
    // Блокируем всплывание стандартного браузерного меню
    return false;
}

// Закрываем контекстное при клике левой или правой кнопкой по документу
// Функция для добавления обработчиков событий
function addHandler(object, event, handler, useCapture) {
    if (object.addEventListener) {
        object.addEventListener(event, handler, useCapture ? useCapture : false);
    } else if (object.attachEvent) {
        object.attachEvent('on' + event, handler);
    } else alert("Add handler is not supported");
}
addHandler(document, "contextmenu", function() {

    number_select = $(this).parents('.curator_i').data('number'); // получить заявку, над которой щелкнули правой кнопкой мыши

    document.getElementById("contextMenuId").style.display = "none";
});
addHandler(document, "click", function() {

    document.getElementById("contextMenuId").style.display = "none";
});

// если нажата правая кнопка мыши в контейнере с заявками
$('body').on('mousedown','.tb_container',function(e, t, eOpts){
    /**
     * Определяет нажатие правой кнопки в контейнере с заявкой
     *
     */
    if (e.button==2){
        $('.curator_i[data-number="'+number_select+'"] [name="LV"]').parents('.tb_container').children('.ticket_body').click();
       // GroupToggle.forEach( function (a) { a.toggle(false)}) // отжать все кнопочки мобильной группы
        //обнуление используемых переменных
        id_button_mg='';
        cim = '';
        pfys = '';
        sit = '';
        elem_sit= '';
        number_select='';
        //---------
        number_select = $(this).parents('.curator_i').data('number'); // получить заявку, над которой щелкнули правой кнопкой мыши
        console.log('Номер выбранной заявки: '+ number_select);
    //    Ext.getCmp('confurd').hide(); // спрятать кнопку подвтерждения
      //  Ext.getCmp('sit_raspred').hide(); // спрятать список ввода сита
     //   Ext.getCmp('sit_raspred').setValue(''); // очистить значение списка ввода сита
   //     Ext.getCmp('times_raspred').setValue('10'); // значение времени по умолчанию
      //  menu_for_raspred.setTitle('Распределение '+number_select+', от бычного к критичному; ESC - отмена') // вывести номер заявки в заголовок
     //   menu_for_raspred.showAt(e.clientX-60,e.clientY-50); // координаты, где вывести окно
        $('.curator_i[data-number="'+number_select+'"] [name="LV"]').parents('.tb_container').children('.ticket_body').click();
    }

});