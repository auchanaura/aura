context_height = localStorage.getItem('context_height');
if (typeof(context_height) != 'undefined' && context_height != "") {
    console.log(1);
    $(".event_body").css("height", 'calc(' + (100 - context_height) + 'vh - 160px)');
    $(".ticket_eml_body").css("height", context_height + 'vh');
}
$(function () {
    $(".ticket_eml_body").resizable({
        handles: "s",
        alsoResize: '.event_body',
        resize: function (e, ui) {
            height = $(".ticket_eml_body").height() + 4;
            if (height < 110) {
                height = 110;
            }
            localStorage.setItem('context_height', height * 100 / window.innerHeight);
            $(".event_body").css({
                height: 'calc(' + (100 - height * 100 / window.innerHeight) + 'vh - 160px)',
                width: '100%'
            });
            $(".ticket_eml_body").css({height: height * 100 / window.innerHeight + 'vh', width: '100%'});
        }
    });
});
if (localStorage.getItem('request_details') == 1) {
    $(document).ready(function () {
        $('.request_details').click();
    });
}
$('.request_details').click(function () {
    hidden = 0;
    if ($(this).hasClass('collapsed')) {
        hidden = 1;
    }
    localStorage.setItem('request_details', hidden);
});
filter = {};
filter.numbers = ticket_list;
filter.params = {'NUMBER': ''};
filter.sortBy = 'NUMBER';
filter.orderBy = 'ASC';
$(document).ready(function () {
    $('.filter_input .filter_page_input').change(function () {
        field = $(this).parents('.filter_input').data('field');
        result = $(this).val();
        if (result == null) {
            result = '';
        }
        filter.params[field] = result;
        ApplyFilter();
    });

    $('.search_page_sort').click(function () {
        $('.search_page_sort.active').removeClass('active');
        $(this).addClass('active');
        filter.sortBy = $(this).parents('.filter_input').data('field');
        filter.orderBy = $(this).data('order');
        ApplyFilter();
    });

    $('.filter_container').click(function () {
        $('.filter_body').toggle();
    });
});

function ApplyFilter() {
    filter.numbers = [];
    $.each(ticket_list, function (key, value) {
        i = 0;
        j = 0;
        $.each(filter.params, function (key2, value2) {
            j++;
            if (Array.isArray(value2)) {
                $.each(value2, function (key3, value3) {
                    if (Tickets[value][key2].indexOf(value3) != -1) {
                        i++;
                        return false;
                    }
                });
            } else {
                if (Tickets[value][key2].indexOf(value2) != -1) {
                    i++;
                }
            }
        });
        if (i == j) {
            filter.numbers.push(value);
        }
    });
    filter.numbers.sort(function (a, b) {
        if (filter.sortBy == 'REAL_SITE' && Tickets[a][filter.sortBy] == '-') {
            a = Tickets[a]['USER_SITE'].toLowerCase();
        } else {
            a = Tickets[a][filter.sortBy].toLowerCase();
        }
        if (filter.sortBy == 'REAL_SITE' && Tickets[b][filter.sortBy] == '-') {
            b = Tickets[b]['USER_SITE'].toLowerCase();
        } else {
            b = Tickets[b][filter.sortBy].toLowerCase();
        }
        if (filter.orderBy == 'ASC') {
            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if (b < a) {
                return -1;
            } else if (b > a) {
                return 1;
            } else {
                return 0;
            }
        }
    });
    $('.curator_i').each(function () {
        if ($.inArray($(this).data('number'), filter.numbers) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    filtered_ticket_list = filter.numbers;
    render_ticket_list(0);
    //$.each(filter.numbers, function (key, value) {
    //    $('.curator_i[data-number="' + value + '"]').appendTo('.tickets_container');
    //});
    $('.filter_container .label').html(filter.numbers.length);
}
$(document).ready(function () {
    $('body').on('change','.ticket_update_options',function(){
        array = [];
        //$.each(changed_tickets,function(key,value){
        ar_number = $(this).parents('.curator_i').data('number');
        array.push([Tickets[ar_number], old_Tickets[ar_number]]);
        //});
        array = JSON.stringify(array);
		console.log(array);
        $.ajax({
            method: "POST",
            url: "/ajax/sendCurationSpool",
            data: {data: array}
        }).done(function (data) {
            $.Notification.notify('success', 'top center', 'Заявка №' + ar_number + ' распределена!', 'Распределение заявки успешно подтверждено.')
            //setTimeout(function(){window.location.replace('/curation?mode=1');}, 1500);
            old_Tickets[ar_number].LV = Tickets[ar_number].LV;
            old_Tickets[ar_number].CIM = Tickets[ar_number].CIM;
            old_Tickets[ar_number].REAL_SITE = Tickets[ar_number].REAL_SITE;
            old_Tickets[ar_number].PHYSICAL = Tickets[ar_number].PHYSICAL;
            old_Tickets[ar_number].KERNEL_JOB_TIME_LIMIT = Tickets[ar_number].KERNEL_JOB_TIME_LIMIT;
            old_Tickets[ar_number].GROUPE = Tickets[ar_number].GROUPE;
            console.log(data);
        });
    });
});

