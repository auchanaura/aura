context_height = localStorage.getItem('context_height');
if (typeof(context_height) != 'undefined' && context_height != "") {
    console.log(1);
	$(".event_body").css("height",'calc('+(100 - context_height)+'vh - 160px)');
	$(".ticket_eml_body").css("height", context_height+'vh');
}
$( function() {
$( ".ticket_eml_body" ).resizable({
	handles: "s",
	alsoResize: '.event_body',
    resize: function(e, ui) {
        height = $(".ticket_eml_body").height() + 4;
        if (height < 110) {
        	height = 110;
        }
        localStorage.setItem('context_height', height*100/window.innerHeight);
        $(".event_body").css({height: 'calc('+(100 - height*100/window.innerHeight)+'vh - 160px)', width: '100%'});
        $(".ticket_eml_body").css({height: height*100/window.innerHeight+'vh', width: '100%'});
    }
});
} );
if (localStorage.getItem('request_details') == 1) {
    $(document).ready(function(){
        $('.request_details').click();
    });
}
$('.request_details').click(function(){
    hidden = 0;
    if ($(this).hasClass('collapsed')) {
        hidden = 1;
    }
    localStorage.setItem('request_details', hidden);
});
//При клике на тело тикета или иконку - открываем подробное содержимое
	$('body').on('click','.ticket_body', function() {
		number = $(this).parents('.curator_i').data('number');
		action = $(this).parents('.curator_i').data('action');
		show_event(number,action);
		select_ticket($(this).parents('.curator_i'));
	});
	$('body').on('click','.curator_i .action_icon_container', function() {
		number = $(this).parents('.curator_i').data('number');
		action = $(this).parents('.curator_i').data('action');
		show_event(number,action);
		select_ticket($(this).parents('.curator_i'));
	});

	function select_ticket(element) {
		i_number = element.data('number');
		action_selector = element.data('action');
		if (typeof(action_selector) != 'undefined') {
			action_selector = '[data-action="'+action_selector+'"]';
		} else {
			action_selector = '';
		}
		if (!$('.curator_i[data-number="'+i_number+'"]'+action_selector).hasClass('loaded')) {
			setTimeout(function(){select_ticket(element)}, 50);
			$("body").css("cursor", "progress");
		} else {
			if ($('.ticket_event_container[style*="display: block;"]').length > 0) {
				if ($('.ticket_event_container[style*="display: block;"]').parent().position().top < $('.curator_i[data-number="'+i_number+'"]').position().top) {
					$('body').scrollTop($('body').scrollTop() - ($('.ticket_event_container[style*="display: block;"]').height() + 17));
				}
			}
			$('.curator_i:not([data-number="'+i_number+'"]'+action_selector+') .ticket_event_container').hide();
			$('.list-group-item-info').removeClass('list-group-item-info');
			element.addClass('list-group-item-info');
			
			if (typeof(Tickets[i_number].htmlbody) != 'undefined' && Tickets[i_number].htmlbody != '') {
				$('.ticket_eml_body_container').html(Tickets[i_number].htmlbody);
			} else {
				$('.ticket_eml_body_container').html(Tickets[i_number].TICKET_DESCRIPTION);
			}
			$("body").css("cursor", "default");
		}
	}
	full_info = 0;
	$('body').on('mouseenter','.curator_i', function() {
		number = $(this).data('number');
		download_info(number,0,full_info);
	});

	function download_info(number,render = 0, full_info = 0, mode = 0) {
		url = 'EmlInfo';
		if (full_info == 1) {
			url = 'GetFullInfo';
		}
		if (typeof(Tickets[number].htmlbody) == 'undefined') {
			$.ajax({
				method: "POST",
				url: "/ajax/"+url,
				data: { ticket: number}
			}).done(function(data) {
				console.log(data);
				data = JSON.parse(data);
				Tickets[number].htmlbody = data.html;
				Tickets[number].attachments = data.attachments;
				if (full_info == 1) {
					$.each(data,function(key,value){
						Tickets[number][key] = value; 
					});
				}
				if (render) {
					$('.curator_i[data-number="'+number+'"] .ticket_body').click();
				}
				$('.curator_i[data-number="'+number+'"]').addClass('loaded');
			});
		}
		if (typeof(Tickets[number].history) == 'undefined') {
			$.ajax({
				method: "POST",
				url: "/ajax/ticketEvents",
				data: { ticket: number, mode: mode}
			}).done(function(data) {
				data = JSON.parse(data);
				Tickets[number].history = [];
				$.each(data,function(key,value) {
					Tickets[number].history[key] = value;
				});
			});
		}
	} 
	//При двойном клике - раскрываем список действий
	$('body').on('dblclick','.curator_i', function() {
		number = $(this).data('number');
		show_detailed_info(number);
	});
	function show_detailed_info(number) {
		$('.curator_i[data-number="'+number+'"] .detailed_info').html($('.detailed_info_template').html());
		$('.curator_i[data-number="'+number+'"] .number').html('<span style="font-weight:bold;">Номер:</span> <span>'+Tickets[number].NUMBER+'</span>');
		$('.curator_i[data-number="'+number+'"] .status0').html('<span style="font-weight:bold;">Время рег.:</span> <span>'+Tickets[number].STATUS0+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_fio').html('<span style="font-weight:bold;">Заявитель:</span> <span>'+Tickets[number].USER_FIO+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_site').html('<span style="font-weight:bold;">Сайт:</span> <span>'+Tickets[number].USER_SITE+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_login').html('<span style="font-weight:bold;">Логин:</span> <span>'+Tickets[number].USER_LOGIN+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_mail').html('<span style="font-weight:bold;">Емейл:</span> <span>'+Tickets[number].USER_MAIL+'</span>');
		$('.curator_i[data-number="'+number+'"] .user_post').html('<span style="font-weight:bold;">Должность:</span> <span>'+Tickets[number].USER_POST+'</span>');
		$('.curator_i[data-number="'+number+'"] .event_number').html('<span style="font-weight:bold;">Номер ивента:</span> <span>'+Tickets[number].NUMBER_EVENT+'</span>');
		$('.curator_i[data-number="'+number+'"] .beeline_number').html('<span style="font-weight:bold;">Номер оутсорс:</span> <span>'+Tickets[number].NUMBER_BEELINE+'</span>');
		if (Tickets[number].STATUS_JOB==2) {$('.curator_i[data-number="'+number+'"] .worker').html('<span style="font-weight:bold;">Исполняется:</span> <span>'+Tickets[number].WORKER+'</span>');}
		else { $('.curator_i[data-number="'+number+'"] .worker').html('');}
		if ($('.curator_i[data-number="'+number+'"] .problem_list').data('blocked') == 1) {
			$('.curator_i[data-number="'+number+'"] .problem_list').html('<span style="font-weight:bold;" class="info_name">Тема:</span><span class="info_input"><select class="form-control problems_list" id="problems_list" name="problems_list" placeholder="Задание"></select></span>');
			$(document).ready(function() {
				$('.curator_i[data-number="'+number+'"] .problems_list').select2();
			});
			$('.curator_i[data-number="'+number+'"] .problems_list').select2({
				placeholder: 'Выберите тип проблемы',
				data: problem_data,
				formatSelection: function(item) {
					return 'test';
				},
				formatResult: function(item) {
					return item.text;
				},
				escapeMarkup: function(markup) {
					return markup;
				},
				templateResult: formatResult,
				theme: "bootstrap",
			});
		} else {
			$('.curator_i[data-number="'+number+'"] .problem_list').html('<span style="font-weight:bold;" class="info_name">Тема: </span><span> '+Tickets[number].PROBLEM+'</span>');
		}
		$('.curator_i[data-number="'+number+'"] .problems_list').val(Tickets[number].PROBLEM_ID).trigger('change');
		$('.curator_i[data-number="'+number+'"] .attachments').html('<span style="font-weight:bold;">Вложения:</span>  '+Tickets[number].attachments);
		
		$('.curator_i:not([data-number="'+number+'"]) .detailed_info').hide();
		$('.curator_i[data-number="'+number+'"] .detailed_info').toggle();
	}

	function show_event(number,action = 0) {
		if (action != 0) {
			action_selector = '[data-action="'+action+'"]';
		} else {
			action_selector = '';
		}
		//$('.curator_i:not([data-number="'+number+'"]'+action_selector+') .ticket_event_container').hide();
		//$('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').toggle();

		//if ($('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').html() == '') {
		$('.event_body_container').html('');
		$.each(Tickets[number].history,function(key,value){
			event($('.event_body_container'),value);
		});
		//}
		/*if (typeof(action) != 'undefined' && action != 0) {
			action = $('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container .list-group-item[data-action="'+action+'"]')
			margin = $('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container .list-group-item').length;
			action.addClass('list-group-item-success');
			scroll = action.position().top;
			$('.curator_i[data-number="'+number+'"]'+action_selector+' .ticket_event_container').scrollTop(scroll);
		}*/
		//$('body').scrollTop($('.curator_i[data-number="'+number+'"]').position().top + $('.list-group').position().top);
		$('.event_body_container .list-group-item-text').each(function() {
			if ($(this).height() > 48) {
				$(this).addClass('collapsed_text');
			}
		});	
	}

	//При загрузке страницы - выбирает первый тикет для подробного отображения и инициируем поля распределения
	$(document).ready(function(){
		number = $('.curator_i').first().data('number');
		download_info(number,1,1);
	});

	$('body').on('click','.manage_list_button',function(){
		number = $(this).parents('.curator_i').data('number');
		template = $('.manage_list_template').html();
		if (typeof(template) != 'undefined') {
			if (Tickets[number].REAL_SITE != '-' || Tickets[number].REAL_SITE != '' || typeof(Tickets[number].REAL_SITE) != 'undefined') {
				template = template.split('%USER_SITE%').join(Tickets[number].REAL_SITE);
			} else {
				template = template.split('%USER_SITE%').join(Tickets[number].USER_SITE);
			}
			$(this).parent().find('.manage_list').html(template);
		}
		if (!$(this).parents('.curator_i').find('.ticket_update_options_select2').hasClass('_updated')) {
			$(this).parents('.curator_i').find('.ticket_update_options_select2').append($('.real_sites_template').html()).addClass('_updated');
		}
	});
	//Распределение заявки
	$('body').on('click','.curation_update_select',function(){
		number = $(this).parents('.curator_i').data('number');
		if (typeof($(this).data('cim')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="CIM"]').val($(this).data('cim'));
			Tickets[number].CIM = $(this).data('cim');
		}
		if (typeof($(this).data('min')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="KERNEL_JOB_TIME_LIMIT"]').val($(this).data('min'));
			Tickets[number].KERNEL_JOB_TIME_LIMIT = $(this).data('min');
		}
		if (typeof($(this).data('group')) != 'undefined') {
			$(this).parents('.curator_i').find('.ticket_update_options[name="LV"]').val($(this).data('group'));
			Tickets[number].LV = $(this).data('group');
			Tickets[number].GROUPE = real_sites[parseInt(String(Tickets[number].LV).replace(/\D+/g,""))];
		}
		$(this).parents('.curator_i').find('.ticket_update_options[name="KERNEL_JOB_TIME_LIMIT"]').change();
		if (Tickets[number].LV != 0 && Tickets[number].KERNEL_JOB_TIME_LIMIT != 0) {
			$(this).parents('.curator_i').addClass('list-group-item-success');
		}
	});

	$('.ticket_update_options').change(function(){
		number = $(this).parents('.curator_i').data('number');
		if ($(this).attr('name') == 'PHYSICAL') {
			Tickets[number][$(this).attr('name')] = $('.curator_i[data-number="'+number+'"] input[name="PHYSICAL"]:checked').val();
			if (typeof(Tickets[number][$(this).attr('name')]) == 'undefined') {
				Tickets[number][$(this).attr('name')] = 0;
			}
		} else {
			Tickets[number][$(this).attr('name')] = $(this).val();
		}
		if ($(this).attr('name') == 'LV') {
			Tickets[number].GROUPE = real_sites[parseInt(String(Tickets[number].LV).replace(/\D+/g,""))];
		}
		if (Tickets[number].LV != 0 && Tickets[number].KERNEL_JOB_TIME_LIMIT != 0) {
			$(this).parents('.curator_i').addClass('list-group-item-success');
		}
	});

	$('body').on('mousedown','.ticket_update_options_select2',function(){
		if (!$(this).hasClass('_updated')) {
			$(this).append($('.real_sites_template').html());
			$(this).addClass('_updated');
		}
	});

	$('body').on('click','.collapsed_text',function(){
		$(this).removeClass('collapsed_text').addClass('uncollapsed_text');
	});
	$('body').on('click','.uncollapsed_text',function(){
		if (window.getSelection().toString() == "") {
			$(this).removeClass('uncollapsed_text').addClass('collapsed_text');
		}
	});
full_info = 1;
