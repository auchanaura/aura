<?php
/**
 * Created by PhpStorm.
 * User: g.lebedev@auchan.ru
 * Date: 018 18.02.17
 * Time: 15:05
 *
 * Консольная утилита для сборки всех js и css ресурсов
 *
 *
 */
require_once 'vendor/autoload.php';

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\AssetManager;
use Assetic\AssetWriter;
use Aura\Core\Utils;



//Подключаем файл с массивом путей к ассетам проекта
require_once 'app/Aura/Configs/project.conf.php';
require_once 'app/Aura/Configs/assets.conf.php';

// Создаем менеджер всех ассетов
$auraAssets = new AssetManager();

//На основе массива $Assets создаем AssetCollection'ы
// и добавляем в AssetManager

foreach ($Assets as $typeOfAssets => $pathToAssetArray) {

    // определяем расширение итогового файла
    $extension = Utils::isinStr($typeOfAssets, 'css') ? 'css' : 'js';
    foreach ($pathToAssetArray as $path) {
        ${$typeOfAssets . 'FileAsset'}[] = new FileAsset($path);
    }
    ${$typeOfAssets} = new AssetCollection(${$typeOfAssets . 'FileAsset'});
    ${$typeOfAssets}->setTargetPath($extension . '/' . $typeOfAssets . '.' . $extension);
    $auraAssets->set($typeOfAssets, ${$typeOfAssets});
}

$assetWriter = new AssetWriter(AURA_PUBLIC_PATH);
$assetWriter->writeManagerAssets($auraAssets);
