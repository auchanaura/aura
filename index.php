<?php

// Автозагрузка классов проекта по стандарту PSR-4
require_once 'vendor/autoload.php';

//Подключение конфигурации проекта

require_once 'app/Aura/Configs/project.conf.php';
require_once AURA_BASE_PATH . 'app/Aura/Configs/variables.conf.php';


//Библиотеки не из composer
require_once AURA_BASE_PATH . 'libs/adLDAP/adLDAP.php';
require_once AURA_BASE_PATH . 'libs/adLDAP/adLDAP2.php';
require_once AURA_BASE_PATH . 'libs/aes/Aes.php';




// Запускаем маршрутизацию


\Aura\Core\Controllers::routeRun();





// Старая система запуска проекта. Сохранено для истории - TEST
/*if (!empty($_GET['password'])) {
	$info = explode(';',Aes::decrypt($_GET['password'],COOKIE_PASS));
	$ad = 1;
    if (preg_match('#^(?i)a\d\d\d\d\d\d\d\d\d#', $info[0])) {
    	$ad = 2;
    }
    $last_user_name = Aes::decrypt($_COOKIE['username'],COOKIE_PASS);
	if (empty($last_user_name) || $last_user_name == $info[0]) {
		Core::saveUserInfo($info[0],2,[0],$ad);
		header("Location:".explode('?',$_SERVER['REQUEST_URI'])[0].'?number='.$info[1]); /* Redirect browser
		//echo "<meta http-equiv='refresh' content='0; URL=".explode('?',$_SERVER['REQUEST_URI'])[0].'?number='.$info[1]."' />";
		exit();
	} else {
		require_once __DIR__ . './libs/PHPMailer/PHPMailerAutoload.php';
        $ip = $_SERVER['REMOTE_ADDR'];

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = '146.240.32.141';
    $mail->SMTPAuth = false;
    $mail->Port = 25;
    $mail->CharSet = "UTF-8";
    $mail->setFrom('it@auchan.ru', 'Центр Поддержки IT');
    $mail->addAddress('d.klyuev@auchan.ru', 'Центр Поддержки IT');
    $mail->addReplyTo('it@auchan.ru', 'Центр Поддержки IT');
    $mail->Body    = $last_user_name . ', ' . $ip. ', tried as ' . $info[0];
    $mail->Subject = 'Авторизация в AURA';
    $mail->send();
    exit();
}
}*/


/*if (empty($url)) {
	$url = $_SERVER['PATH_INFO'];
	$request_uri = explode('/',$_SERVER['REQUEST_URI']);
	if ($request_uri[1] == 'views') {
		$request_uri[3] = explode('.php',$request_uri[3])[0];
		$url = '/'.$request_uri[2].'/'.$request_uri[3];
	}
}
$url = explode('/',$url);
$params = $_GET;

//Обрабатываем запрос и возвращаем запрошенную страницу

include('/controllers/'.$url[1].'.php');
$action = new $url[1];
$action->$url[2]($params);*/
