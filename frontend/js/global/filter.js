filter = {};
filter.numbers = ticket_list;
filter.params = {'NUMBER': ''};
filter.sortBy = 'NUMBER';
filter.orderBy = 'ASC';
$(document).ready(function () {
    $('.filter_input .filter_page_input').change(function () {
        field = $(this).parents('.filter_input').data('field');
        result = $(this).val();
        if (result == null) {
            result = '';
        }
        filter.params[field] = result;
        ApplyFilter();
    });

    $('.search_page_sort').click(function () {
        $('.search_page_sort.active').removeClass('active');
        $(this).addClass('active');
        filter.sortBy = $(this).parents('.filter_input').data('field');
        filter.orderBy = $(this).data('order');
        ApplyFilter();
    });

    $('.filter_container').click(function () {
        $('.filter_body').toggle();
    });
});

function ApplyFilter() {
    filter.numbers = [];
    $.each(ticket_list, function (key, value) {
        i = 0;
        j = 0;
        $.each(filter.params, function (key2, value2) {
            j++;
            if (Array.isArray(value2)) {
                $.each(value2, function (key3, value3) {
                    if (Tickets[value][key2].indexOf(value3) != -1) {
                        i++;
                        return false;
                    }
                });
            } else {
                if (Tickets[value][key2].indexOf(value2) != -1) {
                    i++;
                }
            }
        });
        if (i == j) {
            filter.numbers.push(value);
        }
    });
    filter.numbers.sort(function (a, b) {
        if (filter.sortBy == 'REAL_SITE' && Tickets[a][filter.sortBy] == '-') {
            a = Tickets[a]['USER_SITE'].toLowerCase();
        } else {
            a = Tickets[a][filter.sortBy].toLowerCase();
        }
        if (filter.sortBy == 'REAL_SITE' && Tickets[b][filter.sortBy] == '-') {
            b = Tickets[b]['USER_SITE'].toLowerCase();
        } else {
            b = Tickets[b][filter.sortBy].toLowerCase();
        }
        if (filter.orderBy == 'ASC') {
            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if (b < a) {
                return -1;
            } else if (b > a) {
                return 1;
            } else {
                return 0;
            }
        }
    });
    $('.curator_i').each(function () {
        if ($.inArray($(this).data('number'), filter.numbers) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    filtered_ticket_list = filter.numbers;
    render_ticket_list(0);
    //$.each(filter.numbers, function (key, value) {
    //    $('.curator_i[data-number="' + value + '"]').appendTo('.tickets_container');
    //});
    $('.filter_container .label').html(filter.numbers.length);
}