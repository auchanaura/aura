/**
* Создание и наполнение хранилища под названия ситов 
* @type Array|Object
*/
var sits = []; // переменная под все магазины
// находим все объекты 'optgroup option' -- названиния ситов -- и ложим их в перемнную 
sits = $('optgroup option').each(function(){sits.push($('optgroup option').next().val())}); 
var sites=sits.toArray(); // переделываем данную переменную с названиями в массив 
var data = []; // массив для хранилища ситов  
 // заполняем хранилище только названием сита (отбрасываем все лишнее)
for (var i = 0; i < sites.length; i++) {
	data.push(sites[i].value);
}
// создаем модель под поля хранилища
Ext.define('JobA', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'sit'}
            ]
        });
// создаем хранилище, в котором содержится массив с названиями магазинов
var sit_store = Ext.create('Ext.data.ArrayStore', {
                model: 'JobA',
                data: data,
                expandData: true // this is tied to ArrayStore#loadData only
});
// наполняем данными наше хранилище
sit_store.loadData(data); 

/**
* Создание и наполнение хранилища под логин 
* @type Array|Object
*/
var LOGIN_IN_AURA = [];





