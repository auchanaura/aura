
/**
* Создание меню для распреда
* @type Ext.menu.Menu
*/	
// переменные для меню распреда
var number_select = '';
var sit = '';
var id_button_mg='';
var cim = '';
var pfys = '';
var elem_sit= '';

// кнопки под распределение с приоритетом 0			 
   menu_for_raspred_0 = new Ext.menu.Menu({
	id: 'menu_for_raspred_0',	
	width: 160,
	glyph: 72,   
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM0_1',							 
                        text: 'Центр Поддержки',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Центр Поддержки',
                        handler: function(){
                           // console.log('вызов функции с параметрами CIM0_1'); 
                           set_raspred(0,1,pfys,number_select);
                           
                        }
                    },
                    { 
                        id: 'CIM0_4',							 
                        text: 'Контакный Центр',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Контакный Центр',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_4');
                            set_raspred(0,4,pfys,number_select);
                           
                        }
                    },
                    { 
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-0',
                        items:[
                            { 
                                id: 'CIM0_3',
                                text: 'Мобильная группа',
                                tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Мобильную группу',
                                xtype: 'button',
                                style: "background: #ffffff" ,
                                enableToggle: true,
                                toggleGroup: 'group_f',
                                height: 32,
                                handler: function(){
                                    //console.log('вызов функции с параметрами CIM0_3');
                                    Ext.getCmp('confurd').show();
                                    Ext.getCmp('sit_raspred').show();
									
                                }
                            },
                            {
                                id: 'CIM0_3_f',
                                xtype: 'button',
                                tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом в Мобильную группу с физической поломкой',
                                boxLabel: 'Физ',
                                cls: 'curation-menu-0',
                                name: 'phisical',
                                text: 'Физич.',
                                enableToggle: true,
                                height: 32,
                                toggleGroup: 'group_f',
                                handler: function(){
                                   // console.log('вызов функции с параметрами CIM0_3_f');
                                    Ext.getCmp('confurd').show();
                                    Ext.getCmp('sit_raspred').show();
									
									
                                }
                            }
                        ]
                    },
                    '-',
                    { 
                        id: 'CIM0_5',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом на 2 уровень поддержки АТАК',
                        cls: 'curation-menu-0',
                        text: 'АТАК',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_5');
                            set_raspred(0,5,pfys,number_select);
                           
                        }
                    },
                    { 
                        id: 'CIM0_6',
                        cls: 'curation-menu-0',
                        tooltip: 'Распределить заявку '+number_select+ ' с нормальным приоритетом на 2 уровень поддержки DataQuality',
                        text: 'DataQuality',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM0_6');
                            set_raspred(0,1,pfys,number_select);
                            
                        }
                    }
        ]
		 
});	

// кнопки под распределение с приоритетом 1
menu_for_raspred_1 = new Ext.menu.Menu({
	id: 'menu_for_raspred_1',
	text: 'Высокий',
	xtype: 'menu',
	glyph: 72,    
	width: 160,
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM1_1',							 
                        text: 'Центр Поддержки',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Центр Поддержки',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_1');
                            set_raspred(1,1,pfys,number_select);

                        }
                    },
                    { 
                        id: 'CIM1_4',
                        text: 'Контакный Центр',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Контакный Центр',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_4');
                            set_raspred(1,4,pfys,number_select);
                        }
                    },
                    { 
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-1',
                        items:[
                                {
                                    id: 'CIM1_3',
                                    text: 'Мобильная группа',
                                    tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Мобильную группу',
                                    xtype: 'button',
                                    style: "background: #ffde92" ,
                                    enableToggle: true,
                                    toggleGroup: 'group_f',
                                    name: 'group_f',
                                    height: 32,
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM1_3');
                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                },
                                {
                                    id: 'CIM1_3_f',
                                    xtype: 'button',
                                    boxLabel: 'Физ',
                                    tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом в Мобильную группу с физической поломкой',
                                    cls: 'curation-menu-1',
                                    name: 'group_f',
                                    name: 'phisical',
                                    text: 'Физич.',
                                    enableToggle: true,
                                    height: 32,
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                            //console.log('вызов функции с параметрами CIM1_3_f');
                                            Ext.getCmp('confurd').show();
                                            Ext.getCmp('sit_raspred').show();

                                    }
                            }
                        ]
                    },
                    '-',
                    { 
                        id: 'CIM1_5',
                        text: 'АТАК',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом на 2 уровень поддержки АТАК',
                        cls: 'curation-menu-1',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM10_5');
                            set_raspred(1,5,pfys,number_select);
                        }
                    },
                    { 
                        id: 'CIM1_6',
                        cls: 'curation-menu-1',
                        tooltip: 'Распределить заявку '+number_select+ ' с высоким приоритетом на 2 уровень поддержки DataQuality',
                        text: 'DataQuality',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM1_6');
                            set_raspred(1,1,pfys,number_select);
                        }
                    }
        ]	 
});	
// кнопки под распределение с приоритетом 2
menu_for_raspred_2 = new Ext.menu.Menu({
	id: 'menu_for_raspred_2',
	text: 'Критичный',
	xtype: 'menu',
	width: 160,
	glyph: 72,
        cls: 'curation-menu-2',
	floating: false,  // ждет, пока не будет нажата кнопка
        items: [
                    { 
                        id: 'CIM2_1',							 
                        text: 'Центр Поддержки',
                        tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Центр Поддержки',
                        cls: 'curation-menu-2',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM2_1');
                            set_raspred(2,1,pfys,number_select);

                        }
                    },
                    { 
                        id: 'CIM2_4',
                        text: 'Контакный Центр',
                        tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Контакный Центр',
                        cls: 'curation-menu-2',
                        handler: function(){
                            //console.log('вызов функции с параметрами CIM2_4');
                            set_raspred(2,4,pfys,number_select);								 

                          }
                    },
                    { 
                        xtype: 'container',
			layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        html: '<div style="border-left:1px solid #000;height:100%; display: inline-block;"></div>',
                        cls: 'curation-menu-2',
                        items:[
                                {
                                    id: 'CIM2_3',
                                    style: "background: #ffa292" ,
                                    text: 'Мобильная группа',
                                    cls: 'curation-menu-2',
                                    tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Мобильную группу',
                                    xtype: 'button',
                                    enableToggle: true,
                                    height: 32,
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM2_3');
                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                },
                                {
                                    id: 'CIM2_3_f',
                                    xtype: 'button',
                                    boxLabel: 'Физ',
                                    tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом в Мобильную группу с физической поломкой',
                                    cls: 'curation-menu-2',
                                    name: 'phisical',
                                    enableToggle: true,
                                    height: 32,
                                    text: 'Физич.',
                                    toggleGroup: 'group_f',
                                    handler: function(){
                                        //console.log('вызов функции с параметрами CIM2_3_f');

                                        Ext.getCmp('confurd').show();
                                        Ext.getCmp('sit_raspred').show();

                                    }
                                }
                           ]
                        },
                        '-',
                        { 
                            id: 'CIM2_5',
                            text: 'АТАК',
                            tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом на 2 уровень поддержки АТАК',
                            cls: 'curation-menu-2',
                            handler: function(){
                                //console.log('вызов функции с параметрами CIM2_5');
                                set_raspred(2,5,pfys,number_select);
                            }
                        },
                        { 
                            id: 'CIM2_6',
                            text: 'DataQuality',
                            tooltip: 'Распределить заявку '+number_select+ ' с критичным приоритетом на 2 уровень поддержки DataQuality',
                            cls: 'curation-menu-2',
                            handler: function(){
                               // console.log('вызов функции с параметрами CIM2_6');
                                set_raspred(2,1,pfys,number_select);
                            }
                        }
        ]
});

/**
 * хранилище времени для распределения
 * @type |||
 */
var times = Ext.create('Ext.data.Store', {
    fields: ['times_id', 'times_rus'],
    data : [
            {"times_id":"5", "times_rus":"Пять минут"},
            {"times_id":"8", "times_rus":"Восемь минут"},
            {"times_id":"10", "times_rus":"Десять минут"},
            {"times_id":"15", "times_rus":"Пятнадцать минут"}		
    ]
});

        /**
         * два новых метода для комбо бокса 
         */
Ext.define( "Ext.form.field.ComboBoxOverride", {
    "override": "Ext.form.field.ComboBox",
// берет след элемент хранилища
    "selectNextItem": function( suppressEvent ) {
        var store       = this.getStore();
        var value       = this.getValue();
        var index       = store.find( this.valueField, value );
        var next_index  = index + 1;
        var next_record = store.getAt( next_index );
        if( next_record ) {
            this.select( next_record );

            if( ! suppressEvent ) {
                this.fireEvent( 'select', this, [ next_record ] );
            }
        }
    },
// берет прошлый элемент хранилища
    "selectPrevItem": function( suppressEvent ) {
        var store       = this.getStore();
        var value       = this.getValue();
        var index       = store.find( this.valueField, value );
        var prev_index  = index - 1;
        var prev_record = store.getAt( prev_index );
        if( prev_record ) {
            this.select( prev_record );

            if( ! suppressEvent ) {
                this.fireEvent( 'select', this, [ prev_record ] );
            }
        }
    }
});

// функция распределения
function set_raspred(CIM,LV,pfys,number_select ){
            if (LV==3) { // если морбильная группа
            //получение элемента мобильной группы, который был нажат 
		GroupToggle.forEach( function (a) { if (a.pressed==true) { id_button_mg=a.id;} });
                // заполнение переменных данными	
                    CIM = id_button_mg.slice(3,4);
                    pfys = id_button_mg.slice(7,8);
                    sit=Ext.getCmp('sit_raspred').rawValue;
            // заполнение распределенными данными нужной заявки поля с боку (визуализация) 
                // если нажата кнопка "физич", 
                if ( pfys=='f' ) {                   
                    $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').attr("checked", "checked");
                    Tickets[number_select].PHYSICAL=1;
                } else {
                    $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').removeAttr('checked');
                    Tickets[number_select].PHYSICAL=0;
                } 
                // --- элемент LV
                Tickets[number_select].LV=LV;
                Tickets[number_select].REAL_SITE=sit;
                    // удаляем выбранный элемент (название сита), если заявка уже была один раз распределена
                $('.curator_i[data-number="'+number_select+'"] [name="LV"] [value='+sit.slice(0,3)+']').remove();
                    // добавляем элемент (название сита), к списку элемента на морде 
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').append($('<option value="'+sit.slice(0,3)+'">'+sit+'</option>'));
                    // выбираем добавленный элемент
                $('.curator_i[data-number="'+number_select+'"] [name="LV"] [value='+sit.slice(0,3)+']').attr("selected", "selected");
                // --- элемент KERNEL_JOB_TIME_LIMIT 
                Tickets[number_select].KERNEL_JOB_TIME_LIMIT=Ext.getCmp('times_raspred').getValue();
                $('.curator_i[data-number="'+number_select+'"] [name="KERNEL_JOB_TIME_LIMIT"]').val(Ext.getCmp('times_raspred').getValue());
                 
                Tickets[number_select].CIM=CIM;
                CIM=CIM+''; //преобразование в строку
                // --- элемент CIM
                $('.curator_i[data-number="'+number_select+'"] [name="CIM"]').val(CIM);               
            //  инициализация применения изменений в базу           
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').change();		
            }
            else 
            { // применение параметров для остальных
                // снятие галочки физ нажатие
                $('.curator_i[data-number="'+number_select+'"] [name="PHYSICAL"]').removeAttr('checked');
                Tickets[number_select].PHYSICAL=0;
                // LV
                Tickets[number_select].LV=LV;
                LV=LV+''; //преобразование в строку
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').val(LV);
                // CIM
                Tickets[number_select].CIM=CIM;
                CIM=CIM+''; //преобразование в строку
                $('.curator_i[data-number="'+number_select+'"] [name="CIM"]').val(CIM);
                // limit
                $('.curator_i[data-number="'+number_select+'"] [name="KERNEL_JOB_TIME_LIMIT"]').val(Ext.getCmp('times_raspred').getValue());
                Tickets[number_select].KERNEL_JOB_TIME_LIMIT=Ext.getCmp('times_raspred').getValue();
                $('.curator_i[data-number="'+number_select+'"] [name="LV"]').change();                
            }
            console.log('Номер: '+number_select+';CIM:'+CIM+';LV:'+LV+';физ:'+pfys);
            menu_for_raspred.hide();
}

// переменная, в которую подвешивается функция-обработчик колесика
var wheel_handle = null;
			
// создание обработки собтия вращения колеса		
var mouse_wheel = function(event) {
        if (false == !!event) event = window.event;
        var direction = ((event.wheelDelta) ? event.wheelDelta/120 : event.detail/-3) || false;
        if (direction && !!wheel_handle && typeof wheel_handle == "function") {
                if (event.preventDefault) event.preventDefault();
                event.returnValue = false;
                wheel_handle(direction);
        }
}  

		/**
		*	окно с всеми элементами
		*/
menu_for_raspred = new Ext.Window({		
	id: 'menu_for_raspred',
	title: 'Распределение '+number_select+', от бычного к критичному',
	autoHeight: true,
        closeAction:"hide", //кнопочка на закрытие, которая прячит окно
	//closable: false,
	resizable: false, // возможность изменения размеров окна.
	draggable: true, // возможность перетаскивания окна.
	flex:1,
	autoScroll: true,
	hideHeaders: true,
	hideLabel: false,
	bodyStyle: 'padding:-13 -13 -13 -13', 
	layout:'vbox',
        
	items: [ 
		{
                    xtype: 'panel',
                    layout:'hbox',

                    bodyStyle: 'padding:-3 -3 -3 -3', 
                    items: [
                            menu_for_raspred_0,
                            menu_for_raspred_1,
                            menu_for_raspred_2
                    ]
		},
		{
                    xtype: 'panel',
                    layout:'hbox',			
                    bodyStyle: 'padding:-3 -3 -3 -3', 
                    items: [
                            { 
                                id: 'times_raspred', 
                                name: 'times_raspred', 
                                fieldLabel: 'Минуты',
                                labelWidth: 50,						
                                xtype: 'combobox', 
                                store: times, 
                                queryMode: 'local', 
                                displayField: 'times_rus', 
                                valueField: 'times_id', 
                                allowBlank: false,
                                editable: false,
                                multiSelect: false,
                                disabled: false,
                                hidden: false,
                                triggerAction: 'all',
                                value: "10",
                                style: "background: #e8e8e8",
                                selectOnFocus:true							
                            },
                            { 
                                id: 'sit_raspred', 
                                name: 'it_raspred', 
                                fieldLabel: 'Сит', 
                                xtype: 'combobox', 
                                labelWidth: 20,
                                store: sit_store, 
                                queryMode: 'local', 
                                displayField: 'sit', 
                                valueField: sit_store.data.items, 
                                allowBlank: false,
                                editable: true,
                                multiSelect: false,
                                disabled: false,
                                style: "background: #e8e8e8",
                                hidden: true,
                                triggerAction: 'all',
                                value: "",						
                                selectOnFocus:true,
                                listeners: {
                                        'change': function (combo, newval, odlval) {										
                                            var store = this.store;
                                            store.suspendEvents();
                                            store.clearFilter();
                                            store.resumeEvents();
                                            store.filter({
                                              property: 'sit',
                                              anyMatch: true,
                                              value   : this.getValue()
                                            });
                                        }
                                }
                            },
                            {
                                id: 'confurd',
                                xtype: 'button',
                                width: 65,
                                text: 'Отправить',									
                                handler: function(){
                                                // дублирования действий при нажатии enter											
                                                if (Ext.getCmp('sit_raspred').value!='') {
                                                      set_raspred(0,3,pfys,number_select);
                                                } else {
                                                        // иначе кинуть в поле для выбора сита
                                                        Ext.getCmp('sit_raspred').focus();
                                                }	
                                }
                            }

                    ]
		}
		
	],
	listeners: {
		
		afterRender: function() {
			var temp;
			document.body.onmouseover = document.body.onmouseout = handler;
                        function handler(event) {					
                                var str = event.target.id;
                                var el=str.slice(0,6);
                                if (event.type == 'mouseover' && event.target.id.search("CIM")!=-1) {
                                        temp=document.getElementById(el).style.background;
                                        document.getElementById(el).style.background = '#e8e8e8';
                                }	
                                if (event.type == 'mouseout' && event.target.id.search("CIM")!=-1) {
                                        document.getElementById(el).style.background = temp;
                                }

                        }
			
			// если вращение колеса было в искомом элементе, то обрабатываем, если нет, то функци иобработки не будет
			var set_handle = function(id, func) {
				document.getElementById(id).onmouseover = function() {
					wheel_handle = func;					
				}
				document.getElementById(id).onmouseout = function() {
					wheel_handle = null;
					handler = null;
				}
			}									
			
			 // распазнаем куда крутилось колесо  (значение переменной direction: "1" - в верх и "-1" вниз) 
             // и делаем соотвествующее изменение элемента комбо 
			var set_move_item = function(direction) {
				//console.log('значение '+ direction);
				if (direction==1) {Ext.getCmp('times_raspred').selectNextItem();}
				else {Ext.getCmp('times_raspred').selectPrevItem();}				
			}
			//ловля onmousewheel (события движения колесика мышки) для всего окна (объекта window)
			if (window.addEventListener) window.addEventListener("DOMMouseScroll", mouse_wheel, false);
			window.onmousewheel = document.onmousewheel = mouse_wheel;
			set_handle('menu_for_raspred', set_move_item);
			
		}
	} 
});	

		/**
		* переменная, которая содержит в себе все кнопки с эскалацией в мобильную группу
		*/
var GroupToggle = Ext.ComponentQuery.query('button[toggleGroup = group_f]');

// если нажата кнопка интер  
$('body').on('keypress',function(e){
            /**
            * Определяет нажатие правой кнопки на клавиатуре 
            * 
            */
    if (e.which == 13 && Ext.getCmp('sit_raspred').value!='') { 
		set_raspred(0,3,pfys,number_select);
	} else {
		// иначе кинуть в поле для выбора сита		
		 Ext.getCmp('sit_raspred').focus();		
	}
});

// если нажата клафиша esc при открытом окне распреда
$('body').on('keyup',function(e){
	if (e.which ==27 && menu_for_raspred.hidden==false) { menu_for_raspred.hide(); }
});

// если нажата правая кнопка мыши в контейнере с заявками
$('body').on('mousedown','.tb_container',function(e, t, eOpts){
            /**
            * Определяет нажатие правой кнопки в контейнере с заявкой 
            * 
            */
    if (e.button==2){
       $('.curator_i[data-number="'+number_select+'"] [name="LV"]').parents('.tb_container').children('.ticket_body').click();
        GroupToggle.forEach( function (a) { a.toggle(false)}) // отжать все кнопочки мобильной группы
                //обнуление используемых переменных
		id_button_mg='';
		cim = '';
		pfys = '';
		sit = '';
		elem_sit= '';
                number_select='';
                //---------
        number_select = $(this).parents('.curator_i').data('number'); // получить заявку, над которой щелкнули правой кнопкой мыши
	console.log('Номер выбранной заявки: '+ number_select);
        Ext.getCmp('confurd').hide(); // спрятать кнопку подвтерждения
        Ext.getCmp('sit_raspred').hide(); // спрятать список ввода сита
        Ext.getCmp('sit_raspred').setValue(''); // очистить значение списка ввода сита
        Ext.getCmp('times_raspred').setValue('10'); // значение времени по умолчанию
        menu_for_raspred.setTitle('Распределение '+number_select+', от бычного к критичному; ESC - отмена') // вывести номер заявки в заголовок
	menu_for_raspred.showAt(e.clientX-60,e.clientY-50); // координаты, где вывести окно
	
    }
    
});
