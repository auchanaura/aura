<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top {
        margin-top: 58px;
    }
</style>

<html>
<head>
    <meta charset=utf-8">
    <title>Загрузка расписания</title>
    <link rel="stylesheet" type="text/css" href="/libs/extjs/resources/css/ext-all-gray.css">
    <script type="text/javascript" src="/libs/extjs/bootstrap.js"></script>
</head>
</html>

<script>
    // переменные высоты и ширины
    var myWidth = '';
    var myHeight = '';
    Ext.require([
        'Ext.window.MessageBox',
        'Ext.tip.*'
    ]);


    // Создание массива для ComboBox'а MONTH (Месяцы)
    var monthStore = Ext.create('Ext.data.Store', {
        fields: ['month_id', 'month_rus'],
        data: [
            {"month_id": "1", "month_rus": "Январь"},
            {"month_id": "2", "month_rus": "Февраль"},
            {"month_id": "3", "month_rus": "Март"},
            {"month_id": "4", "month_rus": "Апрель"},
            {"month_id": "5", "month_rus": "Май"},
            {"month_id": "6", "month_rus": "Июнь"},
            {"month_id": "7", "month_rus": "Июль"},
            {"month_id": "8", "month_rus": "Август"},
            {"month_id": "9", "month_rus": "Сентябрь"},
            {"month_id": "10", "month_rus": "Октябрь"},
            {"month_id": "11", "month_rus": "Ноябрь"},
            {"month_id": "12", "month_rus": "Декабрь"}
        ]
    });


    //Создание формы загрузки
    var UprFieldSet = Ext.create('Ext.form.FormPanel', {
        title: ' ',
        hideHeaders: true,
        hideLabel: true,
        id: 'tabpan1',
        autoScroll: true,
        align: 'stretch',
        items: [{
            xtype: 'fieldset',
            collapsible: false,
            defaultType: 'textfield',
            align: 'stretch',
            defaults: {labelAlign: 'top', style: 'margin: 5 0px', width: 'auto'},
            layout: 'anchor',
            items: [
                {
                    id: 'year',
                    name: 'year',
                    fieldLabel: 'Год',
                    maxLength: '4',
                    enforceMaxLength: 'true',
                    labelStyle: 'white-space: nowrap;',
                    emptyText: 'Введите год XXXX',
                    allowBlank: false,
                    value: '2016'
                },

                {
                    id: 'month',
                    name: 'month',
                    fieldLabel: 'Месяц',
                    xtype: 'combobox',
                    store: monthStore,
                    emptyText: 'Расписание на месяц',
                    queryMode: 'local',
                    displayField: 'month_rus',
                    valueField: 'month_id',
                    allowBlank: false,
                    editable: false,
                    multiSelect: false,
                    listeners: {
                        'change': function (combo, newval, odlval) {
                            combo.value = combo.getRawValue();
                        }
                    }
                },


                {
                    xtype: 'button',
                    formBind: true,
                    disabled: false,
                    flex: 1,
                    width: 150,
                    text: 'Загрузка данных из EXCEL',
                    //icon: 'images/chart.png',
                    handler: function () {
                        load_window.show();

                    }
                }
                ,
                {
                    xtype: 'button',
                    formBind: true,
                    disabled: false,
                    flex: 1,
                    width: 150,
                    text: 'Отоброзить логи',
                    //icon: 'images/chart.png',
                    handler: function () {
                        console.log("отобразить логи за выбранные параметры");
                        // загрузка логов
                        LogDataStore.removeAll();
                        LogDataStore.proxy.extraParams['month'] = Ext.getCmp('month').valueModels[0].data['month_id'];
                        LogDataStore.load();

                    }
                }


            ]
        }]
    });

    // модель для отображения загруженного расписания
    Ext.define('Data', {
        extend: 'Ext.data.Model',

        fields: [
            'SUPERVIZOR',
            'FIO',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
            '24',
            '25',
            '26',
            '27',
            '28',
            '29',
            '30',
            '31'
        ]
    });

    // модель для отображения логов расписания
    Ext.define('LogData', {
        extend: 'Ext.data.Model',
        fields: [
            'DATE',
            'TOUR',
            'FIO',
            'CHANGE',
            'COMMENT',
            'WHO',
            'KOGDA'
        ]
    });
    // Хранилище данных для отчета
    var DataStore = Ext.create('Ext.data.Store', {
        model: 'Data',
        proxy: {
            type: 'ajax',
            url: 'getData.php?yers=' + Ext.getCmp('year').getValue(),
            reader: 'json'
        },
        autoLoad: false,
        listeners: {
            Load: function (store, records, options) {
                console.log(this.getTotalCount());
                if (this.getTotalCount() != 0) {
                    Ext.getCmp('send_in_sql').show();
                } else {
                    Ext.getCmp('send_in_sql').hide();
                }
            }
        }
    });
    // Хранилище логов
    var LogDataStore = Ext.create('Ext.data.Store', {
        model: 'LogData',
        proxy: {
            type: 'ajax',
            url: 'Log.php?yers=' + Ext.getCmp('year').getValue(),
            reader: 'json'
        },
        autoLoad: false,
        listeners: {
            Load: function (store, records, options) {
                console.log('Количество загруженных данных логов' + this.getTotalCount());
            }
        }
    });
    //Создание таблицы с данными
    var DataGrid = Ext.create('Ext.grid.Panel', {
        id: 'gridpanel',
        title: 'Просмотр загруженных данных',
        store: DataStore,
        autoScroll: true,
        enableColumnResize: true,
        Height: 300,
        features: [Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: 'Группа  {name} ({rows.length})',
            collapsible: 'true',
        })],
        //align: 'stretch',

        columns: [
            {text: 'Распред', dataIndex: 'SUPERVIZOR', width: 30, tpl: '{name}', hidden: true},
            {
                text: 'Del',
                xtype: 'actioncolumn', // специальный тип колонки в ExtJS для всяких кнопок
                width: 30,
                items: [ // массив кнопок.
                    {
                        icon: '../../img/action/delete.png', // иконка
                        tooltip: 'Удалить строку',              // подсказка
                        handler: function (grid, rowIndex, colIndex) { // обработчик нажатия кнопки
                            var rec = grid.getStore().getAt(rowIndex);  // определяю удаляемую запись по номеру строки
                            grid.store.remove(rec);                     // удаляю запись.
                        }
                    }
                ]
            },
            {
                text: 'ФИО', dataIndex: 'FIO', width: 180, tpl: '{name}',
                tooltip: 'Права на распред - двойной клик',
                renderer: function (value) {
                    if (DataStore.findRecord('FIO', value).data.SUPERVIZOR > 0) {
                        value = '<span style=" color:#ff0000;">' + value + '</span>';
                    } else {
                        value = '<span style="color:#00ff00;">' + value + '</span>';
                    }
                    return value;
                }
            },
            {text: '01', dataIndex: '1', width: 30, tpl: '{name}'},
            {text: '02', dataIndex: '2', width: 30, tpl: '{name}'},
            {text: '03', dataIndex: '3', width: 30, tpl: '{name}'},
            {text: '04', dataIndex: '4', width: 30, tpl: '{name}'},
            {text: '05', dataIndex: '5', width: 30, tpl: '{name}'},
            {text: '06', dataIndex: '6', width: 30, tpl: '{name}'},
            {text: '07', dataIndex: '7', width: 30, tpl: '{name}'},
            {text: '08', dataIndex: '8', width: 30, tpl: '{name}'},
            {text: '09', dataIndex: '9', width: 30, tpl: '{name}'},
            {text: '10', dataIndex: '10', width: 30, tpl: '{name}'},
            {text: '11', dataIndex: '11', width: 30, tpl: '{name}'},
            {text: '12', dataIndex: '12', width: 30, tpl: '{name}'},
            {text: '13', dataIndex: '13', width: 30, tpl: '{name}'},
            {text: '14', dataIndex: '14', width: 30, tpl: '{name}'},
            {text: '15', dataIndex: '15', width: 30, tpl: '{name}'},
            {text: '16', dataIndex: '16', width: 30, tpl: '{name}'},
            {text: '17', dataIndex: '17', width: 30, tpl: '{name}'},
            {text: '18', dataIndex: '18', width: 30, tpl: '{name}'},
            {text: '19', dataIndex: '19', width: 30, tpl: '{name}'},
            {text: '20', dataIndex: '20', width: 30, tpl: '{name}'},
            {text: '21', dataIndex: '21', width: 30, tpl: '{name}'},
            {text: '22', dataIndex: '22', width: 30, tpl: '{name}'},
            {text: '23', dataIndex: '23', width: 30, tpl: '{name}'},
            {text: '24', dataIndex: '24', width: 30, tpl: '{name}'},
            {text: '25', dataIndex: '25', width: 30, tpl: '{name}'},
            {text: '26', dataIndex: '26', width: 30, tpl: '{name}'},
            {text: '27', dataIndex: '27', width: 30, tpl: '{name}'},
            {text: '28', dataIndex: '28', width: 30, tpl: '{name}'},
            {text: '29', dataIndex: '29', width: 30, tpl: '{name}'},
            {text: '30', dataIndex: '30', width: 30, tpl: '{name}'},
            {text: '31', dataIndex: '31', width: 30, tpl: '{name}'},
        ],
        buttons: [
            {
                id: 'send_in_sql',
                text: 'Загурзить расписание в базу',
                hidden: true,
                handler: function () {
                    console.log('Загрузка а базу SQL');
                    Ext.Ajax.request({
                        url: 'Send_in_sql.php?month=' + Ext.getCmp('month').valueModels[0].data['month_id'] + '&yers=' + Ext.getCmp('year').getValue(),
                        jsonData: Ext.encode(Ext.pluck(DataStore.data.items, 'data')),
                        success: function (response, opts) {
                            console.log("успех " + response.responseText);
                            alert(response.responseText.slice(28, -2));
                        },
                        failure: function (response, opts) {
                            console.log("Провал " + response.responseText);
                            alert(response.responseText.slice(28, -2));
                        }
                    });

                }
            }
        ],
        listeners: {
            itemdblClick: function (model, record) {
                if (record) {
                    console.log('Выбрана запись :' + record.data['FIO'] + ' - ' + record.data['SUPERVIZOR']);
                    var FIO = record.data['FIO'];
                    if (record.data['SUPERVIZOR'] == '0') {
                        Ext.MessageBox.show({
                            title: 'Права на распределение',
                            msg: ' Вы действительно хотите дать права на распределение для ' + FIO,
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
                                    DataStore.findRecord('FIO', FIO).data.SUPERVIZOR = '1';
                                    DataGrid.getView().refresh();
                                    //Ext.example.msg('Button Click','Права на распределение для "{0}" выданы', FIO);
                                }
                            }
                        });
                    } else {
                        Ext.MessageBox.show({
                            title: 'Права на распределение',
                            msg: ' Аннулировать права на распределение для ' + FIO,
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
                                    DataStore.findRecord('FIO', FIO).data.SUPERVIZOR = '0';
                                    DataGrid.getView().refresh();
                                    //Ext.example.msg('Button Click','Права на распределение для "{0}" анулированы', FIO);

                                }
                            }
                        });
                    }

                }
            }
        }
    });

    //Создание таблицы с данными
    var LogGrid = Ext.create('Ext.grid.Panel', {
        id: 'Loggridpanel',
        title: 'Логирование',
        store: LogDataStore,
        autoScroll: true,
        enableColumnResize: true,
        //Height: myHeight,
        features: [Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: 'Группа  {name} ({rows.length})',
            collapsible: 'true',
        })],
        align: 'stretch',

        columns: [
            {text: 'День смены', dataIndex: 'DATE', width: 130, tpl: '{name}'},
            {text: 'Смены', dataIndex: 'TOUR', width: 30, tpl: '{name}'},
            {text: 'ФИО', dataIndex: 'FIO', width: 180, tpl: '{name}'},
            {text: 'Изменено', dataIndex: 'CHANGE', flex: 1, tpl: '{name}'},
            {text: 'Комментарий', dataIndex: 'COMMENT', width: 250, tpl: '{name}'},
            {text: 'Кто', dataIndex: 'WHO', width: 180, tpl: '{name}'},
            {text: 'Когда', dataIndex: 'KOGDA', width: 130, tpl: '{name}'}
        ],
        listeners: {
            itemdblClick: function (model, record) {

            }
        }
    });

    // форма загрузки
    var formP = Ext.create('Ext.form.FormPanel', {
        bodyStyle: 'padding:5px 5px 0',
        id: 'formP',
        items: [{
            id: 'Doc',
            xtype: 'filefield',
            name: 'document',
            fieldLabel: 'Выберите файл: ',
            msgTarget: 'side',
            allowBlank: false
        }],
        buttons: [
            {
                text: 'Загрузить файл',
                handler: function () {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
                            url: 'upload.php',
                            waitMsg: 'Загрузка...',
                            success: function (fp, o) {
                                DataStore.removeAll();
                                LogDataStore.removeAll();
                                DataStore.totalCount = 0;
                                console.log('Загрузка прошла успешно', 'Файл ' + o.result.file + " загружен");
                                DataStore.proxy.extraParams['month'] = Ext.getCmp('month').valueModels[0].data['month_id'];
                                LogDataStore.proxy.extraParams['month'] = Ext.getCmp('month').valueModels[0].data['month_id'];
                                DataStore.load();
                                LogDataStore.load();
                                load_window.hide();
                            }
                        });
                    }
                }
            },
            {
                text: 'Назад',
                handler: function () {
                    load_window.hide();

                }
            }


        ]
    });

    // окно загрузки файлов
    var load_window = new Ext.Window({
        title: 'Выбирите расположение файла на локальном компьютере:',
        closable: false,
        modal: true,
        border: false,
        defaults: {autoScroll: true},
        layout: 'fit',
        resizable: false,
        width: 500,
        items: [
            {

                items: [formP]


            }

        ]
        /*
         ,listeners:{
         beforeshow: function(){
         this.width=myWidth*0.6;
         this.height=myHeight*0.8;
         console.log('Ширина:'+this.width + '&Высота:'+this.height);
         }
         }
         */
    });


    // запуск модуля управления расписанием
    Ext.application({
        name: 'MyApp',

        launch: function () {
            var html = document.documentElement;

            myWidth = html.clientWidth;
            myHeight = html.clientHeight;
            console.log(myHeight);
            Ext.create('Ext.container.Viewport', {
                bodyPadding: '4 4 4 4',
                layout: 'border',
                Height: myHeight,
                items: [
                    { //Панель управления расписанием
                        region: 'west',
                        collapsible: true,
                        title: 'Управление',
                        split: true,
                        width: '15%',
                        maxWidth: 230,
                        renderTo: Ext.getBody(),
                        // информация по заявке, личным показателям, показатели отдела
                        items: [
                            UprFieldSet
                        ]
                    },
                    { //Панель информации
                        region: 'center',
                        collapsible: false,
                        title: '',
                        split: true,
                        autoScroll: true,
                        //Height: myHeight,
                        renderTo: Ext.getBody(),
                        // информация по заявке, личным показателям, показатели отдела
                        items: [
                            DataGrid, LogGrid

                        ]
                    }

                ]
            });

        }

    });

</script>
